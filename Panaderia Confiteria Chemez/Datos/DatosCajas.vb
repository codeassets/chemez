﻿Imports System.Data.SqlClient
Public Class DatosCajas
#Region "GetSet"
    Private id_caja As Int64
    Public Property _id_caja() As Int64
        Get
            Return id_caja
        End Get
        Set(ByVal value As Int64)
            id_caja = value
        End Set
    End Property
    Private fecha_inicio As Date
    Public Property _fecha_inicio() As Date
        Get
            Return fecha_inicio
        End Get
        Set(ByVal value As Date)
            fecha_inicio = value
        End Set
    End Property
    Private fecha_fin As Date
    Public Property _fecha_fin() As Date
        Get
            Return fecha_fin
        End Get
        Set(ByVal value As Date)
            fecha_fin = value
        End Set
    End Property
    Private monto_inicial As Double
    Public Property _monto_inicial() As Double
        Get
            Return monto_inicial
        End Get
        Set(ByVal value As Double)
            monto_inicial = value
        End Set
    End Property
    Private ingreso_total As Double
    Public Property _ingreso_total() As Double
        Get
            Return ingreso_total
        End Get
        Set(ByVal value As Double)
            ingreso_total = value
        End Set
    End Property
    Private egreso_total As Double
    Public Property _egreso_total() As Double
        Get
            Return egreso_total
        End Get
        Set(ByVal value As Double)
            egreso_total = value
        End Set
    End Property
    Private total As Double
    Public Property _total() As Double
        Get
            Return total
        End Get
        Set(ByVal value As Double)
            total = value
        End Set
    End Property
    Private estado As String
    Public Property _estado() As String
        Get
            Return estado
        End Get
        Set(ByVal value As String)
            estado = value
        End Set
    End Property
    Private id_cajero_abre As Int64
    Public Property _id_cajero_abre() As Int64
        Get
            Return id_cajero_abre
        End Get
        Set(ByVal value As Int64)
            id_cajero_abre = value
        End Set
    End Property
    Private id_cajero_cierra As Int64
    Public Property _id_cajero_cierra() As Int64
        Get
            Return id_cajero_cierra
        End Get
        Set(ByVal value As Int64)
            id_cajero_cierra = value
        End Set
    End Property
    Private descripcion As String
    Public Property _descripcion() As String
        Get
            Return descripcion
        End Get
        Set(ByVal value As String)
            descripcion = value
        End Set
    End Property
#End Region
    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function InsertarCajaDevolveridcaja()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@fechainicio", fecha_inicio)
        Comando.Parameters.AddWithValue("@fechafin", DBNull.Value)
        Comando.Parameters.AddWithValue("@montoinicial", monto_inicial)
        Comando.Parameters.AddWithValue("@ingresototal", ingreso_total)
        Comando.Parameters.AddWithValue("@egresototal", egreso_total)
        Comando.Parameters.AddWithValue("@total", total)
        Comando.Parameters.AddWithValue("@estado", estado)
        Comando.Parameters.AddWithValue("@idcajeroabre", id_cajero_abre)
        Comando.Parameters.AddWithValue("@idcajerocierra", id_cajero_cierra)

        Comando.CommandText = "INSERT INTO Caja(fecha_inicio, fecha_fin, monto_inicial, ingreso_total, egreso_total, total, estado, id_cajero_abre, id_cajero_cierra) " +
                               "VALUES(@fechainicio, @fechafin, @montoinicial, @ingresototal, @egresototal, @total, @estado, @idcajeroabre, @idcajerocierra)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "select @@identity"
        id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        Return id

    End Function
    Public Sub CerrarCaja()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_caja", id_caja)
        Comando.Parameters.AddWithValue("@fecha_fin", fecha_fin)
        Comando.Parameters.AddWithValue("@ingreso_total", ingreso_total)
        Comando.Parameters.AddWithValue("@egreso_total", egreso_total)
        Comando.Parameters.AddWithValue("@total", total)
        Comando.Parameters.AddWithValue("@id_cajero_cierra", id_cajero_cierra)
        Comando.CommandText = "UPDATE Caja SET estado = 'Cerrada',fecha_fin=@fecha_fin,ingreso_total=@ingreso_total,egreso_total=@egreso_total,total=@total,id_cajero_cierra=@id_cajero_cierra WHERE id_caja =@id_caja "
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
    Public Function BuscaCajaAbierta() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select * from Caja " +
                                            "where estado='Abierta'", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
    Public Function BuscaCajaPorFecha() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select c.id_caja,c.fecha_inicio,CAST(fecha_inicio AS TIME(0)) as Horario,c.fecha_fin,c.monto_inicial,c.ingreso_total,c.egreso_total,c.total,c.estado,c.id_cajero_abre,c.id_cajero_cierra,ua.nombre as Abre, uc.nombre as Cierra from Caja c " +
                                                "left join Usuarios ua on c.id_cajero_abre=ua.id_usuario " +
                                                "left join Usuarios uc on c.id_cajero_cierra=uc.id_usuario " +
                                            "where Convert(date,c.fecha_inicio) =Convert(date, @fecha)", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@fecha", fecha_inicio)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Function ObtenerPedidosPoridcaja() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select p.id_pedido as id,'Cafeteria' as tipo_id,'Cafeteria: ' + m.nro_mesa as Concepto, fecha,total from Pedidos as p " +
                                            "inner join Mesas as m on m.id_mesa=p.id_mesa " +
                                            "where p.id_caja=@id_caja order by p.id_pedido desc", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@id_caja", id_caja)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
    Public Function ObtenerVentasPoridcaja() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select v.id_venta as id,'Panaderia' as tipo_id," +
                                                "CASE " +
                                                "WHEN tipo_venta='Minorista' THEN 'Panaderia: Venta ' + cast(v.id_venta as varchar(50)) " +
                                                "WHEN tipo_venta='Mayorista' THEN 'Mayorista: Venta ' + cast(v.id_venta as varchar(50)) " +
                                                "End As Concepto," +
                                                "fecha,total from Ventas as v where v.id_caja=@id_caja order by v.id_venta desc", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@id_caja", id_caja)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
    'Public Function ObtenerVentasMayoristaPoridcaja() As DataTable

    '    Dim dt As New DataTable
    '    Using adaptador As New SqlDataAdapter("select v.id_venta as id,'Mayorista: Venta ' + cast(v.id_venta as varchar(50)) as Concepto, fecha,total from Ventas as v " +
    '                                        "where v.id_caja=@id_caja and tipo_venta='Mayorista' order by v.id_venta desc", Cadena)
    '        adaptador.SelectCommand.Parameters.AddWithValue("@id_caja", id_caja)
    '        adaptador.Fill(dt)
    '    End Using
    '    Return dt

    'End Function

    Public Function ObtenerEgresos() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select descripcion, fecha, monto from Egresos " +
                                              "where id_caja=@id_caja", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@id_caja", id_caja)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Sub InsertarEgreso()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@id_caja", id_caja)
        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@monto", total)
        Comando.Parameters.AddWithValue("@fecha", fecha_fin)

        Comando.CommandText = "INSERT INTO Egresos(id_caja, descripcion, monto, fecha) " +
                               "VALUES(@id_caja, @descripcion, @monto, @fecha)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        'Dim id As Int64
        'Comando.CommandText = "select @@identity"
        'id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        'Return id

    End Sub
End Class
