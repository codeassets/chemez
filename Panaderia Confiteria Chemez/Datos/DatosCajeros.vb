﻿Imports System.Data.SqlClient

Public Class DatosCajeros

#Region "GetSet"

    Private id_cajeros As Int64
    Public Property _id_cajeros() As Int64
        Get
            Return id_cajeros
        End Get
        Set(ByVal value As Int64)
            id_cajeros = value
        End Set
    End Property

    Private nombre As String
    Public Property _nombre() As String
        Get
            Return nombre
        End Get
        Set(ByVal value As String)
            nombre = value
        End Set
    End Property

    Private contraseña As String
    Public Property _contraseña() As String
        Get
            Return contraseña
        End Get
        Set(ByVal value As String)
            contraseña = value
        End Set
    End Property

#End Region

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function ObtenerTodos() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT * from Cajeros", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Sub Insertar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@nombre", nombre)
        Comando.Parameters.AddWithValue("@contraseña", contraseña)

        Comando.CommandText = "INSERT INTO Cajeros(nombre, contraseña) " +
                               "VALUES(@nombre, @contraseña)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Modificar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@nombre", nombre)
        Comando.Parameters.AddWithValue("@contraseña", contraseña)
        Comando.Parameters.AddWithValue("@id_cajeros", id_cajeros)

        Comando.CommandText = "UPDATE Cajeros Set nombre = @nombre," +
        "contraseña = @contraseña" +
        " WHERE id_cajeros = @id_cajeros"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Eliminar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_cajeros", id_cajeros)
        Comando.CommandText = "DELETE from Cajeros WHERE id_cajeros = @id_cajeros"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class
