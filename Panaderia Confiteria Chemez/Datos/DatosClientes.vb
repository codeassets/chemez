﻿Imports System.Data.SqlClient

Public Class DatosClientes

#Region "GetSet"

    Private id_cliente As Int64
    Public Property _id_cliente() As Int64
        Get
            Return id_cliente
        End Get
        Set(ByVal value As Int64)
            id_cliente = value
        End Set
    End Property

    Private nombre As String
    Public Property _nombre() As String
        Get
            Return nombre
        End Get
        Set(ByVal value As String)
            nombre = value
        End Set
    End Property

    Private cuit As String
    Public Property _cuit() As String
        Get
            Return cuit
        End Get
        Set(ByVal value As String)
            cuit = value
        End Set
    End Property

    Private direccion As String
    Public Property _direccion() As String
        Get
            Return direccion
        End Get
        Set(ByVal value As String)
            direccion = value
        End Set
    End Property

    Private telefono As String
    Public Property _telefono() As String
        Get
            Return telefono
        End Get
        Set(ByVal value As String)
            telefono = value
        End Set
    End Property

#End Region

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Sub Insertar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@nombre", nombre)
        Comando.Parameters.AddWithValue("@CUIT", If(cuit, DBNull.Value))
        Comando.Parameters.AddWithValue("@direccion", If(direccion, DBNull.Value))
        Comando.Parameters.AddWithValue("@telefono", If(telefono, DBNull.Value))

        Comando.CommandText = "INSERT INTO Clientes(nombre, CUIT, direccion, telefono) " +
                               "VALUES(@nombre, @CUIT, @direccion, @telefono)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Modificar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@id_cliente", id_cliente)
        Comando.Parameters.AddWithValue("@nombre", nombre)
        Comando.Parameters.AddWithValue("@CUIT", If(cuit, DBNull.Value))
        Comando.Parameters.AddWithValue("@direccion", If(direccion, DBNull.Value))
        Comando.Parameters.AddWithValue("@telefono", If(telefono, DBNull.Value))

        Comando.CommandText = "UPDATE Clientes Set nombre = @nombre," +
        "CUIT = @CUIT, direccion = @direccion, telefono = @telefono" +
        " WHERE id_cliente = @id_cliente"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Eliminar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_cliente", id_cliente)
        Comando.CommandText = "DELETE from Clientes WHERE id_cliente = @id_cliente"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodo() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT * FROM Clientes"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function ObtenerClienteConId() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT CUIT, nombre, direccion FROM Clientes WHERE id_cliente = @id_cliente", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@id_cliente", id_cliente)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Function ObtenerConCUIT() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select * from Clientes where CUIT= @CUIT", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@CUIT", cuit)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Function Buscar(dt As DataTable, text As String) As Object
        Using adaptador As New SqlDataAdapter("Select * " +
                                              "from Clientes " +
                                              "where CUIT Like '" & "%" + text + "%" & "' or nombre  LIKE '" & "%" + text + "%" & "' ORDER BY nombre", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt
    End Function


End Class
