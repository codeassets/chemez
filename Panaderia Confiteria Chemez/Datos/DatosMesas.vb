﻿Imports System.Data.SqlClient
Public Class DatosMesas
#Region "GetSet"
    Private id_mesa As Int64
    Public Property _id_mesa() As Int64
        Get
            Return id_mesa
        End Get
        Set(ByVal value As Int64)
            id_mesa = value
        End Set
    End Property

    Private nro_mesa As String
    Public Property _nro_mesa() As String
        Get
            Return nro_mesa
        End Get
        Set(ByVal value As String)
            nro_mesa = value
        End Set
    End Property

    Private estado As String
    Public Property _estado() As String
        Get
            Return estado
        End Get
        Set(ByVal value As String)
            estado = value
        End Set
    End Property

    Private id_mozo As Nullable(Of Int64)
    Public Property _id_mozo() As Nullable(Of Int64)
        Get
            Return id_mozo
        End Get
        Set(ByVal value As Nullable(Of Int64))
            id_mozo = value
        End Set
    End Property

#End Region
    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function ObtenerTodas() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT m.*, case when (m.Estado = 'Libre') then '' else mo.nombre end as Mozo" +
                                              " from Mesas m " +
                                              "left join Mozos mo on m.id_mozo=mo.id_mozo order by m.nro_mesa asc", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
    Public Function ObtenerLibres() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT m.*, case when (m.Estado = 'Libre') then '' else mo.nombre end as Mozo from Mesas m " +
                                              "left join Mozos mo on m.id_mozo=mo.id_mozo where m.estado = 'Libre' order by m.nro_mesa asc", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
    Public Function ObtenerOcupadas() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT m.*,mo.nombre as Mozo from Mesas m " +
                                              "left join Mozos mo on m.id_mozo=mo.id_mozo where m.estado = 'Ocupada' order by m.nro_mesa asc", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Sub ActualizarEstadoMesa()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_mesa", id_mesa)
        Comando.Parameters.AddWithValue("@estado", estado)
        Comando.Parameters.AddWithValue("@id_mozo", id_mozo)

        Comando.CommandText = "UPDATE Mesas SET estado = @estado, id_mozo = @id_mozo WHERE id_mesa = @id_mesa"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Insertar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@nro_mesa", nro_mesa)
        Comando.Parameters.AddWithValue("@estado", estado)
        Comando.Parameters.AddWithValue("@id_mozo", If(id_mozo, DBNull.Value))

        Comando.CommandText = "INSERT INTO Mesas(nro_mesa, estado, id_mozo) " +
                               "VALUES(@nro_mesa, @estado, @id_mozo)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Modificar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@id_mesa", id_mesa)
        Comando.Parameters.AddWithValue("@nro_mesa", nro_mesa)
        Comando.Parameters.AddWithValue("@estado", estado)
        Comando.Parameters.AddWithValue("@id_mozo", If(id_mozo, DBNull.Value))

        Comando.CommandText = "UPDATE Mesas Set nro_mesa = @nro_mesa," +
        "estado = @estado, id_mozo = @id_mozo" +
        " WHERE id_mesa = @id_mesa"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Eliminar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_mesa", id_mesa)
        Comando.CommandText = "DELETE from Mesas WHERE id_mesa = @id_mesa"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class
