﻿Imports System.Data.SqlClient

Public Class DatosMozos

#Region "GetSet"

    Private id_mozo As Int64
    Public Property _id_mozo() As Int64
        Get
            Return id_mozo
        End Get
        Set(ByVal value As Int64)
            id_mozo = value
        End Set
    End Property

    Private nombre As String
    Public Property _nombre() As String
        Get
            Return nombre
        End Get
        Set(ByVal value As String)
            nombre = value
        End Set
    End Property

#End Region

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function ObtenerTodos() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT * from Mozos", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Sub Insertar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@nombre", nombre)

        Comando.CommandText = "INSERT INTO Mozos(nombre) VALUES(@nombre)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Modificar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@id_mozo", id_mozo)
        Comando.Parameters.AddWithValue("@nombre", nombre)

        Comando.CommandText = "UPDATE Mozos Set nombre = @nombre WHERE id_mozo = @id_mozo"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Eliminar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_mozo", id_mozo)
        Comando.CommandText = "DELETE from Mozos WHERE id_mozo = @id_mozo"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub


End Class
