﻿Imports System.Data.SqlClient

Public Class DatosPedidos

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private id_pedido As Int64
    Public Property _id_pedido() As Int64
        Get
            Return id_pedido
        End Get
        Set(ByVal value As Int64)
            id_pedido = value
        End Set
    End Property

    Private id_detalle As Int64
    Public Property _id_detalle() As Int64
        Get
            Return id_detalle
        End Get
        Set(ByVal value As Int64)
            id_detalle = value
        End Set
    End Property

    Private id_mesa As Int64
    Public Property _id_mesa() As Int64
        Get
            Return id_mesa
        End Get
        Set(ByVal value As Int64)
            id_mesa = value
        End Set
    End Property
    Private fecha As Date
    Public Property _fecha() As Date
        Get
            Return fecha
        End Get
        Set(ByVal value As Date)
            fecha = value
        End Set
    End Property
    Private estado As String
    Public Property _estado() As String
        Get
            Return estado
        End Get
        Set(ByVal value As String)
            estado = value
        End Set
    End Property
    Private id_usuario As Int64
    Public Property _id_usuario() As Int64
        Get
            Return id_usuario
        End Get
        Set(ByVal value As Int64)
            id_usuario = value
        End Set
    End Property
    Private id_mozo As Int64
    Public Property _id_mozo() As Int64
        Get
            Return id_mozo
        End Get
        Set(ByVal value As Int64)
            id_mozo = value
        End Set
    End Property
    Private descuento As Double
    Public Property _descuento() As Double
        Get
            Return descuento
        End Get
        Set(ByVal value As Double)
            descuento = value
        End Set
    End Property
    Private total As Double
    Public Property _total() As Double
        Get
            Return total
        End Get
        Set(ByVal value As Double)
            total = value
        End Set
    End Property
    Private tipo_pago As String
    Public Property _tipo_pago() As String
        Get
            Return tipo_pago
        End Get
        Set(ByVal value As String)
            tipo_pago = value
        End Set
    End Property

    Private descripcion As String
    Public Property _descripcion() As String
        Get
            Return descripcion
        End Get
        Set(ByVal value As String)
            descripcion = value
        End Set
    End Property

    Private cantidad As Int64
    Public Property _cantidad() As Int64
        Get
            Return cantidad
        End Get
        Set(ByVal value As Int64)
            cantidad = value
        End Set
    End Property

    Private precio_unitario As Double
    Public Property _precio_unitario() As Double
        Get
            Return precio_unitario
        End Get
        Set(ByVal value As Double)
            precio_unitario = value
        End Set
    End Property

    Private subtotal As Double
    Public Property _subtotal() As Double
        Get
            Return subtotal
        End Get
        Set(ByVal value As Double)
            subtotal = value
        End Set
    End Property

    Private fecha_desde As Date
    Public Property _fecha_desde() As Date
        Get
            Return fecha_desde
        End Get
        Set(ByVal value As Date)
            fecha_desde = value
        End Set
    End Property

    Private fecha_hasta As Date
    Public Property _fecha_hasta() As Date
        Get
            Return fecha_hasta
        End Get
        Set(ByVal value As Date)
            fecha_hasta = value
        End Set
    End Property
    Private id_caja As Int64
    Public Property _id_caja() As Int64
        Get
            Return id_caja
        End Get
        Set(ByVal value As Int64)
            id_caja = value
        End Set
    End Property
    Private iva As Double
    Public Property _iva() As Double
        Get
            Return iva
        End Get
        Set(ByVal value As Double)
            iva = value
        End Set
    End Property
    Private importe_iva As Double
    Public Property _importe_iva() As Double
        Get
            Return importe_iva
        End Get
        Set(ByVal value As Double)
            importe_iva = value
        End Set
    End Property

#End Region

    Public Sub CrearPedido()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_mesa", id_mesa)
        Comando.Parameters.AddWithValue("@fecha", fecha)
        Comando.Parameters.AddWithValue("@estado", estado)
        Comando.Parameters.AddWithValue("@id_usuario", id_usuario)
        Comando.Parameters.AddWithValue("@id_mozo", id_mozo)
        Comando.Parameters.AddWithValue("@descuento", descuento)
        Comando.Parameters.AddWithValue("@total", total)

        Comando.CommandText = "INSERT INTO Pedidos(id_mesa, fecha, estado, id_usuario, id_mozo, descuento, total) " +
                               "VALUES(@id_mesa, @fecha, @estado, @id_usuario, @id_mozo, @descuento, @total)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function CrearPedidoReturnId()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_mesa", id_mesa)
        Comando.Parameters.AddWithValue("@fecha", fecha)
        Comando.Parameters.AddWithValue("@estado", estado)
        Comando.Parameters.AddWithValue("@id_usuario", id_usuario)
        Comando.Parameters.AddWithValue("@id_mozo", id_mozo)
        Comando.Parameters.AddWithValue("@descuento", descuento)
        Comando.Parameters.AddWithValue("@total", total)

        Comando.CommandText = "INSERT INTO Pedidos(id_mesa, fecha, estado, id_usuario, id_mozo, descuento, total) " +
                               "VALUES(@id_mesa, @fecha, @estado, @id_usuario, @id_mozo, @descuento, @total)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "select @@identity"
        id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        Return id

    End Function

    Public Sub CrearDetallePedido()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@cantidad", cantidad)
        Comando.Parameters.AddWithValue("@precio_unitario", precio_unitario)
        Comando.Parameters.AddWithValue("@subtotal", subtotal)
        Comando.Parameters.AddWithValue("@id_pedido", id_pedido)
        Comando.Parameters.AddWithValue("@iva", iva)
        Comando.Parameters.AddWithValue("@importe_iva", importe_iva)

        Comando.CommandText = "INSERT INTO DetallePedidos(descripcion, cantidad, precio_unitario, subtotal, id_pedido, IVA, Importe_IVA) " +
                               "VALUES(@descripcion, @cantidad, @precio_unitario, @subtotal, @id_pedido, @iva, @importe_iva)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarDetallePedido()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@cantidad", cantidad)
        Comando.Parameters.AddWithValue("@precio", precio_unitario)
        Comando.Parameters.AddWithValue("@subtotal", subtotal)
        Comando.Parameters.AddWithValue("@id_detalle", id_detalle)
        Comando.Parameters.AddWithValue("@importe_iva", importe_iva)

        Comando.CommandText = "UPDATE DetallePedidos SET cantidad = @cantidad,precio_unitario=@precio, subtotal = @subtotal, Importe_IVA = @importe_iva WHERE id_detalle = @id_detalle"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarPedido()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_pedido", id_pedido)
        Comando.CommandText = "DELETE from Pedidos WHERE id_pedido = @id_pedido"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarDetallePedido()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_detalle", id_detalle)
        Comando.CommandText = "DELETE from DetallePedidos WHERE id_detalle = @id_detalle"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerDetallePedidos() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select dp.*,prod.id_producto,prod.cantidad as EsStock " +
                                              "from DetallePedidos as dp " +
                                            "inner join Pedidos as p on p.id_pedido=dp.id_pedido " +
                                            "left join Productos as prod on prod.descripcion=dp.descripcion " +
                                            "where p.estado='Abierto' and p.id_mesa= @id_mesa", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@id_mesa", id_mesa)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
    Public Function ObtenerDetallePedidosPor_id_pedido() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select dp.*,p.total,p.descuento from DetallePedidos as dp " +
                                            "inner join Pedidos as p on p.id_pedido=dp.id_pedido " +
                                            "where p.id_pedido=@idpedido", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@idpedido", id_pedido)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Function Buscar(fd As String, fh As String) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@fecha_desde", fd)
        comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT p.*, m.nro_mesa, u.nombre as Usuario, mz.nombre as Mozo from Pedidos as p " +
        "left join Mesas as m on p.id_mesa = m.id_mesa left join Usuarios as u on p.id_usuario = u.id_usuario " +
        "left join Mozos as mz on p.id_mozo = mz.id_mozo WHERE p.fecha BETWEEN @fecha_desde AND @fecha_hasta and p.estado <> 'Abierto'"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Sub CobraryModificarPedido()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_pedido", id_pedido)
        Comando.Parameters.AddWithValue("@id_usuario", id_usuario)
        Comando.Parameters.AddWithValue("@descuento", descuento)
        Comando.Parameters.AddWithValue("@total", total)
        Comando.Parameters.AddWithValue("@tipo_pago", tipo_pago)
        Comando.Parameters.AddWithValue("@id_caja", id_caja)

        Comando.CommandText = "UPDATE Pedidos SET estado = 'Cerrado', " +
                                                 "descuento = @descuento," +
                                                 "id_usuario=@id_usuario," +
                                                 "total=@total," +
                                                 "tipo_pago=@tipo_pago," +
                                                 "id_caja=@id_caja " +
                                                 "WHERE id_pedido = @id_pedido"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function InformeCafeteriaPorFecha(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@fecha_desde", fd)
        comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT p.id_pedido, m.nro_mesa, p.fecha, p.estado, u.nombre, mz.nombre, p.descuento, p.id_caja, p.total " +
            "from Pedidos as p inner join Mesas as m on p.id_mesa = m.id_mesa left join Usuarios as u on p.id_usuario = u.id_usuario " +
        " left join Mozos as mz on p.id_mozo = mz.id_mozo WHERE p.fecha BETWEEN @fecha_desde AND @fecha_hasta " +
        "order by p.fecha"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

End Class
