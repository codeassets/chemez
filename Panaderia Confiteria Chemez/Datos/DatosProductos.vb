﻿Imports System.Data.SqlClient
Public Class DatosProductos

#Region "GetSet"

    Private id_producto As Int64
    Public Property _id_producto() As Int64
        Get
            Return id_producto
        End Get
        Set(ByVal value As Int64)
            id_producto = value
        End Set
    End Property
    Private descripcion As String
    Public Property _descripcion() As String
        Get
            Return descripcion
        End Get
        Set(ByVal value As String)
            descripcion = value
        End Set
    End Property
    Private precio As Double
    Public Property _precio() As Double
        Get
            Return precio
        End Get
        Set(ByVal value As Double)
            precio = value
        End Set
    End Property
    Private cantidad As Nullable(Of Integer)
    Public Property _cantidad() As Nullable(Of Integer)
        Get
            Return cantidad
        End Get
        Set(ByVal value As Nullable(Of Integer))
            cantidad = value
        End Set
    End Property
    Private id_tipo As Int64
    Public Property _id_tipo() As Int64
        Get
            Return id_tipo
        End Get
        Set(ByVal value As Int64)
            id_tipo = value
        End Set
    End Property
    Private venta_por As String
    Public Property _venta_por() As String
        Get
            Return venta_por
        End Get
        Set(ByVal value As String)
            venta_por = value
        End Set
    End Property
    Private iva As Double
    Public Property _iva() As Double
        Get
            Return iva
        End Get
        Set(ByVal value As Double)
            iva = value
        End Set
    End Property
    Private importe_iva As Double
    Public Property _importe_iva() As Double
        Get
            Return importe_iva
        End Get
        Set(ByVal value As Double)
            importe_iva = value
        End Set
    End Property

#End Region

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function Buscar(ByVal dt As DataTable) As DataTable

        Using adaptador As New SqlDataAdapter("SELECT p.*, t.descripcion as Tipo, t.stock as Stock " +
                                          "from Productos as p inner join Tipos as t on p.id_tipo = t.id_tipo " +
                                          "where p.descripcion LIKE @descripcion and t.descripcion <> 'Materia prima' ORDER BY p.descripcion", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@descripcion", "%" & descripcion & "%")
            adaptador.Fill(dt)
        End Using

        Return dt

    End Function

    Public Function BuscarProductos(ByVal dt As DataTable) As DataTable

        Using adaptador As New SqlDataAdapter("SELECT p.*, t.descripcion as Tipo, t.stock as Stock " +
                                          "from Productos as p inner join Tipos as t on p.id_tipo = t.id_tipo " +
                                          "where p.descripcion LIKE @descripcion ORDER BY p.descripcion", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@descripcion", "%" & descripcion & "%")
            adaptador.Fill(dt)
        End Using

        Return dt

    End Function

    Public Function BuscarFaltantes(cantidad As Integer) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@cantidad", cantidad)

        comando.CommandText = "Select descripcion, cantidad from Productos where cantidad <= @cantidad ORDER BY cantidad ASC"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function


    Public Function BuscarPorVenta_por(ByVal dt As DataTable) As DataTable 'Esta funcion es igual a la de buscar pero trae productos solamente de venta por cantidad o venta por Kg

        Using adaptador As New SqlDataAdapter("SELECT p.*, t.descripcion as Tipo, t.stock as Stock " +
                                          "from Productos as p inner join Tipos as t on p.id_tipo = t.id_tipo " +
                                          "where p.descripcion LIKE @descripcion and venta_por=@ventapor ORDER BY p.descripcion", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@descripcion", "%" & descripcion & "%")
            adaptador.SelectCommand.Parameters.AddWithValue("@ventapor", venta_por)
            adaptador.Fill(dt)
        End Using

        Return dt

    End Function

    Public Function ObtenerTodos() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT p.*, t.descripcion as Tipo, t.stock as Stock from Productos as p inner join Tipos as t " +
                                              "on p.id_tipo = t.id_tipo ORDER BY p.descripcion", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Sub CrearProducto()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@precio", precio)
        Comando.Parameters.AddWithValue("@cantidad", If(cantidad, DBNull.Value))
        Comando.Parameters.AddWithValue("@id_tipo", id_tipo)
        Comando.Parameters.AddWithValue("@venta_por", venta_por)
        Comando.Parameters.AddWithValue("@iva", iva)
        Comando.Parameters.AddWithValue("@importe_iva", importe_iva)


        Comando.CommandText = "INSERT INTO Productos(descripcion, precio, cantidad, id_tipo, venta_por, iva, importe_iva) " +
                               "VALUES(@descripcion, @precio, @cantidad, @id_tipo, @venta_por, @iva, @importe_iva)"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarProducto()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@precio", precio)
        Comando.Parameters.AddWithValue("@cantidad", If(cantidad, DBNull.Value))
        Comando.Parameters.AddWithValue("@id_tipo", id_tipo)
        Comando.Parameters.AddWithValue("@venta_por", venta_por)
        Comando.Parameters.AddWithValue("@id_producto", id_producto)
        Comando.Parameters.AddWithValue("@iva", iva)
        Comando.Parameters.AddWithValue("@importe_iva", importe_iva)


        Comando.CommandText = "UPDATE Productos SET descripcion = @descripcion, precio = @precio, cantidad = @cantidad, id_tipo = @id_tipo, venta_por = @venta_por, iva = @iva, importe_iva = @importe_iva WHERE id_producto = @id_producto"


        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
    Public Sub DisminuirStock()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@cantidad", If(cantidad, DBNull.Value))
        Comando.Parameters.AddWithValue("@id_producto", id_producto)

        Comando.CommandText = "UPDATE Productos SET cantidad = cantidad - @cantidad WHERE id_producto = @id_producto"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarProducto()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_producto", id_producto)
        Comando.CommandText = "DELETE from Productos WHERE id_producto = @id_producto"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function BuscarDescripcion()

        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@descripcion", descripcion)
        comando.CommandText = "SELECT * from Productos where descripcion =@descripcion"
        comando.Connection = New SqlConnection(Cadena)
        Dim tabla As New DataTable
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(tabla)
        comando.Connection.Close()
        Return tabla

    End Function

End Class
