﻿Imports System.Data.SqlClient
Public Class DatosTipos

#Region "GetSet"

    Private id_tipo As Int64
    Public Property _id_tipo() As Int64
        Get
            Return id_tipo
        End Get
        Set(ByVal value As Int64)
            id_tipo = value
        End Set
    End Property
    Private descripcion As String
    Public Property _descripcion() As String
        Get
            Return descripcion
        End Get
        Set(ByVal value As String)
            descripcion = value
        End Set
    End Property
    Private stock As String
    Public Property _stock() As String
        Get
            Return stock
        End Get
        Set(ByVal value As String)
            stock = value
        End Set
    End Property

#End Region

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function TraerCampoStock() As String

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim stock As String

        Comando.Parameters.AddWithValue("@id_tipo", id_tipo)

        Comando.CommandText = "SELECT stock from Tipos WHERE @id_tipo= id_tipo"
        Comando.Connection = Conexion
        Conexion.Open()
        stock = Comando.ExecuteScalar()
        Conexion.Close()
        Return stock

    End Function

    Public Function ObtenerTodo() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "select id_tipo, descripcion, CASE WHEN stock='0' THEN 'No'" +
                                                "WHEN stock='1' THEN 'Sí' END As 'Mantiene stock' from Tipos"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Sub Insertar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@stock", stock)

        Comando.CommandText = "INSERT INTO Tipos(descripcion, stock) " +
                               "VALUES(@descripcion, @stock)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Modificar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@id_tipo", id_tipo)
        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@stock", stock)

        Comando.CommandText = "UPDATE Tipos Set descripcion = @descripcion," +
        "stock = @stock WHERE id_tipo = @id_tipo"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub Eliminar()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_tipo", id_tipo)
        Comando.CommandText = "DELETE from Tipos WHERE id_tipo = @id_tipo"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub



End Class
