﻿Imports System.Data.SqlClient

Public Class DatosUsuarios

#Region "GetSet"
    Private id_usuario As Int64
    Public Property _id_usuario() As Int64
        Get
            Return id_usuario
        End Get
        Set(ByVal value As Int64)
            id_usuario = value
        End Set
    End Property

    Private nombre As String
    Public Property _nombre() As String
        Get
            Return nombre
        End Get
        Set(ByVal value As String)
            nombre = value
        End Set
    End Property
    Private contraseña As String
    Public Property _contraseña() As String
        Get
            Return contraseña
        End Get
        Set(ByVal value As String)
            contraseña = value
        End Set
    End Property
    Private tipo As String
    Public Property _tipo() As String
        Get
            Return tipo
        End Get
        Set(ByVal value As String)
            tipo = value
        End Set
    End Property

#End Region

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Sub InsertarUsuario()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@nombre", nombre)
        Comando.Parameters.AddWithValue("@contraseña", contraseña)
        Comando.Parameters.AddWithValue("@tipo", tipo)

        Comando.CommandText = "INSERT INTO Usuarios(nombre, contraseña, tipo) " +
                               "VALUES(@nombre, @contraseña, @tipo)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarUsuario()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@id_usuario", id_usuario)
        Comando.Parameters.AddWithValue("@nombre", nombre)
        Comando.Parameters.AddWithValue("@contraseña", contraseña)
        Comando.Parameters.AddWithValue("@tipo", tipo)

        Comando.CommandText = "UPDATE Usuarios Set nombre = @nombre," +
        "contraseña = @contraseña, tipo = @tipo" +
        " WHERE id_usuario = @id_usuario"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarUsuario()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@id_usuario", id_usuario)
        Comando.CommandText = "DELETE from Usuarios WHERE id_usuario = @id_usuario"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodo() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT * FROM Usuarios "
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function ComprobarAcceso(ByVal nombre As String, ByVal contraseña As String) As DataTable
        Try
            Dim dt As New DataTable
            Using adaptador As New SqlDataAdapter("SELECT * FROM Usuarios WHERE nombre = @nombre AND contraseña = @contraseña", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@nombre", nombre)
                adaptador.SelectCommand.Parameters.AddWithValue("@contraseña", contraseña)
                adaptador.Fill(dt)
            End Using
            Return dt
        Catch ex As SqlException
            MsgBox("No se pudo establecer conexión con el servidor de Base de Datos", MsgBoxStyle.Critical, Title:="Atención")
            Return Nothing
        End Try

    End Function


End Class
