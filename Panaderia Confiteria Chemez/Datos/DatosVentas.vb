﻿Imports System.Data.SqlClient

Public Class DatosVentas

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private id_venta As Int64
    Public Property _id_venta() As Int64
        Get
            Return id_venta
        End Get
        Set(ByVal value As Int64)
            id_venta = value
        End Set
    End Property

    Private fecha As Date
    Public Property _fecha() As Date
        Get
            Return fecha
        End Get
        Set(ByVal value As Date)
            fecha = value
        End Set
    End Property

    Private descuento As Double
    Public Property _descuento() As Double
        Get
            Return descuento
        End Get
        Set(ByVal value As Double)
            descuento = value
        End Set
    End Property

    Private total As Double
    Public Property _total() As Double
        Get
            Return total
        End Get
        Set(ByVal value As Double)
            total = value
        End Set
    End Property

    Private id_caja As Int64
    Public Property _id_caja() As Int64
        Get
            Return id_caja
        End Get
        Set(ByVal value As Int64)
            id_caja = value
        End Set
    End Property

    Private id_usuario As Int64
    Public Property _id_usuario() As Int64
        Get
            Return id_usuario
        End Get
        Set(ByVal value As Int64)
            id_usuario = value
        End Set
    End Property

    Private tipo_venta As String
    Public Property _tipo_venta() As String
        Get
            Return tipo_venta
        End Get
        Set(ByVal value As String)
            tipo_venta = value
        End Set
    End Property

    Private tipo_pago As String
    Public Property _tipo_pago() As String
        Get
            Return tipo_pago
        End Get
        Set(ByVal value As String)
            tipo_pago = value
        End Set
    End Property

    Private fecha_desde As Date
    Public Property _fecha_desde() As Date
        Get
            Return fecha_desde
        End Get
        Set(ByVal value As Date)
            fecha_desde = value
        End Set
    End Property

    Private fecha_hasta As Date
    Public Property _fecha_hasta() As Date
        Get
            Return fecha_hasta
        End Get
        Set(ByVal value As Date)
            fecha_hasta = value
        End Set
    End Property
    Private id_detalle As Int64
    Public Property _id_detalle() As Int64
        Get
            Return id_detalle
        End Get
        Set(ByVal value As Int64)
            id_detalle = value
        End Set
    End Property
    Private descripcion As String
    Public Property _descripcion() As String
        Get
            Return descripcion
        End Get
        Set(ByVal value As String)
            descripcion = value
        End Set
    End Property
    Private cantidad As Double
    Public Property _cantidad() As Double
        Get
            Return cantidad
        End Get
        Set(ByVal value As Double)
            cantidad = value
        End Set
    End Property
    Private precio_unitario As Double
    Public Property _precio_unitario() As Double
        Get
            Return precio_unitario
        End Get
        Set(ByVal value As Double)
            precio_unitario = value
        End Set
    End Property
    Private subtotal As Double
    Public Property _subtotal() As Double
        Get
            Return subtotal
        End Get
        Set(ByVal value As Double)
            subtotal = value
        End Set
    End Property
    Private venta_por As String
    Public Property _venta_por() As String
        Get
            Return venta_por
        End Get
        Set(ByVal value As String)
            venta_por = value
        End Set
    End Property
    Private id_cliene As Int64
    Public Property _id_cliente() As Int64
        Get
            Return id_cliene
        End Get
        Set(ByVal value As Int64)
            id_cliene = value
        End Set
    End Property
    Private iva As Double
    Public Property _iva() As Double
        Get
            Return iva
        End Get
        Set(ByVal value As Double)
            iva = value
        End Set
    End Property
    Private importe_iva As Double
    Public Property _importe_iva() As Double
        Get
            Return importe_iva
        End Get
        Set(ByVal value As Double)
            importe_iva = value
        End Set
    End Property
#End Region

    Public Function BuscarMinoristas(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@fecha_desde", fd)
        comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT v.*, u.nombre from Ventas as v left join Usuarios as u on v.id_usuario = u.id_usuario " +
        "WHERE v.fecha >= @fecha_desde AND v.fecha <= @fecha_hasta AND v.tipo_venta = 'Minorista'"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function
    Public Function BuscarDetalleVenta() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@idventa", id_venta)


        comando.CommandText = "SELECT dv.*, v.total, v.descuento " +
            "from DetalleVentas as dv inner join Ventas as v on v.id_venta = dv.id_venta WHERE dv.id_venta =@idventa"


        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function BuscarMayoristas(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@fecha_desde", fd)
        comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT v.id_venta, v.fecha, c.nombre as Cliente, v.tipo_pago, v.descuento, v.total, u.nombre from Ventas as v left join Usuarios as u on v.id_usuario = u.id_usuario " +
        "left join Clientes as c on v.id_cliente = c.id_cliente WHERE v.fecha >= @fecha_desde AND v.fecha <= @fecha_hasta AND v.tipo_venta = 'Mayorista'"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function ObtenerDetalles() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@id_venta", id_venta)

        comando.CommandText = "SELECT * from DetalleVentas WHERE id_venta = @id_venta"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function


    Public Sub CrearVenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@fecha", fecha)
        Comando.Parameters.AddWithValue("@descuento", descuento)
        Comando.Parameters.AddWithValue("@total", total)
        Comando.Parameters.AddWithValue("@id_caja", id_caja)
        Comando.Parameters.AddWithValue("@tipo_venta", tipo_venta)
        Comando.Parameters.AddWithValue("@tipo_pago", tipo_pago)

        Comando.CommandText = "INSERT INTO Ventas(fecha, descuento, total, id_caja, tipo_venta, tipo_pago) " +
                               "VALUES( @fecha, @descuento, @total, @id_caja, @tipo_venta, @tipo_pago)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
    Public Sub CrearDetalleVentas()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@descripcion", descripcion)
        Comando.Parameters.AddWithValue("@cantidad", cantidad)
        Comando.Parameters.AddWithValue("@precio_unitario", precio_unitario)
        Comando.Parameters.AddWithValue("@subtotal", subtotal)
        Comando.Parameters.AddWithValue("@venta_por", venta_por)
        Comando.Parameters.AddWithValue("@iva", iva)
        Comando.Parameters.AddWithValue("@importe_iva", importe_iva)
        Comando.Parameters.AddWithValue("@id_venta", id_venta)

        Comando.CommandText = "INSERT INTO DetalleVentas(descripcion, cantidad, precio_unitario, subtotal, venta_por, iva, importe_iva, id_venta) " +
                               "VALUES(@descripcion, @cantidad, @precio_unitario, @subtotal, @venta_por, @iva, @importe_iva, @id_venta)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
    Public Function CrearVentaReturnId()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@fecha", fecha)
        Comando.Parameters.AddWithValue("@descuento", descuento)
        Comando.Parameters.AddWithValue("@total", total)
        Comando.Parameters.AddWithValue("@id_caja", id_caja)
        Comando.Parameters.AddWithValue("@id_usuario", id_usuario)
        Comando.Parameters.AddWithValue("@tipo_venta", tipo_venta)
        Comando.Parameters.AddWithValue("@tipo_pago", tipo_pago)
        Comando.Parameters.AddWithValue("@id_cliente", id_cliene)

        Comando.CommandText = "INSERT INTO Ventas(fecha, descuento, total, id_caja, id_usuario, tipo_venta, tipo_pago, id_cliente) " +
                               "VALUES(@fecha, @descuento, @total, @id_caja, @id_usuario, @tipo_venta,@tipo_pago, @id_cliente)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "select @@identity"
        id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        Return id
    End Function

    Public Function InformeVentasMinoristasPorFecha(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@fecha_desde", fd)
        comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT v.id_venta, v.fecha, v.descuento, v.id_caja, u.nombre, v.tipo_pago, v.total " +
            "from Ventas as v left join Usuarios as u on v.id_usuario = u.id_usuario WHERE v.Fecha >= @fecha_desde And v.Fecha <=@fecha_hasta " +
        "AND v.tipo_venta = 'Minorista' order by v.fecha"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function InformeVentasMayoristasPorFecha(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@fecha_desde", fd)
        comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT v.id_venta, v.fecha, c.nombre as Cliente, v.tipo_pago, v.descuento, v.total, u.nombre " +
            "from Ventas as v left join Usuarios as u on v.id_usuario = u.id_usuario left join Clientes as c on v.id_cliente=c.id_cliente " +
            "WHERE v.Fecha >= @fecha_desde And v.Fecha <=@fecha_hasta " +
        "AND v.tipo_venta = 'Mayorista' order by v.fecha"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function


End Class
