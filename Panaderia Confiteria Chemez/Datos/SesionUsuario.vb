﻿Public Module SesionUsuario

    Private id_usuario_sesion As Long = 0
    Private nombre_sesion As String = ""
    Private tipo_sesion As String = ""

    ' id_usuario
    Public Function get_usuario() As Long
        Return id_usuario_sesion
    End Function

    Public Function set_usuario(ByVal id_usuario_acceso As Long) As Boolean
        id_usuario_sesion = id_usuario_acceso
        Return True
    End Function

    ' nombre
    Public Function get_nombre() As String
        Return nombre_sesion
    End Function

    Public Function set_nombre(ByVal nombre As String) As Boolean
        nombre_sesion = nombre
        Return True
    End Function

    ' tipo
    Public Function get_tipo() As String
        Return tipo_sesion
    End Function

    Public Function set_tipo(ByVal tipo As String) As Boolean
        tipo_sesion = tipo
        Return True
    End Function

    Public Function estado_sesion() As Boolean
        If id_usuario_sesion <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function cerrar_sesion() As Boolean
        id_usuario_sesion = 0
        nombre_sesion = ""
        Return True
    End Function

End Module
