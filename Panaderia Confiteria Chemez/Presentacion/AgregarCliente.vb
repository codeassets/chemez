﻿Public Class AgregarCliente

    Dim cliente As New Datos.DatosClientes

    Public Function ValidarCUIT(cuit As String) As Integer

        Dim t As Integer, S As Integer, Verif As Integer

        S = 0
        S = Val(Mid(cuit, 1, 1)) * 5
        S = S + Val(Mid(cuit, 2, 1)) * 4
        S = S + Val(Mid(cuit, 3, 1)) * 3
        S = S + Val(Mid(cuit, 4, 1)) * 2
        S = S + Val(Mid(cuit, 5, 1)) * 7
        S = S + Val(Mid(cuit, 6, 1)) * 6
        S = S + Val(Mid(cuit, 7, 1)) * 5
        S = S + Val(Mid(cuit, 8, 1)) * 4
        S = S + Val(Mid(cuit, 9, 1)) * 3
        S = S + Val(Mid(cuit, 10, 1)) * 2
        t = S Mod 11
        t = 11 - t

        If t = 11 Then
            Verif = 0
        ElseIf t = 10 Then
            Verif = 9
        Else
            Verif = t
        End If

        ValidarCUIT = Verif

    End Function

    Private Sub AgregarCliente_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        dgvClientes.DataSource = cliente.ObtenerTodo()
        dgvClientes.ClearSelection()

        Try
            dgvClientes.Columns("id_cliente").Visible = False
            'dgvClientes.Columns("nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvClientes.Columns("CUIL").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvClientes.Columns("direccion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvClientes.Columns("telefono").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvClientes.Columns("nombre").HeaderText = "Nombre"
            dgvClientes.Columns("direccion").HeaderText = "Dirección"
            dgvClientes.Columns("telefono").HeaderText = "Teléfono"
        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Clientes. Detalle:" & ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtNombre.Text <> "" And txtCUIT.Text <> "" And txtDireccion.Text <> "" And txtTelefono.Text <> "" Then

            Try
                    cliente._nombre = txtNombre.Text
                    cliente._cuit = txtCUIT.Text
                    cliente._direccion = txtDireccion.Text
                    cliente._telefono = txtTelefono.Text

                    If ValidarCUIT(txtCUIT.Text) Then
                        cliente.Insertar()

                        MsgBox("Se agregó el cliente con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                        dgvClientes.Enabled = True
                        dgvClientes.DataSource = cliente.ObtenerTodo()
                    Else
                        MessageBox.Show("El CUIT debe tener el formato 99-99999999-9.")
                        Return
                    End If

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el cliente. Detalle:" & ex.Message.ToString)
                End Try


                bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

        Me.DialogResult = DialogResult.OK

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        Me.Close()

    End Sub

    Private Sub txtTelefono_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTelefono.KeyPress
        ' Verifica que sólo se ingresen números
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class