﻿Public Class Ayuda

    Sub New(f As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        If f = "Cafetería" Then
            lblAyuda.Text = "- Buscar: Ctrl + B" + vbCrLf + "- Cobrar: Ctrl + C" + vbCrLf + "- Guardar: Ctrl + G" +
            vbCrLf + "- Seleccionar mozo: Ctrl + M" + vbCrLf + "- Seleccionar mesa: Enter" + vbCrLf + "- Deseleccionar mesa: Escape" +
            vbCrLf + "- Agregar producto: Enter" + vbCrLf + "- Quitar un producto: Backspace" + vbCrLf + "- Eliminar producto: Suprimir" +
            vbCrLf + "- Ir a tabla Agregados: flecha 🡠" + vbCrLf + "- Ir a tabla Productos: flecha 🡢"
        ElseIf f = "Panadería" Or f = "Mayorista" Then
            lblAyuda.Text = "- Buscar: Ctrl + B" + vbCrLf + "- Cobrar: Ctrl + C" + vbCrLf + "- Cancelar: Escape" +
                        vbCrLf + "- Agregar producto: Enter" + vbCrLf + "- Eliminar producto: Suprimir" +
            vbCrLf + "- Ir a tabla Agregados: flecha 🡠" + vbCrLf + "- Ir a tabla Productos: flecha 🡢"
        ElseIf f = "Productos" Then
            lblAyuda.Text = "- Buscar: Ctrl + B" + vbCrLf + "- Agregar: Ctrl + A" + vbCrLf + "- Modificar: Ctrl + M" +
                vbCrLf + "- Eliminar: Ctrl + E" + vbCrLf + "- Cancelar: Escape" + vbCrLf + "- Guardar: Enter"
        Else
            lblAyuda.Text = "- Atajos no disponibles para esta ventana"
        End If

    End Sub

    Private Sub Ayuda_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        ' cerrar
        If e.KeyCode = Keys.Escape Then
            Me.Dispose()
        End If
    End Sub

End Class