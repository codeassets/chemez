﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Caja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpFechaCaja = New System.Windows.Forms.DateTimePicker()
        Me.lblTotalEgresos = New System.Windows.Forms.Label()
        Me.lblTotalIngresos = New System.Windows.Forms.Label()
        Me.lblCaja = New System.Windows.Forms.Label()
        Me.bttAbrir = New System.Windows.Forms.Button()
        Me.bttCerrar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFechaConsulta = New System.Windows.Forms.DateTimePicker()
        Me.bttConsultar = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgvIngresos = New System.Windows.Forms.DataGridView()
        Me.dgvEgresos = New System.Windows.Forms.DataGridView()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.bttAgregarEgreso = New System.Windows.Forms.Button()
        Me.txtDineroInicial = New System.Windows.Forms.TextBox()
        Me.txtNuevoEgreso = New System.Windows.Forms.TextBox()
        Me.lblBalanceTotal = New System.Windows.Forms.Label()
        Me.txtMontoEgreso = New System.Windows.Forms.TextBox()
        Me.lblDineroInicial = New System.Windows.Forms.Label()
        Me.cbCredito = New System.Windows.Forms.CheckBox()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgvIngresos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEgresos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(39, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 20)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Fecha"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFechaCaja
        '
        Me.dtpFechaCaja.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaCaja.CustomFormat = ""
        Me.dtpFechaCaja.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaCaja.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCaja.Location = New System.Drawing.Point(98, 73)
        Me.dtpFechaCaja.Name = "dtpFechaCaja"
        Me.dtpFechaCaja.Size = New System.Drawing.Size(129, 27)
        Me.dtpFechaCaja.TabIndex = 0
        '
        'lblTotalEgresos
        '
        Me.lblTotalEgresos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotalEgresos.AutoSize = True
        Me.lblTotalEgresos.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalEgresos.ForeColor = System.Drawing.Color.Gray
        Me.lblTotalEgresos.Location = New System.Drawing.Point(713, 418)
        Me.lblTotalEgresos.Name = "lblTotalEgresos"
        Me.lblTotalEgresos.Size = New System.Drawing.Size(224, 28)
        Me.lblTotalEgresos.TabIndex = 39
        Me.lblTotalEgresos.Text = "Total Egresos: $ 0,00"
        Me.lblTotalEgresos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalIngresos
        '
        Me.lblTotalIngresos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotalIngresos.AutoSize = True
        Me.lblTotalIngresos.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalIngresos.ForeColor = System.Drawing.Color.Gray
        Me.lblTotalIngresos.Location = New System.Drawing.Point(713, 390)
        Me.lblTotalIngresos.Name = "lblTotalIngresos"
        Me.lblTotalIngresos.Size = New System.Drawing.Size(230, 28)
        Me.lblTotalIngresos.TabIndex = 38
        Me.lblTotalIngresos.Text = "Total Ingresos: $ 0,00"
        Me.lblTotalIngresos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaja
        '
        Me.lblCaja.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCaja.AutoSize = True
        Me.lblCaja.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaja.Location = New System.Drawing.Point(25, 20)
        Me.lblCaja.Name = "lblCaja"
        Me.lblCaja.Size = New System.Drawing.Size(82, 38)
        Me.lblCaja.TabIndex = 47
        Me.lblCaja.Text = "Caja"
        Me.lblCaja.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bttAbrir
        '
        Me.bttAbrir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttAbrir.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttAbrir.FlatAppearance.BorderSize = 0
        Me.bttAbrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAbrir.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAbrir.ForeColor = System.Drawing.Color.Black
        Me.bttAbrir.Location = New System.Drawing.Point(349, 65)
        Me.bttAbrir.Margin = New System.Windows.Forms.Padding(0)
        Me.bttAbrir.Name = "bttAbrir"
        Me.bttAbrir.Size = New System.Drawing.Size(121, 40)
        Me.bttAbrir.TabIndex = 1
        Me.bttAbrir.Text = "Abrir"
        Me.bttAbrir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttAbrir.UseVisualStyleBackColor = False
        '
        'bttCerrar
        '
        Me.bttCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttCerrar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.bttCerrar.FlatAppearance.BorderSize = 0
        Me.bttCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCerrar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCerrar.ForeColor = System.Drawing.Color.Black
        Me.bttCerrar.Location = New System.Drawing.Point(479, 65)
        Me.bttCerrar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttCerrar.Name = "bttCerrar"
        Me.bttCerrar.Size = New System.Drawing.Size(121, 40)
        Me.bttCerrar.TabIndex = 2
        Me.bttCerrar.Text = "Cerrar"
        Me.bttCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCerrar.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(681, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(239, 38)
        Me.Label4.TabIndex = 63
        Me.Label4.Text = "Consultar fecha"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFechaConsulta
        '
        Me.dtpFechaConsulta.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpFechaConsulta.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaConsulta.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaConsulta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaConsulta.Location = New System.Drawing.Point(718, 73)
        Me.dtpFechaConsulta.Name = "dtpFechaConsulta"
        Me.dtpFechaConsulta.Size = New System.Drawing.Size(148, 27)
        Me.dtpFechaConsulta.TabIndex = 4
        '
        'bttConsultar
        '
        Me.bttConsultar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttConsultar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttConsultar.FlatAppearance.BorderSize = 0
        Me.bttConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttConsultar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttConsultar.ForeColor = System.Drawing.Color.Black
        Me.bttConsultar.Location = New System.Drawing.Point(884, 65)
        Me.bttConsultar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttConsultar.Name = "bttConsultar"
        Me.bttConsultar.Size = New System.Drawing.Size(121, 40)
        Me.bttConsultar.TabIndex = 5
        Me.bttConsultar.Text = "Consultar"
        Me.bttConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttConsultar.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(39, 117)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(106, 20)
        Me.Label6.TabIndex = 67
        Me.Label6.Text = "Dinero inicial"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 165)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(193, 38)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Movimientos"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(681, 307)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(298, 38)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Información de caja"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Gray
        Me.Label9.Location = New System.Drawing.Point(3, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(261, 25)
        Me.Label9.TabIndex = 72
        Me.Label9.Text = "Ingresos"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvIngresos, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvEgresos, 2, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(45, 206)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(555, 344)
        Me.TableLayoutPanel1.TabIndex = 73
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Gray
        Me.Label10.Location = New System.Drawing.Point(290, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(262, 25)
        Me.Label10.TabIndex = 71
        Me.Label10.Text = "Egresos"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvIngresos
        '
        Me.dgvIngresos.AllowUserToAddRows = False
        Me.dgvIngresos.AllowUserToDeleteRows = False
        Me.dgvIngresos.AllowUserToResizeRows = False
        Me.dgvIngresos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvIngresos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvIngresos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvIngresos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvIngresos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIngresos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvIngresos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Averta Regular", 9.749999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIngresos.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvIngresos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvIngresos.EnableHeadersVisualStyles = False
        Me.dgvIngresos.Location = New System.Drawing.Point(0, 40)
        Me.dgvIngresos.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvIngresos.MultiSelect = False
        Me.dgvIngresos.Name = "dgvIngresos"
        Me.dgvIngresos.ReadOnly = True
        Me.dgvIngresos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIngresos.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvIngresos.RowHeadersVisible = False
        Me.dgvIngresos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvIngresos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIngresos.ShowEditingIcon = False
        Me.dgvIngresos.Size = New System.Drawing.Size(267, 304)
        Me.dgvIngresos.TabIndex = 59
        '
        'dgvEgresos
        '
        Me.dgvEgresos.AllowUserToAddRows = False
        Me.dgvEgresos.AllowUserToDeleteRows = False
        Me.dgvEgresos.AllowUserToResizeRows = False
        Me.dgvEgresos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvEgresos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvEgresos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvEgresos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvEgresos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEgresos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvEgresos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Averta Regular", 9.749999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEgresos.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvEgresos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEgresos.EnableHeadersVisualStyles = False
        Me.dgvEgresos.Location = New System.Drawing.Point(287, 40)
        Me.dgvEgresos.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvEgresos.MultiSelect = False
        Me.dgvEgresos.Name = "dgvEgresos"
        Me.dgvEgresos.ReadOnly = True
        Me.dgvEgresos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEgresos.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvEgresos.RowHeadersVisible = False
        Me.dgvEgresos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEgresos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEgresos.ShowEditingIcon = False
        Me.dgvEgresos.Size = New System.Drawing.Size(268, 304)
        Me.dgvEgresos.TabIndex = 60
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(681, 136)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(125, 38)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Egresos"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bttAgregarEgreso
        '
        Me.bttAgregarEgreso.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttAgregarEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttAgregarEgreso.FlatAppearance.BorderSize = 0
        Me.bttAgregarEgreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAgregarEgreso.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAgregarEgreso.ForeColor = System.Drawing.Color.Black
        Me.bttAgregarEgreso.Location = New System.Drawing.Point(884, 198)
        Me.bttAgregarEgreso.Margin = New System.Windows.Forms.Padding(0)
        Me.bttAgregarEgreso.Name = "bttAgregarEgreso"
        Me.bttAgregarEgreso.Size = New System.Drawing.Size(121, 40)
        Me.bttAgregarEgreso.TabIndex = 8
        Me.bttAgregarEgreso.Text = "Agregar"
        Me.bttAgregarEgreso.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttAgregarEgreso.UseVisualStyleBackColor = False
        '
        'txtDineroInicial
        '
        Me.txtDineroInicial.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtDineroInicial.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDineroInicial.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDineroInicial.ForeColor = System.Drawing.Color.Black
        Me.txtDineroInicial.Location = New System.Drawing.Point(148, 117)
        Me.txtDineroInicial.Margin = New System.Windows.Forms.Padding(0)
        Me.txtDineroInicial.Name = "txtDineroInicial"
        Me.txtDineroInicial.Size = New System.Drawing.Size(113, 20)
        Me.txtDineroInicial.TabIndex = 3
        '
        'txtNuevoEgreso
        '
        Me.txtNuevoEgreso.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNuevoEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtNuevoEgreso.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNuevoEgreso.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNuevoEgreso.ForeColor = System.Drawing.Color.DarkGray
        Me.txtNuevoEgreso.Location = New System.Drawing.Point(718, 198)
        Me.txtNuevoEgreso.Margin = New System.Windows.Forms.Padding(0)
        Me.txtNuevoEgreso.Name = "txtNuevoEgreso"
        Me.txtNuevoEgreso.Size = New System.Drawing.Size(148, 20)
        Me.txtNuevoEgreso.TabIndex = 6
        Me.txtNuevoEgreso.Text = "Concepto"
        '
        'lblBalanceTotal
        '
        Me.lblBalanceTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBalanceTotal.AutoSize = True
        Me.lblBalanceTotal.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceTotal.ForeColor = System.Drawing.Color.Gray
        Me.lblBalanceTotal.Location = New System.Drawing.Point(713, 446)
        Me.lblBalanceTotal.Name = "lblBalanceTotal"
        Me.lblBalanceTotal.Size = New System.Drawing.Size(228, 28)
        Me.lblBalanceTotal.TabIndex = 39
        Me.lblBalanceTotal.Text = "Balance Total: $ 0,00"
        Me.lblBalanceTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMontoEgreso
        '
        Me.txtMontoEgreso.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMontoEgreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtMontoEgreso.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMontoEgreso.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMontoEgreso.ForeColor = System.Drawing.Color.DarkGray
        Me.txtMontoEgreso.Location = New System.Drawing.Point(718, 221)
        Me.txtMontoEgreso.Margin = New System.Windows.Forms.Padding(0)
        Me.txtMontoEgreso.Name = "txtMontoEgreso"
        Me.txtMontoEgreso.Size = New System.Drawing.Size(148, 20)
        Me.txtMontoEgreso.TabIndex = 7
        Me.txtMontoEgreso.Text = "Monto"
        '
        'lblDineroInicial
        '
        Me.lblDineroInicial.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDineroInicial.AutoSize = True
        Me.lblDineroInicial.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDineroInicial.ForeColor = System.Drawing.Color.Gray
        Me.lblDineroInicial.Location = New System.Drawing.Point(713, 362)
        Me.lblDineroInicial.Name = "lblDineroInicial"
        Me.lblDineroInicial.Size = New System.Drawing.Size(224, 28)
        Me.lblDineroInicial.TabIndex = 39
        Me.lblDineroInicial.Text = "Dinero Inicial: $ 0,00"
        Me.lblDineroInicial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbCredito
        '
        Me.cbCredito.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbCredito.AutoSize = True
        Me.cbCredito.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCredito.Location = New System.Drawing.Point(718, 256)
        Me.cbCredito.Name = "cbCredito"
        Me.cbCredito.Size = New System.Drawing.Size(85, 24)
        Me.cbCredito.TabIndex = 75
        Me.cbCredito.Text = "Crédito"
        Me.cbCredito.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.cbCredito.UseVisualStyleBackColor = True
        '
        'Caja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1039, 590)
        Me.Controls.Add(Me.cbCredito)
        Me.Controls.Add(Me.txtMontoEgreso)
        Me.Controls.Add(Me.txtNuevoEgreso)
        Me.Controls.Add(Me.txtDineroInicial)
        Me.Controls.Add(Me.bttAgregarEgreso)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.lblDineroInicial)
        Me.Controls.Add(Me.lblBalanceTotal)
        Me.Controls.Add(Me.lblTotalEgresos)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblTotalIngresos)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.bttConsultar)
        Me.Controls.Add(Me.dtpFechaConsulta)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.bttAbrir)
        Me.Controls.Add(Me.bttCerrar)
        Me.Controls.Add(Me.dtpFechaCaja)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCaja)
        Me.Name = "Caja"
        Me.Text = "Caja"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.dgvIngresos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEgresos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents dtpFechaCaja As DateTimePicker
    Friend WithEvents lblTotalEgresos As Label
    Friend WithEvents lblTotalIngresos As Label
    Friend WithEvents lblCaja As Label
    Friend WithEvents bttAbrir As Button
    Friend WithEvents bttCerrar As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents dtpFechaConsulta As DateTimePicker
    Friend WithEvents bttConsultar As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label10 As Label
    Friend WithEvents dgvIngresos As DataGridView
    Friend WithEvents Label11 As Label
    Friend WithEvents bttAgregarEgreso As Button
    Friend WithEvents dgvEgresos As DataGridView
    Friend WithEvents txtDineroInicial As TextBox
    Friend WithEvents txtNuevoEgreso As TextBox
    Friend WithEvents lblBalanceTotal As Label
    Friend WithEvents txtMontoEgreso As TextBox
    Friend WithEvents lblDineroInicial As Label
    Friend WithEvents cbCredito As CheckBox
End Class
