﻿Imports Datos

Public Class Caja

    Dim idcaja As Int64
    Dim totalEgresos As Double = 0
    Dim totalIngresos As Double = 0
    Dim total As Double = 0
    Public Sub AjustarDGVIngresos()
        If dgvIngresos.Rows.Count <> 0 Then
            dgvIngresos.Columns("Concepto").FillWeight = 50
            dgvIngresos.Columns("fecha").FillWeight = 25
            dgvIngresos.Columns("total").FillWeight = 25

            dgvIngresos.Columns("id").Visible = False
            dgvIngresos.Columns("tipo_id").Visible = False

            dgvIngresos.Columns("fecha").HeaderText = "Fecha"
            dgvIngresos.Columns("total").HeaderText = "Total"
        End If
    End Sub
    Public Sub AjustarDGVEgresos()
        If dgvEgresos.Rows.Count <> 0 Then
            dgvEgresos.Columns("descripcion").FillWeight = 50
            dgvEgresos.Columns("fecha").FillWeight = 25
            dgvEgresos.Columns("monto").FillWeight = 25

            dgvEgresos.Columns("descripcion").HeaderText = "Descripción"
            dgvEgresos.Columns("fecha").HeaderText = "Fecha"
            dgvEgresos.Columns("monto").HeaderText = "Monto"
        End If
    End Sub
    Private Sub bttAbrir_Click(sender As Object, e As EventArgs) Handles bttAbrir.Click
        Dim caja As New DatosCajas
        Try

            If Trim(txtDineroInicial.Text) <> "" Then

            'modifico el formato del datetimepicker para q me envie la fecha con horario
            dtpFechaCaja.Format = DateTimePickerFormat.Custom
            dtpFechaCaja.CustomFormat = "dd/mm/yyyy hh:mm"
            caja._fecha_inicio = dtpFechaCaja.Value

            'ahora vuelvo a colocar el formato en short
            dtpFechaCaja.Format = DateTimePickerFormat.Short

            caja._monto_inicial = txtDineroInicial.Text
            caja._ingreso_total = 0
            caja._egreso_total = 0
            caja._total = 0
            caja._estado = "Abierta"
                caja._id_cajero_abre = SesionUsuario.get_usuario
                caja._id_cajero_cierra = 0
            idcaja = caja.InsertarCajaDevolveridcaja()

            caja._id_caja = idcaja
                dgvIngresos.DataSource = caja.ObtenerPedidosPoridcaja
                dgvIngresos.Columns("tipo_id").Visible = False
                dgvIngresos.Columns("id").Visible = False

                lblCaja.Text = "Caja Nro: " & idcaja
            txtDineroInicial.Enabled = False
            dtpFechaCaja.Enabled = False
            bttAbrir.Enabled = False
            bttCerrar.Enabled = True
            txtNuevoEgreso.Enabled = True
            txtMontoEgreso.Enabled = True
            bttAgregarEgreso.Enabled = True

            'calcular total de ingresos y egresos y el total general
            If dgvIngresos.Rows.Count <> 0 Then
                For Each row In dgvIngresos.Rows
                    totalIngresos = totalIngresos + row.cells("total").value
                Next
            Else
                totalIngresos = 0
            End If
            If dgvEgresos.Rows.Count <> 0 Then
                For Each row In dgvEgresos.Rows
                    totalEgresos = totalEgresos + row.cells("monto").value
                Next
            Else
                totalEgresos = 0
            End If
                total = totalIngresos + totalEgresos
                lblTotalIngresos.Text = "Total Ingresos: $ " & totalIngresos
            lblTotalEgresos.Text = "Total Egresos: $ " & totalEgresos
            lblDineroInicial.Text = "Dinero Inicial: $ " & txtDineroInicial.Text
                lblBalanceTotal.Text = "Balance Total: $ " & FormatNumber(total, 2)
            Else
            MessageBox.Show("Ingrese el monto de dinero inicial.")
            txtDineroInicial.Focus()
        End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al abrir la caja. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub Caja_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Try


            Dim caja As New DatosCajas
            Dim dt As New DataTable
            dt = caja.BuscaCajaAbierta()

            If dt.Rows.Count = 0 Then
                'no hay caja abierta
                bttAbrir.Enabled = True
                bttCerrar.Enabled = False
                bttAbrir.BackColor = Color.FromArgb(255, 194, 135)
                bttAbrir.ForeColor = Color.Black
                bttCerrar.BackColor = Color.FromArgb(231, 234, 237)
                bttCerrar.ForeColor = Color.Black
                txtNuevoEgreso.Enabled = False
                bttAgregarEgreso.Enabled = False
                txtNuevoEgreso.Enabled = False
                txtMontoEgreso.Enabled = False
            Else
                'hay caja abierta
                lblCaja.Text = "Caja " & "Nro: " & dt.Rows(0)("id_caja")

                idcaja = dt.Rows(0)("id_caja")
                txtDineroInicial.Text = dt.Rows(0)("monto_inicial")
                txtDineroInicial.Enabled = False
                dtpFechaCaja.Value = dt.Rows(0)("fecha_inicio")
                dtpFechaCaja.Enabled = False

                bttAbrir.Enabled = False
                bttCerrar.Enabled = True
                bttAbrir.BackColor = Color.FromArgb(231, 234, 237)
                bttAbrir.ForeColor = Color.Black
                bttCerrar.BackColor = Color.FromArgb(255, 194, 135)
                bttCerrar.ForeColor = Color.Black
                txtNuevoEgreso.Enabled = True
                txtMontoEgreso.Enabled = True
                bttAgregarEgreso.Enabled = True

                'traigo pedidos de cafeteria y ventasa de panaderia y hago un merge de las 2
                caja._id_caja = idcaja
                Dim dtPed As New DataTable
                dtPed = caja.ObtenerPedidosPoridcaja
                Dim dtVen As New DataTable
                dtVen = caja.ObtenerVentasPoridcaja
                dtPed.Merge(dtVen)
                dgvIngresos.DataSource = dtPed
                AjustarDGVIngresos()
                '-----------------------------------

                dgvEgresos.DataSource = caja.ObtenerEgresos
                AjustarDGVEgresos()

                'calcular total de ingresos y egresos y el total general
                If dgvIngresos.Rows.Count <> 0 Then
                    For Each row In dgvIngresos.Rows
                        totalIngresos = totalIngresos + row.cells("total").value
                    Next
                Else
                    totalIngresos = 0
                End If
                If dgvEgresos.Rows.Count <> 0 Then
                    For Each row In dgvEgresos.Rows
                        totalEgresos = totalEgresos + row.cells("monto").value
                    Next
                Else
                    totalEgresos = 0
                End If
                total = totalIngresos + totalEgresos
                lblTotalIngresos.Text = "Total Ingresos: $ " & totalIngresos
                lblTotalEgresos.Text = "Total Egresos: $ " & totalEgresos
                lblDineroInicial.Text = "Dinero Inicial: $ " & txtDineroInicial.Text
                lblBalanceTotal.Text = "Balance Total: $ " & FormatNumber(total, 2)

            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al traer la caja. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttCerrar_Click(sender As Object, e As EventArgs) Handles bttCerrar.Click
        Dim caja As New DatosCajas
        Dim result As DialogResult = MessageBox.Show("¿Está seguro de que desea cerrar la caja Nro. " & idcaja & ", fecha " & dtpFechaCaja.Value & "?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        If result = DialogResult.Yes Then
            Try
                'cargo nuevamente dgvingresos y dgvegresos por las dudas

                'traigo pedidos de cafeteria y ventasa de panaderia y hago un merge de las 2
                caja._id_caja = idcaja
                Dim dtPed As New DataTable
                dtPed = caja.ObtenerPedidosPoridcaja
                Dim dtVen As New DataTable
                dtVen = caja.ObtenerVentasPoridcaja
                dtPed.Merge(dtVen)
                dgvIngresos.DataSource = dtPed
                '-----------------------------------

                dgvEgresos.DataSource = caja.ObtenerEgresos

            totalIngresos = 0
            If dgvIngresos.Rows.Count <> 0 Then
                For Each row In dgvIngresos.Rows
                    totalIngresos = totalIngresos + row.cells("total").value
                Next
            Else
                totalIngresos = 0
            End If

            totalEgresos = 0
            If dgvEgresos.Rows.Count <> 0 Then
                For Each row In dgvEgresos.Rows
                    totalEgresos = totalEgresos + row.cells("monto").value
                Next
            Else
                totalEgresos = 0
            End If
            total = totalIngresos + totalEgresos
                '-------------------------------------------------------

                caja._ingreso_total = totalIngresos
                caja._egreso_total = totalEgresos
                caja._total = total
                caja._fecha_fin = Date.Today
                caja._id_cajero_cierra = SesionUsuario.get_usuario
                caja._id_caja = idcaja
                caja.CerrarCaja()
                MessageBox.Show("Caja Nro. " & idcaja & " cerrada correctamente.")
                bttAbrir.Enabled = True
                bttCerrar.Enabled = False
                bttAbrir.BackColor = Color.FromArgb(255, 194, 135)
                bttAbrir.ForeColor = Color.Black
                bttCerrar.BackColor = Color.FromArgb(231, 234, 237)
                bttCerrar.ForeColor = Color.Black
                txtNuevoEgreso.Enabled = False
            txtMontoEgreso.Enabled = False
            bttAgregarEgreso.Enabled = False
            txtDineroInicial.Enabled = True
                txtDineroInicial.Text = ""
                dtpFechaCaja.Value = DateTime.Now
                dtpFechaCaja.Enabled = True
                lblCaja.Text = "Caja"
            dgvIngresos.DataSource = DBNull.Value
            dgvEgresos.DataSource = DBNull.Value
            lblTotalIngresos.Text = "Total Ingresos: $ 0,00"
            lblTotalEgresos.Text = "Total Egresos: $ 0,00"
            lblDineroInicial.Text = "Dinero Inicial: $ 0,00"
            lblBalanceTotal.Text = "Balance Total: $ 0,00"


            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al cerrar la caja. Detalle: " & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttAgregarEgreso_Click(sender As Object, e As EventArgs) Handles bttAgregarEgreso.Click
        Try
            Dim egreso As New DatosCajas
            egreso._id_caja = idcaja
            egreso._descripcion = txtNuevoEgreso.Text
            If cbCredito.Checked = True Then
                egreso._total = txtMontoEgreso.Text
            Else
                egreso._total = -txtMontoEgreso.Text
            End If

            egreso._fecha_fin = Date.Today
            egreso.InsertarEgreso()
            dgvEgresos.DataSource = egreso.ObtenerEgresos
            AjustarDGVEgresos()

            txtNuevoEgreso.Text = "Concepto"
            txtMontoEgreso.Text = "Monto"
            txtNuevoEgreso.ForeColor = Color.DarkGray
            txtMontoEgreso.ForeColor = Color.DarkGray
            cbCredito.Checked = False

            'calcular total de ingresos y egresos y el total general
            totalIngresos = 0
            If dgvIngresos.Rows.Count <> 0 Then
                For Each row In dgvIngresos.Rows
                    totalIngresos = totalIngresos + row.cells("total").value
                Next
            Else
                totalIngresos = 0
            End If

            totalEgresos = 0
            If dgvEgresos.Rows.Count <> 0 Then
                For Each row In dgvEgresos.Rows
                    totalEgresos = totalEgresos + row.cells("monto").value
                Next
            Else
                totalEgresos = 0
            End If
            total = totalIngresos + totalEgresos
            lblTotalIngresos.Text = "Total Ingresos: $ " & totalIngresos
            lblTotalEgresos.Text = "Total Egresos: $ " & totalEgresos
            lblDineroInicial.Text = "Dinero Inicial: $ " & txtDineroInicial.Text
            lblBalanceTotal.Text = "Balance Total: $ " & FormatNumber(total, 2)

        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar el egreso en caja. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtNuevoEgreso_Click(sender As Object, e As EventArgs) Handles txtNuevoEgreso.Click
        If txtNuevoEgreso.Text = "Concepto" Then
            txtNuevoEgreso.Text = ""
            txtNuevoEgreso.ForeColor = Color.Black
        Else
            txtNuevoEgreso.SelectAll()
        End If

    End Sub

    Private Sub txtNuevoEgreso_Leave(sender As Object, e As EventArgs) Handles txtNuevoEgreso.Leave
        If Trim(txtNuevoEgreso.Text) = "" Then
            txtNuevoEgreso.Text = "Concepto"
            txtNuevoEgreso.ForeColor = Color.DarkGray
        End If
    End Sub

    Private Sub txtMontoEgreso_Click(sender As Object, e As EventArgs) Handles txtMontoEgreso.Click
        If txtMontoEgreso.Text = "Monto" Then
            txtMontoEgreso.Text = ""
            txtMontoEgreso.ForeColor = Color.Black
        Else
            txtMontoEgreso.SelectAll()
        End If
    End Sub

    Private Sub txtMontoEgreso_Leave(sender As Object, e As EventArgs) Handles txtMontoEgreso.Leave
        If Trim(txtMontoEgreso.Text) = "" Then
            txtMontoEgreso.Text = "Monto"
            txtMontoEgreso.ForeColor = Color.DarkGray
        End If
    End Sub

    Private Sub bttConsultar_Click(sender As Object, e As EventArgs) Handles bttConsultar.Click
        Try
            Dim caja As New DatosCajas
            Dim dtcaja As New DataTable

            caja._fecha_inicio = dtpFechaConsulta.Value
            dtcaja = caja.BuscaCajaPorFecha()

            If dtcaja.Rows.Count <> 0 Then
                Dim f As New CajasPorFecha(dtcaja)
                f.ShowDialog()
            Else
                MessageBox.Show("No se encontró ninguna caja.")
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al buscar la caja de la fecha " & dtpFechaConsulta.Value & ". Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub txtMontoEgreso_Enter(sender As Object, e As EventArgs) Handles txtMontoEgreso.Enter
        If txtMontoEgreso.Text = "Monto" Then
            txtMontoEgreso.Text = ""
            txtMontoEgreso.ForeColor = Color.Black
        Else
            txtMontoEgreso.SelectAll()
        End If
    End Sub

    Private Sub dgvIngresos_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvIngresos.CellMouseDoubleClick
        If dgvIngresos.SelectedRows.Count <> 0 Then

            Dim f As New DetallesInformes(dgvIngresos.SelectedRows(0).Cells("id").Value, dgvIngresos.SelectedRows(0).Cells("tipo_id").Value)

            'pregunto si Ok(en la otra ventana declare que el boton cobrar es OK) fue presionado
            If f.ShowDialog() = DialogResult.OK Then

                Else

                End If

            End If
    End Sub

    Private Sub dtpFechaConsulta_KeyDown(sender As Object, e As KeyEventArgs) Handles dtpFechaConsulta.KeyDown
        If e.KeyCode = Keys.Enter Then
            bttConsultar.PerformClick()
        End If
    End Sub
End Class