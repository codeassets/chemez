﻿Imports Datos
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports System.IO
Public Class CajasPorFecha
    Dim dtCajas As New DataTable
    Dim dtIngresos As New DataTable
    Dim dtEgresos As New DataTable

    Sub New(cajas As DataTable)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        dtCajas = cajas
    End Sub
    Public Sub AjustarDGVIngresos()
        If dgvIngresos.Rows.Count <> 0 Then
            dgvIngresos.Columns("Concepto").FillWeight = 50
            dgvIngresos.Columns("fecha").FillWeight = 25
            dgvIngresos.Columns("total").FillWeight = 25

            dgvIngresos.Columns("id").Visible = False
            dgvIngresos.Columns("tipo_id").Visible = False

            dgvIngresos.Columns("fecha").HeaderText = "Fecha"
            dgvIngresos.Columns("total").HeaderText = "Total"
        End If
    End Sub
    Public Sub AjustarDGVEgresos()
        If dgvEgresos.Rows.Count <> 0 Then
            dgvEgresos.Columns("descripcion").FillWeight = 50
            dgvEgresos.Columns("fecha").FillWeight = 25
            dgvEgresos.Columns("monto").FillWeight = 25

            dgvEgresos.Columns("fecha").HeaderText = "Fecha"
            dgvEgresos.Columns("monto").HeaderText = "Monto"
            dgvEgresos.Columns("descripcion").HeaderText = "Desc."
        End If
    End Sub
    Public Sub AjustarDGVCajas()
        'ocultar columnas
        dgvCajas.Columns("monto_inicial").Visible = False
        dgvCajas.Columns("ingreso_total").Visible = False
        dgvCajas.Columns("egreso_total").Visible = False
        dgvCajas.Columns("total").Visible = False
        dgvCajas.Columns("id_cajero_abre").Visible = False
        dgvCajas.Columns("id_cajero_cierra").Visible = False
        dgvCajas.Columns("id_caja").HeaderText = "Nº"
        dgvCajas.Columns("fecha_inicio").HeaderText = "Inicio"
        dgvCajas.Columns("fecha_fin").HeaderText = "Fin"
        dgvCajas.Columns("Horario").HeaderText = "Hora"
        dgvCajas.Columns("estado").HeaderText = "Estado"
        dgvCajas.Columns("id_caja").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
    End Sub
    Private Sub CajasPorFecha_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        dgvCajas.DataSource = dtCajas
        AjustarDGVCajas()

        lblTitulo.Text = "Caja/s de la fecha: " & FormatDateTime(dgvCajas.SelectedRows(0).Cells("fecha_inicio").Value, DateFormat.ShortDate)
    End Sub

    Private Sub dgvCajas_SelectionChanged(sender As Object, e As EventArgs) Handles dgvCajas.SelectionChanged
        Try
            If dgvCajas.SelectedRows.Count <> 0 Then

                'traigo pedidos de cafeteria y ventasa de panaderia y hago un merge de las 2
                Dim caja As New DatosCajas
                caja._id_caja = dgvCajas.SelectedRows(0).Cells("id_caja").Value
                Dim dtPed As New DataTable
                dtPed = caja.ObtenerPedidosPoridcaja
                Dim dtVen As New DataTable
                dtVen = caja.ObtenerVentasPoridcaja
                dtPed.Merge(dtVen)
                dtIngresos = dtPed
                dgvIngresos.DataSource = dtIngresos
                AjustarDGVIngresos()

                dtEgresos = caja.ObtenerEgresos
                dgvEgresos.DataSource = dtEgresos
                AjustarDGVEgresos()

                lblTotalIngresos.Text = "Total Ingresos: $ " & dgvCajas.SelectedRows(0).Cells("ingreso_total").Value
                lblTotalEgresos.Text = "Total Egresos: $ " & dgvCajas.SelectedRows(0).Cells("egreso_total").Value
                lblDineroInicial.Text = "Dinero Inicial: $ " & dgvCajas.SelectedRows(0).Cells("monto_inicial").Value
                lblBalanceTotal.Text = "Balance Total: $ " & dgvCajas.SelectedRows(0).Cells("total").Value
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los ingresos/egresos de caja. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub dgvIngresos_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvIngresos.CellMouseDoubleClick
        If dgvIngresos.SelectedRows.Count <> 0 Then

            Dim f As New DetallesInformes(dgvIngresos.SelectedRows(0).Cells("id").Value, dgvIngresos.SelectedRows(0).Cells("tipo_id").Value)

            'pregunto si Ok(en la otra ventana declare que el boton cobrar es OK) fue presionado
            If f.ShowDialog() = DialogResult.OK Then

            Else

            End If

        End If
    End Sub

    Private Sub bttGenerarInforme_Click(sender As Object, e As EventArgs) Handles bttGenerarInforme.Click

        If dgvCajas.SelectedRows(0).Cells("estado").Value = "Cerrada" Then

            Using excel As New ExcelPackage()
                Try
                    Dim fechainicio As String = dgvCajas.SelectedRows(0).Cells("fecha_inicio").Value.ToShortDateString()
                    Dim hora As String = dgvCajas.SelectedRows(0).Cells("Horario").Value.ToString

                    Dim workSheet = excel.Workbook.Worksheets.Add("Sheet1")

                    'les quito las columnas q no quiero imprimir 
                    Dim dtIngresosInforme As New DataTable
                    dtIngresosInforme = dtIngresos
                    If dtIngresosInforme.Columns.Contains("id") Then
                        dtIngresosInforme.Columns.Remove("id")
                        dtIngresosInforme.Columns.Remove("tipo_id")
                        dtIngresosInforme.Columns.Remove("fecha")
                    End If


                    Dim dtEgresosInforme As New DataTable
                    dtEgresosInforme = dtEgresos
                    If dtEgresosInforme.Columns.Contains("fecha") Then
                        dtEgresosInforme.Columns.Remove("fecha")
                    End If

                    workSheet.Cells("A3").LoadFromDataTable(dtIngresosInforme, False)

                    'posicion donde va a cargar Egresos
                    Dim pos As Integer = dtIngresos.Rows.Count + 3
                    workSheet.Cells(pos, 3).LoadFromDataTable(dtEgresosInforme, False)

                    'posicion donde va a cargar los totales
                    Dim posTotales As Integer = pos + dtEgresosInforme.Rows.Count
                    workSheet.Cells(posTotales, 1).Value = "Total Ingresos: $ " & dgvCajas.SelectedRows(0).Cells("ingreso_total").Value
                    workSheet.Cells(posTotales, 1, posTotales, 2).Merge = True


                    workSheet.Cells(posTotales, 3).Value = "Total Egresos: $ " & dgvCajas.SelectedRows(0).Cells("egreso_total").Value
                    workSheet.Cells(posTotales, 3, posTotales, 4).Merge = True

                    workSheet.Cells(posTotales + 1, 1).Value = "Dinero Inicial: $ " & dgvCajas.SelectedRows(0).Cells("monto_inicial").Value
                    workSheet.Cells(posTotales + 1, 1, posTotales + 1, 4).Merge = True

                    workSheet.Cells(posTotales + 2, 1).Value = "Balance Total: $ " & dgvCajas.SelectedRows(0).Cells("total").Value
                    workSheet.Cells(posTotales + 2, 1, posTotales + 2, 4).Merge = True


                    'aplicar estilos a la primera fila
                    workSheet.TabColor = System.Drawing.Color.Black
                    workSheet.DefaultRowHeight = 12
                    workSheet.Row(1).Height = 20
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(1).Style.Font.Bold = True
                    workSheet.Cells(1, 1).Value = "Caja Nro: " & dgvCajas.SelectedRows(0).Cells("id_caja").Value & ", Fecha: " & fechainicio & ", Hora: " & hora

                    'aplicar estilos a la segunda fila
                    workSheet.Row(2).Height = 20
                    workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(2).Style.Font.Bold = True

                    'aplicar estilos a los totales
                    workSheet.Row(posTotales).Height = 20
                    workSheet.Row(posTotales).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(posTotales).Style.Font.Bold = True

                    workSheet.Row(posTotales + 1).Height = 20
                    workSheet.Row(posTotales + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(posTotales + 1).Style.Font.Bold = True

                    workSheet.Row(posTotales + 2).Height = 20
                    workSheet.Row(posTotales + 2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(posTotales + 2).Style.Font.Bold = True

                    workSheet.Cells(2, 1).Value = "Ingresos"
                    ' workSheet.Cells(2, 2).Value = "Total"
                    workSheet.Cells("A2:B2").Merge = True

                    workSheet.Cells(2, 3).Value = "Egresos"
                    'workSheet.Cells(2, 4).Value = "Monto"
                    workSheet.Cells("C2:D2").Merge = True

                    workSheet.Cells("A1:D1").Merge = True



                    workSheet.Column(1).AutoFit()

                    workSheet.Column(2).AutoFit()
                    workSheet.Column(2).Style.Numberformat.Format = "$###,###,##0.00"

                    workSheet.Column(3).AutoFit()

                    workSheet.Column(4).AutoFit()
                    workSheet.Column(4).Style.Numberformat.Format = "$###,###,##0.00"


                    workSheet.Cells("A1:I1").Style.Locked = True

                    'acomodar el sheet entero
                    workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()

                    Dim a As String = "Caja " & dgvCajas.SelectedRows(0).Cells("id_caja").Value & " _ " & fechainicio & " _ Hora " & hora & ".xlsx"
                    Dim excelName As String = a.Replace("/", "-").Replace(":", ".")


                    Try
                        'create a SaveFileDialog instance with some properties
                        Dim saveFileDialog1 As New SaveFileDialog()
                        saveFileDialog1.Title = "Save Excel sheet"
                        saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                        saveFileDialog1.FileName = excelName
                        'check if user clicked the save button
                        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                            'Get the FileInfo
                            Dim fi As New FileInfo(saveFileDialog1.FileName)
                            excel.SaveAs(fi)
                            'write the file to the disk
                        End If

                    Catch ex As Exception
                        MessageBox.Show("Surgió un problema al guardar el archivo Excel. Detalle: " & ex.Message.ToString)
                    End Try
                Catch ex As Exception
                    MessageBox.Show("Surgió un problema al crear el archivo Excel. Detalle: " & ex.Message.ToString)
                End Try
            End Using

        Else
                MessageBox.Show("Debe cerrar la caja antes de generar el informe.")
        End If

    End Sub

    Private Sub PrintDocumentCaja_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocumentCaja.PrintPage
        Dim graphic As Graphics = e.Graphics

        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center

        Dim font As New Font("Ticketing", 12)
        Dim fontbold As New Font("Ticketing", 16)
        Dim fontheight As Double = font.GetHeight
        Dim startx As Integer = 10
        Dim starty As Integer = 0
        Dim offset As Integer = 40
        Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

        ' Encabezado
        graphic.DrawString("CIERRE DE CAJA", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Nro. Caja: " & dgvCajas.SelectedRows(0).Cells("id_caja").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Fecha de Apertura: " & dgvCajas.SelectedRows(0).Cells("fecha_inicio").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Fecha de Cierre: " & dgvCajas.SelectedRows(0).Cells("fecha_fin").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("INGRESOS", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        ' Detalles ingresos
        For Each row As DataGridViewRow In dgvIngresos.Rows

            Dim conc As String = row.Cells("Concepto").Value
            conc = conc.Substring(0, Math.Min(conc.Length, 21))
            Dim total As Double = row.Cells("total").Value
            Dim formattedTotal As String = String.Format("{0:n}", total)

            graphic.DrawString(conc, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString(formattedTotal, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

        Next

        ' Total ingresos
        offset = offset + CInt(fontheight) + 3

        graphic.DrawString("TOTAL INGRESOS", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
        graphic.DrawString("$" & dgvCajas.SelectedRows(0).Cells("ingreso_total").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
        offset = offset + CInt(fontheight) + 3

        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        graphic.DrawString("EGRESOS", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        ' Detalles egresos
        For Each row As DataGridViewRow In dgvEgresos.Rows

            Dim conc As String = row.Cells("descripcion").Value
            conc = conc.Substring(0, Math.Min(conc.Length, 21))
            Dim total As Double = row.Cells("monto").Value
            Dim formattedTotal As String = String.Format("{0:n}", total)

            graphic.DrawString(conc, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString(formattedTotal, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

        Next

        ' Total egresos

        offset = offset + CInt(fontheight) + 3

        graphic.DrawString("TOTAL EGRESOS", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
        graphic.DrawString("$" & dgvCajas.SelectedRows(0).Cells("egreso_total").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
        offset = offset + CInt(fontheight) + 3

        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        graphic.DrawString("BALANCE TOTAL", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
        graphic.DrawString("$" & dgvCajas.SelectedRows(0).Cells("total").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

        e.HasMorePages = False

    End Sub

    Private Sub bttImprimir_Click(sender As Object, e As EventArgs) Handles bttImprimir.Click

        If dgvCajas.SelectedRows(0).Cells("estado").Value = "Cerrada" Then
            PrintDocumentCaja.PrinterSettings.PrinterName = My.Settings.Printer
            PrintDocumentCaja.Print()
        Else
            MessageBox.Show("Debe cerrar la caja antes de imprimir el resumen.")
        End If

    End Sub
End Class