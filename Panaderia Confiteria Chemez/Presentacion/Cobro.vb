﻿Imports Datos
Imports QRCoder
Imports System.Drawing.Printing
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports Presentacion.WSFE.HOMO
Imports System.Net
Imports log4net

Public Class Cobro

    ' Log de errores
    Private Shared _logger As ILog = Nothing

    Private Shared ReadOnly Property Logger As log4net.ILog
        Get

            If _logger Is Nothing Then
                _logger = LogManager.GetLogger("file")
                log4net.Config.XmlConfigurator.Configure()
            End If

            Return _logger
        End Get
    End Property

    'pictureboxQR
    Dim pb_codQR As New PictureBox

    ' Para el constructor
    Dim idped As Int64
    Dim tabladet As New DataTable
    Dim total As Double
    Dim totalconDesc As Double
    Dim descuento As Double
    Dim idmesa As Int64
    Dim idmozo As Int64
    Dim idcaja As Int64
    Dim mozo As String

    ' Otras
    Dim tipopago As String = "Efectivo"
    Dim nombrecliente As String = "1"
    Dim telefonocliente As String = "1"
    Dim direccioncliente As String = "1"

    ' para mandar  al webservice
    Dim ptovta As Integer = 4 ' punto de venta
    Dim cbtetipo As Integer  ' tipo de comprobante
    Dim cbtetipo_desc As String
    Dim concepto As Integer = 2 ' concepto: 1 prod 2 serv 3 prod y serv
    Dim concepto_desc As String = "Servicio"
    Dim doctipo As Integer  ' tipo de documento
    Dim doctipo_desc As String
    Dim docnro As Int64 ' numero de documento
    Dim CbteDesde As Integer
    Dim CbteHasta As Integer
    Dim cbtefch As String ' fecha de comprobante: yyyymmdd

    Dim importeneto As Double ' importe neto no gravado TOTAL
    Dim importeneto21 As Double ' importe neto 21%
    Dim importeneto10 As Double ' importe neto 10.5%
    Dim importetotal As Double ' importe total del comprobante

    Dim importeiva21 As Double = 0 ' suma de importes de IVA 21
    Dim importeiva10 As Double = 0 ' suma de importes de IVA 10.5
    Dim iva As String = "0" ' 

    Dim imptotalconc As Double ' importe neto no gravado
    Dim impopex As Double ' importe exento. Debe ser menor o igual al importe total y no debe ser menor a 0
    Dim imptrib As Double ' suma de los importes del array de tributos

    Dim idiva21 As Integer ' codigo de tipo iva 21
    Dim idiva10 As Integer ' codigo de tipo iva 10
    Dim baseimp21 As Double = 0 ' base imponible para la determinación de la alicuota 21
    Dim baseimp10 As Double = 0 ' base imponible para la determinación de la alicuota 10.5

    Dim estado As String


    ' para AFIP
    Private l As LoginAFIP
    Property Login As LoginAFIP
    Property TiposComprobantes As CbteTipoResponse
    Property TipoConceptos As ConceptoTipoResponse
    Property TipoDoc As DocTipoResponse
    Property Monedas As MonedaResponse
    Property puntosventa As FEPtoVentaResponse
    Property TiposIVA As IvaTipoResponse
    Property opcionales As OpcionalTipoResponse
    Property authRequest As FEAuthRequest
    Private url As String

    Private Function getServicio() As Service
        Dim s As New Service
        s.Url = My.Settings.url_service
        Return s
    End Function

    Public Function CalcChecksum(S As String) As Byte
        Dim chk As Byte ' Final Checksum
        Dim T As Long   ' Char-pointer...

        ' Step 1
        chk = Asc(Mid(S, 1, 2))    ' Get first Char value

        ' Initialize counter
        T = 1
        While T < Len(S)

            ' Step 2
            chk = chk And 127   ' Bitwise operation. Clear bit 7 (127 -> 0111 1111)

            ' Step 3
            chk = chk Or 64     ' Bitwise operation. Set bit 6 (64 -> 01000000)

            ' Increase counter...
            T = T + 1

            ' Step 4
            chk = Asc(Mid(S, T, 1)) + chk
        End While

        CalcChecksum = chk
    End Function

    Sub New(idpedido As Int64, dt As DataTable, _idmesa As Int64, _idmozo As Int64, _idcaja As Int64, _mozo As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        idped = idpedido
        tabladet = dt
        idmesa = _idmesa
        idmozo = _idmozo
        idcaja = _idcaja
        mozo = _mozo
    End Sub

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Private Sub Cobro_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        ' Verificar conexión a internet
        Try
            Using client = New WebClient()
                Using stream = client.OpenRead("http://www.google.com")

                End Using
            End Using
        Catch ex As Exception
            MsgBox("Verifique su conexión a internet. Detalle: " + ex.Message.ToString)
        End Try

        cboxComprobante.SelectedIndex = 2

        ' Cargar detalles en la tabla resumen
        dgvResumen.DataSource = tabladet

        If dgvResumen.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            dgvResumen.Columns("descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvResumen.Columns("cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            '' Cambiar nombres de encabezados
            dgvResumen.Columns("cantidad").HeaderText = "Cantidad"
            dgvResumen.Columns("descripcion").HeaderText = "Producto"
            dgvResumen.Columns("precio_unitario").HeaderText = "Precio Unitario"
            dgvResumen.Columns("subtotal").HeaderText = "Subtotal"
            '' Ocultar columnas
            dgvResumen.Columns("id_detalle").Visible = False
            dgvResumen.Columns("id_pedido").Visible = False
            dgvResumen.Columns("id_producto").Visible = False
            dgvResumen.Columns("EsStock").Visible = False
            dgvResumen.Columns("Accion").Visible = False
            dgvResumen.Columns("IVA").Visible = False
            dgvResumen.Columns("Importe_IVA").Visible = False
            dgvResumen.ClearSelection()
        End If

        'Calcular Total
        For Each row As DataGridViewRow In dgvResumen.Rows
            total = total + row.Cells("subtotal").Value
        Next

        totalconDesc = total

        ' Completar totales
        importeneto = total
        importetotal = total

        ' Calcular importes IVA
        Dim totaliva21 As Double
        Dim totaliva10 As Double
        For Each row As DataGridViewRow In dgvResumen.Rows
            If row.Cells("iva").Value = 21.0 Then
                totaliva21 = totaliva21 + row.Cells("importe_iva").Value
                importeneto21 = importeneto21 + row.Cells("subtotal").Value
            ElseIf row.Cells("iva").Value = 10.5 Then
                totaliva10 = totaliva10 + row.Cells("importe_iva").Value
                importeneto10 = importeneto10 + row.Cells("subtotal").Value
            End If
        Next
        lblImporteIVA10.Text = "Importe IVA 10,5%: $" & totaliva10
        lblImporteIVA21.Text = "Importe IVA 21%: $" & totaliva21

        ' Completar totales
        importeneto = (importeneto21 + importeneto10) - (totaliva21 + totaliva10)
        importetotal = total

        lblTotal.Text = "Total: $" & total
        lblImporteNeto.Text = "Importe Neto: $" & importeneto
        lbldescuento.Text = "Descuento: $" & descuento

        ' var para mandar al afip, por defecto factura B
        cbtetipo = 6
        cbtetipo_desc = "Factura B"
        concepto = 2
        concepto_desc = "Servicio"
        doctipo = 96
        doctipo_desc = "DNI"
        docnro = txtDocumento.Text
        cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
        imptotalconc = 0
        impopex = 0
        imptrib = 0

        ' Comprobar cuantos tipos de iva distintos hay en la factura
        If totaliva10 <> 0 Then

            'iva 10.5
            idiva10 = 4
            baseimp10 = importeneto10
            importeiva10 = totaliva10

        End If

        If totaliva21 <> 0 Then

            'iva 21
            idiva21 = 5
            baseimp21 = importeneto21
            importeiva21 = totaliva21

        End If

        estado = "No Facturado"

        ' Deshabilitar fechas posteriores a hoy 
        dtpFecha.MaxDate = Date.Today

    End Sub

    Private Sub RbttPorc_CheckedChanged(sender As Object, e As EventArgs) Handles rbttPorc.CheckedChanged
        txtDescVal.Enabled = False
        txtDescPorc.Enabled = True
        txtDescVal.Text = 0
        txtDescPorc.Focus()
    End Sub

    Private Sub RbttVal_CheckedChanged(sender As Object, e As EventArgs) Handles rbttVal.CheckedChanged
        txtDescPorc.Enabled = False
        txtDescVal.Enabled = True
        txtDescPorc.Text = 0
        txtDescVal.Focus()
    End Sub

    Private Sub bttCancelar_Click_1(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.Close()
    End Sub

    Private Sub bttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click

        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12

        Try
            Using client = New WebClient()
                Using stream = client.OpenRead("http://www.google.com")
                    ' Verificar login
                    l = New LoginAFIP(My.Settings.def_serv, My.Settings.def_url, My.Settings.def_cert, My.Settings.def_pass)
                    l.hacerLogin()
                End Using
            End Using
        Catch ex As Exception
            MsgBox("Verifique su conexión a internet. Detalle: " + ex.Message.ToString)
            Return
        End Try

        Dim result As DialogResult = MessageBox.Show("Valores a cobrar: Monto total: $" & totalconDesc & ", " & "descuento: -$" & descuento & ". ¿Desea continuar?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        If result = DialogResult.Yes Then
            Try

#Region "Guardar en BD Pedidos y DetallesPedidos y disminuir stock y actualizar estado de mesa"
                Dim cobrar As New DatosPedidos
                cobrar._id_pedido = idped
                cobrar._id_usuario = SesionUsuario.get_usuario
                cobrar._descuento = -descuento
                cobrar._total = totalconDesc
                cobrar._id_caja = idcaja
                If rbEfectivo.Checked = True Then
                    cobrar._tipo_pago = "Efectivo"
                ElseIf rbCredDeb.Checked = True Then
                    cobrar._tipo_pago = "Crédito/Débito"
                End If
                cobrar.CobraryModificarPedido()

                'restar stock

                Dim prod As New DatosProductos
                For Each row As DataGridViewRow In dgvResumen.Rows
                    If Not IsDBNull(row.Cells("EsStock").Value) Then 'pregunto, si es stock(osea q tiene una cantidad >= 0) hace el update y disminuye la cantidad del producto, si es no stock (oseaa q tiene una cantidad =NULL) no hace el update
                        prod._id_producto = row.Cells("id_producto").Value
                        prod._cantidad = row.Cells("cantidad").Value
                        prod.DisminuirStock()
                    End If
                Next



                'actualizar mesa
                Dim mesa As New DatosMesas
                mesa._id_mesa = idmesa
                mesa._estado = "Libre"
                mesa._id_mozo = idmozo
                mesa.ActualizarEstadoMesa()
#End Region

#Region "Facturar"

                Dim f As New DatosFacturas
                Dim idfactura As Long
                ''Asignar valores
                f._IdVenta = idped
                f._TipoId = "Pedido"
                f._PtoVta = ptovta
                f._CbteTipo = cbtetipo
                f._CbteTipo_desc = cbtetipo_desc
                f._Concepto = concepto
                f._Concepto_desc = concepto_desc
                f._DocTipo = doctipo
                f._DocTipo_desc = doctipo_desc
                f._DocNro = txtDocumento.Text
                f._CbeteDesde = CbteDesde
                f._CbteHasta = CbteHasta
                f._CbteFch = dtpFecha.Value
                f._ImpTotal = FormatNumber(importetotal, 2)
                f._ImpTotConc = 0
                f._ImpNeto = FormatNumber(importeneto, 2)
                f._ImpOpEx = 0
                f._ImpTrib = 0
                f._ImpIVA = FormatNumber(importeiva21 + importeiva10, 2)
                f._MonId = "PES"
                f._MonCotiz = 1
                f._AlicIva_Id21 = idiva21
                f._AlicIva_BaseImp21 = baseimp21
                f._AlicIva_Importe21 = importeiva21
                f._AlicIva_Id105 = idiva10
                f._AlicIva_BaseImp105 = baseimp10
                f._AlicIva_Importe105 = importeiva10
                f._TipoPago = tipopago

                If cboxComprobante.SelectedIndex = 0 Then 'guardar en bd como ticket comun
                    f._Estado = "No Facturado"
                    f._CbeteDesde = Nothing
                    f._CbteHasta = Nothing
                    f._CAE = Nothing
                    f._CAE_vto = Nothing
                    f._Mensaje = Nothing
                    MessageBox.Show("Factura guardada.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                    ' Imprimir ticket

                    If cbTicket.Checked = True Then
                        PrintDocument1.PrinterSettings.PrinterName = My.Settings.Printer
                        PrintDocument1.Print()
                    End If

                Else
                    f._Estado = "Facturado"

#Region "Conexion a AFIP y obtencion del CAE"
                    authRequest = New FEAuthRequest()
                    authRequest.Cuit = My.Settings.def_cuit
                    authRequest.Sign = Login.Sign
                    authRequest.Token = Login.Token

                    Dim service As WSFE.HOMO.Service = getServicio()
                    service.ClientCertificates.Add(Login.certificado)


                    Dim req As New FECAERequest
                    Dim cab As New FECAECabRequest
                    Dim det As New FECAEDetRequest

                    cab.CantReg = 1
                    cab.PtoVta = ptovta
                    cab.CbteTipo = cbtetipo
                    req.FeCabReq = cab

                    With det

                        .Concepto = concepto
                        .DocTipo = doctipo
                        .DocNro = txtDocumento.Text

                        Dim lastRes As FERecuperaLastCbteResponse = service.FECompUltimoAutorizado(authRequest, ptovta, cbtetipo)

                        Dim last As Integer = lastRes.CbteNro

                        CbteDesde = last + 1
                        CbteHasta = last + 1
                        .CbteDesde = last + 1
                        .CbteHasta = last + 1

                        .CbteFch = cbtefch

                        If concepto = 2 Then
                            .FchServDesde = cbtefch
                            .FchServHasta = cbtefch
                            .FchVtoPago = cbtefch
                        End If

                        .ImpNeto = FormatNumber(importeneto, 2)
                        .ImpIVA = FormatNumber(importeiva21 + importeiva10, 2)
                        .ImpTotal = FormatNumber(importetotal, 2)

                        .ImpTotConc = 0
                        .ImpOpEx = 0
                        .ImpTrib = 0

                        .MonId = "PES"
                        .MonCotiz = 1

                        .FchServDesde = cbtefch
                        .FchServHasta = cbtefch
                        .FchVtoPago = cbtefch

                        Dim alicuota As New AlicIva

                        If importeiva21 <> 0 And importeiva10 <> 0 Then

                            'iva 10.5
                            alicuota.Id = idiva10
                            alicuota.BaseImp = FormatNumber(baseimp10 - importeiva10, 2)
                            alicuota.Importe = FormatNumber(importeiva10, 2)

                            'iva 21
                            Dim alicuota2 As New AlicIva
                            alicuota2.Id = idiva21
                            alicuota2.BaseImp = FormatNumber(baseimp21 - importeiva21, 2)
                            alicuota2.Importe = FormatNumber(importeiva21, 2)

                            .Iva = {alicuota, alicuota2}

                        End If

                        If importeiva21 = 0 And importeiva10 <> 0 Then

                            'iva 10.5
                            alicuota.Id = idiva10
                            alicuota.BaseImp = FormatNumber(baseimp10 - importeiva10, 2)
                            alicuota.Importe = FormatNumber(importeiva10, 2)

                            .Iva = {alicuota}

                        End If

                        If importeiva10 = 0 And importeiva21 <> 0 Then

                            'iva 21
                            alicuota.Id = idiva21
                            alicuota.BaseImp = FormatNumber(baseimp21 - importeiva21, 2)
                            alicuota.Importe = FormatNumber(importeiva21, 2)

                            .Iva = {alicuota}

                        End If

                    End With


                    req.FeDetReq = {det}

                    ' Obtenemos el CAE
                    Dim r = service.FECAESolicitar(authRequest, req)
#End Region

#Region "Imprimimos las observaciones y errores de la variable r con la que obtuvimos el CAE"

                    Dim mensaje_obs_errores_eventos As String = ""
                    If r.FeDetResp(0).Observaciones IsNot Nothing Then
                        For Each o In r.FeDetResp(0).Observaciones
                            mensaje_obs_errores_eventos &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & "; "
                        Next
                    End If
                    If r.Errors IsNot Nothing Then
                        For Each er In r.Errors
                            mensaje_obs_errores_eventos &= String.Format("Er: {0}: {1}", er.Code, er.Msg) & "; "
                        Next
                    End If
                    If r.Events IsNot Nothing Then
                        For Each ev In r.Events
                            mensaje_obs_errores_eventos &= String.Format("Ev: {0}: {1}", ev.Code, ev.Msg) & "; "
                        Next
                    End If
                    'Resultado.Text = m
#End Region



#Region "Armo el codigo y genero el codigo QR"
                    ' Codigo QR
                    Dim qrcodestring As String = String.Concat(authRequest.Cuit,
                                          cbtetipo.ToString("00"),
                                          ptovta.ToString("0000"),
                                          r.FeDetResp(0).CAE,
                                          r.FeDetResp(0).CAEFchVto)
                        qrcodestring &= CalcChecksum(qrcodestring)


                    Dim gen As New QRCodeGenerator
                        Dim data = gen.CreateQrCode(qrcodestring, QRCodeGenerator.ECCLevel.Q)

                        Dim code As New QRCode(data)
                        pb_codQR.BackgroundImage = code.GetGraphic(6)

#End Region

                        ' Asigno valores que faltaban

                        ' Verificar si el estado de r es a(aprobado) o r(rechazado)
                        If r.FeDetResp(0).Resultado = "R" Then
                            f._Estado = "No Facturado"
                            f._CbeteDesde = Nothing
                            f._CbteHasta = Nothing
                            f._CAE = Nothing
                            f._CAE_vto = Nothing
                            f._Mensaje = mensaje_obs_errores_eventos
                            MessageBox.Show("Se rechazó la factura. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Error)

                        Else
                            f._Estado = "Facturado"
                            f._CbeteDesde = CbteDesde
                            f._CbteHasta = CbteHasta
                            f._CAE = r.FeDetResp(0).CAE
                            f._CAE_vto = r.FeDetResp(0).CAEFchVto
                            f._Mensaje = mensaje_obs_errores_eventos
                            MessageBox.Show("Facturado exitosamente. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                            ' Imprimir ticket

                            If cbTicket.Checked = True Then
                            PrintDocument1.PrinterSettings.PrinterName = My.Settings.Printer
                            Try
                                PrintDocument1.Print()
                            Catch ex As Exception
                                MessageBox.Show("No hay ninguna impresora configurada. Entre a configuración y reintente.")
                            End Try
                        End If

                        End If
                    End If

                    ' Crear factura
                    idfactura = f.InsertarFactura

                ' Crear detalles facturas
                For Each row As DataGridViewRow In dgvResumen.Rows
                    Dim detalle As New DatosFacturas
                    detalle._IdProducto = Nothing
                    detalle._Descripcion = row.Cells("descripcion").Value
                    detalle._Cantidad = row.Cells("cantidad").Value
                    detalle._PrecioUnitario = row.Cells("precio_unitario").Value
                    detalle._Subtotal = row.Cells("subtotal").Value
                    detalle._Importe_IVA = row.Cells("importe_iva").Value
                    detalle._IVA = row.Cells("iva").Value
                    detalle._IdFactura = idfactura
                    detalle.InsertarDetalleFactura()
                Next

#End Region

                'declaro Me(osea este boton q seria cobrar) como el resultado Ok que usare cuando se cierre esta ventana
                Me.DialogResult = DialogResult.OK
                'Me.Close()

            Catch ex As Exception
                MessageBox.Show("Ocurrió un error al registrar el cobro. Detalle: " & ex.Message.ToString)
                Logger.Error("Sección cafetería: error al registrar la factura. Detalle: ", ex)
            End Try
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        If cboxComprobante.SelectedIndex = 0 Then
#Region "Ticket Común"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("FLORENCIA PANADERÍA - CAFETERÍA", font, New SolidBrush(Color.Black), 30, starty + offset)
            offset = offset + CInt(fontheight) + 10
            graphic.DrawString("Dorrego 140 - Tel: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Mozo: " & mozo.ToString, font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row.Cells("cantidad").Value
                Dim precio As Double = row.Cells("precio_unitario").Value
                Dim subt As Double = row.Cells("subtotal").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc
                Dim linea2total As String = "$" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

                offset = offset + CInt(fontheight) + 3
            Next

            'If descuento <> 0 Then

            '    offset = offset + CInt(fontheight) + 2
            '    graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            '    graphic.DrawString("$" & descuento, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            '    offset = offset + 3

            'End If

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & totalconDesc, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
            offset = offset + CInt(fontheight) + 20
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region
        ElseIf cboxComprobante.SelectedIndex = 1 Then
#Region "Factura A"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("CHEMEZ LUIS ENRIQUE", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DORREGO 140 - TEL: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("FERNANDEZ - CP (4322)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/19", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA A", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & ptovta.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & CbteDesde.ToString("00000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(nombrecliente, font, New SolidBrush(Color.Black), startx, starty + offset) 'nombre cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(tipopago, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(txtDocumento.Text, font, New SolidBrush(Color.Black), startx, starty + offset) 'cuit cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(direccioncliente, font, New SolidBrush(Color.Black), startx, starty + offset) 'direccion cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row.Cells("cantidad").Value
                Dim precio As Double = row.Cells("precio_unitario").Value
                Dim subt As Double = row.Cells("subtotal").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio


                Dim linea2 As String = desc & "(" & row.Cells("iva").Value & ")"


                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & importeneto, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IMP. IVA", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & FormatNumber(importeiva21 + importeiva10, 2), font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            'If descuento <> 0 Then
            '    offset = offset + CInt(fontheight) + 3
            '    graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            '    graphic.DrawString("$" & descuento, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            'End If

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & totalconDesc, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("RECIBI(MOS)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU PAGO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtPago.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU VUELTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtVuelto.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

            'defino parametros del picturebox
            pb_codQR.Width = 100
            pb_codQR.Height = 100
            pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

            Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 80, starty + offset - 20)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region
        ElseIf cboxComprobante.SelectedIndex = 2 Then
#Region "Factura B"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("CHEMEZ LUIS ENRIQUE", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DORREGO 140 - TEL: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("FERNANDEZ - CP (4322)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/19", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA B", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & ptovta.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & CbteDesde.ToString("000000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(tipopago, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DNI Nro.: " & txtDocumento.Text, font, New SolidBrush(Color.Black), startx, starty + offset) 'documento cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("A CONSUMIDOR FINAL", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row.Cells("cantidad").Value
                Dim precio As Double = row.Cells("precio_unitario").Value
                Dim subt As Double = row.Cells("subtotal").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(" & row.Cells("iva").Value & ")"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & totalconDesc, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            'If descuento <> 0 Then
            '    offset = offset + CInt(fontheight) + 3
            '    graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            '    graphic.DrawString("$" & descuento, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            'End If

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & totalconDesc, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("RECIBI(MOS)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU PAGO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtPago.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU VUELTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtVuelto.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

            'defino parametros del picturebox
            pb_codQR.Width = 100
            pb_codQR.Height = 100
            pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

            Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 80, starty + offset - 20)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region

        End If

    End Sub

    Private Sub txtPago_TextChanged(sender As Object, e As EventArgs) Handles txtPago.TextChanged
        If txtPago.Text <> Trim("") Then
            If txtPago.Text <> 0 Then
                If txtPago.Text >= totalconDesc Then
                    txtVuelto.Text = Double.Parse(txtPago.Text) - totalconDesc
                Else
                    txtVuelto.Text = ""
                End If

            Else
                txtVuelto.Text = ""
            End If
        Else
            txtVuelto.Text = ""
        End If
    End Sub

    Private Sub txtPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPago.KeyPress
        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtPago.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If

            End If
        End If
    End Sub

    Private Sub cboxComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxComprobante.SelectedIndexChanged

        If cboxComprobante.SelectedIndex = 0 Then ' ticket comun

            'var para mandar al afip, por defecto factura B
            cbtetipo = 6
            cbtetipo_desc = "Factura B"
            concepto = 2
            concepto_desc = "Servicio"
            doctipo = 96
            doctipo_desc = "DNI"
            docnro = txtDocumento.Text
            cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
            estado = "No Facturado"

        ElseIf cboxComprobante.SelectedIndex = 1 Then ' factura A

            'var para mandar al afip, por defecto factura B
            cbtetipo = 1
            cbtetipo_desc = "Factura A"
            concepto = 2
            concepto_desc = "Servicio"
            doctipo = 80
            doctipo_desc = "CUIT"
            docnro = txtDocumento.Text
            cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
            estado = "Facturado"

        ElseIf cboxComprobante.SelectedIndex = 2 Then ' factura B

            'var para mandar al afip, por defecto factura B
            cbtetipo = 6
            cbtetipo_desc = "Factura B"
            concepto = 2
            concepto_desc = "Servicio"
            doctipo = 96
            doctipo_desc = "DNI"
            docnro = txtDocumento.Text
            cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
            estado = "Facturado"

        End If

    End Sub

    Private Sub txtDescPorc_TextChanged(sender As Object, e As EventArgs) Handles txtDescPorc.TextChanged

        Try
            ' Calcular Total
            total = 0

            For Each row As DataGridViewRow In dgvResumen.Rows
                total = total + row.Cells("subtotal").Value
            Next

            If txtDescPorc.Text <> Trim("") Then

                If txtDescPorc.Text > 100 Then
                    txtDescPorc.Text = 100
                Else

                    'totalconDesc = total - (txtDescPorc.Text * total) / 100
                    'descuento = (txtDescPorc.Text * total) / 100

                    'importetotal = totalconDesc

                    'Dim mul As Decimal = FormatNumber(1 + (21 / 100), 2)
                    'importeneto = FormatNumber(importetotal / mul, 2)
                    'importeiva = FormatNumber(importetotal - importeneto, 2)

                    'baseimp = importeneto

                    'lblImporteIVA.Text = "Importe IVA: $ " & importeiva
                    'lblImporteNeto.Text = "Importe Neto: $ " & importeneto
                    'lbldescuento.Text = "Descuento: -$" & descuento
                    'lblTotal.Text = "Total: $ " & importetotal



#Region "Refrescar Pagacon y vuelto"
                    If txtPago.Text <> Trim("") Then
                        If txtPago.Text <> 0 Then
                            If txtPago.Text >= totalconDesc Then
                                txtVuelto.Text = Double.Parse(txtPago.Text) - totalconDesc
                            Else
                                txtVuelto.Text = ""
                            End If

                        Else
                            txtVuelto.Text = ""
                        End If
                    Else
                        txtVuelto.Text = ""
                    End If
#End Region

                End If

            Else
                txtDescPorc.Text = 0
                txtDescPorc.SelectAll()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtDescVal_TextChanged(sender As Object, e As EventArgs) Handles txtDescVal.TextChanged

        Try
            'Calcular Total
            total = 0
            For Each row As DataGridViewRow In dgvResumen.Rows
                total = total + row.Cells("subtotal").Value
            Next


            If txtDescVal.Text <= total Then

                'totalconDesc = total - txtDescVal.Text
                'descuento = txtDescVal.Text


                'importetotal = totalconDesc

                '' Ticket común o factura B
                'If cboxComprobante.SelectedIndex = 0 Or cboxComprobante.SelectedIndex = 2 Then
                '    importeneto = totalconDesc
                'Else ' factura A

                '    Dim mul As Decimal = FormatNumber(1 + (21 / 100), 2)
                '    importeneto = FormatNumber(importetotal / mul, 2)
                '    importeiva = FormatNumber(importetotal - importeneto, 2)
                'End If

                'baseimp = importeneto

                'lblImporteIVA.Text = "Importe IVA: $ " & importeiva
                'lblImporteNeto.Text = "Importe Neto: $ " & importeneto
                'lbldescuento.Text = "Descuento: -$" & descuento
                'lblTotal.Text = "Total: $ " & importetotal

#Region "Refrescar Pagacon y vuelto"
                If txtPago.Text <> Trim("") Then
                    If txtPago.Text <> 0 Then
                        If txtPago.Text >= totalconDesc Then
                            txtVuelto.Text = Double.Parse(txtPago.Text) - totalconDesc
                        Else
                            txtVuelto.Text = ""
                        End If

                    Else
                        txtVuelto.Text = ""
                    End If
                Else
                    txtVuelto.Text = ""
                End If
#End Region

            Else
                txtDescVal.Text = total
                txtDescVal.SelectAll()

            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub rbEfectivo_CheckedChanged(sender As Object, e As EventArgs) Handles rbEfectivo.CheckedChanged
        tipopago = "Efectivo"
    End Sub

    Private Sub rbCredDeb_CheckedChanged(sender As Object, e As EventArgs) Handles rbCredDeb.CheckedChanged
        tipopago = "Crédito/Débito"
    End Sub

    Private Sub Cobro_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub bttElegirCliente_Click(sender As Object, e As EventArgs) Handles bttElegirCliente.Click
        Dim f As New ElegirCliente

        Try
            If f.ShowDialog() = DialogResult.OK Then

                txtDocumento.Text = f.dgvClientes.SelectedCells.Item(2).Value.ToString
                nombrecliente = f.dgvClientes.SelectedCells.Item(1).Value.ToString
                telefonocliente = f.dgvClientes.SelectedCells.Item(4).Value.ToString
                direccioncliente = f.dgvClientes.SelectedCells.Item(3).Value.ToString

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
    End Sub
End Class