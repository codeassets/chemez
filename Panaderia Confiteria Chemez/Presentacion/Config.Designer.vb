﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Config
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Config))
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PanelColor = New System.Windows.Forms.Panel()
        Me.bttUsuarios = New System.Windows.Forms.Button()
        Me.ImageListIconos = New System.Windows.Forms.ImageList(Me.components)
        Me.bttTipos = New System.Windows.Forms.Button()
        Me.bttMesas = New System.Windows.Forms.Button()
        Me.bttCajeros = New System.Windows.Forms.Button()
        Me.bttMozos = New System.Windows.Forms.Button()
        Me.PanelALL = New System.Windows.Forms.Panel()
        Me.bttClientes = New System.Windows.Forms.Button()
        Me.bttImpresora = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(25, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(215, 38)
        Me.Label10.TabIndex = 35
        Me.Label10.Text = "Configuración"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PanelColor
        '
        Me.PanelColor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.PanelColor.Location = New System.Drawing.Point(66, 123)
        Me.PanelColor.Name = "PanelColor"
        Me.PanelColor.Size = New System.Drawing.Size(45, 5)
        Me.PanelColor.TabIndex = 41
        '
        'bttUsuarios
        '
        Me.bttUsuarios.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttUsuarios.BackColor = System.Drawing.Color.White
        Me.bttUsuarios.FlatAppearance.BorderSize = 0
        Me.bttUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttUsuarios.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttUsuarios.ForeColor = System.Drawing.Color.Black
        Me.bttUsuarios.ImageIndex = 0
        Me.bttUsuarios.ImageList = Me.ImageListIconos
        Me.bttUsuarios.Location = New System.Drawing.Point(66, 88)
        Me.bttUsuarios.Margin = New System.Windows.Forms.Padding(0)
        Me.bttUsuarios.Name = "bttUsuarios"
        Me.bttUsuarios.Size = New System.Drawing.Size(111, 35)
        Me.bttUsuarios.TabIndex = 0
        Me.bttUsuarios.Text = "Usuarios"
        Me.bttUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttUsuarios.UseVisualStyleBackColor = False
        '
        'ImageListIconos
        '
        Me.ImageListIconos.ImageStream = CType(resources.GetObject("ImageListIconos.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListIconos.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListIconos.Images.SetKeyName(0, "usuarios.png")
        Me.ImageListIconos.Images.SetKeyName(1, "tipos.png")
        Me.ImageListIconos.Images.SetKeyName(2, "mesas.png")
        Me.ImageListIconos.Images.SetKeyName(3, "cajeros.png")
        Me.ImageListIconos.Images.SetKeyName(4, "mozos.png")
        Me.ImageListIconos.Images.SetKeyName(5, "clientes.png")
        Me.ImageListIconos.Images.SetKeyName(6, "impresora.png")
        '
        'bttTipos
        '
        Me.bttTipos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttTipos.BackColor = System.Drawing.Color.White
        Me.bttTipos.FlatAppearance.BorderSize = 0
        Me.bttTipos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttTipos.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttTipos.ForeColor = System.Drawing.Color.Black
        Me.bttTipos.ImageIndex = 1
        Me.bttTipos.ImageList = Me.ImageListIconos
        Me.bttTipos.Location = New System.Drawing.Point(177, 88)
        Me.bttTipos.Margin = New System.Windows.Forms.Padding(0)
        Me.bttTipos.Name = "bttTipos"
        Me.bttTipos.Size = New System.Drawing.Size(82, 35)
        Me.bttTipos.TabIndex = 1
        Me.bttTipos.Text = "Tipos"
        Me.bttTipos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttTipos.UseVisualStyleBackColor = False
        '
        'bttMesas
        '
        Me.bttMesas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttMesas.BackColor = System.Drawing.Color.White
        Me.bttMesas.FlatAppearance.BorderSize = 0
        Me.bttMesas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttMesas.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttMesas.ForeColor = System.Drawing.Color.Black
        Me.bttMesas.ImageIndex = 2
        Me.bttMesas.ImageList = Me.ImageListIconos
        Me.bttMesas.Location = New System.Drawing.Point(259, 88)
        Me.bttMesas.Margin = New System.Windows.Forms.Padding(0)
        Me.bttMesas.Name = "bttMesas"
        Me.bttMesas.Size = New System.Drawing.Size(86, 35)
        Me.bttMesas.TabIndex = 2
        Me.bttMesas.Text = "Mesas"
        Me.bttMesas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttMesas.UseVisualStyleBackColor = False
        '
        'bttCajeros
        '
        Me.bttCajeros.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttCajeros.BackColor = System.Drawing.Color.White
        Me.bttCajeros.FlatAppearance.BorderSize = 0
        Me.bttCajeros.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCajeros.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCajeros.ForeColor = System.Drawing.Color.Black
        Me.bttCajeros.ImageIndex = 3
        Me.bttCajeros.ImageList = Me.ImageListIconos
        Me.bttCajeros.Location = New System.Drawing.Point(345, 88)
        Me.bttCajeros.Margin = New System.Windows.Forms.Padding(0)
        Me.bttCajeros.Name = "bttCajeros"
        Me.bttCajeros.Size = New System.Drawing.Size(99, 35)
        Me.bttCajeros.TabIndex = 3
        Me.bttCajeros.Text = "Cajeros"
        Me.bttCajeros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCajeros.UseVisualStyleBackColor = False
        '
        'bttMozos
        '
        Me.bttMozos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttMozos.BackColor = System.Drawing.Color.White
        Me.bttMozos.FlatAppearance.BorderSize = 0
        Me.bttMozos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttMozos.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttMozos.ForeColor = System.Drawing.Color.Black
        Me.bttMozos.ImageIndex = 4
        Me.bttMozos.ImageList = Me.ImageListIconos
        Me.bttMozos.Location = New System.Drawing.Point(444, 88)
        Me.bttMozos.Margin = New System.Windows.Forms.Padding(0)
        Me.bttMozos.Name = "bttMozos"
        Me.bttMozos.Size = New System.Drawing.Size(87, 35)
        Me.bttMozos.TabIndex = 4
        Me.bttMozos.Text = "Mozos"
        Me.bttMozos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttMozos.UseVisualStyleBackColor = False
        '
        'PanelALL
        '
        Me.PanelALL.Location = New System.Drawing.Point(66, 135)
        Me.PanelALL.Name = "PanelALL"
        Me.PanelALL.Size = New System.Drawing.Size(758, 336)
        Me.PanelALL.TabIndex = 64
        '
        'bttClientes
        '
        Me.bttClientes.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttClientes.BackColor = System.Drawing.Color.White
        Me.bttClientes.FlatAppearance.BorderSize = 0
        Me.bttClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttClientes.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttClientes.ForeColor = System.Drawing.Color.Black
        Me.bttClientes.ImageIndex = 5
        Me.bttClientes.ImageList = Me.ImageListIconos
        Me.bttClientes.Location = New System.Drawing.Point(531, 88)
        Me.bttClientes.Margin = New System.Windows.Forms.Padding(0)
        Me.bttClientes.Name = "bttClientes"
        Me.bttClientes.Size = New System.Drawing.Size(102, 35)
        Me.bttClientes.TabIndex = 5
        Me.bttClientes.Text = "Clientes"
        Me.bttClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttClientes.UseVisualStyleBackColor = False
        '
        'bttImpresora
        '
        Me.bttImpresora.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttImpresora.BackColor = System.Drawing.Color.White
        Me.bttImpresora.FlatAppearance.BorderSize = 0
        Me.bttImpresora.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttImpresora.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttImpresora.ForeColor = System.Drawing.Color.Black
        Me.bttImpresora.ImageIndex = 6
        Me.bttImpresora.ImageList = Me.ImageListIconos
        Me.bttImpresora.Location = New System.Drawing.Point(633, 88)
        Me.bttImpresora.Margin = New System.Windows.Forms.Padding(0)
        Me.bttImpresora.Name = "bttImpresora"
        Me.bttImpresora.Size = New System.Drawing.Size(118, 35)
        Me.bttImpresora.TabIndex = 65
        Me.bttImpresora.Text = "Impresora"
        Me.bttImpresora.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttImpresora.UseVisualStyleBackColor = False
        '
        'Config
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(891, 523)
        Me.Controls.Add(Me.bttImpresora)
        Me.Controls.Add(Me.bttClientes)
        Me.Controls.Add(Me.PanelALL)
        Me.Controls.Add(Me.PanelColor)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.bttMozos)
        Me.Controls.Add(Me.bttCajeros)
        Me.Controls.Add(Me.bttMesas)
        Me.Controls.Add(Me.bttTipos)
        Me.Controls.Add(Me.bttUsuarios)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Config"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.White
        Me.Text = "Configuración"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label10 As Label
    Friend WithEvents PanelColor As Panel
    Friend WithEvents bttUsuarios As Button
    Friend WithEvents bttTipos As Button
    Friend WithEvents bttMesas As Button
    Friend WithEvents bttCajeros As Button
    Friend WithEvents bttMozos As Button
    Friend WithEvents PanelALL As Panel
    Friend WithEvents ImageListIconos As ImageList
    Friend WithEvents bttClientes As Button
    Friend WithEvents bttImpresora As Button
End Class
