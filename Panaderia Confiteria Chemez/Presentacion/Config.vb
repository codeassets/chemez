﻿Public Class Config

    Dim botoninicial As Button

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        botoninicial = bttUsuarios

    End Sub

    Sub New(cliente As Form)

        ' This call is required by the designer.
        InitializeComponent()

        botoninicial = bttClientes

    End Sub

    Private Sub BttUsuarios_Click(sender As Object, e As EventArgs) Handles bttUsuarios.Click
        PanelColor.Location = New Point(66, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigUsuarios With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttTipos_Click(sender As Object, e As EventArgs) Handles bttTipos.Click
        PanelColor.Location = New Point(177, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigTipos With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttMesas_Click(sender As Object, e As EventArgs) Handles bttMesas.Click
        PanelColor.Location = New Point(259, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigMesas With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttCajeros_Click(sender As Object, e As EventArgs) Handles bttCajeros.Click
        PanelColor.Location = New Point(345, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigCajeros With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttMozos_Click(sender As Object, e As EventArgs) Handles bttMozos.Click
        PanelColor.Location = New Point(444, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigMozos With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub bttClientes_Click(sender As Object, e As EventArgs) Handles bttClientes.Click
        PanelColor.Location = New Point(531, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigClientes With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub bttImpresora_Click(sender As Object, e As EventArgs) Handles bttImpresora.Click
        PanelColor.Location = New Point(633, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigImpresora With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub Config_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        botoninicial.PerformClick()

    End Sub

    Private Sub Config_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        ' Cerrar
        If e.KeyCode = Keys.Escape Then
            Me.Dispose()
        End If

    End Sub

End Class