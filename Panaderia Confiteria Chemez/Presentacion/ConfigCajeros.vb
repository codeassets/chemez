﻿Imports Datos

Public Class ConfigCajeros

    Dim opcion As Integer
    Dim cajero As New DatosCajeros

    Private Sub ConfigCajeros_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvCajeros.DataSource = cajero.ObtenerTodos()
        dgvCajeros.ClearSelection()

        Try
            dgvCajeros.Columns("id_cajeros").Visible = False
            dgvCajeros.Columns("nombre").HeaderText = "Nombre"
            dgvCajeros.Columns("contraseña").HeaderText = "Contraseña"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Cajeros. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvCajeros_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCajeros.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtNombre.Text = dgvCajeros.CurrentRow.Cells("nombre").Value
            txtPassword.Text = dgvCajeros.CurrentRow.Cells("contraseña").Value

        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        ' Agregar usuario
        txtNombre.Enabled = True
        txtNombre.Clear()

        txtPassword.Enabled = True
        txtPassword.Clear()

        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvCajeros.Enabled = False

        opcion = 1

    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        txtNombre.Enabled = True
        txtPassword.Enabled = True

        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvCajeros.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                cajero._id_cajeros = dgvCajeros.CurrentRow.Cells("id_cajeros").Value
                cajero.Eliminar()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvCajeros.Enabled = True
                dgvCajeros.DataSource = cajero.ObtenerTodos()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtNombre.Text <> "" And txtPassword.Text <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    cajero._nombre = txtNombre.Text
                    cajero._contraseña = txtPassword.Text
                    cajero.Insertar()
                    MsgBox("Se agregó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvCajeros.Enabled = True
                    dgvCajeros.DataSource = cajero.ObtenerTodos()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    cajero._nombre = txtNombre.Text
                    cajero._contraseña = txtPassword.Text
                    cajero._id_cajeros = dgvCajeros.SelectedRows(0).Cells("id_cajeros").Value
                    cajero.Modificar()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvCajeros.Enabled = True
                    dgvCajeros.DataSource = cajero.ObtenerTodos()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvCajeros.Enabled = True
        dgvCajeros.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtNombre.Enabled = False
        txtNombre.Clear()
        txtPassword.Enabled = False
        txtPassword.Clear()

    End Sub
End Class