﻿Imports Datos

Public Class ConfigClientes

    Dim opcion As Integer

    Dim cliente As New Datos.DatosClientes

    Public Function ValidarCUIT(cuit As String) As Integer

        Dim t As Integer, S As Integer, Verif As Integer

        S = 0
        S = Val(Mid(cuit, 1, 1)) * 5
        S = S + Val(Mid(cuit, 2, 1)) * 4
        S = S + Val(Mid(cuit, 3, 1)) * 3
        S = S + Val(Mid(cuit, 4, 1)) * 2
        S = S + Val(Mid(cuit, 5, 1)) * 7
        S = S + Val(Mid(cuit, 6, 1)) * 6
        S = S + Val(Mid(cuit, 7, 1)) * 5
        S = S + Val(Mid(cuit, 8, 1)) * 4
        S = S + Val(Mid(cuit, 9, 1)) * 3
        S = S + Val(Mid(cuit, 10, 1)) * 2
        t = S Mod 11
        t = 11 - t

        If t = 11 Then
            Verif = 0
        ElseIf t = 10 Then
            Verif = 9
        Else
            Verif = t
        End If

        ValidarCUIT = Verif

    End Function


    Private Sub ConfigClientes_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvClientes.DataSource = cliente.ObtenerTodo()
        dgvClientes.ClearSelection()

        Try
            dgvClientes.Columns("id_cliente").Visible = False
            'dgvClientes.Columns("nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvClientes.Columns("CUIL").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvClientes.Columns("direccion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvClientes.Columns("telefono").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvClientes.Columns("nombre").HeaderText = "Nombre"
            dgvClientes.Columns("direccion").HeaderText = "Dirección"
            dgvClientes.Columns("telefono").HeaderText = "Teléfono"
        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Clientes. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvClientes_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvClientes.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtNombre.Text = dgvClientes.CurrentRow.Cells("nombre").Value
            txtCUIT.Text = dgvClientes.CurrentRow.Cells("CUIT").Value
            txtDireccion.Text = dgvClientes.CurrentRow.Cells("direccion").Value
            txtTelefono.Text = dgvClientes.CurrentRow.Cells("telefono").Value
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        ' Agregar
        txtNombre.Enabled = True
        txtNombre.Clear()

        txtCUIT.Enabled = True
        txtCUIT.Clear()

        txtDireccion.Enabled = True
        txtDireccion.Clear()

        txtTelefono.Enabled = True
        txtTelefono.Clear()

        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvClientes.Enabled = False

        opcion = 1

    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        txtNombre.Enabled = True
        txtCUIT.Enabled = True
        txtDireccion.Enabled = True
        txtTelefono.Enabled = True

        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvClientes.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este cliente?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                cliente._id_cliente = dgvClientes.CurrentRow.Cells("id_cliente").Value
                cliente.Eliminar()
                MsgBox("Se eliminó el cliente con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvClientes.Enabled = True
                dgvClientes.DataSource = cliente.ObtenerTodo()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el cliente. Detalle:" & ex.Message.ToString)
            End Try

        End If

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtNombre.Text <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    cliente._nombre = Trim(txtNombre.Text)
                    cliente._cuit = Trim(txtCUIT.Text)
                    cliente._direccion = Trim(txtDireccion.Text)
                    cliente._telefono = Trim(txtTelefono.Text)

                    cliente.Insertar()

                    MsgBox("Se agregó el cliente con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvClientes.Enabled = True
                    dgvClientes.DataSource = cliente.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el cliente. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    cliente._nombre = Trim(txtNombre.Text)
                    cliente._cuit = Trim(txtCUIT.Text)
                    cliente._direccion = Trim(txtDireccion.Text)
                    cliente._telefono = Trim(txtTelefono.Text)
                    cliente._id_cliente = dgvClientes.SelectedRows(0).Cells("id_cliente").Value

                    cliente.Modificar()

                    MsgBox("Se modificó el cliente con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvClientes.Enabled = True
                    dgvClientes.DataSource = cliente.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el cliente. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Ingrese el nombre del cliente.")
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvClientes.Enabled = True
        dgvClientes.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtNombre.Enabled = False
        txtNombre.Clear()
        txtCUIT.Enabled = False
        txtCUIT.Clear()
        txtDireccion.Enabled = False
        txtDireccion.Clear()
        txtTelefono.Enabled = False
        txtTelefono.Clear()

    End Sub

    Private Sub txtTelefono_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTelefono.KeyPress
        ' Verifica que sólo se ingresen números
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

End Class