﻿Public Class ConfigImpresora

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click
        My.Settings.Printer = txtImpresora.Text
        My.Settings.Save()
        MessageBox.Show("Impresora configurada correctamente.")
    End Sub

    Private Sub ConfigImpresora_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        My.Settings.Printer = txtImpresora.Text
        My.Settings.Save()
    End Sub

    Private Sub ConfigImpresora_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        txtImpresora.Text = My.Settings.Printer
    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttSeleccionar.Click
        If PrintDialog1.ShowDialog = DialogResult.Cancel Then
            Exit Sub
        End If

        txtImpresora.Text = PrintDialog1.PrinterSettings.PrinterName
    End Sub
End Class