﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigMesas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvMesas = New System.Windows.Forms.DataGridView()
        Me.bttCancelar = New System.Windows.Forms.Button()
        Me.bttGuardar = New System.Windows.Forms.Button()
        Me.txtNroMesa = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bttEliminar = New System.Windows.Forms.Button()
        Me.bttModificar = New System.Windows.Forms.Button()
        Me.bttAgregar = New System.Windows.Forms.Button()
        CType(Me.dgvMesas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvMesas
        '
        Me.dgvMesas.AllowUserToAddRows = False
        Me.dgvMesas.AllowUserToDeleteRows = False
        Me.dgvMesas.AllowUserToResizeRows = False
        Me.dgvMesas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMesas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvMesas.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvMesas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvMesas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvMesas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMesas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMesas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMesas.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvMesas.Location = New System.Drawing.Point(18, 55)
        Me.dgvMesas.MultiSelect = False
        Me.dgvMesas.Name = "dgvMesas"
        Me.dgvMesas.ReadOnly = True
        Me.dgvMesas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMesas.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvMesas.RowHeadersVisible = False
        Me.dgvMesas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMesas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMesas.ShowEditingIcon = False
        Me.dgvMesas.Size = New System.Drawing.Size(690, 363)
        Me.dgvMesas.TabIndex = 83
        '
        'bttCancelar
        '
        Me.bttCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttCancelar.FlatAppearance.BorderSize = 0
        Me.bttCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCancelar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCancelar.ForeColor = System.Drawing.Color.Black
        Me.bttCancelar.Location = New System.Drawing.Point(725, 378)
        Me.bttCancelar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttCancelar.Name = "bttCancelar"
        Me.bttCancelar.Size = New System.Drawing.Size(121, 40)
        Me.bttCancelar.TabIndex = 5
        Me.bttCancelar.Text = "Cancelar"
        Me.bttCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCancelar.UseVisualStyleBackColor = False
        '
        'bttGuardar
        '
        Me.bttGuardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.bttGuardar.Enabled = False
        Me.bttGuardar.FlatAppearance.BorderSize = 0
        Me.bttGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttGuardar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttGuardar.ForeColor = System.Drawing.Color.Black
        Me.bttGuardar.Location = New System.Drawing.Point(725, 329)
        Me.bttGuardar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttGuardar.Name = "bttGuardar"
        Me.bttGuardar.Size = New System.Drawing.Size(121, 40)
        Me.bttGuardar.TabIndex = 4
        Me.bttGuardar.Text = "Guardar"
        Me.bttGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttGuardar.UseVisualStyleBackColor = False
        '
        'txtNroMesa
        '
        Me.txtNroMesa.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtNroMesa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNroMesa.Enabled = False
        Me.txtNroMesa.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroMesa.ForeColor = System.Drawing.Color.Black
        Me.txtNroMesa.Location = New System.Drawing.Point(153, 16)
        Me.txtNroMesa.Margin = New System.Windows.Forms.Padding(0)
        Me.txtNroMesa.Name = "txtNroMesa"
        Me.txtNroMesa.Size = New System.Drawing.Size(171, 20)
        Me.txtNroMesa.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 20)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Número de mesa"
        '
        'bttEliminar
        '
        Me.bttEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttEliminar.Enabled = False
        Me.bttEliminar.FlatAppearance.BorderSize = 0
        Me.bttEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttEliminar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttEliminar.ForeColor = System.Drawing.Color.Black
        Me.bttEliminar.Image = Global.Presentacion.My.Resources.Resources.remove
        Me.bttEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttEliminar.Location = New System.Drawing.Point(725, 155)
        Me.bttEliminar.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.bttEliminar.Name = "bttEliminar"
        Me.bttEliminar.Size = New System.Drawing.Size(121, 40)
        Me.bttEliminar.TabIndex = 3
        Me.bttEliminar.Text = "Eliminar"
        Me.bttEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttEliminar.UseVisualStyleBackColor = False
        '
        'bttModificar
        '
        Me.bttModificar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttModificar.Enabled = False
        Me.bttModificar.FlatAppearance.BorderSize = 0
        Me.bttModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttModificar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttModificar.ForeColor = System.Drawing.Color.Black
        Me.bttModificar.Image = Global.Presentacion.My.Resources.Resources.editar1
        Me.bttModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttModificar.Location = New System.Drawing.Point(725, 105)
        Me.bttModificar.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.bttModificar.Name = "bttModificar"
        Me.bttModificar.Size = New System.Drawing.Size(121, 40)
        Me.bttModificar.TabIndex = 2
        Me.bttModificar.Text = "Modificar"
        Me.bttModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttModificar.UseVisualStyleBackColor = False
        '
        'bttAgregar
        '
        Me.bttAgregar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttAgregar.FlatAppearance.BorderSize = 0
        Me.bttAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAgregar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAgregar.ForeColor = System.Drawing.Color.Black
        Me.bttAgregar.Image = Global.Presentacion.My.Resources.Resources.plus
        Me.bttAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttAgregar.Location = New System.Drawing.Point(725, 55)
        Me.bttAgregar.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.bttAgregar.Name = "bttAgregar"
        Me.bttAgregar.Size = New System.Drawing.Size(121, 40)
        Me.bttAgregar.TabIndex = 1
        Me.bttAgregar.Text = "Agregar"
        Me.bttAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttAgregar.UseVisualStyleBackColor = False
        '
        'ConfigMesas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(860, 432)
        Me.Controls.Add(Me.dgvMesas)
        Me.Controls.Add(Me.bttCancelar)
        Me.Controls.Add(Me.bttGuardar)
        Me.Controls.Add(Me.bttEliminar)
        Me.Controls.Add(Me.bttModificar)
        Me.Controls.Add(Me.bttAgregar)
        Me.Controls.Add(Me.txtNroMesa)
        Me.Controls.Add(Me.Label2)
        Me.Name = "ConfigMesas"
        Me.Text = "ConfigMesas"
        CType(Me.dgvMesas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvMesas As DataGridView
    Friend WithEvents bttCancelar As Button
    Friend WithEvents bttGuardar As Button
    Friend WithEvents bttEliminar As Button
    Friend WithEvents bttModificar As Button
    Friend WithEvents bttAgregar As Button
    Friend WithEvents txtNroMesa As TextBox
    Friend WithEvents Label2 As Label
End Class
