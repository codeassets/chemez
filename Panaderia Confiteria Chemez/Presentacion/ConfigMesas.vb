﻿Imports Datos

Public Class ConfigMesas

    Dim opcion As Integer
    Dim mesas As New DatosMesas

    Private Sub ConfigMesas_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvMesas.DataSource = mesas.ObtenerTodas()
        dgvMesas.ClearSelection()

        Try
            dgvMesas.Columns("id_mesa").Visible = False
            dgvMesas.Columns("id_mozo").Visible = False
            dgvMesas.Columns("nro_mesa").HeaderText = "N° de Mesa"
            dgvMesas.Columns("estado").HeaderText = "Estado"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Tipos. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvMesas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMesas.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtNroMesa.Text = dgvMesas.CurrentRow.Cells("nro_mesa").Value

        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        ' Agregar
        txtNroMesa.Enabled = True
        txtNroMesa.Clear()

        txtNroMesa.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvMesas.Enabled = False

        opcion = 1
    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        txtNroMesa.Enabled = True

        txtNroMesa.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvMesas.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                mesas._id_mesa = dgvMesas.CurrentRow.Cells("id_mesa").Value
                mesas.Eliminar()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvMesas.Enabled = True
                dgvMesas.DataSource = mesas.ObtenerTodas()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtNroMesa.Text <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    mesas._nro_mesa = txtNroMesa.Text
                    mesas._estado = "Libre"
                    mesas._id_mozo = Nothing
                    mesas.Insertar()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvMesas.Enabled = True
                    dgvMesas.DataSource = mesas.ObtenerTodas()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try
                    If dgvMesas.SelectedRows(0).Cells("estado").Value = "Libre" Then
                        mesas._nro_mesa = txtNroMesa.Text
                        mesas._estado = "Libre"
                        mesas._id_mozo = dgvMesas.SelectedRows(0).Cells("id_mozo").Value
                        mesas._id_mesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value
                        mesas.Modificar()
                        MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                        dgvMesas.Enabled = True
                        dgvMesas.DataSource = mesas.ObtenerTodas()
                    Else
                        MessageBox.Show("Por favor libere la mesa antes de realizar la modificacion.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If


                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvMesas.Enabled = True
        dgvMesas.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtNroMesa.Enabled = False
        txtNroMesa.Clear()

    End Sub
End Class