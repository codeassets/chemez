﻿Imports Datos

Public Class ConfigMozos

    Dim opcion As Integer

    Dim mozos As New Datos.DatosMozos

    Private Sub ConfigMozos_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvMozos.DataSource = mozos.ObtenerTodos()
        dgvMozos.ClearSelection()

        Try
            dgvMozos.Columns("id_mozo").Visible = False
            dgvMozos.Columns("nombre").HeaderText = "Nombre"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Mozos. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvMozos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMozos.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtNombre.Text = dgvMozos.CurrentRow.Cells("nombre").Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        ' Agregar
        txtNombre.Enabled = True
        txtNombre.Clear()
        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvMozos.Enabled = False

        opcion = 1

    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        txtNombre.Enabled = True
        txtNombre.Enabled = True
        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvMozos.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                mozos._id_mozo = dgvMozos.CurrentRow.Cells("id_mozo").Value
                mozos.Eliminar()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvMozos.Enabled = True
                dgvMozos.DataSource = mozos.ObtenerTodos()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtNombre.Text <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    mozos._nombre = txtNombre.Text
                    mozos.Insertar()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvMozos.Enabled = True
                    dgvMozos.DataSource = mozos.ObtenerTodos()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    mozos._nombre = txtNombre.Text
                    mozos._id_mozo = dgvMozos.SelectedRows(0).Cells("id_mozo").Value
                    mozos.Modificar()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvMozos.Enabled = True
                    dgvMozos.DataSource = mozos.ObtenerTodos

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvMozos.Enabled = True
        dgvMozos.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtNombre.Enabled = False
        txtNombre.Clear()

    End Sub

End Class