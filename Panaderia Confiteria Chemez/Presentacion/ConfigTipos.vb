﻿Imports Datos

Public Class ConfigTipos

    Dim opcion As Integer

    Dim tipos As New Datos.DatosTipos

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        ' Agregar
        txtTipo.Enabled = True
        txtTipo.Clear()

        cboxStock.Enabled = True
        cboxStock.Checked = False

        txtTipo.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvTipos.Enabled = False

        opcion = 1
    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click
        txtTipo.Enabled = True
        cboxStock.Enabled = True

        txtTipo.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvTipos.Enabled = False

        opcion = 2
    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                tipos._id_tipo = dgvTipos.CurrentRow.Cells("id_tipo").Value
                tipos.Eliminar()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvTipos.Enabled = True
                dgvTipos.DataSource = tipos.ObtenerTodo()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtTipo.Text <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    tipos._descripcion = txtTipo.Text
                    If cboxStock.Checked = True Then
                        tipos._stock = "1"
                    Else
                        tipos._stock = "0"
                    End If
                    tipos.Insertar()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvTipos.Enabled = True
                    dgvTipos.DataSource = tipos.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    tipos._descripcion = txtTipo.Text
                    If cboxStock.Checked = True Then
                        tipos._stock = "1"
                    Else
                        tipos._stock = "0"
                    End If
                    tipos._id_tipo = dgvTipos.SelectedRows(0).Cells("id_tipo").Value
                    tipos.Modificar()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvTipos.Enabled = True
                    dgvTipos.DataSource = tipos.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        dgvTipos.Enabled = True
        dgvTipos.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtTipo.Enabled = False
        txtTipo.Clear()
        cboxStock.Enabled = False
        cboxStock.Checked = False
    End Sub

    Private Sub dgvTipos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTipos.CellClick
        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtTipo.Text = dgvTipos.CurrentRow.Cells("descripcion").Value
            If dgvTipos.CurrentRow.Cells("Mantiene stock").Value = "Sí" Then
                cboxStock.Checked = True
            Else
                cboxStock.Checked = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConfigTipos_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvTipos.DataSource = tipos.ObtenerTodo()
        dgvTipos.ClearSelection()

        Try
            dgvTipos.Columns("id_tipo").Visible = False
            dgvTipos.Columns("descripcion").HeaderText = "Tipo"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Tipos. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

End Class