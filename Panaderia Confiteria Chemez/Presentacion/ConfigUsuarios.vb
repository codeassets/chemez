﻿Public Class ConfigUsuarios

    Dim opcion As Integer

    Dim mesa As New Datos.DatosMesas
    Dim mozo As New Datos.DatosMozos
    Dim usuario As New Datos.DatosUsuarios

    Dim IdColumnaSeleccionada As Integer
    Private Sub BttAgregarUsuario_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        ' Agregar usuario
        txtNombre.Enabled = True
        txtNombre.Clear()

        txtPassword.Enabled = True
        txtPassword.Clear()

        cboxAdmin.Enabled = True
        cboxAdmin.Checked = False

        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvUsuarios.Enabled = False

        opcion = 1
    End Sub

    Private Sub BttModificarUsuario_Click(sender As Object, e As EventArgs) Handles bttModificar.Click
        txtNombre.Enabled = True
        txtPassword.Enabled = True
        cboxAdmin.Enabled = True

        txtNombre.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvUsuarios.Enabled = False

        opcion = 2
    End Sub

    Private Sub BttEliminarUsuario_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este usuario?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                usuario._id_usuario = dgvUsuarios.CurrentRow.Cells("id_usuario").Value
                usuario.EliminarUsuario()
                MsgBox("Se eliminó el usuario con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvUsuarios.Enabled = True
                dgvUsuarios.DataSource = usuario.ObtenerTodo()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el usuario. Detalle:" & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub BttGuardarUsuario_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click
        If txtNombre.Text <> "" And txtPassword.Text <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    usuario._nombre = txtNombre.Text
                    usuario._contraseña = txtPassword.Text
                    If cboxAdmin.Checked = True Then
                        usuario._tipo = "Administrador"
                    Else
                        usuario._tipo = "Común"
                    End If
                    usuario.InsertarUsuario()
                    MsgBox("Se agregó el usuario con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvUsuarios.Enabled = True
                    dgvUsuarios.DataSource = usuario.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el usuario. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    usuario._nombre = txtNombre.Text
                    usuario._contraseña = txtPassword.Text
                    If cboxAdmin.Checked = True Then
                        usuario._tipo = "Administrador"
                    Else
                        usuario._tipo = "Común"
                    End If
                    usuario._id_usuario = dgvUsuarios.SelectedRows(0).Cells("id_usuario").Value
                    usuario.ModificarUsuario()
                    MsgBox("Se modificó el usuario con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvUsuarios.Enabled = True
                    dgvUsuarios.DataSource = usuario.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el usuario. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If
    End Sub

    Private Sub BttCancelarUsuario_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        dgvUsuarios.Enabled = True
        dgvUsuarios.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtNombre.Enabled = False
        txtNombre.Clear()
        txtPassword.Enabled = False
        txtPassword.Clear()
        cboxAdmin.Enabled = False
        cboxAdmin.Checked = False
    End Sub

    Private Sub DgvUsuarios_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvUsuarios.CellClick
        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtNombre.Text = dgvUsuarios.CurrentRow.Cells("Nombre").Value
            txtPassword.Text = dgvUsuarios.CurrentRow.Cells("Contraseña").Value
            If dgvUsuarios.CurrentRow.Cells("Tipo").Value = "Administrador" Then
                cboxAdmin.Checked = True
            Else
                cboxAdmin.Checked = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConfigUsuarios_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvUsuarios.DataSource = usuario.ObtenerTodo()
        dgvUsuarios.ClearSelection()

        Try
            dgvUsuarios.Columns("id_usuario").Visible = False
            'dgvUsuarios.Columns("nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvUsuarios.Columns("contraseña").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvUsuarios.Columns("tipo").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvUsuarios.Columns("nombre").HeaderText = "Nombre"
            dgvUsuarios.Columns("contraseña").HeaderText = "Contraseña"
            dgvUsuarios.Columns("tipo").HeaderText = "Tipo"
        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al cargar la lista de Usuarios. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

    Private Sub ConfigUsuarios_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        ' cerrar
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class