﻿Imports Datos

Public Class DetallesFacturas

    Dim IdFactura As Long

    Sub New(_id As Long)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        IdFactura = _id

    End Sub

    Private Sub DetallesFacturas_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Dim detalle As New DatosFacturas

        detalle._IdFactura = IdFactura
        dgvDetalles.DataSource = detalle.ObtenerDetalles()

        If dgvDetalles.RowCount <> 0 Then

            dgvDetalles.Columns("IdDetalle").Visible = False
            dgvDetalles.Columns("IdProducto").Visible = False
            dgvDetalles.Columns("IdFactura").Visible = False
            dgvDetalles.Columns("Total").Visible = False
            dgvDetalles.Columns("Descripcion").HeaderText = "Producto"
            dgvDetalles.Columns("PrecioUnitario").HeaderText = "Precio Unit."

            lblTotal.Text = "Total: $ " & dgvDetalles.Rows(0).Cells("Total").Value
        End If

    End Sub

End Class