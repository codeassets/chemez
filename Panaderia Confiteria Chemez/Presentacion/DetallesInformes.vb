﻿Imports Datos
Public Class DetallesInformes
    Dim id As Long
    Dim detalle_de As String
    Sub New(_id As Long, _detalle_de As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        id = _id
        detalle_de = _detalle_de

    End Sub

    Private Sub DetallesInformes_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Try

            If detalle_de = "Panaderia" Or detalle_de = "VentaMayorista" Then
                Dim pan As New DatosVentas
                pan._id_venta = id
                dgvVentas.DataSource = pan.BuscarDetalleVenta()

                'tamaño de las columnas
                dgvVentas.Columns("descripcion").FillWeight = 40
                dgvVentas.Columns("cantidad").FillWeight = 20
                dgvVentas.Columns("precio_unitario").FillWeight = 20
                dgvVentas.Columns("subtotal").FillWeight = 20

                'columnas visibles
                dgvVentas.Columns("id_detalle").Visible = False
                dgvVentas.Columns("id_venta").Visible = False
                dgvVentas.Columns("total").Visible = False
                dgvVentas.Columns("descuento").Visible = False
                dgvVentas.Columns("venta_por").Visible = False

                'nombre de columnas
                dgvVentas.Columns("descripcion").HeaderText = "Producto"
                dgvVentas.Columns("cantidad").HeaderText = "Cantidad"
                dgvVentas.Columns("precio_unitario").HeaderText = "Precio"
                dgvVentas.Columns("subtotal").HeaderText = "Subtotal"
                'dgvVentas.Columns("venta_por").HeaderText = "Venta por"
                'dgvVentas.Columns("total").HeaderText = "Total"
                'dgvVentas.Columns("descuento").HeaderText = "Descuento"
            ElseIf detalle_de = "Cafeteria" Then
                Dim cafe As New DatosPedidos
                cafe._id_pedido = id
                dgvVentas.DataSource = cafe.ObtenerDetallePedidosPor_id_pedido()
                'tamaño de las columnas
                dgvVentas.Columns("descripcion").FillWeight = 40
                dgvVentas.Columns("cantidad").FillWeight = 20
                dgvVentas.Columns("precio_unitario").FillWeight = 20
                dgvVentas.Columns("subtotal").FillWeight = 20

                'columnas visibles
                dgvVentas.Columns("id_detalle").Visible = False
                dgvVentas.Columns("id_pedido").Visible = False
                dgvVentas.Columns("total").Visible = False
                dgvVentas.Columns("descuento").Visible = False

                'nombre de las columnas
                dgvVentas.Columns("descripcion").HeaderText = "Producto"
                dgvVentas.Columns("cantidad").HeaderText = "Cantidad"
                dgvVentas.Columns("precio_unitario").HeaderText = "Precio"
                dgvVentas.Columns("subtotal").HeaderText = "Subtotal"
                'dgvVentas.Columns("total").HeaderText = "Total"
                'dgvVentas.Columns("descuento").HeaderText = "Descuento"
            End If
            lblTotal.Text = "Total: $ " & dgvVentas.SelectedRows(0).Cells("total").Value
            lblDescuento.Text = "Descuento: $ " & dgvVentas.SelectedRows(0).Cells("descuento").Value
        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al mostrar el detalle de " & detalle_de & ". Detalle: " & ex.Message.ToString)
        End Try
    End Sub


End Class