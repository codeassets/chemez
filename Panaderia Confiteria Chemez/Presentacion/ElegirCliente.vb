﻿Imports Datos

Public Class ElegirCliente

    Public b As Int64

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Dim cli As New DatosClientes
                Dim dt As New DataTable
                dgvClientes.DataSource = cli.Buscar(dt, txtBuscar.Text)

                dgvClientes.Columns("id_cliente").Visible = False

                dgvClientes.Columns("nombre").HeaderText = "Nombre"
                dgvClientes.Columns("direccion").HeaderText = "Dirección"
                dgvClientes.Columns("telefono").HeaderText = "Teléfono"

            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al buscar el cliente. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttSeleccionar_Click(sender As Object, e As EventArgs) Handles bttSeleccionar.Click
        Try
            If b = 0 Then

                Me.DialogResult = DialogResult.OK

                'NuevaVenta.bttElegirCliente.Text = DataGridView1.SelectedCells.Item(3).Value.ToString + ", " + DataGridView1.SelectedCells.Item(2).Value.ToString

                'NuevaVenta.idcliente = DataGridView1.SelectedCells.Item(0).Value.ToString

                'NuevaVenta.txtCodVenta.Focus()

            ElseIf b = 1 Then
                'Ventas.txtDNI.Text = DataGridView1.SelectedCells.Item(1).Value.ToString
            ElseIf b = 2 Then

            End If

            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al seleccionar el cliente. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

End Class