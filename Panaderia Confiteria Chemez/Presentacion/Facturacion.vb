﻿Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports Presentacion.WSFE.HOMO
Imports QRCoder
Imports Datos
Imports System.Net
Imports log4net

Public Class Facturacion

    ' Log de errores
    Private Shared _logger As ILog = Nothing

    Private Shared ReadOnly Property Logger As log4net.ILog
        Get

            If _logger Is Nothing Then
                _logger = LogManager.GetLogger("file")
                log4net.Config.XmlConfigurator.Configure()
            End If

            Return _logger
        End Get
    End Property

    ' para el constructor
    Dim fila As New DataGridViewRow

    'pictureboxQR
    Dim pb_codQR As New PictureBox

    ' Otras
    Dim nombrecliente As String = "1"
    Dim telefonocliente As String = "1"
    Dim direccioncliente As String = "1"

    ' para mandar  al webservice
    Dim cbtetipo As Integer  ' tipo de comprobante
    Dim cbtetipo_desc As String
    Dim concepto As Integer = 1 ' concepto: 1 prod 2 serv 3 prod y serv
    Dim concepto_desc As String = "Producto"
    Dim doctipo As Integer  ' tipo de documento
    Dim doctipo_desc As String
    Dim docnro As Int64 ' numero de documento
    Dim CbteDesde As Integer
    Dim CbteHasta As Integer
    Dim cbtefch As String ' fecha de comprobante: yyyymmdd

    Dim importeneto As Double ' importe neto no gravado TOTAL
    Dim importeneto21 As Double ' importe neto 21%
    Dim importeneto10 As Double ' importe neto 10.5%
    Dim importetotal As Double ' importe total del comprobante

    Dim importeiva21 As Double = 0 ' suma de importes de IVA 21
    Dim importeiva10 As Double = 0 ' suma de importes de IVA 10.5
    Dim iva As String = "0" ' 

    Dim imptotalconc As Double ' importe neto no gravado
    Dim impopex As Double ' importe exento. Debe ser menor o igual al importe total y no debe ser menor a 0
    Dim imptrib As Double ' suma de los importes del array de tributos

    Dim idiva21 As Integer ' codigo de tipo iva 21
    Dim idiva10 As Integer ' codigo de tipo iva 10
    Dim baseimp21 As Double = 0 ' base imponible para la determinación de la alicuota 21
    Dim baseimp10 As Double = 0 ' base imponible para la determinación de la alicuota 10.5

    Dim estado As String

    ' para AFIP
    Private l As LoginAFIP
    Property Login As LoginAFIP
    Property TiposComprobantes As CbteTipoResponse
    Property TipoConceptos As ConceptoTipoResponse
    Property TipoDoc As DocTipoResponse
    Property Monedas As MonedaResponse
    Property puntosventa As FEPtoVentaResponse
    Property TiposIVA As IvaTipoResponse
    Property opcionales As OpcionalTipoResponse
    Property authRequest As FEAuthRequest


    Private Function getServicio() As Service
        Dim s As New Service
        s.Url = My.Settings.url_service
        Return s
    End Function

    Public Function CalcChecksum(S As String) As Byte
        Dim chk As Byte ' Final Checksum
        Dim T As Long   ' Char-pointer...

        ' Step 1
        chk = Asc(Mid(S, 1, 2))    ' Get first Char value

        ' Initialize counter
        T = 1
        While T < Len(S)

            ' Step 2
            chk = chk And 127   ' Bitwise operation. Clear bit 7 (127 -> 0111 1111)

            ' Step 3
            chk = chk Or 64     ' Bitwise operation. Set bit 6 (64 -> 01000000)

            ' Increase counter...
            T = T + 1

            ' Step 4
            chk = Asc(Mid(S, T, 1)) + chk
        End While

        CalcChecksum = chk
    End Function

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Sub New(_fila As DataGridViewRow)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        fila = _fila


    End Sub

    Private Sub Facturacion_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        cboxComprobante.SelectedIndex = cboxComprobante.FindStringExact(fila.Cells("CbteTipo_desc").Value)

        dtpFecha.Value = fila.Cells("CbteFch").Value
        txtDocumento.Text = fila.Cells("DocNro").Value

        ' Cargar datos cliente
        Dim cliente As New DatosClientes
        Dim dt As New DataTable
        cliente._cuit = fila.Cells("DocNro").Value
        dt = cliente.ObtenerConCUIT()
        If dt.Rows.Count <> 0 AndAlso dt.Rows(0)("CUIT") <> "" Then
            nombrecliente = dt.Rows(0)("nombre")
            direccioncliente = dt.Rows(0)("direccion")
        End If

        ' cargar los detalles en la tabla resumen
        Dim detalle As New DatosFacturas
        detalle._IdFactura = fila.Cells("IdFactura").Value
        dgvResumen.DataSource = detalle.ObtenerDetalles

        AjustarTabla()

        ' completar totales
        importeneto = fila.Cells("ImpNeto").Value


        For Each row As DataGridViewRow In dgvResumen.Rows
            importetotal = importetotal + row.Cells("subtotal").Value
        Next

        'importetotal = fila.Cells("ImpTotal").Value

        'var para mandar al afip, por defecto factura B
        cbtetipo = fila.Cells("CbteTipo").Value
        cbtetipo_desc = fila.Cells("CbteTipo_desc").Value
        concepto = fila.Cells("Concepto").Value
        concepto_desc = fila.Cells("Concepto_desc").Value
        doctipo = fila.Cells("DocTipo").Value
        doctipo_desc = fila.Cells("DocTipo_desc").Value
        docnro = fila.Cells("DocNro").Value
        cbtefch = fila.Cells("CbteFch").Value
        imptotalconc = 0
        impopex = 0
        imptrib = 0

        ' Comprobar cuantos tipos de iva distintos hay en la factura
        If fila.Cells("AlicIva_Importe105").Value <> 0 Then

            'iva 10.5
            idiva10 = 4
            baseimp10 = fila.Cells("AlicIva_BaseImp105").Value
            importeiva10 = fila.Cells("AlicIva_Importe105").Value

        End If

        If fila.Cells("AlicIva_Importe21").Value <> 0 Then

            'iva 21
            idiva21 = 5
            baseimp21 = fila.Cells("AlicIva_BaseImp21").Value
            importeiva21 = fila.Cells("AlicIva_Importe21").Value

        End If

        estado = "Facturado"

        lblImporteIVA10.Text = "Importe IVA 10,5%: $" & fila.Cells("AlicIva_Importe105").Value
        lblImporteIVA21.Text = "Importe IVA 21%: $" & fila.Cells("AlicIva_Importe21").Value

        lblImporteNeto.Text = "Importe Neto: $ " & fila.Cells("ImpNeto").Value
        lblTotal.Text = "Total: $ " & importetotal

        ' Deshabilitar fechas posteriores a hoy 
        dtpFecha.MaxDate = Date.Today

    End Sub

    Public Sub AjustarTabla()

        dgvResumen.Columns("IdFactura").Visible = False
        dgvResumen.Columns("IdDetalle").Visible = False
        dgvResumen.Columns("IdProducto").Visible = False
        dgvResumen.Columns("Total").Visible = False
        dgvResumen.Columns("Importe_IVA").Visible = False

        dgvResumen.Columns("PrecioUnitario").HeaderText = "Precio Unit."
        dgvResumen.Columns("Descripcion").HeaderText = "Prod."
        dgvResumen.Columns("Cantidad").HeaderText = "Cant."
        dgvResumen.Columns("Subtotal").HeaderText = "Subt."


    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub txtPago_TextChanged(sender As Object, e As EventArgs) Handles txtPago.TextChanged
        If txtPago.Text = "" OrElse txtPago.Text = 0 Then
            txtVuelto.Text = 0
            txtPago.Text = 0
            txtPago.SelectAll()
        Else
            txtVuelto.Text = txtPago.Text - importetotal
        End If
    End Sub

    Private Sub txtPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPago.KeyPress
        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtPago.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Sub txtPago_Click(sender As Object, e As EventArgs) Handles txtPago.Click
        txtPago.SelectAll()
    End Sub

    Private Sub txtDocumento_TextChanged(sender As Object, e As EventArgs) Handles txtDocumento.TextChanged
        If txtDocumento.Text = "" Then
            txtDocumento.Text = "99999999"
        End If
    End Sub

    Private Sub bttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click

        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12

        Try
            Using client = New WebClient()
                Using stream = client.OpenRead("http://www.google.com")
                    'verificar login
                    l = New LoginAFIP(My.Settings.def_serv, My.Settings.def_url, My.Settings.def_cert, My.Settings.def_pass)
                    l.hacerLogin()
                End Using
            End Using
        Catch ex As Exception
            MsgBox("Verifique su conexión a internet. Detalle: " + ex.Message.ToString)
            Return
        End Try

        Try

            Dim f As New DatosFacturas

#Region "Conexion a AFIP y obtencion del CAE"
            authRequest = New FEAuthRequest()
            authRequest.Cuit = My.Settings.def_cuit
            authRequest.Sign = Login.Sign
            authRequest.Token = Login.Token

            Dim service As WSFE.HOMO.Service = getServicio()
            service.ClientCertificates.Add(Login.certificado)


            Dim req As New FECAERequest
            Dim cab As New FECAECabRequest
            Dim det As New FECAEDetRequest

            cab.CantReg = 1
            cab.PtoVta = fila.Cells("PtoVta").Value
            cab.CbteTipo = cbtetipo
            req.FeCabReq = cab

            With det

                .Concepto = concepto
                .DocTipo = doctipo
                .DocNro = txtDocumento.Text

                Dim lastRes As FERecuperaLastCbteResponse = service.FECompUltimoAutorizado(authRequest, fila.Cells("PtoVta").Value, cbtetipo)
                Dim last As Integer = lastRes.CbteNro

                CbteDesde = last + 1
                CbteHasta = last + 1
                .CbteDesde = last + 1
                .CbteHasta = last + 1

                .CbteFch = dtpFecha.Value.ToString("yyyyMMdd")

                If concepto = 2 Then
                    .FchServDesde = dtpFecha.Value.ToString("yyyyMMdd")
                    .FchServHasta = dtpFecha.Value.ToString("yyyyMMdd")
                    .FchVtoPago = dtpFecha.Value.ToString("yyyyMMdd")
                End If

                .ImpNeto = FormatNumber(importeneto, 2)
                .ImpIVA = FormatNumber(importeiva21 + importeiva10, 2)
                .ImpTotal = FormatNumber(importetotal, 2)

                .ImpTotConc = 0
                .ImpOpEx = 0
                .ImpTrib = 0

                .MonId = "PES"
                .MonCotiz = 1

                Dim alicuota As New AlicIva

                If importeiva21 <> 0 And importeiva10 <> 0 Then

                    'iva 10.5
                    alicuota.Id = idiva10
                    alicuota.BaseImp = FormatNumber(baseimp10 - importeiva10, 2)
                    alicuota.Importe = FormatNumber(importeiva10, 2)

                    'iva 21
                    Dim alicuota2 As New AlicIva
                    alicuota2.Id = idiva21
                    alicuota2.BaseImp = FormatNumber(baseimp21 - importeiva21, 2)
                    alicuota2.Importe = FormatNumber(importeiva21, 2)

                    .Iva = {alicuota, alicuota2}

                End If

                If importeiva21 = 0 And importeiva10 <> 0 Then

                    'iva 10.5
                    alicuota.Id = idiva10
                    alicuota.BaseImp = FormatNumber(baseimp10 - importeiva10, 2)
                    alicuota.Importe = FormatNumber(importeiva10, 2)

                    .Iva = {alicuota}

                End If

                If importeiva10 = 0 And importeiva21 <> 0 Then

                    'iva 21
                    alicuota.Id = idiva21
                    alicuota.BaseImp = FormatNumber(baseimp21 - importeiva21, 2)
                    alicuota.Importe = FormatNumber(importeiva21, 2)

                    .Iva = {alicuota}

                End If


            End With

            req.FeDetReq = {det}

            'con esta linea de codigo obtenemos el CAE
            Dim r = service.FECAESolicitar(authRequest, req)
#End Region



#Region "Imprimimos las observaciones y errores de la variable r con la que obtuvimos el CAE"

            Dim mensaje_obs_errores_eventos As String = ""
            If r.FeDetResp(0).Observaciones IsNot Nothing Then
                For Each o In r.FeDetResp(0).Observaciones
                    mensaje_obs_errores_eventos &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & vbCrLf
                Next
            End If
            If r.Errors IsNot Nothing Then
                For Each er In r.Errors
                    mensaje_obs_errores_eventos &= String.Format("Er: {0}: {1}", er.Code, er.Msg) & vbCrLf
                Next
            End If
            If r.Events IsNot Nothing Then
                For Each ev In r.Events
                    mensaje_obs_errores_eventos &= String.Format("Ev: {0}: {1}", ev.Code, ev.Msg) & vbCrLf
                Next
            End If
#End Region

#Region "Armo el codigo y genero el codigo QR"
            ' Codigo QR
            Dim qrcodestring As String = String.Concat(authRequest.Cuit,
                                      cbtetipo.ToString("00"),
                                      fila.Cells("PtoVta").Value.ToString("0000"),
                                      r.FeDetResp(0).CAE,
                                      r.FeDetResp(0).CAEFchVto)
            qrcodestring &= CalcChecksum(qrcodestring)



            Dim gen As New QRCodeGenerator
            Dim data = gen.CreateQrCode(qrcodestring, QRCodeGenerator.ECCLevel.Q)

            Dim code As New QRCode(data)
            pb_codQR.BackgroundImage = code.GetGraphic(6)

#End Region

            'asigno valores que faltaban

            'verificar si el estado de r es a(aprobado) o r(rechazado)
            If r.FeDetResp(0).Resultado = "R" Then

                f._Mensaje = mensaje_obs_errores_eventos
                MessageBox.Show("Se rechazó la factura. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            Else
                f._Estado = "Facturado"
                f._CbeteDesde = CbteDesde
                f._CbteHasta = CbteHasta
                f._CbteFch = dtpFecha.Value
                f._DocNro = txtDocumento.Text
                f._CAE = r.FeDetResp(0).CAE
                f._CAE_vto = r.FeDetResp(0).CAEFchVto
                f._Mensaje = ""

                ' Actualizar factura
                f._IdFactura = fila.Cells("IdFactura").Value
                f.ActualizarFactura()

                MessageBox.Show("Facturado exitosamente. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                ' Imprimir ticket

                If cbTicket.Checked = True Then
                    PrintDocument1.PrinterSettings.PrinterName = My.Settings.Printer
                    Try
                        PrintDocument1.Print()
                    Catch ex As Exception
                        MessageBox.Show("No hay ninguna impresora configurada. Entre a configuración y reintente.")
                    End Try
                End If

            End If

            Me.DialogResult = DialogResult.OK
            Me.Close()

        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al registrar la factura. Detalle: " & ex.Message.ToString)
            Logger.Error("Sección facturas: error al registrar la factura. Detalle: ", ex)
        End Try

    End Sub

    Private Sub bttElegirCliente_Click(sender As Object, e As EventArgs) Handles bttElegirCliente.Click

        Dim f As New ElegirCliente

        Try
            If f.ShowDialog() = DialogResult.OK Then

                txtDocumento.Text = f.dgvClientes.SelectedCells.Item(2).Value.ToString
                nombrecliente = f.dgvClientes.SelectedCells.Item(1).Value.ToString
                telefonocliente = f.dgvClientes.SelectedCells.Item(4).Value.ToString
                direccioncliente = f.dgvClientes.SelectedCells.Item(3).Value.ToString

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        If cboxComprobante.SelectedIndex = 0 Then
#Region "Factura A"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("CHEMEZ LUIS ENRIQUE", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DORREGO 140 - TEL: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("FERNANDEZ - CP (4322)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/19", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA A", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & fila.Cells("PtoVta").Value.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & CbteDesde.ToString("00000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(nombrecliente, font, New SolidBrush(Color.Black), startx, starty + offset) 'nombre cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(fila.Cells("TipoPago").Value, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(txtDocumento.Text, font, New SolidBrush(Color.Black), startx, starty + offset) 'cuit cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(direccioncliente, font, New SolidBrush(Color.Black), startx, starty + offset) 'direccion cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("Descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row.Cells("Cantidad").Value
                Dim precio As Double = row.Cells("PrecioUnitario").Value
                Dim subt As Double = row.Cells("Subtotal").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(" & row.Cells("iva").Value & ")"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & importeneto, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IMP. IVA", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & FormatNumber(importeiva21 + importeiva10, 2), font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & importetotal, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("RECIBI(MOS)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU PAGO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtPago.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU VUELTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtVuelto.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

            'defino parametros del picturebox
            pb_codQR.Width = 100
            pb_codQR.Height = 100
            pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

            Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 80, starty + offset - 20)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region
        ElseIf cboxComprobante.SelectedIndex = 1 Then
#Region "Factura B"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("CHEMEZ LUIS ENRIQUE", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DORREGO 140 - TEL: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("FERNANDEZ - CP (4322)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/19", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA B", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & fila.Cells("PtoVta").Value.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & CbteDesde.ToString("000000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(fila.Cells("TipoPago").Value, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DNI Nro.: " & txtDocumento.Text, font, New SolidBrush(Color.Black), startx, starty + offset) 'documento cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("A CONSUMIDOR FINAL", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("Descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row.Cells("Cantidad").Value
                Dim precio As Double = row.Cells("PrecioUnitario").Value
                Dim subt As Double = row.Cells("Subtotal").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(" & row.Cells("iva").Value & ")"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & importetotal, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & importetotal, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("RECIBI(MOS)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU PAGO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtPago.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU VUELTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtVuelto.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

            'defino parametros del picturebox
            pb_codQR.Width = 100
            pb_codQR.Height = 100
            pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

            Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 80, starty + offset - 20)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region

        End If

    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
    End Sub


End Class