﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Facturas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblFacturas = New System.Windows.Forms.Label()
        Me.dgvFacturas = New System.Windows.Forms.DataGridView()
        Me.bttFacturar = New System.Windows.Forms.Button()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.bttImprimir = New System.Windows.Forms.Button()
        Me.lblTotalNoFacturado = New System.Windows.Forms.Label()
        Me.lblTotalFacturado = New System.Windows.Forms.Label()
        Me.lblTotalNoFacturadoMes = New System.Windows.Forms.Label()
        Me.lblTotalFacturadoMes = New System.Windows.Forms.Label()
        Me.lblTituloResumen = New System.Windows.Forms.Label()
        Me.cboxTipo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bttBuscarCliente = New System.Windows.Forms.Button()
        Me.bttAceptar = New System.Windows.Forms.Button()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDocumento = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        CType(Me.dgvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFacturas
        '
        Me.lblFacturas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblFacturas.AutoSize = True
        Me.lblFacturas.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFacturas.Location = New System.Drawing.Point(25, 20)
        Me.lblFacturas.Name = "lblFacturas"
        Me.lblFacturas.Size = New System.Drawing.Size(288, 38)
        Me.lblFacturas.TabIndex = 48
        Me.lblFacturas.Text = "Listado de facturas"
        Me.lblFacturas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvFacturas
        '
        Me.dgvFacturas.AllowUserToAddRows = False
        Me.dgvFacturas.AllowUserToDeleteRows = False
        Me.dgvFacturas.AllowUserToResizeRows = False
        Me.dgvFacturas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFacturas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvFacturas.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvFacturas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvFacturas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvFacturas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFacturas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvFacturas.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvFacturas.Location = New System.Drawing.Point(45, 224)
        Me.dgvFacturas.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvFacturas.MultiSelect = False
        Me.dgvFacturas.Name = "dgvFacturas"
        Me.dgvFacturas.ReadOnly = True
        Me.dgvFacturas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFacturas.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvFacturas.RowHeadersVisible = False
        Me.dgvFacturas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvFacturas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFacturas.ShowEditingIcon = False
        Me.dgvFacturas.Size = New System.Drawing.Size(905, 110)
        Me.dgvFacturas.TabIndex = 86
        '
        'bttFacturar
        '
        Me.bttFacturar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttFacturar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.bttFacturar.Enabled = False
        Me.bttFacturar.FlatAppearance.BorderSize = 0
        Me.bttFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttFacturar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttFacturar.ForeColor = System.Drawing.Color.Black
        Me.bttFacturar.Location = New System.Drawing.Point(829, 403)
        Me.bttFacturar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttFacturar.Name = "bttFacturar"
        Me.bttFacturar.Size = New System.Drawing.Size(121, 40)
        Me.bttFacturar.TabIndex = 101
        Me.bttFacturar.Text = "Facturar"
        Me.bttFacturar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttFacturar.UseVisualStyleBackColor = False
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.Gray
        Me.lblTitulo.Location = New System.Drawing.Point(40, 192)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(91, 25)
        Me.lblTitulo.TabIndex = 110
        Me.lblTitulo.Text = "Facturas"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblObservacion
        '
        Me.lblObservacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblObservacion.AutoEllipsis = True
        Me.lblObservacion.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObservacion.ForeColor = System.Drawing.Color.Gray
        Me.lblObservacion.Location = New System.Drawing.Point(45, 334)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(905, 25)
        Me.lblObservacion.TabIndex = 111
        Me.lblObservacion.Text = "Observaciones:"
        Me.lblObservacion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bttImprimir
        '
        Me.bttImprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttImprimir.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttImprimir.FlatAppearance.BorderSize = 0
        Me.bttImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttImprimir.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttImprimir.ForeColor = System.Drawing.Color.Black
        Me.bttImprimir.Location = New System.Drawing.Point(699, 403)
        Me.bttImprimir.Margin = New System.Windows.Forms.Padding(0)
        Me.bttImprimir.Name = "bttImprimir"
        Me.bttImprimir.Size = New System.Drawing.Size(121, 40)
        Me.bttImprimir.TabIndex = 112
        Me.bttImprimir.Text = "Imprimir"
        Me.bttImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttImprimir.UseVisualStyleBackColor = False
        '
        'lblTotalNoFacturado
        '
        Me.lblTotalNoFacturado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalNoFacturado.AutoSize = True
        Me.lblTotalNoFacturado.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalNoFacturado.Location = New System.Drawing.Point(39, 411)
        Me.lblTotalNoFacturado.Name = "lblTotalNoFacturado"
        Me.lblTotalNoFacturado.Size = New System.Drawing.Size(325, 32)
        Me.lblTotalNoFacturado.TabIndex = 114
        Me.lblTotalNoFacturado.Text = "Total no facturado: $00,00"
        Me.lblTotalNoFacturado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalFacturado
        '
        Me.lblTotalFacturado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalFacturado.AutoSize = True
        Me.lblTotalFacturado.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFacturado.Location = New System.Drawing.Point(39, 374)
        Me.lblTotalFacturado.Name = "lblTotalFacturado"
        Me.lblTotalFacturado.Size = New System.Drawing.Size(289, 32)
        Me.lblTotalFacturado.TabIndex = 113
        Me.lblTotalFacturado.Text = "Total facturado: $00,00"
        Me.lblTotalFacturado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalNoFacturadoMes
        '
        Me.lblTotalNoFacturadoMes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalNoFacturadoMes.AutoSize = True
        Me.lblTotalNoFacturadoMes.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalNoFacturadoMes.ForeColor = System.Drawing.Color.Gray
        Me.lblTotalNoFacturadoMes.Location = New System.Drawing.Point(682, 109)
        Me.lblTotalNoFacturadoMes.Name = "lblTotalNoFacturadoMes"
        Me.lblTotalNoFacturadoMes.Size = New System.Drawing.Size(222, 25)
        Me.lblTotalNoFacturadoMes.TabIndex = 117
        Me.lblTotalNoFacturadoMes.Text = "Total no facturado: $ 0"
        Me.lblTotalNoFacturadoMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalFacturadoMes
        '
        Me.lblTotalFacturadoMes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalFacturadoMes.AutoSize = True
        Me.lblTotalFacturadoMes.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFacturadoMes.ForeColor = System.Drawing.Color.Gray
        Me.lblTotalFacturadoMes.Location = New System.Drawing.Point(682, 74)
        Me.lblTotalFacturadoMes.Name = "lblTotalFacturadoMes"
        Me.lblTotalFacturadoMes.Size = New System.Drawing.Size(193, 25)
        Me.lblTotalFacturadoMes.TabIndex = 116
        Me.lblTotalFacturadoMes.Text = "Total facturado: $ 0"
        Me.lblTotalFacturadoMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTituloResumen
        '
        Me.lblTituloResumen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTituloResumen.AutoSize = True
        Me.lblTituloResumen.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloResumen.Location = New System.Drawing.Point(654, 20)
        Me.lblTituloResumen.Name = "lblTituloResumen"
        Me.lblTituloResumen.Size = New System.Drawing.Size(188, 38)
        Me.lblTituloResumen.TabIndex = 115
        Me.lblTituloResumen.Text = "Resumen de"
        Me.lblTituloResumen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboxTipo
        '
        Me.cboxTipo.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.cboxTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboxTipo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboxTipo.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxTipo.FormattingEnabled = True
        Me.cboxTipo.Items.AddRange(New Object() {"Todos los comprobantes", "Facturas A", "Facturas B", "No Facturados"})
        Me.cboxTipo.Location = New System.Drawing.Point(89, 149)
        Me.cboxTipo.Name = "cboxTipo"
        Me.cboxTipo.Size = New System.Drawing.Size(238, 28)
        Me.cboxTipo.TabIndex = 127
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(41, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 20)
        Me.Label1.TabIndex = 126
        Me.Label1.Text = "Tipo"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bttBuscarCliente
        '
        Me.bttBuscarCliente.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.bttBuscarCliente.FlatAppearance.BorderSize = 0
        Me.bttBuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttBuscarCliente.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttBuscarCliente.ForeColor = System.Drawing.Color.Black
        Me.bttBuscarCliente.Location = New System.Drawing.Point(480, 74)
        Me.bttBuscarCliente.Name = "bttBuscarCliente"
        Me.bttBuscarCliente.Size = New System.Drawing.Size(121, 30)
        Me.bttBuscarCliente.TabIndex = 123
        Me.bttBuscarCliente.Text = "Elegir cliente"
        Me.bttBuscarCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttBuscarCliente.UseVisualStyleBackColor = False
        '
        'bttAceptar
        '
        Me.bttAceptar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.bttAceptar.FlatAppearance.BorderSize = 0
        Me.bttAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAceptar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAceptar.ForeColor = System.Drawing.Color.Black
        Me.bttAceptar.Location = New System.Drawing.Point(480, 110)
        Me.bttAceptar.Name = "bttAceptar"
        Me.bttAceptar.Size = New System.Drawing.Size(121, 30)
        Me.bttAceptar.TabIndex = 120
        Me.bttAceptar.Text = "Aceptar"
        Me.bttAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttAceptar.UseVisualStyleBackColor = False
        '
        'dtpHasta
        '
        Me.dtpHasta.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHasta.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(311, 110)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(143, 27)
        Me.dtpHasta.TabIndex = 124
        '
        'dtpDesde
        '
        Me.dtpDesde.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.dtpDesde.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(103, 110)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(142, 27)
        Me.dtpDesde.TabIndex = 125
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(41, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 20)
        Me.Label3.TabIndex = 122
        Me.Label3.Text = "Documento cliente"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(251, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 20)
        Me.Label5.TabIndex = 118
        Me.Label5.Text = "Hasta"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDocumento
        '
        Me.txtDocumento.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtDocumento.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDocumento.Enabled = False
        Me.txtDocumento.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocumento.Location = New System.Drawing.Point(196, 79)
        Me.txtDocumento.MaxLength = 15
        Me.txtDocumento.Name = "txtDocumento"
        Me.txtDocumento.Size = New System.Drawing.Size(258, 20)
        Me.txtDocumento.TabIndex = 121
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(41, 115)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 20)
        Me.Label6.TabIndex = 119
        Me.Label6.Text = "Desde"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PrintDocument1
        '
        '
        'Facturas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(989, 480)
        Me.Controls.Add(Me.cboxTipo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bttBuscarCliente)
        Me.Controls.Add(Me.bttAceptar)
        Me.Controls.Add(Me.dtpHasta)
        Me.Controls.Add(Me.dtpDesde)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDocumento)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblTotalNoFacturadoMes)
        Me.Controls.Add(Me.lblTotalFacturadoMes)
        Me.Controls.Add(Me.lblTituloResumen)
        Me.Controls.Add(Me.lblTotalNoFacturado)
        Me.Controls.Add(Me.lblTotalFacturado)
        Me.Controls.Add(Me.bttImprimir)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.bttFacturar)
        Me.Controls.Add(Me.dgvFacturas)
        Me.Controls.Add(Me.lblFacturas)
        Me.Location = New System.Drawing.Point(40, 78)
        Me.Name = "Facturas"
        Me.Text = "Facturas"
        CType(Me.dgvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblFacturas As Label
    Friend WithEvents dgvFacturas As DataGridView
    Friend WithEvents bttFacturar As Button
    Friend WithEvents lblTitulo As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents bttImprimir As Button
    Friend WithEvents lblTotalNoFacturado As Label
    Friend WithEvents lblTotalFacturado As Label
    Friend WithEvents lblTotalNoFacturadoMes As Label
    Friend WithEvents lblTotalFacturadoMes As Label
    Friend WithEvents lblTituloResumen As Label
    Friend WithEvents cboxTipo As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents bttBuscarCliente As Button
    Friend WithEvents bttAceptar As Button
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtDocumento As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
End Class
