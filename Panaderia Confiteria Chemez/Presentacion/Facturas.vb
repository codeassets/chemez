﻿Imports QRCoder
Imports Datos

Public Class Facturas

    Public idcliente As Int64
    Dim dt As New DataTable
    Dim totalfacturado As Double
    Dim totalnofacturado As Double
    'pictureboxQR
    Dim pb_codQR As New PictureBox

    Public Function CalcChecksum(S As String) As Byte
        Dim chk As Byte ' Final Checksum
        Dim T As Long   ' Char-pointer...

        ' Step 1
        chk = Asc(Mid(S, 1, 2))    ' Get first Char value

        ' Initialize counter
        T = 1
        While T < Len(S)

            ' Step 2
            chk = chk And 127   ' Bitwise operation. Clear bit 7 (127 -> 0111 1111)

            ' Step 3
            chk = chk Or 64     ' Bitwise operation. Set bit 6 (64 -> 01000000)

            ' Increase counter...
            T = T + 1

            ' Step 4
            chk = Asc(Mid(S, T, 1)) + chk
        End While

        CalcChecksum = chk
    End Function

    Public Sub AjustarTablaFacturas()
        dgvFacturas.Columns("IdFactura").Visible = False
        dgvFacturas.Columns("IdVenta").Visible = False
        dgvFacturas.Columns("TipoId").Visible = False
        dgvFacturas.Columns("CbteTipo").Visible = False
        'dgvFacturas.Columns("Concepto").Visible = False
        dgvFacturas.Columns("DocTipo").Visible = False
        dgvFacturas.Columns("CbteDesde").Visible = False
        dgvFacturas.Columns("CbteHasta").Visible = False
        dgvFacturas.Columns("ImpTotConc").Visible = False
        dgvFacturas.Columns("ImpOpEx").Visible = False
        dgvFacturas.Columns("ImpTrib").Visible = False
        dgvFacturas.Columns("MonId").Visible = False
        dgvFacturas.Columns("MonCotiz").Visible = False
        dgvFacturas.Columns("AlicIva_Id105").Visible = False
        dgvFacturas.Columns("AlicIva_BaseImp105").Visible = False
        dgvFacturas.Columns("AlicIva_Importe105").Visible = False
        dgvFacturas.Columns("AlicIva_Id21").Visible = False
        dgvFacturas.Columns("AlicIva_BaseImp21").Visible = False
        dgvFacturas.Columns("AlicIva_Importe21").Visible = False
        dgvFacturas.Columns("CAE").Visible = False
        dgvFacturas.Columns("CAE_vto").Visible = False
        dgvFacturas.Columns("PtoVta").Visible = False
        'dgvFacturas.Columns("Concepto_desc").Visible = False

        dgvFacturas.Columns("CbteFch").HeaderText = "Fecha"
        dgvFacturas.Columns("CbteTipo_desc").HeaderText = "Tipo cbte."
        dgvFacturas.Columns("DocTipo_desc").HeaderText = "Tipo doc."
        dgvFacturas.Columns("DocNro").HeaderText = "Documento"
        dgvFacturas.Columns("ImpTotal").HeaderText = "Imp. Total"
        dgvFacturas.Columns("ImpNeto").HeaderText = "Imp. Neto"
        dgvFacturas.Columns("ImpIVA").HeaderText = "IVA"
        dgvFacturas.Columns("TipoPago").HeaderText = "Pago"
        dgvFacturas.Columns("Concepto_desc").HeaderText = "Concepto"

        For Each row As DataGridViewRow In dgvFacturas.Rows
            If row.Cells("Estado").Value = "Facturado" Then
                row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
            ElseIf row.Cells("Estado").Value = "No Facturado" Then
                row.DefaultCellStyle.BackColor = Color.Salmon
            End If
        Next
    End Sub

    Private Sub CalcularTotales()

        totalnofacturado = 0
        totalfacturado = 0

        For Each row As DataGridViewRow In dgvFacturas.Rows
            If row.Cells("Estado").Value = "Facturado" Then
                totalfacturado = totalfacturado + row.Cells("ImpTotal").Value
            Else
                totalnofacturado = totalnofacturado + row.Cells("ImpTotal").Value
            End If
        Next

    End Sub

    Private Sub CalcularTotalesMes()

        Dim dt As New DataTable
        Dim totalmesfacturado As Double
        Dim totalmesnofacturado As Double

        Dim f As New DatosFacturas
        dt = f.ObtenerTodasMes

        For Each row As DataRow In dt.Rows
            If row("Estado") = "Facturado" Then
                totalmesfacturado = totalmesfacturado + row("ImpTotal")
            Else
                totalmesnofacturado = totalmesnofacturado + row("ImpTotal")
            End If
        Next

        lblTotalFacturadoMes.Text = "Total facturado: $ " & totalmesfacturado
        lblTotalNoFacturadoMes.Text = "Total no facturado: $ " & totalmesnofacturado

    End Sub

    Private Sub Facturas_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        cboxTipo.SelectedIndex = 0

        CalcularTotalesMes()

    End Sub

    Private Sub bttBuscarCliente_Click(sender As Object, e As EventArgs) Handles bttBuscarCliente.Click

        Dim f As New ElegirCliente
        Dim factura As New DatosFacturas
        f.b = 0

        Try
            If f.ShowDialog() = DialogResult.OK Then

                idcliente = f.dgvClientes.SelectedCells.Item(0).Value.ToString
                txtDocumento.Text = f.dgvClientes.SelectedCells.Item(2).Value.ToString
                factura._DocNro = txtDocumento.Text
                dt = factura.ObtenerTodasCliente
                dgvFacturas.DataSource = dt
                AjustarTablaFacturas()
                dgvFacturas.Columns("DocNro").Visible = False

                cboxTipo.SelectedIndex = 0

                CalcularTotales()
                lblTotalFacturado.Text = "Total facturado: $ " & totalfacturado
                lblTotalNoFacturado.Text = "Total no facturado: $ " & totalnofacturado

                lblTitulo.Text = "Facturas del cliente " & txtDocumento.Text

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub cboxTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxTipo.SelectedIndexChanged

        If dt.Rows.Count <> 0 Then

            If cboxTipo.SelectedIndex = 0 Then ' todas
                'Dim vista As New DataView(dt, "CbteTipo_desc=" & "'Factura A' or CbteTipo_desc=" & "'Factura B'", "CbteFch desc", DataViewRowState.CurrentRows)
                dgvFacturas.DataSource = dt
            ElseIf cboxTipo.SelectedIndex = 1 Then ' facturas A
                Dim vista As New DataView(dt, "CbteTipo_desc=" & "'Factura A'", "CbteFch desc", DataViewRowState.CurrentRows)
                dgvFacturas.DataSource = vista.ToTable
            ElseIf cboxTipo.SelectedIndex = 2 Then ' facturas B
                Dim vista As New DataView(dt, "CbteTipo_desc=" & "'Factura B'", "CbteFch desc", DataViewRowState.CurrentRows)
                dgvFacturas.DataSource = vista.ToTable
            Else ' no facturados
                Dim vista As New DataView(dt, "Estado=" & "'No Facturado'", "CbteFch desc", DataViewRowState.CurrentRows)
                dgvFacturas.DataSource = vista.ToTable
            End If

            For Each row As DataGridViewRow In dgvFacturas.Rows
                If row.Cells("Estado").Value = "Facturado" Then
                    row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                ElseIf row.Cells("Estado").Value = "No Facturado" Then
                    row.DefaultCellStyle.BackColor = Color.Salmon
                End If
            Next

            CalcularTotales()
            lblTotalFacturado.Text = "Total facturado: $ " & totalfacturado
            lblTotalNoFacturado.Text = "Total no facturado: $ " & totalnofacturado

        End If

    End Sub

    Private Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttAceptar.Click

        Dim factura As New DatosFacturas
        dt = factura.ObtenerPorFecha(dtpDesde.Value.ToShortDateString, dtpHasta.Value.ToShortDateString)
        dgvFacturas.DataSource = dt
        AjustarTablaFacturas()

        CalcularTotales()
        lblTotalFacturado.Text = "Total facturado: $ " & totalfacturado
        lblTotalNoFacturado.Text = "Total no facturado: $ " & totalnofacturado

        cboxTipo.SelectedIndex = 0

        txtDocumento.Clear()
        lblTitulo.Text = "Facturas del día " & dtpDesde.Value.ToShortDateString & " al día " & dtpHasta.Value.ToShortDateString

    End Sub

    Private Sub dgvFacturas_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvFacturas.CellMouseDoubleClick

        If dgvFacturas.SelectedRows.Count <> 0 Then

            Dim f As New DetallesFacturas(dgvFacturas.SelectedRows(0).Cells("IdFactura").Value)

            'pregunto si Ok(en la otra ventana declare que el boton cobrar es OK) fue presionado
            If f.ShowDialog() = DialogResult.OK Then

            Else

            End If
        End If

    End Sub

    Private Sub dgvFacturas_SelectionChanged(sender As Object, e As EventArgs) Handles dgvFacturas.SelectionChanged

        If dgvFacturas.SelectedRows.Count <> 0 Then
            lblObservacion.Text = "Observaciones: " & dgvFacturas.SelectedRows(0).Cells("Mensaje").Value

            If dgvFacturas.SelectedRows(0).Cells("Estado").Value = "No Facturado" Then
                bttFacturar.Enabled = True
                bttImprimir.Enabled = False
            Else
                bttFacturar.Enabled = False
                bttImprimir.Enabled = True
            End If

        End If

    End Sub

    Private Sub bttFacturar_Click(sender As Object, e As EventArgs) Handles bttFacturar.Click

        'abrir ventana de facturación
        Dim f As New Facturacion(dgvFacturas.SelectedRows(0))

        f.Login = Principal.l
        If f.ShowDialog() = DialogResult.OK Then

            Dim facturas As New DatosFacturas
            dt = facturas.ObtenerTodas
            dgvFacturas.DataSource = dt
            AjustarTablaFacturas()

            CalcularTotales()
            lblTotalFacturado.Text = "Total facturado: $ " & totalfacturado
            lblTotalNoFacturado.Text = "Total no facturado: $ " & totalnofacturado

            lblTitulo.Text = "Todas las facturas"
            txtDocumento.Clear()
            dtpDesde.Value = Today
            dtpHasta.Value = Today
            cboxTipo.SelectedIndex = 0

            CalcularTotalesMes()

        End If

    End Sub

    Private Sub dgvFacturas_Sorted(sender As Object, e As EventArgs) Handles dgvFacturas.Sorted

        If dgvFacturas.RowCount <> 0 Then
            For Each row As DataGridViewRow In dgvFacturas.Rows
                If row.Cells("Estado").Value = "Facturado" Then
                    row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                ElseIf row.Cells("Estado").Value = "No Facturado" Then
                    row.DefaultCellStyle.BackColor = Color.Salmon
                End If
            Next
        End If

    End Sub

    Private Sub bttImprimir_Click(sender As Object, e As EventArgs) Handles bttImprimir.Click

        If dgvFacturas.SelectedRows.Count <> 0 Then
            Dim result As DialogResult = MessageBox.Show("¿Desea imprimir la Factura Nro.: " & dgvFacturas.SelectedRows(0).Cells("CbteDesde").Value & "?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
            If result = DialogResult.Yes Then
                PrintDocument1.PrinterSettings.PrinterName = My.Settings.Printer
                Try
                    PrintDocument1.Print()
                Catch ex As Exception
                    MessageBox.Show("No hay ninguna impresora configurada. Entre a configuración y reintente.")
                End Try
            End If
        End If

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        ' Traer el resto de los datos
        Dim dt As New DataTable
        Dim detalle As New DatosFacturas
        detalle._IdFactura = dgvFacturas.SelectedRows(0).Cells("IdFactura").Value
        dt = detalle.ObtenerDetalles()

        Dim dtcliente As New DataTable
        Dim cliente As New DatosClientes
        cliente._cuit = dgvFacturas.SelectedRows(0).Cells("DocNro").Value
        dtcliente = cliente.ObtenerConCUIT()

        Dim puntoventa As Integer = dgvFacturas.SelectedRows(0).Cells("PtoVta").Value
        Dim nroticket As Integer = dgvFacturas.SelectedRows(0).Cells("CbteDesde").Value
        Dim tipocbte As Integer = dgvFacturas.SelectedRows(0).Cells("CbteTipo").Value

        ' Codigo QR
        Dim qrcodestring As String = String.Concat(My.Settings.def_cuit,
                                      tipocbte.ToString("00"),
                                      puntoventa.ToString("0000"),
                                      dgvFacturas.SelectedRows(0).Cells("CAE").Value,
                                      dgvFacturas.SelectedRows(0).Cells("CAE_vto").Value)
        qrcodestring &= CalcChecksum(qrcodestring)

        Dim gen As New QRCodeGenerator
        Dim data = gen.CreateQrCode(qrcodestring, QRCodeGenerator.ECCLevel.Q)

        Dim code As New QRCode(data)
        pb_codQR.BackgroundImage = code.GetGraphic(6)

        'defino parametros del picturebox
        pb_codQR.Width = 100
        pb_codQR.Height = 100
        pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

        Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

        If dgvFacturas.SelectedRows(0).Cells("CbteTipo_desc").Value = "Factura A" Then
#Region "Factura A"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("CHEMEZ LUIS ENRIQUE", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DORREGO 140 - TEL: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("FERNANDEZ - CP (4322)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/19", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA A", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & puntoventa.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & nroticket.ToString("00000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & DateTime.Parse(dgvFacturas.SelectedRows(0).Cells("CbteFch").Value).ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(dtcliente.Rows(0)("nombre"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nombre cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(dgvFacturas.SelectedRows(0).Cells("TipoPago").Value, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(dgvFacturas.SelectedRows(0).Cells("DocNro").Value, font, New SolidBrush(Color.Black), startx, starty + offset) 'cuit cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(dtcliente.Rows(0)("direccion"), font, New SolidBrush(Color.Black), startx, starty + offset) 'direccion cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataRow In dt.Rows

                Dim desc As String = row("Descripcion")
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row("Cantidad")
                Dim precio As Double = row("PrecioUnitario")
                Dim subt As Double = row("Subtotal")
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(" & row("iva") & ")"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & dgvFacturas.SelectedRows(0).Cells("ImpNeto").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IMP. IVA", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & dgvFacturas.SelectedRows(0).Cells("ImpIVA").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & dgvFacturas.SelectedRows(0).Cells("ImpTotal").Value, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 80, starty + offset - 20)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False

#End Region
        ElseIf dgvFacturas.SelectedRows(0).Cells("CbteTipo_desc").Value = "Factura B" Then
#Region "Factura B"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("CHEMEZ LUIS ENRIQUE", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DORREGO 140 - TEL: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("FERNANDEZ - CP (4322)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-40171522-4", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/19", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA B", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & puntoventa.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & nroticket.ToString("000000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & DateTime.Parse(dgvFacturas.SelectedRows(0).Cells("CbteFch").Value).ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(dgvFacturas.SelectedRows(0).Cells("TipoPago").Value, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DNI Nro.: " & dgvFacturas.SelectedRows(0).Cells("DocNro").Value, font, New SolidBrush(Color.Black), startx, starty + offset) 'documento cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("A CONSUMIDOR FINAL", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataRow In dt.Rows

                Dim desc As String = row("Descripcion")
                desc = desc.Substring(0, Math.Min(desc.Length, 21))
                Dim cant As String = row("Cantidad")
                Dim precio As Double = row("PrecioUnitario")
                Dim subt As Double = row("Subtotal")
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(" & row("iva") & ")"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & dgvFacturas.SelectedRows(0).Cells("ImpTotal").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & dgvFacturas.SelectedRows(0).Cells("ImpTotal").Value, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 80, starty + offset - 20)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False

#End Region
        End If

    End Sub

    Private Sub Facturas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        lblTituloResumen.Text = "Resumen de " & DateTime.Now.ToString("MMMM")

    End Sub
End Class