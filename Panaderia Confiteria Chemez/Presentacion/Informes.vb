﻿Imports Datos
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports System.IO

Public Class Informes
    Dim dtInforme As New DataTable
    Dim detalle_de As String
    Private fecha_desde As Date
    Private fecha_hasta As Date

    Private Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttAceptar.Click

        Try
            dgvVentas.DataSource = Nothing

            Dim ventas As New DatosVentas
            Dim pedidos As New DatosPedidos
            Dim productos As New DatosProductos

            fecha_desde = dtpFechaDesde.Value
            fecha_hasta = dtpFechaHasta.Value
            Dim fd As String = fecha_desde.ToString("yyyy-MM-dd")
            Dim fh As String = fecha_hasta.ToString("yyyy-MM-dd")

            If rbPanaderia.Checked = True Then
                dtInforme = ventas.BuscarMinoristas(fd, fh)
                dgvVentas.DataSource = dtInforme
                detalle_de = "Panaderia"

                dgvVentas.Columns("id_venta").Visible = False
                dgvVentas.Columns("id_usuario").Visible = False
                dgvVentas.Columns("id_cliente").Visible = False
                dgvVentas.Columns("fecha").HeaderText = "Fecha"
                dgvVentas.Columns("descuento").HeaderText = "Descuento"
                dgvVentas.Columns("total").HeaderText = "Total"
                dgvVentas.Columns("id_caja").HeaderText = "Caja"
                dgvVentas.Columns("nombre").HeaderText = "Usuario"
                dgvVentas.Columns("tipo_venta").HeaderText = "Tipo de venta"
                dgvVentas.Columns("tipo_pago").HeaderText = "Pago"

                lblTitulo.Text = "Listado de ventas de panadería"

            ElseIf rbCafeteria.Checked = True Then
                dtInforme = pedidos.Buscar(fd, fh)
                dgvVentas.DataSource = dtInforme
                detalle_de = "Cafeteria"

                dgvVentas.Columns("id_pedido").Visible = False
                dgvVentas.Columns("id_mesa").Visible = False
                dgvVentas.Columns("id_usuario").Visible = False
                dgvVentas.Columns("id_mozo").Visible = False
                dgvVentas.Columns("nro_mesa").HeaderText = "Mesa"
                dgvVentas.Columns("fecha").HeaderText = "Fecha"
                dgvVentas.Columns("estado").HeaderText = "Estado"
                dgvVentas.Columns("descuento").HeaderText = "Descuento"
                dgvVentas.Columns("total").HeaderText = "Total"
                dgvVentas.Columns("tipo_pago").HeaderText = "Pago"
                dgvVentas.Columns("id_caja").HeaderText = "Caja"

                lblTitulo.Text = "Listado de ventas de cafetería"

            ElseIf rbMayorista.Checked = True Then
                dtInforme = ventas.BuscarMayoristas(fd, fh)
                dgvVentas.DataSource = dtInforme
                detalle_de = "VentaMayorista"

                dgvVentas.Columns("id_venta").Visible = False
                dgvVentas.Columns("fecha").HeaderText = "Fecha"
                dgvVentas.Columns("tipo_pago").HeaderText = "Pago"
                dgvVentas.Columns("descuento").HeaderText = "Descuento"
                dgvVentas.Columns("total").HeaderText = "Total"
                dgvVentas.Columns("nombre").HeaderText = "Usuario"

                lblTitulo.Text = "Listado de ventas mayoristas"

            ElseIf rbProductos.Checked = True Then
                dtInforme = productos.BuscarFaltantes(numCantidad.Value)
                dgvVentas.DataSource = dtInforme
                detalle_de = "Productos"

                dgvVentas.Columns("descripcion").HeaderText = "Producto"
                dgvVentas.Columns("cantidad").HeaderText = "Cantidad"

                lblTitulo.Text = "Listado de productos"

                Return
            End If

            ' Calcular y mostrar el total
            Dim total As Double

            For Each row As DataGridViewRow In dgvVentas.Rows
                total = total + row.Cells("total").Value
            Next

            lblTotalPanaderia.Text = "Total: $ " & total

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub bttGenerarInforme_Click(sender As Object, e As EventArgs) Handles bttGenerarInforme.Click

        fecha_desde = dtpFechaDesde.Value
        fecha_hasta = dtpFechaHasta.Value
        Dim fd As String = fecha_desde.ToShortDateString()
        Dim fh As String = fecha_hasta.ToShortDateString()
        Dim stock As Integer = numCantidad.Value
        Using excel As New ExcelPackage()

            Dim workSheet = excel.Workbook.Worksheets.Add("Sheet1")

            If rbPanaderia.Checked = True Then

                Dim ventas As New DatosVentas

                Try
                    Dim dt As DataTable = ventas.InformeVentasMinoristasPorFecha(fd, fh)
                    workSheet.Cells("A3").LoadFromDataTable(dt, False)
                    workSheet.TabColor = System.Drawing.Color.Black
                    workSheet.DefaultRowHeight = 12
                    workSheet.Row(1).Height = 20
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(1).Style.Font.Bold = True
                    workSheet.Cells(1, 1).Value = "Listado de Ventas de Panadería del " & fd & " al " & fh

                    workSheet.Cells(2, 1).Value = "Nro. de venta"
                    workSheet.Cells(2, 2).Value = "Fecha"
                    workSheet.Cells(2, 3).Value = "Descuento"
                    workSheet.Cells(2, 4).Value = "Caja"
                    workSheet.Cells(2, 5).Value = "Usuario"
                    workSheet.Cells(2, 6).Value = "Pago"
                    workSheet.Cells(2, 7).Value = "Total"

                    'posicion donde va a cargar el total
                    Dim pos As Integer = dt.Rows.Count + 3
                    workSheet.Cells(pos, 6).Value = "Total:"
                    workSheet.Cells(pos, 7).Formula = "SUM(" & workSheet.Cells(3, 7).Address & ":" & workSheet.Cells(pos - 1, 7).Address & ")"
                    workSheet.Row(pos).Style.Font.Bold = True

                    workSheet.Cells("A1:I1").Merge = True

                    workSheet.Column(1).AutoFit()
                    workSheet.Column(1).Style.Font.Bold = True
                    workSheet.Column(2).Style.Numberformat.Format = "dd-mm-yyyy"
                    workSheet.Column(2).AutoFit()
                    workSheet.Column(2).Style.Locked = False
                    workSheet.Column(3).AutoFit()
                    workSheet.Column(3).Style.Locked = False
                    workSheet.Column(3).Style.Numberformat.Format = "$###,###,##0.00"
                    workSheet.Column(4).AutoFit()
                    workSheet.Column(4).Style.Locked = False
                    workSheet.Column(5).AutoFit()
                    workSheet.Column(5).Style.Locked = False
                    workSheet.Column(6).AutoFit()
                    workSheet.Column(6).Style.Locked = False
                    workSheet.Column(7).AutoFit()
                    workSheet.Column(7).Style.Locked = False
                    workSheet.Column(7).Style.Numberformat.Format = "$###,###,##0.00"

                    workSheet.Cells("A1:I1").Style.Locked = True
                    workSheet.Cells("A1:I1").Style.Font.Bold = True
                    workSheet.Cells("A2:I2").Style.Locked = True
                    workSheet.Cells("A2:I2").Style.Font.Bold = True
                    workSheet.Protection.IsProtected = True

                    Dim a As String = "Listado de Ventas de Panadería del " & fd & " al " & fh & ".xlsx"
                    Dim excelName As String = a.Replace("/", "-")

                    Try
                        'create a SaveFileDialog instance with some properties
                        Dim saveFileDialog1 As New SaveFileDialog()
                        saveFileDialog1.Title = "Save Excel sheet"
                        saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                        saveFileDialog1.FileName = excelName
                        'check if user clicked the save button
                        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                            'Get the FileInfo
                            Dim fi As New FileInfo(saveFileDialog1.FileName)
                            excel.SaveAs(fi)
                            'write the file to the disk
                        End If

                    Catch ex As Exception
                        MessageBox.Show("Surgió un problema al guardar el archivo Excel. Detalle: " & ex.Message.ToString)
                    End Try
                Catch ex As Exception
                    MessageBox.Show("Surgió un problema al recuperar los datos de ventas. Detalle: " & ex.Message.ToString)
                End Try

            ElseIf rbCafeteria.Checked = True Then

                Dim pedidos As New DatosPedidos

                Try
                    Dim dt As DataTable = pedidos.InformeCafeteriaPorFecha(fd, fh)
                    workSheet.Cells("A3").LoadFromDataTable(dt, False)
                    workSheet.TabColor = System.Drawing.Color.Black
                    workSheet.DefaultRowHeight = 12
                    workSheet.Row(1).Height = 20
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(1).Style.Font.Bold = True
                    workSheet.Cells(1, 1).Value = "Listado de Ventas de Cafetería del " & fd & " al " & fh

                    workSheet.Cells(2, 1).Value = "Nro. de pedido"
                    workSheet.Cells(2, 2).Value = "Mesa"
                    workSheet.Cells(2, 3).Value = "Fecha"
                    workSheet.Cells(2, 4).Value = "Estado"
                    workSheet.Cells(2, 5).Value = "Usuario"
                    workSheet.Cells(2, 6).Value = "Mozo"
                    workSheet.Cells(2, 7).Value = "Descuento"
                    workSheet.Cells(2, 8).Value = "Caja"
                    workSheet.Cells(2, 9).Value = "Total"

                    'posicion donde va a cargar el total
                    Dim pos As Integer = dt.Rows.Count + 3
                    workSheet.Cells(pos, 8).Value = "Total:"
                    workSheet.Cells(pos, 9).Formula = "SUM(" & workSheet.Cells(3, 9).Address & ":" & workSheet.Cells(pos - 1, 9).Address & ")"
                    workSheet.Row(pos).Style.Font.Bold = True

                    workSheet.Cells("A1:I1").Merge = True

                    workSheet.Column(1).AutoFit()
                    workSheet.Column(1).Style.Font.Bold = True
                    workSheet.Column(2).AutoFit()
                    workSheet.Column(2).Style.Locked = False
                    workSheet.Column(3).AutoFit()
                    workSheet.Column(3).Style.Locked = False
                    workSheet.Column(3).Style.Numberformat.Format = "dd-mm-yyyy"
                    workSheet.Column(4).AutoFit()
                    workSheet.Column(4).Style.Locked = False
                    workSheet.Column(5).AutoFit()
                    workSheet.Column(5).Style.Locked = False
                    workSheet.Column(6).AutoFit()
                    workSheet.Column(6).Style.Locked = False
                    workSheet.Column(7).AutoFit()
                    workSheet.Column(7).Style.Locked = False
                    workSheet.Column(7).Style.Numberformat.Format = "$###,###,##0.00"
                    workSheet.Column(8).AutoFit()
                    workSheet.Column(8).Style.Locked = False
                    workSheet.Column(9).AutoFit()
                    workSheet.Column(9).Style.Locked = False
                    workSheet.Column(9).Style.Numberformat.Format = "$###,###,##0.00"

                    workSheet.Cells("A1:I1").Style.Locked = True
                    workSheet.Cells("A1:I1").Style.Font.Bold = True
                    workSheet.Cells("A2:I2").Style.Locked = True
                    workSheet.Cells("A2:I2").Style.Font.Bold = True
                    workSheet.Protection.IsProtected = True

                    Dim a As String = "Listado de Ventas de Cafetería del " & fd & " al " & fh & ".xlsx"
                    Dim excelName As String = a.Replace("/", "-")

                    Try
                        'create a SaveFileDialog instance with some properties
                        Dim saveFileDialog1 As New SaveFileDialog()
                        saveFileDialog1.Title = "Save Excel sheet"
                        saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                        saveFileDialog1.FileName = excelName
                        'check if user clicked the save button
                        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                            'Get the FileInfo
                            Dim fi As New FileInfo(saveFileDialog1.FileName)
                            excel.SaveAs(fi)
                            'write the file to the disk
                        End If

                    Catch ex As Exception
                        MessageBox.Show("Surgió un problema al guardar el archivo Excel. Detalle: " & ex.Message.ToString)
                    End Try
                Catch ex As Exception
                    MessageBox.Show("Surgió un problema al recuperar los datos de ventas. Detalle: " & ex.Message.ToString)
                End Try

            ElseIf rbMayorista.Checked = True Then

                Dim ventas As New DatosVentas

                Try
                    Dim dt As DataTable = ventas.InformeVentasMayoristasPorFecha(fd, fh)
                    workSheet.Cells("A3").LoadFromDataTable(dt, False)
                    workSheet.TabColor = System.Drawing.Color.Black
                    workSheet.DefaultRowHeight = 12
                    workSheet.Row(1).Height = 20
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(1).Style.Font.Bold = True
                    workSheet.Cells(1, 1).Value = "Listado de Ventas Mayoristas del " & fd & " al " & fh

                    workSheet.Cells(2, 1).Value = "Nro. de venta"
                    workSheet.Cells(2, 2).Value = "Fecha"
                    workSheet.Cells(2, 3).Value = "Cliente"
                    workSheet.Cells(2, 4).Value = "Pago"
                    workSheet.Cells(2, 5).Value = "Descuento"
                    workSheet.Cells(2, 6).Value = "Total"
                    workSheet.Cells(2, 7).Value = "Usuario"

                    'posicion donde va a cargar el total
                    Dim pos As Integer = dt.Rows.Count + 3
                    workSheet.Cells(pos, 5).Value = "Total:"
                    workSheet.Cells(pos, 6).Formula = "SUM(" & workSheet.Cells(3, 6).Address & ":" & workSheet.Cells(pos - 1, 6).Address & ")"
                    workSheet.Row(pos).Style.Font.Bold = True

                    workSheet.Cells("A1:G1").Merge = True

                    workSheet.Column(1).AutoFit()
                    workSheet.Column(1).Style.Font.Bold = True
                    workSheet.Column(2).Style.Numberformat.Format = "dd-mm-yyyy"
                    workSheet.Column(2).AutoFit()
                    workSheet.Column(2).Style.Locked = False
                    workSheet.Column(3).AutoFit()
                    workSheet.Column(3).Style.Locked = False
                    workSheet.Column(4).AutoFit()
                    workSheet.Column(4).Style.Locked = False
                    workSheet.Column(5).AutoFit()
                    workSheet.Column(5).Style.Locked = False
                    workSheet.Column(5).Style.Numberformat.Format = "$###,###,##0.00"
                    workSheet.Column(6).AutoFit()
                    workSheet.Column(6).Style.Locked = False
                    workSheet.Column(6).Style.Numberformat.Format = "$###,###,##0.00"

                    workSheet.Cells("A1:G1").Style.Locked = True
                    workSheet.Cells("A1:G1").Style.Font.Bold = True
                    workSheet.Cells("A2:G2").Style.Locked = True
                    workSheet.Cells("A2:G2").Style.Font.Bold = True
                    workSheet.Protection.IsProtected = True

                    Dim a As String = "Listado de Ventas Mayoristas del " & fd & " al " & fh & ".xlsx"
                    Dim excelName As String = a.Replace("/", "-")

                    Try
                        'create a SaveFileDialog instance with some properties
                        Dim saveFileDialog1 As New SaveFileDialog()
                        saveFileDialog1.Title = "Save Excel sheet"
                        saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                        saveFileDialog1.FileName = excelName
                        'check if user clicked the save button
                        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                            'Get the FileInfo
                            Dim fi As New FileInfo(saveFileDialog1.FileName)
                            excel.SaveAs(fi)
                            'write the file to the disk
                        End If

                    Catch ex As Exception
                        MessageBox.Show("Surgió un problema al guardar el archivo Excel. Detalle: " & ex.Message.ToString)
                    End Try
                Catch ex As Exception
                    MessageBox.Show("Surgió un problema al recuperar los datos de ventas. Detalle: " & ex.Message.ToString)
                End Try

            ElseIf rbProductos.Checked = True Then

                Dim productos As New DatosProductos

                Try
                    Dim dt As DataTable = productos.BuscarFaltantes(stock)
                    workSheet.Cells("A3").LoadFromDataTable(dt, False)
                    workSheet.TabColor = System.Drawing.Color.Black
                    workSheet.DefaultRowHeight = 12
                    workSheet.Row(1).Height = 20
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    workSheet.Row(1).Style.Font.Bold = True
                    workSheet.Cells(1, 1).Value = "Listado de Productos faltantes " & Date.Today & ""

                    workSheet.Cells(2, 1).Value = "Producto"
                    workSheet.Cells(2, 2).Value = "Cantidad"

                    workSheet.Cells("A1:B1").Merge = True

                    workSheet.Column(1).AutoFit()
                    workSheet.Column(1).Style.Locked = False
                    workSheet.Column(2).AutoFit()
                    workSheet.Column(2).Style.Font.Bold = True

                    workSheet.Cells("A1:F1").Style.Locked = True
                    workSheet.Cells("A1:F1").Style.Font.Bold = True
                    workSheet.Cells("A2:F2").Style.Locked = True
                    workSheet.Cells("A2:F2").Style.Font.Bold = True
                    workSheet.Protection.IsProtected = True

                    Dim a As String = "Listado de Productos faltantes " & Date.Today & ".xlsx"
                    Dim excelName As String = a.Replace("/", "-")

                    Try
                        'create a SaveFileDialog instance with some properties
                        Dim saveFileDialog1 As New SaveFileDialog()
                        saveFileDialog1.Title = "Save Excel sheet"
                        saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                        saveFileDialog1.FileName = excelName
                        'check if user clicked the save button
                        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                            'Get the FileInfo
                            Dim fi As New FileInfo(saveFileDialog1.FileName)
                            excel.SaveAs(fi)
                            'write the file to the disk
                        End If

                    Catch ex As Exception
                        MessageBox.Show("Surgió un problema al guardar el archivo Excel. Detalle: " & ex.Message.ToString)
                    End Try
                Catch ex As Exception
                    MessageBox.Show("Surgió un problema al recuperar los datos de ventas. Detalle: " & ex.Message.ToString)
                End Try

            End If

        End Using

    End Sub


    Private Sub dgvVentas_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVentas.CellMouseDoubleClick
        If dgvVentas.SelectedRows.Count <> 0 Then
            If lblTitulo.Text <> "Listado de productos" Then
                Dim f As New DetallesInformes(dgvVentas.SelectedRows(0).Cells(0).Value, detalle_de)

                'pregunto si Ok(en la otra ventana declare que el boton cobrar es OK) fue presionado
                If f.ShowDialog() = DialogResult.OK Then

                Else

                End If
            End If
        End If
    End Sub

    Private Sub rbProductos_CheckedChanged(sender As Object, e As EventArgs) Handles rbProductos.CheckedChanged

        If rbProductos.Checked = True Then
            dtpFechaDesde.Enabled = False
            dtpFechaHasta.Enabled = False
            numCantidad.Enabled = True
        Else
            dtpFechaDesde.Enabled = True
            dtpFechaHasta.Enabled = True
            numCantidad.Enabled = False
        End If
    End Sub

    Private Sub bttImprimirTicket_Click(sender As Object, e As EventArgs) Handles bttImprimirTicket.Click
        PrintDocument1.PrinterSettings.PrinterName = My.Settings.Printer
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        Try
            Dim dt As New DataTable

            If rbCafeteria.Checked = True Then ' Ticket común de cafetería

                Dim detalles As New DatosPedidos
                detalles._id_pedido = dgvVentas.SelectedRows(0).Cells("id_pedido").Value
                dt = detalles.ObtenerDetallePedidosPor_id_pedido

                Dim graphic As Graphics = e.Graphics

                Dim sf As New StringFormat
                sf.Alignment = StringAlignment.Center

                Dim font As New Font("Ticketing", 12)
                Dim fontbold As New Font("Ticketing", 16)
                Dim fontheight As Double = font.GetHeight
                Dim startx As Integer = 10
                Dim starty As Integer = 0
                Dim offset As Integer = 40
                Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

                ' Encabezado
                graphic.DrawString("FLORENCIA PANADERÍA - CAFETERÍA", font, New SolidBrush(Color.Black), 30, starty + offset)
                offset = offset + CInt(fontheight) + 10
                graphic.DrawString("Dorrego 140 - Tel: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("Fecha: " & dgvVentas.SelectedRows(0).Cells("Fecha").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("Mozo: " & dgvVentas.SelectedRows(0).Cells("Mozo").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3

                ' Detalles
                For Each row As DataRow In dt.Rows

                    Dim desc As String = row("descripcion")
                    desc = desc.Substring(0, Math.Min(desc.Length, 21))
                    Dim cant As String = row("cantidad")
                    Dim precio As Double = row("precio_unitario")
                    Dim subt As Double = row("cantidad") * row("precio_unitario")
                    Dim formattedSubt As String = String.Format("{0:n}", subt)
                    Dim linea1 As String = cant & "u x $" & precio
                    Dim linea2 As String = desc
                    Dim linea2total As String = "$" & formattedSubt

                    graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                    offset = offset + CInt(fontheight) + 2
                    graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                    graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

                    offset = offset + CInt(fontheight) + 3
                Next

                If dgvVentas.SelectedRows(0).Cells("descuento").Value <> 0 Then

                    offset = offset + CInt(fontheight) + 2
                    graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                    graphic.DrawString("$" & dgvVentas.SelectedRows(0).Cells("descuento").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                    offset = offset + 3

                End If

                offset = offset + 20

                Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

                graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
                graphic.DrawString("$" & dgvVentas.SelectedRows(0).Cells("total").Value, fontbold, New SolidBrush(Color.Black), rect, format)

                offset = offset + CInt(fontheight) + 20

                graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
                offset = offset + CInt(fontheight) + 20
                graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

                e.HasMorePages = False

            ElseIf rbPanaderia.Checked = True Or rbMayorista.Checked Then

                Dim detalles As New DatosVentas
                detalles._id_venta = dgvVentas.SelectedRows(0).Cells("id_venta").Value
                dt = detalles.ObtenerDetalles

                Dim graphic As Graphics = e.Graphics

                Dim sf As New StringFormat
                sf.Alignment = StringAlignment.Center

                Dim font As New Font("Ticketing", 12)
                Dim fontbold As New Font("Ticketing", 16)
                Dim fontheight As Double = font.GetHeight
                Dim startx As Integer = 10
                Dim starty As Integer = 0
                Dim offset As Integer = 40
                Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

                ' Encabezado
                graphic.DrawString("FLORENCIA PANADERÍA - CAFETERÍA", font, New SolidBrush(Color.Black), 30, starty + offset)
                offset = offset + CInt(fontheight) + 10
                graphic.DrawString("Dorrego 140 - Tel: 491-2293", font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("Fecha: " & dgvVentas.SelectedRows(0).Cells("Fecha").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 3

                ' Detalles
                For Each row As DataRow In dt.Rows

                    Dim desc As String = row("descripcion")
                    desc = desc.Substring(0, Math.Min(desc.Length, 21))
                    Dim cant As String = row("cantidad")
                    Dim precio As Double = row("precio_unitario")
                    Dim subt As Double = row("cantidad") * row("precio_unitario")
                    Dim formattedSubt As String = String.Format("{0:n}", subt)
                    Dim linea1 As String = cant & "u x $" & precio
                    Dim linea2 As String = desc
                    Dim linea2total As String = "$" & formattedSubt

                    graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                    offset = offset + CInt(fontheight) + 2
                    graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                    graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

                    offset = offset + CInt(fontheight) + 3
                Next

                If dgvVentas.SelectedRows(0).Cells("descuento").Value <> 0 Then

                    offset = offset + CInt(fontheight) + 2
                    graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                    graphic.DrawString("$" & dgvVentas.SelectedRows(0).Cells("descuento").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                    offset = offset + 3

                End If

                offset = offset + 20

                Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

                graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
                graphic.DrawString("$" & dgvVentas.SelectedRows(0).Cells("total").Value, fontbold, New SolidBrush(Color.Black), rect, format)

                offset = offset + CInt(fontheight) + 20

                graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
                offset = offset + CInt(fontheight) + 20
                graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

                e.HasMorePages = False

            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgvVentas_SelectionChanged(sender As Object, e As EventArgs) Handles dgvVentas.SelectionChanged

        If rbCafeteria.Checked = True Or rbMayorista.Checked = True Or rbPanaderia.Checked = True Then
            bttImprimirTicket.Enabled = True
        Else
            bttImprimirTicket.Enabled = False
        End If

    End Sub
End Class