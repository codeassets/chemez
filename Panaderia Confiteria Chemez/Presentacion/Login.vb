﻿Imports Datos

Public Class Login
    Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

    End Sub

    Private Sub Login_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        txtUsuario.Focus()
    End Sub

    'Valida que todos los campos esten completos
    Private Function ValidarCampos() As Boolean
        If Len(Trim(txtUsuario.Text)) = 0 Then
            MsgBox("Debe ingresar su usuario.", MsgBoxStyle.Exclamation, Title:="Atención")
            txtUsuario.Focus()
            Return False
        ElseIf Len(Trim(txtPassword.Text)) = 0 Then
            MsgBox("Debe ingresar su contraseña.", MsgBoxStyle.Exclamation, Title:="Atención")
            txtPassword.Focus()
            Return False
        Else
            Return True
        End If
    End Function



    Private Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttAceptar.Click

        'Comprueba si todos los campos estan completos
        Dim resultado_campos As Boolean = ValidarCampos()

        If (resultado_campos = True) Then

            Dim usuario As New DatosUsuarios
            Dim row_usuario As New DataTable
            row_usuario = usuario.ComprobarAcceso(txtUsuario.Text, txtPassword.Text)

            'Determina si se encontro o no el usuario
            If row_usuario.Rows.Count <> 0 Then

                SesionUsuario.set_usuario(row_usuario(0)("id_usuario"))

                SesionUsuario.set_nombre(row_usuario(0)("nombre"))

                SesionUsuario.set_tipo(row_usuario(0)("tipo"))

                Me.DialogResult = DialogResult.OK

                Me.Dispose()

            Else

                MsgBox("Usuario o contraseña incorrectos.", MsgBoxStyle.Critical, Title:="Atención")
                txtUsuario.Focus()

            End If

        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.Dispose()
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
        ' login
        If e.KeyCode = Keys.Enter Then
            bttAceptar.PerformClick()
        End If
        ' cerrar
        If e.KeyCode = Keys.Escape Then
            Me.Dispose()
        End If
    End Sub

    Private Sub txtUsuario_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUsuario.KeyDown
        ' login
        If e.KeyCode = Keys.Enter Then
            bttAceptar.PerformClick()
        End If
        ' cerrar
        If e.KeyCode = Keys.Escape Then
            Me.Dispose()
        End If
    End Sub

End Class