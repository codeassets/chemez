﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Panaderia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblPedido = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.bttAgregar = New System.Windows.Forms.Button()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.cbKgGrs = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.bttQuitar = New System.Windows.Forms.Button()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.dgvProductos = New System.Windows.Forms.DataGridView()
        Me.dgvAgregados = New System.Windows.Forms.DataGridView()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.bttCobrar = New System.Windows.Forms.Button()
        Me.rbttVal = New System.Windows.Forms.RadioButton()
        Me.txtDescVal = New System.Windows.Forms.TextBox()
        Me.rbttPorc = New System.Windows.Forms.RadioButton()
        Me.txtDescPorc = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bttCancelar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.rbCredDeb = New System.Windows.Forms.RadioButton()
        Me.rbEfectivo = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.PanelTicket = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblNroTicket = New System.Windows.Forms.Label()
        Me.lblFechaTicket = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PicBarCode = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblTotalTicket = New System.Windows.Forms.Label()
        Me.lblid = New System.Windows.Forms.Label()
        Me.dgvTicket = New System.Windows.Forms.DataGridView()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAgregados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.PanelTicket.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBarCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPedido
        '
        Me.lblPedido.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblPedido.AutoSize = True
        Me.lblPedido.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPedido.Location = New System.Drawing.Point(25, 20)
        Me.lblPedido.Name = "lblPedido"
        Me.lblPedido.Size = New System.Drawing.Size(158, 38)
        Me.lblPedido.TabIndex = 49
        Me.lblPedido.Text = "Productos"
        Me.lblPedido.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtBuscar, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvProductos, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvAgregados, 2, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(41, 78)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(543, 474)
        Me.TableLayoutPanel1.TabIndex = 50
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Gray
        Me.Label1.Location = New System.Drawing.Point(284, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(256, 25)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "Agregados"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.09962!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.54023!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.87164!))
        Me.TableLayoutPanel3.Controls.Add(Me.bttAgregar, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.txtCantidad, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cbKgGrs, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 414)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(261, 60)
        Me.TableLayoutPanel3.TabIndex = 63
        '
        'bttAgregar
        '
        Me.bttAgregar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.bttAgregar.Enabled = False
        Me.bttAgregar.FlatAppearance.BorderSize = 0
        Me.bttAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAgregar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAgregar.ForeColor = System.Drawing.Color.Black
        Me.bttAgregar.Image = Global.Presentacion.My.Resources.Resources.plus
        Me.bttAgregar.Location = New System.Drawing.Point(140, 10)
        Me.bttAgregar.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.bttAgregar.Name = "bttAgregar"
        Me.bttAgregar.Size = New System.Drawing.Size(121, 40)
        Me.bttAgregar.TabIndex = 2
        Me.bttAgregar.Text = "Agregar"
        Me.bttAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttAgregar.UseVisualStyleBackColor = False
        '
        'txtCantidad
        '
        Me.txtCantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCantidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCantidad.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidad.Location = New System.Drawing.Point(92, 20)
        Me.txtCantidad.MaxLength = 10
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(45, 20)
        Me.txtCantidad.TabIndex = 1
        Me.txtCantidad.Text = "0"
        '
        'cbKgGrs
        '
        Me.cbKgGrs.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbKgGrs.BackColor = System.Drawing.Color.White
        Me.cbKgGrs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbKgGrs.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbKgGrs.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbKgGrs.FormattingEnabled = True
        Me.cbKgGrs.Location = New System.Drawing.Point(3, 16)
        Me.cbKgGrs.Name = "cbKgGrs"
        Me.cbKgGrs.Size = New System.Drawing.Size(83, 28)
        Me.cbKgGrs.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.bttQuitar, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(281, 414)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(262, 60)
        Me.TableLayoutPanel2.TabIndex = 58
        '
        'bttQuitar
        '
        Me.bttQuitar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.bttQuitar.Enabled = False
        Me.bttQuitar.FlatAppearance.BorderSize = 0
        Me.bttQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttQuitar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttQuitar.ForeColor = System.Drawing.Color.Black
        Me.bttQuitar.Image = Global.Presentacion.My.Resources.Resources.remove
        Me.bttQuitar.Location = New System.Drawing.Point(136, 10)
        Me.bttQuitar.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.bttQuitar.Name = "bttQuitar"
        Me.bttQuitar.Size = New System.Drawing.Size(121, 40)
        Me.bttQuitar.TabIndex = 46
        Me.bttQuitar.Text = "Eliminar"
        Me.bttQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttQuitar.UseVisualStyleBackColor = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBuscar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBuscar.Enabled = False
        Me.txtBuscar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtBuscar.Location = New System.Drawing.Point(0, 10)
        Me.txtBuscar.Margin = New System.Windows.Forms.Padding(0)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(261, 20)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.Text = "Presione ENTER para buscar"
        '
        'dgvProductos
        '
        Me.dgvProductos.AllowUserToAddRows = False
        Me.dgvProductos.AllowUserToDeleteRows = False
        Me.dgvProductos.AllowUserToResizeRows = False
        Me.dgvProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvProductos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvProductos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvProductos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvProductos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProductos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvProductos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvProductos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProductos.Location = New System.Drawing.Point(0, 40)
        Me.dgvProductos.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvProductos.MultiSelect = False
        Me.dgvProductos.Name = "dgvProductos"
        Me.dgvProductos.ReadOnly = True
        Me.dgvProductos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProductos.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvProductos.RowHeadersVisible = False
        Me.dgvProductos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProductos.ShowEditingIcon = False
        Me.dgvProductos.Size = New System.Drawing.Size(261, 374)
        Me.dgvProductos.TabIndex = 1
        '
        'dgvAgregados
        '
        Me.dgvAgregados.AllowUserToAddRows = False
        Me.dgvAgregados.AllowUserToDeleteRows = False
        Me.dgvAgregados.AllowUserToResizeRows = False
        Me.dgvAgregados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAgregados.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvAgregados.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvAgregados.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvAgregados.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAgregados.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvAgregados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAgregados.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvAgregados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAgregados.Location = New System.Drawing.Point(281, 40)
        Me.dgvAgregados.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvAgregados.MultiSelect = False
        Me.dgvAgregados.Name = "dgvAgregados"
        Me.dgvAgregados.ReadOnly = True
        Me.dgvAgregados.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAgregados.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvAgregados.RowHeadersVisible = False
        Me.dgvAgregados.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAgregados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAgregados.ShowEditingIcon = False
        Me.dgvAgregados.Size = New System.Drawing.Size(262, 374)
        Me.dgvAgregados.TabIndex = 60
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(627, 454)
        Me.lblTotal.Margin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(251, 38)
        Me.lblTotal.TabIndex = 55
        Me.lblTotal.Text = "Total: $ 0,00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(638, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 38)
        Me.Label3.TabIndex = 51
        Me.Label3.Text = "Pago"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bttCobrar
        '
        Me.bttCobrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttCobrar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.bttCobrar.Enabled = False
        Me.bttCobrar.FlatAppearance.BorderSize = 0
        Me.bttCobrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCobrar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCobrar.ForeColor = System.Drawing.Color.Black
        Me.bttCobrar.Location = New System.Drawing.Point(757, 512)
        Me.bttCobrar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttCobrar.Name = "bttCobrar"
        Me.bttCobrar.Size = New System.Drawing.Size(121, 40)
        Me.bttCobrar.TabIndex = 0
        Me.bttCobrar.Text = "Cobrar"
        Me.bttCobrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCobrar.UseVisualStyleBackColor = False
        '
        'rbttVal
        '
        Me.rbttVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rbttVal.AutoSize = True
        Me.rbttVal.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbttVal.ForeColor = System.Drawing.Color.Black
        Me.rbttVal.Location = New System.Drawing.Point(12, 47)
        Me.rbttVal.Name = "rbttVal"
        Me.rbttVal.Size = New System.Drawing.Size(67, 24)
        Me.rbttVal.TabIndex = 64
        Me.rbttVal.Text = "Valor"
        Me.rbttVal.UseVisualStyleBackColor = True
        '
        'txtDescVal
        '
        Me.txtDescVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescVal.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtDescVal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescVal.Enabled = False
        Me.txtDescVal.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescVal.Location = New System.Drawing.Point(130, 49)
        Me.txtDescVal.Name = "txtDescVal"
        Me.txtDescVal.Size = New System.Drawing.Size(69, 20)
        Me.txtDescVal.TabIndex = 66
        Me.txtDescVal.Text = "0,00"
        '
        'rbttPorc
        '
        Me.rbttPorc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rbttPorc.AutoSize = True
        Me.rbttPorc.Checked = True
        Me.rbttPorc.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbttPorc.ForeColor = System.Drawing.Color.Black
        Me.rbttPorc.Location = New System.Drawing.Point(11, 12)
        Me.rbttPorc.Name = "rbttPorc"
        Me.rbttPorc.Size = New System.Drawing.Size(108, 24)
        Me.rbttPorc.TabIndex = 63
        Me.rbttPorc.TabStop = True
        Me.rbttPorc.Text = "Porcentaje"
        Me.rbttPorc.UseVisualStyleBackColor = True
        '
        'txtDescPorc
        '
        Me.txtDescPorc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescPorc.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtDescPorc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescPorc.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescPorc.Location = New System.Drawing.Point(130, 14)
        Me.txtDescPorc.MaxLength = 3
        Me.txtDescPorc.Name = "txtDescPorc"
        Me.txtDescPorc.Size = New System.Drawing.Size(69, 20)
        Me.txtDescPorc.TabIndex = 65
        Me.txtDescPorc.Text = "0"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Gray
        Me.Label4.Location = New System.Drawing.Point(640, 211)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 25)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Descuento"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label4.Visible = False
        '
        'bttCancelar
        '
        Me.bttCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttCancelar.FlatAppearance.BorderSize = 0
        Me.bttCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCancelar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCancelar.ForeColor = System.Drawing.Color.Black
        Me.bttCancelar.Location = New System.Drawing.Point(627, 512)
        Me.bttCancelar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttCancelar.Name = "bttCancelar"
        Me.bttCancelar.Size = New System.Drawing.Size(121, 40)
        Me.bttCancelar.TabIndex = 1
        Me.bttCancelar.Text = "Cancelar"
        Me.bttCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCancelar.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.rbttPorc)
        Me.Panel1.Controls.Add(Me.txtDescPorc)
        Me.Panel1.Controls.Add(Me.txtDescVal)
        Me.Panel1.Controls.Add(Me.rbttVal)
        Me.Panel1.Location = New System.Drawing.Point(661, 241)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(208, 81)
        Me.Panel1.TabIndex = 72
        Me.Panel1.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.Controls.Add(Me.rbCredDeb)
        Me.Panel3.Controls.Add(Me.rbEfectivo)
        Me.Panel3.Location = New System.Drawing.Point(661, 111)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(208, 81)
        Me.Panel3.TabIndex = 75
        '
        'rbCredDeb
        '
        Me.rbCredDeb.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rbCredDeb.AutoSize = True
        Me.rbCredDeb.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCredDeb.ForeColor = System.Drawing.Color.Black
        Me.rbCredDeb.Location = New System.Drawing.Point(13, 44)
        Me.rbCredDeb.Name = "rbCredDeb"
        Me.rbCredDeb.Size = New System.Drawing.Size(142, 24)
        Me.rbCredDeb.TabIndex = 70
        Me.rbCredDeb.Text = "Crédito/Débito"
        Me.rbCredDeb.UseVisualStyleBackColor = True
        '
        'rbEfectivo
        '
        Me.rbEfectivo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rbEfectivo.AutoSize = True
        Me.rbEfectivo.Checked = True
        Me.rbEfectivo.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbEfectivo.ForeColor = System.Drawing.Color.Black
        Me.rbEfectivo.Location = New System.Drawing.Point(13, 14)
        Me.rbEfectivo.Name = "rbEfectivo"
        Me.rbEfectivo.Size = New System.Drawing.Size(87, 24)
        Me.rbEfectivo.TabIndex = 69
        Me.rbEfectivo.TabStop = True
        Me.rbEfectivo.Text = "Efectivo"
        Me.rbEfectivo.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Gray
        Me.Label6.Location = New System.Drawing.Point(640, 83)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 25)
        Me.Label6.TabIndex = 74
        Me.Label6.Text = "Pago"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblError.AutoSize = True
        Me.lblError.Font = New System.Drawing.Font("Averta Regular", 9.749999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(41, 555)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 16)
        Me.lblError.TabIndex = 76
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'PanelTicket
        '
        Me.PanelTicket.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelTicket.AutoSize = True
        Me.PanelTicket.BackColor = System.Drawing.Color.White
        Me.PanelTicket.Controls.Add(Me.Label2)
        Me.PanelTicket.Controls.Add(Me.Label7)
        Me.PanelTicket.Controls.Add(Me.lblNroTicket)
        Me.PanelTicket.Controls.Add(Me.lblFechaTicket)
        Me.PanelTicket.Controls.Add(Me.Label9)
        Me.PanelTicket.Controls.Add(Me.Label10)
        Me.PanelTicket.Controls.Add(Me.PictureBox2)
        Me.PanelTicket.Controls.Add(Me.PicBarCode)
        Me.PanelTicket.Controls.Add(Me.Label11)
        Me.PanelTicket.Controls.Add(Me.lblTotalTicket)
        Me.PanelTicket.Controls.Add(Me.lblid)
        Me.PanelTicket.Controls.Add(Me.dgvTicket)
        Me.PanelTicket.Location = New System.Drawing.Point(898, 20)
        Me.PanelTicket.Name = "PanelTicket"
        Me.PanelTicket.Size = New System.Drawing.Size(276, 438)
        Me.PanelTicket.TabIndex = 81
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(34, 351)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(209, 20)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "¡Gracias por su compra!"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 152)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(259, 15)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "---------------------------------------------------------------"
        '
        'lblNroTicket
        '
        Me.lblNroTicket.AutoSize = True
        Me.lblNroTicket.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroTicket.Location = New System.Drawing.Point(9, 133)
        Me.lblNroTicket.Name = "lblNroTicket"
        Me.lblNroTicket.Size = New System.Drawing.Size(101, 19)
        Me.lblNroTicket.TabIndex = 59
        Me.lblNroTicket.Text = "N° Ticket: 0"
        '
        'lblFechaTicket
        '
        Me.lblFechaTicket.AutoSize = True
        Me.lblFechaTicket.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaTicket.Location = New System.Drawing.Point(9, 114)
        Me.lblFechaTicket.Name = "lblFechaTicket"
        Me.lblFechaTicket.Size = New System.Drawing.Size(244, 19)
        Me.lblFechaTicket.TabIndex = 58
        Me.lblFechaTicket.Text = "Fecha: dd/mm/aa Hora: 00:00"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(28, 83)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(225, 19)
        Me.Label9.TabIndex = 57
        Me.Label9.Text = "Dorrego 140 - Tel: 491-2293"
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(8, 233)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(259, 15)
        Me.Label10.TabIndex = 56
        Me.Label10.Text = "---------------------------------------------------------------"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Image = Global.Presentacion.My.Resources.Resources.logo_ticket
        Me.PictureBox2.Location = New System.Drawing.Point(43, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(195, 77)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 55
        Me.PictureBox2.TabStop = False
        '
        'PicBarCode
        '
        Me.PicBarCode.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.PicBarCode.BackColor = System.Drawing.Color.Transparent
        Me.PicBarCode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PicBarCode.Location = New System.Drawing.Point(8, 285)
        Me.PicBarCode.Name = "PicBarCode"
        Me.PicBarCode.Size = New System.Drawing.Size(259, 50)
        Me.PicBarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PicBarCode.TabIndex = 54
        Me.PicBarCode.TabStop = False
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(8, 268)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(259, 15)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "---------------------------------------------------------------"
        '
        'lblTotalTicket
        '
        Me.lblTotalTicket.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotalTicket.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalTicket.Location = New System.Drawing.Point(9, 250)
        Me.lblTotalTicket.Name = "lblTotalTicket"
        Me.lblTotalTicket.Size = New System.Drawing.Size(256, 20)
        Me.lblTotalTicket.TabIndex = 4
        Me.lblTotalTicket.Text = "Total: $"
        Me.lblTotalTicket.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblid
        '
        Me.lblid.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblid.Font = New System.Drawing.Font("Averta Regular", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblid.Location = New System.Drawing.Point(8, 328)
        Me.lblid.Name = "lblid"
        Me.lblid.Size = New System.Drawing.Size(259, 24)
        Me.lblid.TabIndex = 1
        Me.lblid.Text = "Id venta"
        Me.lblid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvTicket
        '
        Me.dgvTicket.AllowUserToAddRows = False
        Me.dgvTicket.AllowUserToDeleteRows = False
        Me.dgvTicket.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTicket.BackgroundColor = System.Drawing.Color.White
        Me.dgvTicket.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTicket.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvTicket.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTicket.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvTicket.Location = New System.Drawing.Point(8, 170)
        Me.dgvTicket.Name = "dgvTicket"
        Me.dgvTicket.ReadOnly = True
        Me.dgvTicket.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTicket.Size = New System.Drawing.Size(259, 63)
        Me.dgvTicket.TabIndex = 0
        '
        'PrintDocument1
        '
        '
        'Panaderia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(902, 597)
        Me.Controls.Add(Me.PanelTicket)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.bttCancelar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.bttCobrar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.lblPedido)
        Me.KeyPreview = True
        Me.Name = "Panaderia"
        Me.Text = "Panaderia"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAgregados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.PanelTicket.ResumeLayout(False)
        Me.PanelTicket.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBarCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPedido As Label
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents bttQuitar As Button
    Friend WithEvents txtBuscar As TextBox
    Friend WithEvents lblTotal As Label
    Friend WithEvents dgvProductos As DataGridView
    Friend WithEvents dgvAgregados As DataGridView
    Friend WithEvents Label3 As Label
    Friend WithEvents bttCobrar As Button
    Friend WithEvents rbttVal As RadioButton
    Friend WithEvents txtDescVal As TextBox
    Friend WithEvents rbttPorc As RadioButton
    Friend WithEvents txtDescPorc As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents bttCancelar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents rbCredDeb As RadioButton
    Friend WithEvents rbEfectivo As RadioButton
    Friend WithEvents Label6 As Label
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents bttAgregar As Button
    Friend WithEvents txtCantidad As TextBox
    Friend WithEvents cbKgGrs As ComboBox
    Friend WithEvents lblError As Label
    Friend WithEvents PanelTicket As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents lblNroTicket As Label
    Friend WithEvents lblFechaTicket As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PicBarCode As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents lblTotalTicket As Label
    Friend WithEvents lblid As Label
    Friend WithEvents dgvTicket As DataGridView
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
End Class
