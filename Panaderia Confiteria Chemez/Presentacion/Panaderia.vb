﻿Imports Datos
Imports log4net

Public Class Panaderia


    Dim totalconDesc As Double
    Dim total As Double
    Dim idcaja As Int64
    Dim t As Double
    Dim tipopago As String = "Efectivo"

    ' Log de errores
    Private Shared _logger As ILog = Nothing

    Private Shared ReadOnly Property Logger As log4net.ILog
        Get

            If _logger Is Nothing Then
                _logger = LogManager.GetLogger("file")
                log4net.Config.XmlConfigurator.Configure()
            End If

            Return _logger
        End Get
    End Property


    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function


    Private Sub Panaderia_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim caja As New DatosCajas
        Dim dt As New DataTable
        dt = caja.BuscaCajaAbierta()

        If dt.Rows.Count = 0 Then
            'no hay caja abierta
            Dim result As DialogResult = MessageBox.Show("Antes de comenzar, debe abrir la caja. Presione Aceptar para continuar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            If result = DialogResult.OK Then
                Principal.bttCaja.PerformClick()
            End If
        Else
            'hay caja abierta

            idcaja = dt.Rows(0)("id_caja") 'Cualquier cosa que se agrega se hace debajo de esto
            txtBuscar.Enabled = True
            txtBuscar.Focus()
            txtBuscar.Clear()

        End If


    End Sub

    Public Sub AjustarTabla()
        If dgvProductos.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            dgvProductos.Columns("descripcion").FillWeight = 50
            dgvProductos.Columns("precio").FillWeight = 30
            dgvProductos.Columns("cantidad").FillWeight = 20
            '' Cambiar nombres de encabezados
            dgvProductos.Columns("cantidad").HeaderText = "Cant."
            dgvProductos.Columns("descripcion").HeaderText = "Producto"
            dgvProductos.Columns("venta_por").HeaderText = "Venta por"
            dgvProductos.Columns("precio").HeaderText = "Precio"
            '' Ocultar columnas
            dgvProductos.Columns("id_producto").Visible = False
            dgvProductos.Columns("id_tipo").Visible = False
            dgvProductos.Columns("Stock").Visible = False
            dgvProductos.Columns("venta_por").Visible = False
            dgvProductos.Columns("Tipo").Visible = False
            dgvProductos.Columns("iva").Visible = False
            dgvProductos.Columns("importe_iva").Visible = False
            '' Seleccionar la primera fila
            dgvProductos.Rows(0).Selected = True
            dgvProductos.Select()
        End If
    End Sub

    Public Sub AjustarTablaAgregados()
        If dgvAgregados.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            'dgvAgregados.Columns("Descripción").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvAgregados.Columns("Cantidad/Kg").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            dgvAgregados.Columns("Descripción").FillWeight = 40
            dgvAgregados.Columns("Precio").FillWeight = 20
            dgvAgregados.Columns("Cantidad/Kg").FillWeight = 20
            dgvAgregados.Columns("Subtotal").FillWeight = 20
            '' Cambiar nombres de encabezados
            dgvAgregados.Columns("Cantidad/Kg").HeaderText = "Cant."
            dgvAgregados.Columns("Descripción").HeaderText = "Producto"
            dgvAgregados.Columns("Precio").HeaderText = "Precio"
            dgvAgregados.Columns("Subtotal").HeaderText = "Subtotal"
            '' Ocultar columnas
            dgvAgregados.Columns("Venta_por").Visible = False
            dgvAgregados.Columns("EsStock").Visible = False
            dgvAgregados.Columns("Importe_IVA").Visible = False
            dgvAgregados.Columns("IVA").Visible = False
            '' Seleccionar la primera fila
            'dgvAgregados.Rows(0).Selected = True
            'dgvProductos.Select()
        End If


    End Sub

    Private Sub CrearColumnasAgregados()
        dgvAgregados.ColumnCount = 9
        dgvAgregados.Columns(0).Name = "Id"
        dgvAgregados.Columns(1).Name = "Descripción"
        dgvAgregados.Columns(2).Name = "Precio"
        dgvAgregados.Columns(3).Name = "Cantidad/Kg"
        dgvAgregados.Columns(4).Name = "Venta_por"
        dgvAgregados.Columns(5).Name = "Subtotal"
        dgvAgregados.Columns(6).Name = "EsStock"
        dgvAgregados.Columns(7).Name = "IVA"
        dgvAgregados.Columns(8).Name = "Importe_IVA"

        dgvAgregados.Columns("Id").Visible = False
        dgvAgregados.Columns(3).ReadOnly = False
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Dim prod As New DatosProductos
                Dim dt As New DataTable
                prod._descripcion = txtBuscar.Text
                dgvProductos.DataSource = prod.Buscar(dt)
                AjustarTabla()
                'colorear de gris productos con stock 0
                For Each row As DataGridViewRow In dgvProductos.Rows
                    If Not IsDBNull(row.Cells("cantidad").Value) Then
                        If row.Cells("cantidad").Value = 0 Then
                            row.DefaultCellStyle.BackColor = Color.DarkGray
                        End If
                    End If
                Next

            End If


        Catch ex As Exception
            MessageBox.Show("Error al cargar los datos de productos en ventana panaderia. Detalle: " & ex.Message.ToString)
        End Try


    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave

        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If

    End Sub


    Private Sub dgvProductos_SelectionChanged(sender As Object, e As EventArgs) Handles dgvProductos.SelectionChanged
        If dgvProductos.SelectedRows.Count <> 0 Then

            'Limpiar lblError
            If lblError.Text = "La cantidad ingresada no es correcta" Then
                lblError.Text = ""
            End If

            'me fijo si es kg me deja eligir kg o gramo si es cantidad solo cant
            If dgvProductos.SelectedRows(0).Cells("venta_por").Value = "Kg" Then
                cbKgGrs.Enabled = True
                cbKgGrs.Items.Clear()
                cbKgGrs.Items.Add("Monto")
                cbKgGrs.Items.Add("Gramos")
                cbKgGrs.Items.Add("Kilogramos")
                cbKgGrs.SelectedIndex = 0
            Else
                cbKgGrs.Items.Clear()
                cbKgGrs.Items.Add("Cantidad")
                cbKgGrs.SelectedIndex = 0
                cbKgGrs.Enabled = False
            End If

            Dim total1 As Double
            If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                    Dim idprod As Integer
                    idprod = dgvProductos.CurrentRow.Cells("id_producto").Value
                    total1 = 0

                    'Comprobar que la cantidad agregada no sea mayor a la disponible
                    For Each row As DataGridViewRow In dgvAgregados.Rows

                        If idprod = row.Cells("Id").Value Then

                            total1 = total1 + row.Cells("Cantidad/Kg").Value

                        End If

                    Next

                    If total1 < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                        bttAgregar.Enabled = True
                    Else
                        bttAgregar.Enabled = False
                    End If

                Else
                    bttAgregar.Enabled = False
                End If
            Else
                bttAgregar.Enabled = True
            End If
        End If
    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        Dim b As Integer = 0
        Dim idprod As Integer
        idprod = dgvProductos.CurrentRow.Cells("id_producto").Value

        'Limpiar LblError
        If lblError.Text = "La cantidad ingresada no es correcta" Then
            lblError.Text = ""
        End If

        'en caso de que no sea stock (cant=null) tiene que preguntar si dgvproductos.cantidad es null y que txtcant sea <> de 0
        'en caso de que sea stock(cant=numero) tiene que preguntar si dgvproductos.cantidad >= txtcantidad y que txtcantidad sea <> 0 y que no sea null
        If (IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) And txtCantidad.Text <> 0) OrElse (Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) AndAlso txtCantidad.Text <= dgvProductos.CurrentRow.Cells("cantidad").Value And txtCantidad.Text <> 0) Then
            '' Controla que la tabla agregados no esté vacía
            If dgvAgregados.Rows.Count <> 0 Then

#Region "for each para buscar si existe el producto"
                For Each row As DataGridViewRow In dgvAgregados.Rows
                    If idprod = row.Cells("Id").Value Then '' Si ya está el producto en la tabla agregados
                        If row.Cells("Venta_por").Value <> "Kg" Then
                            'en el caso de que tenga cantidad
                            'si es null es no stock(como el cafe) y si no null es stock (como la torta)
                            If Not IsDBNull(dgvProductos.SelectedRows(0).Cells("cantidad").Value) Then
                                'es no null y es stock
                                'tiene que preguntar que la suma de txtcantidad + row.cellss(caantidad/kg).value <= dgvproductos.cantidad
                                'para no pasasrse de la cantidad existente
                                If (txtCantidad.Text + Double.Parse(row.Cells("Cantidad/Kg").Value) <= dgvProductos.SelectedRows(0).Cells("cantidad").Value) Then
                                    If Trim(txtCantidad.Text) = "" OrElse Trim(txtCantidad.Text) = 0 Then
                                        row.Cells("Cantidad/Kg").Value = row.Cells("Cantidad/Kg").Value + 1
                                        row.Cells("Subtotal").Value = row.Cells("Precio").Value * row.Cells("Cantidad/Kg").Value
                                        row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                        row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                    Else '' Dim mul As Decimal = FormatNumber(1 + (ivaval / 100), 2)
                                        row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + txtCantidad.Text
                                        row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value
                                        row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                        row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                    End If
                                Else
                                    lblError.Text = "La cantidad ingresada no es correcta"
                                End If
                            Else
                                'es null y es no stock
                                If Trim(txtCantidad.Text) = "" OrElse Trim(txtCantidad.Text) = 0 Then
                                    row.Cells("Cantidad/Kg").Value = row.Cells("Cantidad/Kg").Value + 1
                                    row.Cells("Subtotal").Value = row.Cells("Precio").Value * row.Cells("Cantidad/Kg").Value
                                    row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                    row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                Else
                                    row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + txtCantidad.Text
                                    row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value
                                    row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                    row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                End If
                            End If

                        Else
                            'en el caso de que sea gramos
                            If cbKgGrs.Text = "Gramos" Then
                                If Trim(txtCantidad.Text) = "" OrElse Trim(txtCantidad.Text) = 0 Then
                                    row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + 0.1
                                    row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value
                                    row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                    row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                Else
                                    row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + (txtCantidad.Text / 1000)
                                    row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value
                                    row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                    row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                End If
                            ElseIf cbKgGrs.Text = "Kilogramos" Then
                                'en el caso de que sea kilo
                                If Trim(txtCantidad.Text) = "" OrElse Trim(txtCantidad.Text) = 0 Then
                                    row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + 1
                                    row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value
                                    row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                    row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                                End If
                                row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + txtCantidad.Text
                                row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value
                                row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                            ElseIf cbKgGrs.Text = "Monto" Then
                                '    en el caso de que sea monto
                                row.Cells("Cantidad/Kg").Value = FormatNumber((txtCantidad.Text / dgvProductos.SelectedRows(0).Cells("precio").Value), 3) + Double.Parse(row.Cells("Cantidad/Kg").Value)
                                row.Cells("Subtotal").Value = txtCantidad.Text + Double.Parse(row.Cells("Subtotal").Value)
                                row.Cells("IVA").Value = dgvProductos.SelectedRows(0).Cells("iva").Value
                                row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))
                            End If
                        End If
                        b = 1
                        Exit For

                    End If
                Next
#End Region

#Region "en el caso de que el producto no esta agregado(sea uno nuevo)"
                If b = 0 Then
                    If dgvProductos.CurrentRow.Cells("venta_por").Value <> "Kg" Then
                        'en el caso de que sea cantidad 
                        If Trim(txtCantidad.Text) = "" OrElse Trim(txtCantidad.Text) = 0 Then
                            dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                  dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                  dgvProductos.CurrentRow.Cells("precio").Value,
                                                  1,
                                                  dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                 dgvProductos.CurrentRow.Cells("precio").Value,
                                                 dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                                 dgvProductos.CurrentRow.Cells("iva").Value,
                                                 (dgvProductos.CurrentRow.Cells("precio").Value * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                        Else
                            dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                  dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                  Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                  txtCantidad.Text,
                                                  dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                  Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text,
                                                  dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                                  dgvProductos.CurrentRow.Cells("iva").Value,
                                                 ((Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text) * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                        End If
                    Else
                        'EN LA TABLA PRODUCTOS APARECE COMO KG
                        Dim subtotal As Double
                        Dim cantidad As Double
                        If cbKgGrs.Text = "Gramos" Then
                            'en el caso de que sea gramos
                            subtotal = Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * (txtCantidad.Text / 1000)
                            dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                      txtCantidad.Text / 1000,
                                                      dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                      subtotal,
                                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                                       dgvProductos.CurrentRow.Cells("iva").Value,
                                                 (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})

                        ElseIf cbKgGrs.Text = "Kilogramos" Then
                            ' en el caso de que sea kilo
                            subtotal = Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text
                            dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                      txtCantidad.Text, dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                     subtotal,
                                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                                      dgvProductos.CurrentRow.Cells("iva").Value,
                                                 (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                        ElseIf cbKgGrs.Text = "Monto" Then
                            '' en el caso de que sea monto

                            cantidad = FormatNumber((txtCantidad.Text / dgvProductos.SelectedRows(0).Cells("precio").Value), 3)
                            subtotal = txtCantidad.Text
                            dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                          dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                          Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                          cantidad,
                                                          dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                          subtotal,
                                                          dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                                          dgvProductos.CurrentRow.Cells("iva").Value,
                                                 (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                        End If
                    End If
                End If
#End Region

            Else '' Cuando la tabla agregados está vacía. Pasa una sola vez

                CrearColumnasAgregados()
                Dim subtotal As Double
                Dim cantidad As Double
                If dgvProductos.CurrentRow.Cells("venta_por").Value <> "Kg" Then

                    If Trim(txtCantidad.Text) = "" Or Trim(txtCantidad.Text) = 0 Then
                        txtCantidad.Text = 1
                        subtotal = dgvProductos.CurrentRow.Cells("precio").Value

                        dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                              dgvProductos.CurrentRow.Cells("descripcion").Value,
                                              Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                              1,
                                              dgvProductos.CurrentRow.Cells("venta_por").Value,
                                              subtotal,
                                              dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                              dgvProductos.CurrentRow.Cells("iva").Value,
                                              (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                    Else
                        subtotal = Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text
                        dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                  dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                  Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                  txtCantidad.Text,
                                                  dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                  subtotal,
                                                  dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                              dgvProductos.CurrentRow.Cells("iva").Value,
                                              (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                    End If
                Else
                    If cbKgGrs.Text = "Gramos" Then
                        'en el caso de que sea gramos
                        subtotal = Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * (txtCantidad.Text / 1000)
                        dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                      txtCantidad.Text / 1000,
                                                      dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                      subtotal,
                                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                              dgvProductos.CurrentRow.Cells("iva").Value,
                                              (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                    ElseIf cbKgGrs.Text = "Kilogramos" Then

                        ' en el caso de que sea kilo
                        subtotal = Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text
                        dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                      txtCantidad.Text,
                                                      dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                      subtotal,
                                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                              dgvProductos.CurrentRow.Cells("iva").Value,
                                              (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})

                    ElseIf cbKgGrs.Text = "Monto" Then
                        ' en el caso de que sea monto
                        subtotal = txtCantidad.Text
                        cantidad = FormatNumber((txtCantidad.Text / dgvProductos.SelectedRows(0).Cells("precio").Value), 3)
                        dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                      cantidad,
                                                      dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                      subtotal,
                                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString,
                                              dgvProductos.CurrentRow.Cells("iva").Value,
                                              (subtotal * dgvProductos.CurrentRow.Cells("iva").Value) / (100 + dgvProductos.CurrentRow.Cells("iva").Value)})
                    End If

                End If
            End If
        Else
            lblError.Text = "La cantidad ingresada no es correcta"

        End If
        'Verifico que no este agregando mas del stock que tengo
        Dim total2 As Integer

        If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
            If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                'Comprobar que la cantidad agregada no sea mayor a la disponible
                For Each row As DataGridViewRow In dgvAgregados.Rows

                    If idprod = row.Cells("Id").Value Then

                        total2 = total2 + row.Cells("Cantidad/Kg").Value

                    End If

                Next

                If total2 < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                    bttAgregar.Enabled = True
                Else
                    bttAgregar.Enabled = False
                End If

            Else
                bttAgregar.Enabled = False
            End If
        Else
            bttAgregar.Enabled = True

        End If

        ' Calcular total
        total = 0
        For Each row As DataGridViewRow In dgvAgregados.Rows
            'If cbKgGrs.Text = "Monto" Then
            '    total = total + row.Cells("Precio").Value
            'End If
            total = total + row.Cells("Subtotal").Value
        Next
        lblTotal.Text = "Total: $ " & total

        'reinicio el valor de txtcantidad dependiendo si es gramos,cantidad o kilogramos
        If cbKgGrs.Text = "Cantidad" Then
            txtCantidad.Text = 1
        ElseIf cbKgGrs.Text = "Gramos" Then
            txtCantidad.Text = 100
        ElseIf cbKgGrs.Text = "Kilogramos" Then
            txtCantidad.Text = 1
        End If

        bttCobrar.Enabled = True

        'Limpiar el txtCantidad
        txtCantidad.Text = 1

        AjustarTablaAgregados()

        '#Region "Seleccionar ultimo registro del dgvAgregados"
        '        dgvAgregados.ClearSelection()
        '        Dim nRowIndex As Integer = dgvAgregados.Rows.Count - 1
        '        Dim nColumnIndex As Integer = 3
        '        dgvAgregados.Rows(nRowIndex).Selected = True
        '        dgvAgregados.Rows(nRowIndex).Cells(nColumnIndex).Selected = True

        '        'scrollea en caso de q haya scrollbar
        '        dgvAgregados.FirstDisplayedScrollingRowIndex = nRowIndex
        '#End Region

        dgvProductos.Focus()
        dgvAgregados.ClearSelection()

    End Sub


    Private Sub bttQuitar_Click(sender As Object, e As EventArgs) Handles bttQuitar.Click
        t = 0
        dgvAgregados.Rows.Remove(dgvAgregados.CurrentRow)

        For Each row As DataGridViewRow In dgvAgregados.Rows
            t = t + row.Cells("Subtotal").Value
        Next

        dgvProductos.ClearSelection()
        bttAgregar.Enabled = False

        If dgvAgregados.RowCount <> 0 Then
            bttCobrar.Enabled = True
        Else
            bttCobrar.Enabled = False
        End If


        lblTotal.Text = "Total: $ " & t
    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        Dim result As DialogResult = MessageBox.Show("¿Está seguro de que desea cancelar la venta?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        If result = DialogResult.Yes Then

            txtBuscar.Text = "Presione ENTER para buscar"
            dgvProductos.DataSource = Nothing
            txtCantidad.Text = "0"
            dgvAgregados.Rows.Clear()
            dgvAgregados.Columns.Clear()
            bttAgregar.Enabled = False
            bttQuitar.Enabled = False
            bttCobrar.Enabled = False
            lblTotal.Text = "Total: $ 0,00"

            txtBuscar.Focus()
            txtBuscar.SelectAll()
        End If

    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtDescPorc_TextChanged(sender As Object, e As EventArgs) Handles txtDescPorc.TextChanged
        Try
            'Calcular Total
            total = 0
            For Each row As DataGridViewRow In dgvAgregados.Rows
                total = total + row.Cells("subtotal").Value
            Next

            If txtDescPorc.Text <> Trim("") Then

                If txtDescPorc.Text > 100 Then
                    txtDescPorc.Text = 100
                Else

                    totalconDesc = total - (txtDescPorc.Text * total) / 100

                    lblTotal.Text = "Total: $ " & totalconDesc

                End If

            Else
                txtDescPorc.Text = 0
                txtDescPorc.SelectAll()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtDescVal_TextChanged(sender As Object, e As EventArgs) Handles txtDescVal.TextChanged

        Try
            'Calcular SubTotal sin descuento
            total = 0
            For Each row As DataGridViewRow In dgvAgregados.Rows
                total = total + row.Cells("subtotal").Value
            Next
            '------------------------------------------------

            If txtDescVal.Text <= total Then


                totalconDesc = total - txtDescVal.Text

                lblTotal.Text = "Total: $ " & totalconDesc

            Else
                txtDescVal.Text = total
                txtDescVal.SelectAll()

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rbttPorc_CheckedChanged(sender As Object, e As EventArgs) Handles rbttPorc.CheckedChanged
        txtDescVal.Enabled = False
        txtDescPorc.Enabled = True
        txtDescVal.Text = 0
        txtDescPorc.Focus()
    End Sub

    Private Sub rbttVal_CheckedChanged(sender As Object, e As EventArgs) Handles rbttVal.CheckedChanged
        txtDescPorc.Enabled = False
        txtDescVal.Enabled = True
        txtDescPorc.Text = 0
        txtDescVal.Focus()
    End Sub

    Private Sub bttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click

        'crear VENTA
        'Try
        Dim v As New DatosVentas

        v._fecha = Date.Today

        If txtDescPorc.Text = 0 And txtDescVal.Text = 0 Then
            v._descuento = 0
            v._total = total
        Else
            v._total = totalconDesc
            v._descuento = -(total - totalconDesc)
        End If

        v._id_caja = idcaja
        v._id_usuario = SesionUsuario.get_usuario
        v._tipo_venta = "Minorista"

        v._tipo_pago = tipopago

        Dim idventa As Int64 = v.CrearVentaReturnId

        'crear detalles de venta
        For Each row In dgvAgregados.Rows

            Dim dv As New DatosVentas

            dv._descripcion = row.Cells("Descripción").value
            dv._cantidad = row.Cells("Cantidad/Kg").value
            dv._precio_unitario = row.Cells("Precio").value
            If (cbKgGrs.SelectedIndex = 0) Then
                '' Cuando es por monto, para que el subtotal no de con decimales, enviamos el valor ingresado
                dv._subtotal = row.Cells("Subtotal").value
            Else
                '' Cuando es por kg o gr
                dv._subtotal = row.Cells("Cantidad/Kg").value * row.Cells("Precio").value
            End If
            dv._venta_por = "Minorista"
            dv._iva = row.Cells("IVA").value
            dv._importe_iva = row.Cells("Importe_IVA").value
            dv._id_venta = idventa
            dv.CrearDetalleVentas()

            'disminuir stock
            Dim prod As New DatosProductos

            If row.Cells("EsStock").Value <> "" Then 'pregunto, si es stock(osea q tiene una cantidad >= 0) hace el update y disminuye la cantidad del producto, si es no stock (oseaa q tiene una cantidad =NULL) no hace el update
                prod._id_producto = row.Cells("Id").Value

                prod._cantidad = row.Cells("Cantidad/Kg").Value.ToString
                prod.DisminuirStock()

            End If

        Next

        Logger.Info("Venta de panadería realizada. IdVenta: " & idventa.ToString)

        'abrir ventana de facturación

        Dim f As New FacturacionPanaderia(idventa, tipopago, "Venta", 0)

        f.Login = Principal.l
        If f.ShowDialog() = DialogResult.OK Then

        End If

        ' limpiar todo

        total = 0
        totalconDesc = 0

        txtBuscar.Text = "Presione ENTER para buscar"
        dgvProductos.DataSource = Nothing
        txtCantidad.Text = 1
        dgvAgregados.Rows.Clear()
        dgvAgregados.Columns.Clear()
        bttAgregar.Enabled = False
        bttQuitar.Enabled = False
        bttCobrar.Enabled = False
        lblTotal.Text = "Total: $ 0,00"
        ' Catch ex As Exception
        '    MessageBox.Show("Ocurrió un error al crear la venta. Detalle: " & ex.Message.ToString)
        'End Try

        txtBuscar.Focus()
        txtBuscar.SelectAll()

    End Sub

    Private Sub cbKgGrs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbKgGrs.SelectedIndexChanged
        If cbKgGrs.Text = "Cantidad" Then
            txtCantidad.Text = 1
        ElseIf cbKgGrs.Text = "Gramos" Then
            txtCantidad.Text = 100
        ElseIf cbKgGrs.Text = "Kilogramos" Then
            txtCantidad.Text = 1
        ElseIf cbKgGrs.Text = "Monto" Then
            txtCantidad.Text = 0
        End If
    End Sub

    Private Sub txtCantidad_Enter(sender As Object, e As EventArgs) Handles txtCantidad.Enter
        txtCantidad.SelectAll()
    End Sub

    Private Sub txtCantidad_Click(sender As Object, e As EventArgs) Handles txtCantidad.Click
        If lblError.Text = "La cantidad ingresada no es correcta" Then
            lblError.Text = ""
        End If
        txtCantidad.SelectAll()
    End Sub

    Private Sub dgvAgregados_SelectionChanged(sender As Object, e As EventArgs) Handles dgvAgregados.SelectionChanged
        If dgvAgregados.SelectedRows.Count <> 0 Then 'hay algo seleccionado?
            bttQuitar.Enabled = True
        Else
            bttQuitar.Enabled = False
        End If
    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress

        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtCantidad.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If

            End If
        End If

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim bm As New Bitmap(PanelTicket.Width, PanelTicket.Height)

        PanelTicket.DrawToBitmap(bm, New Rectangle(0, 0, PanelTicket.Width, PanelTicket.Height))

        e.Graphics.DrawImage(bm, 0, 0)

        Dim aPS As New PageSetupDialog
        aPS.Document = PrintDocument1
    End Sub

    Private Sub GridSettings()
        dgvTicket.ScrollBars = ScrollBars.None
        dgvTicket.ClearSelection()

        dgvTicket.RowHeadersVisible = False
        dgvTicket.ColumnCount = 4

        dgvTicket.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvTicket.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvTicket.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'DataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal
        dgvTicket.CellBorderStyle = DataGridViewCellBorderStyle.None

    End Sub

    Private Sub resizeDGV()
        dgvTicket.Height = dgvTicket.ColumnHeadersHeight + dgvTicket.Rows.Cast(Of DataGridViewRow).Sum(Function(r) r.Height)

        PanelTicket.Height = dgvTicket.Height + 380
        dgvTicket.ClearSelection()

    End Sub

    Private Sub dgvTicket_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvTicket.RowsAdded
        resizeDGV()
    End Sub

    Private Sub txtCantidad_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCantidad.KeyDown

        ' agregar
        If bttAgregar.Enabled = True Then
            If e.KeyCode = Keys.Enter Then
                bttAgregar.PerformClick()
                txtBuscar.Focus()
            End If

        End If

    End Sub

    Private Sub dgvProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvProductos.KeyDown

        ' agregar
        If e.KeyCode = Keys.Enter Then
            txtCantidad.Focus()
            e.SuppressKeyPress = True
        End If


        ' ir a tabla agregados
        If e.KeyCode = Keys.Right Then
            If dgvAgregados.Rows.Count <> 0 Then
                dgvProductos.ClearSelection()
                dgvAgregados.Rows(0).Selected = True
                dgvAgregados.Focus()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvAgregados_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvAgregados.KeyDown

        ' eliminar todo el producto
        If e.KeyCode = Keys.Delete Then
            If dgvAgregados.SelectedRows.Count <> 0 Then
                bttQuitar.PerformClick()
            End If

            e.SuppressKeyPress = True
        End If

        ' ir a tabla productos
        If e.KeyCode = Keys.Left Then
            If dgvProductos.Rows.Count <> 0 Then
                dgvAgregados.ClearSelection()
                dgvProductos.Rows(0).Selected = True
                dgvProductos.Focus()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub Panaderia_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        ' buscar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.B AndAlso e.Modifiers = Keys.Control Then
            txtBuscar.Focus()
        End If

        ' cobrar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.C AndAlso e.Modifiers = Keys.Control Then
            bttCobrar.PerformClick()
        End If

        ' cancelar venta
        If e.KeyCode = Keys.Escape Then
            bttCancelar.PerformClick()
        End If

        ' abrir ayuda
        If e.KeyCode = Keys.F1 Then
            Dim f As New Ayuda("Panadería")
            f.ShowDialog()
        End If

    End Sub

    Private Sub rbEfectivo_CheckedChanged(sender As Object, e As EventArgs) Handles rbEfectivo.CheckedChanged
        tipopago = "Efectivo"
    End Sub

    Private Sub rbCredDeb_CheckedChanged(sender As Object, e As EventArgs) Handles rbCredDeb.CheckedChanged
        tipopago = "Crédito/Débito"
    End Sub
End Class