﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Principal
    Inherits MetroFramework.Forms.MetroForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.PanelMenu = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelColor = New System.Windows.Forms.Panel()
        Me.bttProductos = New System.Windows.Forms.Button()
        Me.bttCaja = New System.Windows.Forms.Button()
        Me.bttMayorista = New System.Windows.Forms.Button()
        Me.bttInformes = New System.Windows.Forms.Button()
        Me.bttPan = New System.Windows.Forms.Button()
        Me.bttCafe = New System.Windows.Forms.Button()
        Me.bttFacturas = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.bttConfig = New System.Windows.Forms.Button()
        Me.bttSalir = New System.Windows.Forms.Button()
        Me.bttCerrarSesion = New System.Windows.Forms.Button()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.PanelALL = New System.Windows.Forms.Panel()
        Me.bttAyuda = New System.Windows.Forms.Button()
        Me.imagenUsuario = New System.Windows.Forms.PictureBox()
        Me.PanelMenu.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imagenUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelMenu
        '
        Me.PanelMenu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.PanelMenu.Controls.Add(Me.Panel1)
        Me.PanelMenu.Controls.Add(Me.PictureBox1)
        Me.PanelMenu.Controls.Add(Me.bttConfig)
        Me.PanelMenu.Controls.Add(Me.bttSalir)
        Me.PanelMenu.Controls.Add(Me.bttCerrarSesion)
        Me.PanelMenu.Location = New System.Drawing.Point(-3, -4)
        Me.PanelMenu.Name = "PanelMenu"
        Me.PanelMenu.Size = New System.Drawing.Size(239, 733)
        Me.PanelMenu.TabIndex = 29
        Me.PanelMenu.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.Controls.Add(Me.PanelColor)
        Me.Panel1.Controls.Add(Me.bttProductos)
        Me.Panel1.Controls.Add(Me.bttCaja)
        Me.Panel1.Controls.Add(Me.bttMayorista)
        Me.Panel1.Controls.Add(Me.bttInformes)
        Me.Panel1.Controls.Add(Me.bttPan)
        Me.Panel1.Controls.Add(Me.bttCafe)
        Me.Panel1.Controls.Add(Me.bttFacturas)
        Me.Panel1.Location = New System.Drawing.Point(0, 169)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(239, 417)
        Me.Panel1.TabIndex = 0
        '
        'PanelColor
        '
        Me.PanelColor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.PanelColor.Location = New System.Drawing.Point(3, 15)
        Me.PanelColor.Name = "PanelColor"
        Me.PanelColor.Size = New System.Drawing.Size(5, 50)
        Me.PanelColor.TabIndex = 26
        Me.PanelColor.Visible = False
        '
        'bttProductos
        '
        Me.bttProductos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttProductos.FlatAppearance.BorderSize = 0
        Me.bttProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttProductos.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttProductos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttProductos.Image = Global.Presentacion.My.Resources.Resources.productos
        Me.bttProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttProductos.Location = New System.Drawing.Point(3, 295)
        Me.bttProductos.Name = "bttProductos"
        Me.bttProductos.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttProductos.Size = New System.Drawing.Size(236, 50)
        Me.bttProductos.TabIndex = 5
        Me.bttProductos.Text = "   Productos"
        Me.bttProductos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttProductos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttProductos.UseVisualStyleBackColor = True
        Me.bttProductos.Visible = False
        '
        'bttCaja
        '
        Me.bttCaja.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttCaja.FlatAppearance.BorderSize = 0
        Me.bttCaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCaja.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCaja.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttCaja.Image = Global.Presentacion.My.Resources.Resources.caja
        Me.bttCaja.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttCaja.Location = New System.Drawing.Point(3, 183)
        Me.bttCaja.Name = "bttCaja"
        Me.bttCaja.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttCaja.Size = New System.Drawing.Size(236, 50)
        Me.bttCaja.TabIndex = 3
        Me.bttCaja.Text = "   Caja"
        Me.bttCaja.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttCaja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCaja.UseVisualStyleBackColor = True
        Me.bttCaja.Visible = False
        '
        'bttMayorista
        '
        Me.bttMayorista.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttMayorista.FlatAppearance.BorderSize = 0
        Me.bttMayorista.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttMayorista.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttMayorista.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttMayorista.Image = Global.Presentacion.My.Resources.Resources.ventas
        Me.bttMayorista.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttMayorista.Location = New System.Drawing.Point(3, 127)
        Me.bttMayorista.Name = "bttMayorista"
        Me.bttMayorista.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttMayorista.Size = New System.Drawing.Size(236, 50)
        Me.bttMayorista.TabIndex = 2
        Me.bttMayorista.Text = "   Venta Mayorista"
        Me.bttMayorista.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttMayorista.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttMayorista.UseVisualStyleBackColor = True
        Me.bttMayorista.Visible = False
        '
        'bttInformes
        '
        Me.bttInformes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttInformes.FlatAppearance.BorderSize = 0
        Me.bttInformes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttInformes.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttInformes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttInformes.Image = Global.Presentacion.My.Resources.Resources.informes
        Me.bttInformes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttInformes.Location = New System.Drawing.Point(3, 239)
        Me.bttInformes.Name = "bttInformes"
        Me.bttInformes.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttInformes.Size = New System.Drawing.Size(236, 50)
        Me.bttInformes.TabIndex = 6
        Me.bttInformes.Text = "   Informes"
        Me.bttInformes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttInformes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttInformes.UseVisualStyleBackColor = True
        Me.bttInformes.Visible = False
        '
        'bttPan
        '
        Me.bttPan.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttPan.FlatAppearance.BorderSize = 0
        Me.bttPan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttPan.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttPan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttPan.Image = Global.Presentacion.My.Resources.Resources.pan
        Me.bttPan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttPan.Location = New System.Drawing.Point(3, 71)
        Me.bttPan.Name = "bttPan"
        Me.bttPan.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttPan.Size = New System.Drawing.Size(236, 50)
        Me.bttPan.TabIndex = 1
        Me.bttPan.Text = "   Panadería"
        Me.bttPan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttPan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttPan.UseVisualStyleBackColor = True
        Me.bttPan.Visible = False
        '
        'bttCafe
        '
        Me.bttCafe.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttCafe.FlatAppearance.BorderSize = 0
        Me.bttCafe.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCafe.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCafe.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttCafe.Image = Global.Presentacion.My.Resources.Resources.cafe2
        Me.bttCafe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttCafe.Location = New System.Drawing.Point(3, 15)
        Me.bttCafe.Name = "bttCafe"
        Me.bttCafe.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttCafe.Size = New System.Drawing.Size(236, 50)
        Me.bttCafe.TabIndex = 0
        Me.bttCafe.Text = "   Cafetería"
        Me.bttCafe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttCafe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCafe.UseVisualStyleBackColor = False
        Me.bttCafe.Visible = False
        '
        'bttFacturas
        '
        Me.bttFacturas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttFacturas.FlatAppearance.BorderSize = 0
        Me.bttFacturas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttFacturas.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttFacturas.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttFacturas.Image = Global.Presentacion.My.Resources.Resources.facturas
        Me.bttFacturas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttFacturas.Location = New System.Drawing.Point(3, 351)
        Me.bttFacturas.Name = "bttFacturas"
        Me.bttFacturas.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttFacturas.Size = New System.Drawing.Size(236, 50)
        Me.bttFacturas.TabIndex = 4
        Me.bttFacturas.Text = "   Facturas"
        Me.bttFacturas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttFacturas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttFacturas.UseVisualStyleBackColor = True
        Me.bttFacturas.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.Presentacion.My.Resources.Resources.logo
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(26, 22)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(195, 100)
        Me.PictureBox1.TabIndex = 43
        Me.PictureBox1.TabStop = False
        '
        'bttConfig
        '
        Me.bttConfig.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttConfig.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttConfig.FlatAppearance.BorderSize = 0
        Me.bttConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttConfig.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttConfig.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttConfig.Image = Global.Presentacion.My.Resources.Resources.settings
        Me.bttConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttConfig.Location = New System.Drawing.Point(3, 621)
        Me.bttConfig.Name = "bttConfig"
        Me.bttConfig.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttConfig.Size = New System.Drawing.Size(236, 30)
        Me.bttConfig.TabIndex = 0
        Me.bttConfig.Text = "    Configuración"
        Me.bttConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttConfig.UseVisualStyleBackColor = True
        Me.bttConfig.Visible = False
        '
        'bttSalir
        '
        Me.bttSalir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttSalir.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttSalir.FlatAppearance.BorderSize = 0
        Me.bttSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttSalir.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttSalir.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttSalir.Image = Global.Presentacion.My.Resources.Resources.close
        Me.bttSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttSalir.Location = New System.Drawing.Point(3, 687)
        Me.bttSalir.Name = "bttSalir"
        Me.bttSalir.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttSalir.Size = New System.Drawing.Size(236, 30)
        Me.bttSalir.TabIndex = 2
        Me.bttSalir.Text = "    Salir"
        Me.bttSalir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttSalir.UseVisualStyleBackColor = True
        Me.bttSalir.Visible = False
        '
        'bttCerrarSesion
        '
        Me.bttCerrarSesion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttCerrarSesion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttCerrarSesion.FlatAppearance.BorderSize = 0
        Me.bttCerrarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCerrarSesion.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCerrarSesion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.bttCerrarSesion.Image = Global.Presentacion.My.Resources.Resources.logout
        Me.bttCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttCerrarSesion.Location = New System.Drawing.Point(3, 654)
        Me.bttCerrarSesion.Name = "bttCerrarSesion"
        Me.bttCerrarSesion.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttCerrarSesion.Size = New System.Drawing.Size(236, 30)
        Me.bttCerrarSesion.TabIndex = 1
        Me.bttCerrarSesion.Text = "    Cerrar sesión"
        Me.bttCerrarSesion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttCerrarSesion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCerrarSesion.UseVisualStyleBackColor = True
        Me.bttCerrarSesion.Visible = False
        '
        'lblUsuario
        '
        Me.lblUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUsuario.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.lblUsuario.Location = New System.Drawing.Point(881, 60)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(284, 19)
        Me.lblUsuario.TabIndex = 41
        Me.lblUsuario.Text = "Usuario"
        Me.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblUsuario.Visible = False
        '
        'PanelALL
        '
        Me.PanelALL.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelALL.Location = New System.Drawing.Point(264, 93)
        Me.PanelALL.Name = "PanelALL"
        Me.PanelALL.Size = New System.Drawing.Size(983, 594)
        Me.PanelALL.TabIndex = 43
        '
        'bttAyuda
        '
        Me.bttAyuda.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttAyuda.BackgroundImage = Global.Presentacion.My.Resources.Resources.help
        Me.bttAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.bttAyuda.FlatAppearance.BorderSize = 0
        Me.bttAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAyuda.Location = New System.Drawing.Point(1212, 51)
        Me.bttAyuda.Name = "bttAyuda"
        Me.bttAyuda.Size = New System.Drawing.Size(35, 35)
        Me.bttAyuda.TabIndex = 44
        Me.bttAyuda.UseVisualStyleBackColor = True
        Me.bttAyuda.Visible = False
        '
        'imagenUsuario
        '
        Me.imagenUsuario.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.imagenUsuario.BackgroundImage = Global.Presentacion.My.Resources.Resources.user1
        Me.imagenUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.imagenUsuario.Location = New System.Drawing.Point(1171, 52)
        Me.imagenUsuario.Name = "imagenUsuario"
        Me.imagenUsuario.Size = New System.Drawing.Size(35, 35)
        Me.imagenUsuario.TabIndex = 42
        Me.imagenUsuario.TabStop = False
        Me.imagenUsuario.Visible = False
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1279, 728)
        Me.Controls.Add(Me.bttAyuda)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.imagenUsuario)
        Me.Controls.Add(Me.PanelALL)
        Me.Controls.Add(Me.PanelMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(1279, 728)
        Me.Name = "Principal"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.White
        Me.PanelMenu.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imagenUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PanelMenu As Panel
    Friend WithEvents bttCafe As Button
    Friend WithEvents bttInformes As Button
    Friend WithEvents bttPan As Button
    Friend WithEvents bttMayorista As Button
    Friend WithEvents PanelColor As Panel
    Friend WithEvents bttCaja As Button
    Friend WithEvents PanelALL As Panel
    Friend WithEvents bttConfig As Button
    Friend WithEvents bttSalir As Button
    Friend WithEvents bttCerrarSesion As Button
    Friend WithEvents bttProductos As Button
    Friend WithEvents imagenUsuario As PictureBox
    Friend WithEvents lblUsuario As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents bttFacturas As Button
    Friend WithEvents bttAyuda As Button
End Class
