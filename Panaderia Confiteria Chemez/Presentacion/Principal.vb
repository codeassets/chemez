﻿Imports System.Net

Public Class Principal

    Public Forms As New List(Of Form)
    Dim formname As String

    ' para login AFIP
    Public l As LoginAFIP
    'url homologacion
    'Private url As String = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms"

    'url produccion
    Private url As String = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl"

    Private Sub BttCafe_Click(sender As Object, e As EventArgs) Handles bttCafe.Click
        PanelColor.Location = New Point(3, 15)
        bttCafe.Image = My.Resources.cafe2
        bttPan.Image = My.Resources.pan
        bttCaja.Image = My.Resources.caja
        bttMayorista.Image = My.Resources.ventas
        bttProductos.Image = My.Resources.productos
        bttInformes.Image = My.Resources.informes
        bttFacturas.Image = My.Resources.facturas

        bttCafe.BackColor = Color.FromArgb(239, 242, 245)
        bttPan.BackColor = Color.FromArgb(231, 234, 237)
        bttCaja.BackColor = Color.FromArgb(231, 234, 237)
        bttMayorista.BackColor = Color.FromArgb(231, 234, 237)
        bttProductos.BackColor = Color.FromArgb(231, 234, 237)
        bttInformes.BackColor = Color.FromArgb(231, 234, 237)
        bttFacturas.BackColor = Color.FromArgb(231, 234, 237)

        formname = "Cafetería"

        PanelALL.Controls.Clear()
        Dim f As New prueba With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttPan_Click(sender As Object, e As EventArgs) Handles bttPan.Click
        PanelColor.Location = New Point(3, 71)
        bttCafe.Image = My.Resources.cafe
        bttPan.Image = My.Resources.pan2
        bttCaja.Image = My.Resources.caja
        bttMayorista.Image = My.Resources.ventas
        bttProductos.Image = My.Resources.productos
        bttInformes.Image = My.Resources.informes
        bttFacturas.Image = My.Resources.facturas

        bttCafe.BackColor = Color.FromArgb(231, 234, 237)
        bttPan.BackColor = Color.FromArgb(239, 242, 245)
        bttCaja.BackColor = Color.FromArgb(231, 234, 237)
        bttMayorista.BackColor = Color.FromArgb(231, 234, 237)
        bttProductos.BackColor = Color.FromArgb(231, 234, 237)
        bttInformes.BackColor = Color.FromArgb(231, 234, 237)
        bttFacturas.BackColor = Color.FromArgb(231, 234, 237)

        formname = "Panadería"

        PanelALL.Controls.Clear()
        Dim f As New Panaderia With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttCaja_Click(sender As Object, e As EventArgs) Handles bttCaja.Click
        PanelColor.Location = New Point(3, 183)
        bttCafe.Image = My.Resources.cafe
        bttPan.Image = My.Resources.pan
        bttCaja.Image = My.Resources.caja2
        bttMayorista.Image = My.Resources.ventas
        bttProductos.Image = My.Resources.productos
        bttInformes.Image = My.Resources.informes
        bttFacturas.Image = My.Resources.facturas

        bttCafe.BackColor = Color.FromArgb(231, 234, 237)
        bttPan.BackColor = Color.FromArgb(231, 234, 237)
        bttCaja.BackColor = Color.FromArgb(239, 242, 245)
        bttMayorista.BackColor = Color.FromArgb(231, 234, 237)
        bttProductos.BackColor = Color.FromArgb(231, 234, 237)
        bttInformes.BackColor = Color.FromArgb(231, 234, 237)
        bttFacturas.BackColor = Color.FromArgb(231, 234, 237)

        formname = "Caja"

        PanelALL.Controls.Clear()
        Dim f As New Caja With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttVentas_Click(sender As Object, e As EventArgs) Handles bttMayorista.Click
        PanelColor.Location = New Point(3, 127)
        bttCafe.Image = My.Resources.cafe
        bttPan.Image = My.Resources.pan
        bttCaja.Image = My.Resources.caja
        bttMayorista.Image = My.Resources.ventas2
        bttProductos.Image = My.Resources.productos
        bttInformes.Image = My.Resources.informes
        bttFacturas.Image = My.Resources.facturas

        bttCafe.BackColor = Color.FromArgb(231, 234, 237)
        bttPan.BackColor = Color.FromArgb(231, 234, 237)
        bttCaja.BackColor = Color.FromArgb(231, 234, 237)
        bttMayorista.BackColor = Color.FromArgb(239, 242, 245)
        bttProductos.BackColor = Color.FromArgb(231, 234, 237)
        bttInformes.BackColor = Color.FromArgb(231, 234, 237)
        bttFacturas.BackColor = Color.FromArgb(231, 234, 237)

        formname = "Mayorista"

        PanelALL.Controls.Clear()
        Dim f As New VentaMayorista With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttInformes_Click(sender As Object, e As EventArgs) Handles bttInformes.Click
        PanelColor.Location = New Point(3, 239)
        bttCafe.Image = My.Resources.cafe
        bttPan.Image = My.Resources.pan
        bttCaja.Image = My.Resources.caja
        bttMayorista.Image = My.Resources.ventas
        bttProductos.Image = My.Resources.productos
        bttInformes.Image = My.Resources.informes2
        bttFacturas.Image = My.Resources.facturas

        bttCafe.BackColor = Color.FromArgb(231, 234, 237)
        bttPan.BackColor = Color.FromArgb(231, 234, 237)
        bttCaja.BackColor = Color.FromArgb(231, 234, 237)
        bttMayorista.BackColor = Color.FromArgb(231, 234, 237)
        bttProductos.BackColor = Color.FromArgb(231, 234, 237)
        bttInformes.BackColor = Color.FromArgb(239, 242, 245)
        bttFacturas.BackColor = Color.FromArgb(231, 234, 237)

        formname = "Informes"

        PanelALL.Controls.Clear()
        Dim f As New Informes With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub BttSalir_Click(sender As Object, e As EventArgs) Handles bttSalir.Click
        Me.Close()
    End Sub

    Private Sub BttConfig_Click(sender As Object, e As EventArgs) Handles bttConfig.Click
        Dim f As New Config()
        f.ShowDialog()
    End Sub



    Private Sub BttProductos_Click(sender As Object, e As EventArgs) Handles bttProductos.Click
        PanelColor.Location = New Point(3, 295)
        bttCafe.Image = My.Resources.cafe
        bttPan.Image = My.Resources.pan
        bttCaja.Image = My.Resources.caja
        bttMayorista.Image = My.Resources.ventas
        bttProductos.Image = My.Resources.productos2
        bttInformes.Image = My.Resources.informes
        bttFacturas.Image = My.Resources.facturas

        bttCafe.BackColor = Color.FromArgb(231, 234, 237)
        bttPan.BackColor = Color.FromArgb(231, 234, 237)
        bttCaja.BackColor = Color.FromArgb(231, 234, 237)
        bttMayorista.BackColor = Color.FromArgb(231, 234, 237)
        bttProductos.BackColor = Color.FromArgb(239, 242, 245)
        bttInformes.BackColor = Color.FromArgb(231, 234, 237)
        bttFacturas.BackColor = Color.FromArgb(231, 234, 237)

        formname = "Productos"

        PanelALL.Controls.Clear()
        Dim f As New Productos With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub bttFacturas_Click(sender As Object, e As EventArgs) Handles bttFacturas.Click
        PanelColor.Location = New Point(3, 351)
        bttCafe.Image = My.Resources.cafe
        bttPan.Image = My.Resources.pan
        bttCaja.Image = My.Resources.caja
        bttMayorista.Image = My.Resources.ventas
        bttProductos.Image = My.Resources.productos
        bttInformes.Image = My.Resources.informes
        bttFacturas.Image = My.Resources.facturas1

        bttCafe.BackColor = Color.FromArgb(231, 234, 237)
        bttPan.BackColor = Color.FromArgb(231, 234, 237)
        bttCaja.BackColor = Color.FromArgb(231, 234, 237)
        bttMayorista.BackColor = Color.FromArgb(231, 234, 237)
        bttProductos.BackColor = Color.FromArgb(231, 234, 237)
        bttInformes.BackColor = Color.FromArgb(231, 234, 237)
        bttFacturas.BackColor = Color.FromArgb(239, 242, 245)

        formname = "Facturas"

        PanelALL.Controls.Clear()
        Dim f As New Facturas With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub Principal_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12

        ' Login de usuario
        Dim Login As New Login

        If Login.ShowDialog() = DialogResult.OK Then
            ' Login correcto, mostrar todo lo que el usuario puede ver
            lblUsuario.Text = Datos.SesionUsuario.get_nombre

            If Datos.SesionUsuario.get_tipo = "Administrador" Then

                bttConfig.Visible = True
                bttFacturas.Visible = True

            Else

                bttConfig.Visible = False
                bttFacturas.Visible = False

            End If

            lblUsuario.Visible = True
            bttCafe.Visible = True
            bttPan.Visible = True
            bttMayorista.Visible = True
            bttCaja.Visible = True
            bttProductos.Visible = True
            bttInformes.Visible = True
            bttCerrarSesion.Visible = True
            bttSalir.Visible = True
            bttAyuda.Visible = True
            PanelColor.Visible = True
            PanelMenu.Visible = True
            imagenUsuario.Visible = True
            bttCafe.PerformClick()
            Me.Text = "Florencia Soft"

            ' Comprobar conexión a internet
            Try
                Using client = New WebClient()
                    Using stream = client.OpenRead("http://www.google.com")
                        ' Login AFIP
                        l = New LoginAFIP(My.Settings.def_serv, My.Settings.def_url, My.Settings.def_cert, My.Settings.def_pass)
                        l.hacerLogin()
                    End Using
                End Using
            Catch ex As Exception
                MsgBox("Verifique su conexión a internet. Detalle: " + ex.Message.ToString)
            End Try

        Else
            ' Login fallido, ocultar todo
            Me.Dispose()
        End If


    End Sub

    Private Sub bttCerrarSesion_Click(sender As Object, e As EventArgs) Handles bttCerrarSesion.Click

        lblUsuario.Visible = False
        bttCafe.Visible = False
        bttPan.Visible = False
        bttMayorista.Visible = False
        bttCaja.Visible = False
        bttProductos.Visible = False
        bttInformes.Visible = False
        bttFacturas.Visible = False
        bttConfig.Visible = False
        bttCerrarSesion.Visible = False
        bttSalir.Visible = False
        bttAyuda.Visible = False
        PanelColor.Visible = False
        PanelMenu.Visible = False
        imagenUsuario.Visible = False
        PanelALL.Controls.Clear()
        Me.Text = ""

        Dim Login As New Login

        If Login.ShowDialog() = DialogResult.OK Then
            ' Login correcto, mostrar todo lo que el usuario puede ver
            lblUsuario.Text = Datos.SesionUsuario.get_nombre

            If Datos.SesionUsuario.get_tipo = "Administrador" Then

                bttConfig.Visible = True
                bttFacturas.Visible = True

            Else

                bttConfig.Visible = False
                bttFacturas.Visible = False

            End If

            lblUsuario.Visible = True
            bttCafe.Visible = True
            bttPan.Visible = True
            bttMayorista.Visible = True
            bttCaja.Visible = True
            bttProductos.Visible = True
            bttInformes.Visible = True
            bttCerrarSesion.Visible = True
            bttSalir.Visible = True
            bttAyuda.Visible = True
            PanelColor.Visible = True
            PanelMenu.Visible = True
            imagenUsuario.Visible = True
            bttCafe.PerformClick()
            Me.Text = "Florencia Soft"

        Else
            ' Login fallido, ocultar todo
            Me.Dispose()
        End If

    End Sub

    Private Sub bttAyuda_Click(sender As Object, e As EventArgs) Handles bttAyuda.Click

        Dim f As New Ayuda(formname)
        f.ShowDialog()

    End Sub

    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class