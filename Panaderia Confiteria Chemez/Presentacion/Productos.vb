﻿Imports Datos

Public Class Productos

    Dim opcion As Integer

    Public Sub AjustarTabla()
        If dgvProductos.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            dgvProductos.Columns("descripcion").FillWeight = 45
            dgvProductos.Columns("precio").FillWeight = 15
            dgvProductos.Columns("cantidad").FillWeight = 10
            dgvProductos.Columns("venta_por").FillWeight = 15
            dgvProductos.Columns("tipo").FillWeight = 15
            '' Cambiar nombres de encabezados
            dgvProductos.Columns("descripcion").HeaderText = "Descripción"
            dgvProductos.Columns("precio").HeaderText = "Precio"
            dgvProductos.Columns("cantidad").HeaderText = "Cant."
            dgvProductos.Columns("venta_por").HeaderText = "Venta por"
            '' Ocultar columnas
            dgvProductos.Columns("id_producto").Visible = False
            dgvProductos.Columns("id_tipo").Visible = False
            dgvProductos.Columns("Stock").Visible = False
            dgvProductos.Columns("iva").Visible = False
            dgvProductos.Columns("importe_iva").Visible = False
            '' Seleccionar la primera fila
            dgvProductos.Rows(0).Selected = True
            dgvProductos.Select()
        End If
    End Sub

    Private Sub Llenar_CombosBox_Tipos(ds As DataTable)

        cboxTipo.DataSource = ds
        cboxTipo.DisplayMember = "descripcion"
        cboxTipo.ValueMember = "id_tipo"
        cboxTipo.SelectedIndex = -1

    End Sub

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim prod As New DatosProductos
            Dim dt As New DataTable
            prod._descripcion = txtBuscar.Text
            dgvProductos.DataSource = prod.BuscarProductos(dt)
            AjustarTabla()
        End If
    End Sub

    Private Sub bttGuardarUsuario_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        Dim prod As New DatosProductos

        If opcion = 1 Then ' Agregar producto

            ' Controla que los campos no estén vacíos
            If txtDescripcion.Text <> "" And txtCantidad.Text <> "" And txtPrecio.Text <> "" And cboxTipo.SelectedIndex <> -1 And cboxVentaPor.SelectedIndex <> -1 Then

                Try
                    prod._descripcion = Trim(txtDescripcion.Text)
                    prod._precio = txtPrecio.Text
                    prod._id_tipo = cboxTipo.SelectedValue
                    prod._venta_por = cboxVentaPor.Text

                    If cboxIVA.SelectedIndex = 0 Then ' 21%
                        prod._iva = 21.0
                    ElseIf cboxIVA.SelectedIndex = 1 Then ' 10.5%
                        prod._iva = 10.5
                    End If

                    prod._importe_iva = txtIVA.Text

                    If txtCantidad.Text = "No mantiene stock" Then
                        prod._cantidad = Nothing
                    Else
                        prod._cantidad = txtCantidad.Text
                    End If

                    ' Busca un producto con la misma descripcion
                    prod._descripcion = Trim(txtDescripcion.Text)
                    Dim buscar_descripcion = prod.BuscarDescripcion()

                    If buscar_descripcion.rows.count() > 0 Then

                        ' La descripción ya existe
                        MessageBox.Show("Ya existe un producto con esa descripción, por favor ingrese otra.")

                    Else

                        prod.CrearProducto()

                        MessageBox.Show("Producto agregado exitosamente.")
                        'dgvProductos.DataSource = prod.ObtenerTodos()
                        Dim dt As New DataTable


                        If txtBuscar.Text = "Presione ENTER para buscar" Then
                            prod._descripcion = ""
                        Else
                            prod._descripcion = txtBuscar.Text
                        End If

                        dgvProductos.DataSource = prod.Buscar(dt)

                        AjustarTabla()

                            txtDescripcion.Clear()
                            txtPrecio.Clear()
                            txtCantidad.Clear()
                            txtIVA.Clear()
                            cboxTipo.SelectedIndex = -1
                            cboxVentaPor.SelectedIndex = -1
                            cboxIVA.SelectedIndex = -1

                            bttEliminar.Enabled = False
                            bttModificar.Enabled = False

                            txtDescripcion.Focus()


                        End If

                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al guardar el producto. Detalle: " & ex.Message.ToString)
                End Try

            Else
                MessageBox.Show("Complete todos los campos.")
            End If

        ElseIf opcion = 2 Then 'Modificar producto

            ' Controla que los campos no estén vacíos
            If txtDescripcion.Text <> "" And txtCantidad.Text <> "" And txtPrecio.Text <> "" And cboxTipo.SelectedIndex <> -1 And cboxVentaPor.SelectedIndex <> -1 Then

                Try
                    prod._descripcion = txtDescripcion.Text
                    prod._precio = txtPrecio.Text
                    prod._id_tipo = cboxTipo.SelectedValue
                    prod._venta_por = cboxVentaPor.Text
                    prod._id_producto = dgvProductos.SelectedRows(0).Cells("id_producto").Value

                    If cboxIVA.SelectedIndex = 0 Then ' 21%
                        prod._iva = 21.0
                    ElseIf cboxIVA.SelectedIndex = 1 Then ' 10.5%
                        prod._iva = 10.5
                    End If

                    prod._importe_iva = txtIVA.Text

                    If txtCantidad.Text = "No mantiene stock" Then
                        prod._cantidad = Nothing
                    Else
                        prod._cantidad = txtCantidad.Text
                    End If

                    ' Si la descripción es distinta que la de la tabla, controla que no exista, si no, lo guarda
                    If txtDescripcion.Text <> dgvProductos.SelectedRows(0).Cells("descripcion").Value Then

                        ' Busca un producto con la misma descripcion
                        prod._descripcion = Trim(txtDescripcion.Text)
                        Dim buscar_descripcion = prod.BuscarDescripcion()

                        If buscar_descripcion.rows.count() > 0 Then

                            ' La descripción ya existe
                            MessageBox.Show("Ya existe un producto con esa descripción, por favor ingrese otra.")
                            Return

                        End If

                    End If

                    ' La descripción es la misma
                    prod.ModificarProducto()
                    MessageBox.Show("Producto modificado exitosamente.")

                    txtDescripcion.Clear()
                    txtPrecio.Clear()
                    txtCantidad.Clear()
                    txtIVA.Clear()
                    cboxTipo.SelectedIndex = -1
                    cboxVentaPor.SelectedIndex = -1
                    cboxIVA.SelectedIndex = -1

                    bttCancelar.PerformClick()
                    Dim dt As New DataTable

                    If txtBuscar.Text = "Presione ENTER para buscar" Then
                        prod._descripcion = ""
                    Else
                        prod._descripcion = txtBuscar.Text
                    End If

                    dgvProductos.DataSource = prod.Buscar(dt)

                    AjustarTabla()

                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al guardar el producto. Detalle: " & ex.Message.ToString)
                End Try

            Else
                MessageBox.Show("Complete todos los campos.")
            End If

        End If

    End Sub

    Private Sub Productos_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Dim tipos As New DatosTipos

        Llenar_CombosBox_Tipos(tipos.ObtenerTodo)

        txtBuscar.Focus()
        txtBuscar.Clear()

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False

        txtBuscar.Enabled = False

        txtDescripcion.Clear()
        txtPrecio.Clear()
        txtCantidad.Clear()
        cboxTipo.SelectedIndex = -1
        cboxVentaPor.SelectedIndex = -1

        tlpDatosProducto.Enabled = True

        dgvProductos.Enabled = False
        dgvProductos.ClearSelection()

        txtDescripcion.Focus()

        opcion = 1

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        dgvProductos.Enabled = True

        txtDescripcion.Clear()
        txtCantidad.Clear()
        txtPrecio.Clear()
        txtIVA.Clear()
        cboxTipo.SelectedIndex = -1
        cboxIVA.SelectedIndex = -1
        cboxVentaPor.SelectedIndex = -1
        tlpDatosProducto.Enabled = False

        txtBuscar.Enabled = True

    End Sub

    Private Sub cboxTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxTipo.SelectedIndexChanged

        Dim t As New DatosTipos
        Dim tipo As String

        Try
            t._id_tipo = cboxTipo.SelectedValue
            tipo = t.TraerCampoStock()

            ' Tipo no stock
            If tipo = "0" Then
                txtCantidad.Enabled = False
                txtCantidad.Text = "No mantiene stock"
                ' Tipo stock
            Else
                txtCantidad.Enabled = True
                txtCantidad.Clear()
            End If
        Catch ex As Exception

        End Try



    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False

        txtBuscar.Enabled = False

        tlpDatosProducto.Enabled = True

        dgvProductos.Enabled = False

        txtDescripcion.Focus()

        opcion = 2


    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim prod As New DatosProductos
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este producto?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then

            Try
                prod._id_producto = dgvProductos.SelectedRows(0).Cells("id_producto").Value
                prod.EliminarProducto()

                MessageBox.Show("Producto eliminado exitosamente.")

                txtDescripcion.Clear()
                txtPrecio.Clear()
                txtCantidad.Clear()
                cboxTipo.SelectedIndex = -1
                cboxVentaPor.SelectedIndex = -1

                bttCancelar.PerformClick()
                dgvProductos.DataSource = prod.ObtenerTodos()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un error al intentar eliminar el producto. Detalle: " & ex.Message.ToString)
            End Try

        End If

    End Sub

    Private Sub dgvProductos_SelectionChanged(sender As Object, e As EventArgs) Handles dgvProductos.SelectionChanged

        If dgvProductos.SelectedRows.Count <> 0 Then

            Try

                txtDescripcion.Text = dgvProductos.SelectedRows(0).Cells("descripcion").Value
                cboxTipo.SelectedValue = dgvProductos.SelectedRows(0).Cells("id_tipo").Value
                txtPrecio.Text = dgvProductos.SelectedRows(0).Cells("precio").Value

                If Not IsDBNull(dgvProductos.SelectedRows(0).Cells("cantidad").Value) Then
                    txtCantidad.Text = dgvProductos.SelectedRows(0).Cells("cantidad").Value
                Else
                    txtCantidad.Text = "No mantiene stock"
                End If

                If dgvProductos.SelectedRows(0).Cells("venta_por").Value = "Cantidad" Then
                    cboxVentaPor.SelectedIndex = 0
                Else
                    cboxVentaPor.SelectedIndex = 1
                End If

                If Not IsDBNull(dgvProductos.SelectedRows(0).Cells("iva").Value) Then
                    If dgvProductos.SelectedRows(0).Cells("iva").Value = "21,00" Then
                        cboxIVA.SelectedIndex = 0
                    ElseIf dgvProductos.SelectedRows(0).Cells("iva").Value = "10,50" Then
                        cboxIVA.SelectedIndex = 1
                    End If
                Else
                        cboxIVA.SelectedIndex = -1
                End If

                If Not IsDBNull(dgvProductos.SelectedRows(0).Cells("importe_iva").Value) Then
                    txtIVA.Text = dgvProductos.SelectedRows(0).Cells("importe_iva").Value
                Else
                    txtIVA.Text = ""
                End If

                bttModificar.Enabled = True
                bttEliminar.Enabled = True

            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
            End Try

        End If

    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress

        ' Verifica que sólo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtPrecio.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If

            End If
        End If

    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress

        ' Verifica que sólo se ingresen números
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub Productos_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        If Principal.Width > 1300 Then
            bttAgregar.Text = "Agregar"
            bttModificar.Text = "Modificar"
            bttEliminar.Text = "Eliminar"
            bttAgregar.Size = New Size(121, 40)
            bttModificar.Size = New Size(121, 40)
            bttEliminar.Size = New Size(121, 40)
        Else
            bttAgregar.Text = ""
            bttModificar.Text = ""
            bttEliminar.Text = ""
            bttAgregar.Size = New Size(40, 40)
            bttModificar.Size = New Size(40, 40)
            bttEliminar.Size = New Size(40, 40)
        End If
    End Sub


    Private Sub Productos_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        ' cancelar
        If e.KeyCode = Keys.Escape Then
            bttCancelar.PerformClick()
            txtBuscar.Focus()
        End If

        ' guardar
        If e.KeyCode = Keys.Enter Then
            bttGuardar.PerformClick()
            txtDescripcion.Focus()
        End If

        ' agregar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.A AndAlso e.Modifiers = Keys.Control Then
            bttAgregar.PerformClick()
        End If

        ' modificar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.M AndAlso e.Modifiers = Keys.Control Then
            bttModificar.PerformClick()
        End If

        ' eliminar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.E AndAlso e.Modifiers = Keys.Control Then
            bttEliminar.PerformClick()
        End If

        ' buscar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.B AndAlso e.Modifiers = Keys.Control Then
            bttCancelar.PerformClick()
            txtBuscar.Focus()
        End If

        ' abrir ayuda
        If e.KeyCode = Keys.F1 Then
            Dim f As New Ayuda("Productos")
            f.ShowDialog()
        End If

    End Sub

    Private Sub cboxIVA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxIVA.SelectedIndexChanged

        If Trim(txtPrecio.Text) <> "" Then
            If cboxIVA.SelectedIndex = 0 Then ' 21%

                txtIVA.Text = FormatNumber((txtPrecio.Text * 21) / 121, 2)

            ElseIf cboxIVA.SelectedIndex = 1 Then ' 10.5%

                txtIVA.Text = FormatNumber((txtPrecio.Text * 10.5) / 110.5, 2)

            End If
        End If

    End Sub

    Private Sub txtPrecio_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio.TextChanged

        If Trim(txtPrecio.Text) <> "" Then
            If cboxIVA.SelectedIndex = 0 Then ' 21%

                txtIVA.Text = FormatNumber((txtPrecio.Text * 21) / 121, 2)

            ElseIf cboxIVA.SelectedIndex = 1 Then ' 10.5%

                txtIVA.Text = FormatNumber((txtPrecio.Text * 10.5) / 110.5, 2)

            End If
        End If

    End Sub

    Private Sub Productos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class