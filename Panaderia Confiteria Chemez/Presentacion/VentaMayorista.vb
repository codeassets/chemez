﻿Imports Datos

Public Class VentaMayorista

    Dim totalconDesc As Double
    Dim total As Double
    Dim idcaja As Int64
    Dim t As Double
    Dim tipopago As String = "Efectivo"

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Public Sub AjustarTabla()
        If dgvProductos.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            'dgvProductos.Columns("descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvProductos.Columns("descripcion").FillWeight = 60
            dgvProductos.Columns("cantidad").FillWeight = 20
            dgvProductos.Columns("precio").FillWeight = 20
            '' Cambiar nombres de encabezados
            dgvProductos.Columns("cantidad").HeaderText = "Cant."
            dgvProductos.Columns("descripcion").HeaderText = "Producto"
            dgvProductos.Columns("precio").HeaderText = "Precio"
            '' Ocultar columnas
            dgvProductos.Columns("id_producto").Visible = False
            dgvProductos.Columns("id_tipo").Visible = False
            dgvProductos.Columns("Stock").Visible = False
            dgvProductos.Columns("venta_por").Visible = False
            dgvProductos.Columns("Tipo").Visible = False
            dgvProductos.Columns("IVA").Visible = False
            dgvProductos.Columns("Importe_IVA").Visible = False
            '' Seleccionar la primera fila
            dgvProductos.Rows(0).Selected = True
            dgvProductos.Select()
        End If
    End Sub
    Public Sub AjustarTablaAgregados()
        dgvAgregados.Columns("Descripción").FillWeight = 40
        dgvAgregados.Columns("Precio Minorista").FillWeight = 15
        dgvAgregados.Columns("Precio Mayorista").FillWeight = 15
        dgvAgregados.Columns("Cantidad/Kg").FillWeight = 10
        dgvAgregados.Columns("Subtotal").FillWeight = 20

    End Sub

    Private Sub Llenar_CombosBox_Clientes(ds As DataTable)

        cboxCliente.DataSource = ds
        cboxCliente.DisplayMember = "nombre"
        cboxCliente.ValueMember = "id_cliente"
        cboxCliente.SelectedIndex = -1

    End Sub

    Private Sub CrearColumnasAgregados()
        dgvAgregados.ColumnCount = 9
        dgvAgregados.Columns(0).Name = "Id"
        dgvAgregados.Columns(1).Name = "Descripción"
        dgvAgregados.Columns(2).Name = "Precio Minorista"
        dgvAgregados.Columns(3).Name = "Precio Mayorista"
        dgvAgregados.Columns(4).Name = "Cantidad/Kg"
        dgvAgregados.Columns(5).Name = "Venta_por"
        dgvAgregados.Columns(6).Name = "Subtotal"
        dgvAgregados.Columns(7).Name = "IVA"
        dgvAgregados.Columns(8).Name = "Importe_IVA"

        dgvAgregados.Columns("Id").Visible = False
        'dgvAgregados.Columns("Cantidad/Kg").Visible = False
        dgvAgregados.Columns("Venta_por").Visible = False
        dgvAgregados.Columns("IVA").Visible = False
        dgvAgregados.Columns("Importe_IVA").Visible = False
        dgvAgregados.Columns(3).ReadOnly = False


    End Sub

    Private Sub VentaMayorista_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Dim cliente As New DatosClientes
        Dim caja As New DatosCajas
        Dim dt As New DataTable

        Try
            Llenar_CombosBox_Clientes(cliente.ObtenerTodo)
            cboxCliente.SelectedIndex = 0
        Catch ex As Exception

        End Try

        dt = caja.BuscaCajaAbierta()

        If dt.Rows.Count = 0 Then
            'no hay caja abierta
            Dim result As DialogResult = MessageBox.Show("Antes de comenzar, debe abrir la caja. Presione Aceptar para continuar.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            If result = DialogResult.OK Then
                Principal.bttCaja.PerformClick()
            End If
        Else
            'hay caja abierta

            idcaja = dt.Rows(0)("id_caja") 'Cualquier cosa que se agrega se hace debajo de esto
            txtBuscar.Enabled = True
            txtBuscar.Focus()
            txtBuscar.Clear()

        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Dim prod As New DatosProductos
                Dim dt As New DataTable
                prod._descripcion = txtBuscar.Text
                dgvProductos.DataSource = prod.Buscar(dt)
                AjustarTabla()
                'colorear de gris productos con stock 0
                For Each row As DataGridViewRow In dgvProductos.Rows
                    If Not IsDBNull(row.Cells("cantidad").Value) Then
                        If row.Cells("cantidad").Value = 0 Then
                            row.DefaultCellStyle.BackColor = Color.DarkGray
                        End If
                    End If
                Next

            End If


        Catch ex As Exception
            MessageBox.Show("Error al cargar los datos de productos en ventana panaderia. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub dgvProductos_SelectionChanged(sender As Object, e As EventArgs) Handles dgvProductos.SelectionChanged

        Try
            If dgvProductos.SelectedRows(0).Cells("venta_por").Value = "Cantidad" Then
                lblCantKg.Text = "Cantidad"
            Else
                lblCantKg.Text = "Kilogramos"
            End If
            txtCantidad.Text = 1
            txtPrecio.Text = dgvProductos.SelectedRows(0).Cells("precio").Value


            Dim total1 As Double

            If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                    Dim idprod As Integer
                    idprod = dgvProductos.CurrentRow.Cells("id_producto").Value
                    total1 = 0



                    If total1 < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                        bttAgregar.Enabled = True
                    Else
                        bttAgregar.Enabled = False
                    End If

                Else
                    bttAgregar.Enabled = False
                End If
            Else
                bttAgregar.Enabled = True
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        Dim b As Integer = 0
        Dim idprod As Integer
        idprod = dgvProductos.CurrentRow.Cells("id_producto").Value

        If dgvAgregados.Rows.Count <> 0 Then


#Region "for each para buscar si existe el producto"
            For Each row As DataGridViewRow In dgvAgregados.Rows
                If idprod = row.Cells("Id").Value Then '' Si ya está el producto en la tabla agregados
                    ' If row.Cells("Venta_por").Value <> "Kg" Then
                    'en el caso de q sea cantidad
                    row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + txtCantidad.Text
                        row.Cells("Precio Mayorista").Value = txtPrecio.Text
                    row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio Mayorista").Value) * row.Cells("Cantidad/Kg").Value
                    row.Cells("Importe_IVA").Value = row.Cells("Subtotal").Value * (row.Cells("IVA").Value) / (100 + (row.Cells("IVA").Value))

                    'Else
                    '    'en el caso de que sea kilo
                    '    row.Cells("Cantidad/Kg").Value = Double.Parse(row.Cells("Cantidad/Kg").Value) + txtCantidad.Text
                    '    row.Cells("Subtotal").Value = Double.Parse(row.Cells("Precio").Value) * row.Cells("Cantidad/Kg").Value

                    ' End If

                    b = 1
                    Exit For

                End If
            Next
#End Region


#Region "en el caso de que el producto no esta agregado(sea uno nuevo)"
            If b = 0 Then
                'If dgvProductos.CurrentRow.Cells("venta_por").Value <> "Kg" Then
                'en el caso de que sea cantidad 
                dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                  dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                  Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                                  txtPrecio.Text,
                                                  txtCantidad.Text,
                                                  dgvProductos.CurrentRow.Cells("venta_por").Value,
                                                  Double.Parse(txtPrecio.Text) * Integer.Parse(txtCantidad.Text),
                                                                                                    dgvProductos.CurrentRow.Cells("IVA").Value,
                                                  FormatNumber((Double.Parse(txtPrecio.Text) * Integer.Parse(txtCantidad.Text)) * dgvProductos.CurrentRow.Cells("IVA").Value / (dgvProductos.CurrentRow.Cells("IVA").Value + 100), 2)
                                      })

                'Else
                '    'EN LA TABLA PRODUCTOS APARECE COMO KG
                '    ' en el caso de que sea kilo
                '    dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                '                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
                '                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                '                                      txtCantidad.Text, dgvProductos.CurrentRow.Cells("venta_por").Value,
                '                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text,
                '                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString})
                ' End If

            End If
#End Region

        Else
            ' Cuando la tabla agregados está vacía. Pasa una sola vez

            CrearColumnasAgregados()
            'If dgvProductos.CurrentRow.Cells("venta_por").Value <> "Kg" Then
            'cuando es cantidad

            dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                              dgvProductos.CurrentRow.Cells("descripcion").Value,
                                              Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
                                              txtPrecio.Text,
                                              txtCantidad.Text,
                                              dgvProductos.CurrentRow.Cells("venta_por").Value,
                                              Double.Parse(txtPrecio.Text) * txtCantidad.Text,
                                              dgvProductos.CurrentRow.Cells("IVA").Value,
                                              FormatNumber((Double.Parse(txtPrecio.Text) * txtCantidad.Text) * dgvProductos.CurrentRow.Cells("IVA").Value / (dgvProductos.CurrentRow.Cells("IVA").Value + 100), 2)})

            'Else   ' en el caso de que sea kilo

            '    dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
            '                                      dgvProductos.CurrentRow.Cells("descripcion").Value,
            '                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value),
            '                                      txtCantidad.Text,
            '                                      dgvProductos.CurrentRow.Cells("venta_por").Value,
            '                                      Double.Parse(dgvProductos.CurrentRow.Cells("precio").Value) * txtCantidad.Text,
            '                                      dgvProductos.CurrentRow.Cells("cantidad").Value.ToString})

            'End If


        End If

            'Verifico que no este agregando mas del stock que tengo
            Dim total2 As Integer

        If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
            If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                'Comprobar que la cantidad agregada no sea mayor a la disponible
                For Each row As DataGridViewRow In dgvAgregados.Rows

                    If idprod = row.Cells("Id").Value Then

                        total2 = total2 + row.Cells("Cantidad/Kg").Value

                    End If

                Next

                If total2 < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                    bttAgregar.Enabled = True
                Else
                    bttAgregar.Enabled = False
                End If

            Else
                bttAgregar.Enabled = False
            End If
        Else
            bttAgregar.Enabled = True

        End If

        ' Calcular total
        total = 0
        For Each row As DataGridViewRow In dgvAgregados.Rows
            total = total + row.Cells("Subtotal").Value
        Next
        lblTotal.Text = "Total: $ " & total

        bttCobrar.Enabled = True

        'Limpiar el txtCantidad
        txtCantidad.Text = 1

        AjustarTablaAgregados()

        dgvProductos.Focus()
        dgvAgregados.ClearSelection()

    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress

        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"

        If dgvProductos.SelectedRows(0).Cells("venta_por").Value = "Kg" Then
            If Asc(e.KeyChar) <> 8 Then
                If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                    If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                        Dim count As Integer = CountCharacter(txtCantidad.Text, e.KeyChar)
                        If count <> 0 Then
                            e.Handled = True
                        End If
                    Else
                        e.Handled = True
                    End If

                End If
            End If
        Else
            'cantidad
            If Asc(e.KeyChar) <> 8 Then
                If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then

                    e.Handled = True


                End If
            End If
        End If


    End Sub

    Private Sub bttQuitar_Click(sender As Object, e As EventArgs) Handles bttQuitar.Click
        t = 0
        dgvAgregados.Rows.Remove(dgvAgregados.CurrentRow)

        For Each row As DataGridViewRow In dgvAgregados.Rows
            t = t + row.Cells("Subtotal").Value
        Next

        dgvProductos.ClearSelection()
        bttAgregar.Enabled = False

        If dgvAgregados.RowCount <> 0 Then
            bttCobrar.Enabled = True
        Else
            bttCobrar.Enabled = False
        End If
        lblTotal.Text = "Total: $ " & t
    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Dim result As DialogResult = MessageBox.Show("¿Está seguro de que desea cancelar la venta?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        If result = DialogResult.Yes Then

            txtBuscar.Text = "Presione ENTER para buscar"
            dgvProductos.DataSource = Nothing
            txtCantidad.Text = "0"
            dgvAgregados.Rows.Clear()
            dgvAgregados.Columns.Clear()
            bttAgregar.Enabled = False
            bttQuitar.Enabled = False
            bttCobrar.Enabled = False
            lblTotal.Text = "Total: $ 0,00"

            txtBuscar.Focus()
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub dgvAgregados_SelectionChanged(sender As Object, e As EventArgs) Handles dgvAgregados.SelectionChanged
        If dgvAgregados.SelectedRows.Count <> 0 Then 'hay algo seleccionado?
            bttQuitar.Enabled = True
        Else
            bttQuitar.Enabled = False
        End If
    End Sub

    Private Sub txtDescPorc_TextChanged(sender As Object, e As EventArgs) Handles txtDescPorc.TextChanged
        Try
            'Calcular Total
            total = 0
            For Each row As DataGridViewRow In dgvAgregados.Rows
                total = total + row.Cells("Subtotal").Value
            Next

            If txtDescPorc.Text <> Trim("") Then

                If txtDescPorc.Text > 100 Then
                    txtDescPorc.Text = 100
                Else

                    totalconDesc = total - (txtDescPorc.Text * total) / 100

                    lblTotal.Text = "Total: $ " & totalconDesc

                End If

            Else
                txtDescPorc.Text = 0
                txtDescPorc.SelectAll()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtDescVal_TextChanged(sender As Object, e As EventArgs) Handles txtDescVal.TextChanged
        Try
            'Calcular SubTotal sin descuento
            total = 0
            For Each row As DataGridViewRow In dgvAgregados.Rows
                total = total + row.Cells("Subtotal").Value
            Next
            '------------------------------------------------

            If txtDescVal.Text <= total Then


                totalconDesc = total - txtDescVal.Text

                lblTotal.Text = "Total: $ " & totalconDesc

            Else
                txtDescVal.Text = total
                txtDescVal.SelectAll()

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rbttVal_CheckedChanged(sender As Object, e As EventArgs) Handles rbttVal.CheckedChanged
        txtDescPorc.Enabled = False
        txtDescVal.Enabled = True
        txtDescPorc.Text = 0
        txtDescVal.Focus()
    End Sub

    Private Sub rbttPorc_CheckedChanged(sender As Object, e As EventArgs) Handles rbttPorc.CheckedChanged
        txtDescVal.Enabled = False
        txtDescPorc.Enabled = True
        txtDescVal.Text = 0
        txtDescPorc.Focus()
    End Sub

    Private Sub bttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click

        'crear VENTA
        Try
                Dim v As New DatosVentas

                v._fecha = Date.Today
                If txtDescPorc.Text = 0 And txtDescVal.Text = 0 Then
                    v._descuento = 0
                    v._total = total
                Else
                    v._total = totalconDesc
                    v._descuento = -(total - totalconDesc)
                End If
                v._id_caja = idcaja
                v._id_usuario = SesionUsuario.get_usuario
                v._tipo_venta = "Mayorista"
                v._tipo_pago = tipopago
                v._id_cliente = cboxCliente.SelectedValue

                Dim idventa As Int64 = v.CrearVentaReturnId


                'crear detalles de pedido
                For Each row In dgvAgregados.Rows

                    Dim dv As New DatosVentas

                    dv._descripcion = row.Cells("Descripción").value
                    dv._cantidad = row.Cells("Cantidad/Kg").value
                    dv._precio_unitario = row.Cells("Precio Mayorista").value
                    dv._subtotal = row.Cells("Cantidad/Kg").value * row.Cells("Precio Mayorista").value
                dv._venta_por = "Mayorista"
                dv._importe_iva = row.Cells("Importe_IVA").value
                dv._iva = row.Cells("IVA").value
                dv._id_venta = idventa
                    dv.CrearDetalleVentas()


                Next

                'abrir ventana de facturación

                Dim f As New FacturacionPanaderia(idventa, tipopago, "Venta Mayorista", cboxCliente.SelectedValue)

                f.Login = Principal.l
                If f.ShowDialog() = DialogResult.OK Then

                End If

                txtBuscar.Text = "Presione ENTER para buscar"
                dgvProductos.DataSource = Nothing
                txtCantidad.Text = 1
                dgvAgregados.Rows.Clear()
                dgvAgregados.Columns.Clear()
                bttAgregar.Enabled = False
                bttQuitar.Enabled = False
                bttCobrar.Enabled = False
                lblTotal.Text = "Total: $ 0,00"
            Catch ex As Exception
                MessageBox.Show("Ocurrió un error al crear la venta. Detalle: " & ex.Message.ToString)
            End Try

    End Sub

    Private Sub txtCantidad_Click(sender As Object, e As EventArgs) Handles txtCantidad.Click
        txtCantidad.SelectAll()
    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub bttAgregarCliente_Click(sender As Object, e As EventArgs) Handles bttAgregarCliente.Click

        Dim f As New AgregarCliente()

        If f.ShowDialog() = DialogResult.OK Then
            Dim cliente As New DatosClientes
            Llenar_CombosBox_Clientes(cliente.ObtenerTodo)
            cboxCliente.SelectedIndex = 0
        End If

    End Sub

    Private Sub txtCantidad_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCantidad.KeyDown

        If e.KeyCode = Keys.Enter Then
            txtPrecio.Focus()
        End If

        '' escape
        'If e.KeyCode = Keys.Escape Then
        '    dgvProductos.Focus()
        'End If
    End Sub

    Private Sub VentaMayorista_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        ' buscar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.B AndAlso e.Modifiers = Keys.Control Then
            txtBuscar.Focus()
        End If

        ' cobrar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.C AndAlso e.Modifiers = Keys.Control Then
            bttCobrar.PerformClick()
        End If

        ' cancelar venta
        If e.KeyCode = Keys.Escape Then
            bttCancelar.PerformClick()
        End If

        ' abrir ayuda
        If e.KeyCode = Keys.F1 Then
            Dim f As New Ayuda("Mayorista")
            f.ShowDialog()
        End If

    End Sub

    Private Sub dgvAgregados_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvAgregados.KeyDown

        ' eliminar todo el producto
        If e.KeyCode = Keys.Delete Then
            If dgvAgregados.SelectedRows.Count <> 0 Then
                bttQuitar.PerformClick()
            End If

            e.SuppressKeyPress = True
        End If

        ' ir a tabla productos
        If e.KeyCode = Keys.Left Then
            If dgvProductos.Rows.Count <> 0 Then
                dgvAgregados.ClearSelection()
                dgvProductos.Rows(0).Selected = True
                dgvProductos.Focus()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvProductos.KeyDown
        ' agregar
        If e.KeyCode = Keys.Enter Then
            txtCantidad.Focus()
            e.SuppressKeyPress = True
        End If

        ' ir a tabla agregados
        If e.KeyCode = Keys.Right Then
            If dgvAgregados.Rows.Count <> 0 Then
                dgvProductos.ClearSelection()
                dgvAgregados.Rows(0).Selected = True
                dgvAgregados.Focus()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub txtPrecio_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPrecio.KeyDown
        ' agregar
        If e.KeyCode = Keys.Enter Then
            If bttAgregar.Enabled = True Then
                bttAgregar.PerformClick()
                txtBuscar.Focus()
            End If
        End If

        ' escape
        If e.KeyCode = Keys.Escape Then
            dgvProductos.Focus()
        End If
    End Sub

    Private Sub txtPrecio_Click(sender As Object, e As EventArgs) Handles txtPrecio.Click
        txtPrecio.SelectAll()
    End Sub

    Private Sub rbEfectivo_CheckedChanged(sender As Object, e As EventArgs) Handles rbEfectivo.CheckedChanged
        tipopago = "Efectivo"
    End Sub

    Private Sub rbCredDeb_CheckedChanged(sender As Object, e As EventArgs) Handles rbCredDeb.CheckedChanged
        tipopago = "Crédito/Débito"
    End Sub

    Private Sub cboxCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxCliente.SelectedIndexChanged

        If Not (IsNothing(cboxCliente.SelectedItem)) Then
            Dim dv As DataRowView = CType(cboxCliente.SelectedItem, DataRowView)
            Dim direccion As String = CStr(dv.Row("direccion").ToString)
            Dim nombre As String = CStr(dv.Row("nombre").ToString)
            'Dim m1 As Integer = CInt(dv.Row("kID"))
        End If
    End Sub
End Class