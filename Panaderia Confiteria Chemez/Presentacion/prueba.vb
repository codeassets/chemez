﻿Imports Datos
Imports log4net

Public Class prueba
    Dim idmesa As Int64 = 0   ''puede tomar el valor de un id existente (mesa ocupada) o 0 (mesa libre)
    Dim tabladetalles As New DataTable
    Dim idpedido As Long
    Dim idcaja As Long

    ' Log de errores
    Private Shared _logger As ILog = Nothing

    Private Shared ReadOnly Property Logger As log4net.ILog
        Get

            If _logger Is Nothing Then
                _logger = LogManager.GetLogger("file")
                log4net.Config.XmlConfigurator.Configure()
            End If

            Return _logger
        End Get
    End Property

    Public Sub CargarMesasyLimpiarTodo()
        'carga mesas de nuevo
        If dgvMesas.RowCount <> 0 Then
            If cboxTipo.Text = "Libres" Then
                Llenar_DataGrid_Libres()
            ElseIf cboxTipo.Text = "Ocupadas" Then
                Llenar_DataGrid_Ocupadas()
            ElseIf cboxTipo.Text = "Todas" Then
                Llenar_DataGrid_Todos()
            End If
            AjustarTablaMesas()

            'colorear la seleccion
            If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
            End If

            If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.Salmon Then
                dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
            End If
        End If


        'limpiar todo
        dgvMesas.Enabled = True
        'cboxMozos.SelectedIndex = -1

        dgvProductos.Enabled = False
        dgvAgregados.Enabled = False
        bttAgregar.Enabled = False
        bttQuitar.Enabled = False
        bttQuitarUno.Enabled = False
        txtBuscar.Enabled = False
        bttCancelar.Enabled = False
        bttGuardar.Enabled = False
        bttCobrar.Enabled = False

        lblPedido.Text = "Pedido"

        bttCancelarSeleccion.Enabled = False
        cboxMozos.Enabled = True

        lblTotal.Text = "Total: $0,00"
    End Sub
    Public Sub AjustarTabla()
        If dgvProductos.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            'dgvProductos.Columns("descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvProductos.Columns("descripcion").FillWeight = 50
            dgvProductos.Columns("cantidad").FillWeight = 25
            dgvProductos.Columns("precio").FillWeight = 25
            '' Cambiar nombres de encabezados
            dgvProductos.Columns("cantidad").HeaderText = "Cant."
            dgvProductos.Columns("descripcion").HeaderText = "Producto"
            dgvProductos.Columns("precio").HeaderText = "Precio"
            'dgvProductos.Columns("venta_por").HeaderText = "Venta por"
            '' Ocultar columnas
            dgvProductos.Columns("id_producto").Visible = False
            dgvProductos.Columns("id_tipo").Visible = False
            dgvProductos.Columns("Stock").Visible = False
            dgvProductos.Columns("venta_por").Visible = False
            dgvProductos.Columns("Tipo").Visible = False
            dgvProductos.Columns("IVA").Visible = False
            dgvProductos.Columns("Importe_IVA").Visible = False
            '' Seleccionar la primera fila
            dgvProductos.Rows(0).Selected = True
            dgvProductos.Select()
        End If


    End Sub
    Public Sub AjustarTablaAgregadosMesaOcupada()
        If dgvAgregados.Rows.Count <> 0 Then
            '' Ajustar ancho de columnas
            dgvAgregados.Columns("descripcion").FillWeight = 40
            dgvAgregados.Columns("cantidad").FillWeight = 20
            dgvAgregados.Columns("precio_unitario").FillWeight = 20
            dgvAgregados.Columns("subtotal").FillWeight = 20
            '' Cambiar nombres de encabezados
            dgvAgregados.Columns("cantidad").HeaderText = "Cant."
            dgvAgregados.Columns("descripcion").HeaderText = "Producto"
            dgvAgregados.Columns("precio_unitario").HeaderText = "Precio"
            dgvAgregados.Columns("subtotal").HeaderText = "Subtotal"
            '' Ocultar columnas
            dgvAgregados.Columns("id_detalle").Visible = False
            dgvAgregados.Columns("id_pedido").Visible = False
            dgvAgregados.Columns("id_producto").Visible = False
            dgvAgregados.Columns("EsStock").Visible = False
            dgvAgregados.Columns("Accion").Visible = False
            dgvAgregados.Columns("IVA").Visible = False
            dgvAgregados.Columns("Importe_IVA").Visible = False
            '' Seleccionar la primera fila
            'dgvAgregados.Rows(0).Selected = True
            'dgvProductos.Select()
        End If


    End Sub

    Public Sub AjustarTablaMesas()
        If dgvMesas.Rows.Count <> 0 Then
            For Each row As DataGridViewRow In dgvMesas.Rows
                If row.Cells("estado").Value = "Libre" Then
                    row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                ElseIf row.Cells("estado").Value = "Ocupada" Then
                    row.DefaultCellStyle.BackColor = Color.Salmon
                End If
            Next

            '' Cambiar nombres de encabezados
            dgvMesas.Columns("nro_mesa").HeaderText = "Mesa"
            dgvMesas.Columns("estado").HeaderText = "Estado"
            '' Ocultar columnas
            dgvMesas.Columns("id_mesa").Visible = False
            dgvMesas.Columns("id_mozo").Visible = False
            dgvMesas.Columns("estado").Visible = False
        End If


    End Sub

    Private Sub Llenar_DataGrid_Todos()
        Dim mesas As New DatosMesas
        dgvMesas.DataSource = mesas.ObtenerTodas()
    End Sub

    Private Sub Llenar_DataGrid_Libres()
        Dim mesas As New DatosMesas
        dgvMesas.DataSource = mesas.ObtenerLibres()
    End Sub

    Private Sub Llenar_DataGrid_Ocupadas()
        Dim mesas As New DatosMesas
        dgvMesas.DataSource = mesas.ObtenerOcupadas()
    End Sub

    Private Sub Llenar_CombosBox_Mozos(ds As DataTable)
        Try

            cboxMozos.DataSource = ds
            cboxMozos.DisplayMember = "nombre"
            cboxMozos.ValueMember = "id_mozo"
            cboxMozos.SelectedIndex = -1
        Catch ex As Exception
            MessageBox.Show("Ocurrio un Error al cargar el listado de los mozos. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub CrearColumnasAgregados()
        dgvAgregados.ColumnCount = 6
        dgvAgregados.Columns(0).Name = "Id"
        dgvAgregados.Columns(1).Name = "Descripción"
        dgvAgregados.Columns(2).Name = "Precio"
        dgvAgregados.Columns(3).Name = "Cantidad"
        'dgvAgregados.Columns(4).Name = "Guardado"
        dgvAgregados.Columns(4).Name = "IVA"
        dgvAgregados.Columns(5).Name = "Importe_IVA"

        dgvAgregados.Columns("Id").Visible = False
        dgvAgregados.Columns(4).Visible = False
        dgvAgregados.Columns(5).Visible = False
        ' dgvAgregados.Columns("Guardado").Visible = False
        dgvAgregados.Columns(3).ReadOnly = False
    End Sub

    Private Sub cboxTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxTipo.SelectedIndexChanged

        Try

            If cboxTipo.Text = "Libres" Then
                Llenar_DataGrid_Libres()
            ElseIf cboxTipo.Text = "Ocupadas" Then
                Llenar_DataGrid_Ocupadas()
            ElseIf cboxTipo.Text = "Todas" Then
                Llenar_DataGrid_Todos()
            End If

            AjustarTablaMesas()

            If dgvMesas.RowCount <> 0 Then
                'colorear la seleccion
                If dgvMesas.SelectedRows.Count <> 0 Then
                    If dgvMesas.SelectedRows(0).DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                        dgvMesas.SelectedRows(0).DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
                    End If

                    If dgvMesas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Salmon Then
                        dgvMesas.SelectedRows(0).DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Error al traer las mesas. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click

        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If

    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave

        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If

    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown

        If e.KeyCode = Keys.Enter Then
            Dim prod As New DatosProductos
            Dim dt As New DataTable
            prod._descripcion = txtBuscar.Text
            prod._venta_por = "Cantidad"
            dgvProductos.DataSource = prod.BuscarPorVenta_por(dt)
            AjustarTabla()

            'colorear de gris productos con stock 0
            For Each row As DataGridViewRow In dgvProductos.Rows
                If Not IsDBNull(row.Cells("cantidad").Value) Then
                    If row.Cells("cantidad").Value = 0 Then
                        row.DefaultCellStyle.BackColor = Color.DarkGray
                    End If
                End If
            Next
        End If

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        Try
            Dim b As Integer = 0
            Dim idprod As Integer
            idprod = dgvProductos.CurrentRow.Cells("id_producto").Value


            If idmesa = 0 Then
                'Mesa libre,  pedido y detalle NUEVO
#Region "Agregar nuevo pedido"
                '' Controla que la tabla agregados no esté vacía
                If dgvAgregados.Rows.Count <> 0 Then

                    For Each row As DataGridViewRow In dgvAgregados.Rows

                        If idprod = row.Cells("Id").Value Then '' Si ya está el producto en la tabla agregados
                            row.Cells("cantidad").Value = row.Cells("cantidad").Value + 1
                            row.Cells("Importe_IVA").Value = (row.Cells("Precio").Value * row.Cells("Cantidad").Value) * row.Cells("IVA").Value / (row.Cells("IVA").Value + 100)
                            b = 1
                            Exit For

                        End If

                    Next

                    If b = 0 Then

                        dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                              dgvProductos.CurrentRow.Cells("descripcion").Value,
                                              dgvProductos.CurrentRow.Cells("precio").Value,
                                              1, dgvProductos.CurrentRow.Cells("IVA").Value,
                                              dgvProductos.CurrentRow.Cells("Precio").Value * dgvProductos.CurrentRow.Cells("IVA").Value / (dgvProductos.CurrentRow.Cells("IVA").Value + 100)})

                    End If

                Else '' Cuando la tabla agregados está vacía. Pasa una sola vez

                    CrearColumnasAgregados()

                    dgvAgregados.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("id_producto").Value,
                                          dgvProductos.CurrentRow.Cells("descripcion").Value,
                                          dgvProductos.CurrentRow.Cells("precio").Value,
                                          1,
                                          dgvProductos.CurrentRow.Cells("IVA").Value,
                                          dgvProductos.CurrentRow.Cells("Precio").Value * dgvProductos.CurrentRow.Cells("IVA").Value / (dgvProductos.CurrentRow.Cells("IVA").Value + 100)})



                End If


                'Verifico que no este agregando mas del stock que tengo
                Dim total As Integer
                total = 0
                If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                    If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                        'Comprobar que la cantidad agregada no sea mayor a la disponible
                        For Each row As DataGridViewRow In dgvAgregados.Rows

                            If idprod = row.Cells("Id").Value Then

                                total = total + row.Cells("cantidad").Value

                            End If

                        Next

                        If total < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                            bttAgregar.Enabled = True
                        Else
                            bttAgregar.Enabled = False
                        End If

                    Else
                        bttAgregar.Enabled = False
                    End If
                Else
                    bttAgregar.Enabled = True
                End If

                ' Calcular total

                Dim t As Double
                For Each row As DataGridViewRow In dgvAgregados.Rows
                    t = t + (row.Cells("Cantidad").Value * row.Cells("Precio").Value)
                Next

                lblTotal.Text = "Total: $ " & t

#End Region


            Else
                'Mesa ocupada, Agregar MAS detalles
#Region "Modificar Pedido Existente"
                Dim pedido As New DatosPedidos

                idmesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value

                For Each row As DataRow In tabladetalles.Rows

                    If dgvProductos.SelectedRows(0).Cells("descripcion").Value = row("descripcion") Then
                        ' Si ya está el producto en la tabla agregados

                        row("cantidad") = row("cantidad") + 1
                        row("precio_unitario") = dgvProductos.SelectedRows(0).Cells("precio").Value
                        row("subtotal") = row("precio_unitario") * row("cantidad")
                        row("Importe_IVA") = row("subtotal") * row("IVA") / (100 + row("IVA"))

                        If Not IsDBNull(row("Accion")) AndAlso row("Accion") = "A" Then
                            'si sumo una fila mueva sera un Alta y no una modificacion y se mantiene el valor A en el caso de que se sumen mas cantidad
                        Else
                            'sale por este else porq es "B" y como ya se encuentra en la tabla, cambia su valor por "M"
                            row("Accion") = "M"
                        End If

                        b = 1
                        Exit For

                    End If

                Next

                If b = 0 Then
                    'b = 0 significa que no se encontro el producto en la tabladetalle y se agrega una nueva fila con Accion A
                    If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                        tabladetalles.Rows.Add(New String() {0,
                                                                   dgvProductos.CurrentRow.Cells("descripcion").Value,
                                                                   1,
                                                                   dgvProductos.CurrentRow.Cells("precio").Value,
                                                                   dgvProductos.CurrentRow.Cells("precio").Value,
                                                                   idpedido,
                                                                   dgvProductos.CurrentRow.Cells("IVA").Value,
                                                                   dgvProductos.CurrentRow.Cells("precio").Value * dgvProductos.CurrentRow.Cells("Importe_IVA").Value / (dgvProductos.CurrentRow.Cells("Importe_IVA").Value + 100),
                                                                   dgvProductos.CurrentRow.Cells("id_producto").Value,
                                                                   dgvProductos.CurrentRow.Cells("Cantidad").Value,
                                                                   "A"})
                    Else
                        tabladetalles.Rows.Add(New String() {0,
                                           dgvProductos.CurrentRow.Cells("descripcion").Value,
                                           1,
                                           dgvProductos.CurrentRow.Cells("precio").Value,
                                           dgvProductos.CurrentRow.Cells("precio").Value,
                                           idpedido,
                                           dgvProductos.CurrentRow.Cells("IVA").Value,
                                           dgvProductos.CurrentRow.Cells("precio").Value * dgvProductos.CurrentRow.Cells("Importe_IVA").Value / (dgvProductos.CurrentRow.Cells("Importe_IVA").Value + 100),
                                           dgvProductos.CurrentRow.Cells("id_producto").Value,
                                           Nothing,
                                           "A"})
                    End If

                Else

                End If

                'Verifico que no este agregando mas del stock que tengo
                Dim total As Integer
                total = 0
                If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                    If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                        'Comprobar que la cantidad agregada no sea mayor a la disponible
                        For Each row As DataRow In tabladetalles.Rows

                            If dgvProductos.SelectedRows(0).Cells("descripcion").Value = row("descripcion") Then

                                total = total + row("cantidad")

                            End If

                        Next

                        If total < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                            bttAgregar.Enabled = True
                        Else
                            bttAgregar.Enabled = False
                        End If

                    Else
                        bttAgregar.Enabled = False
                    End If
                Else
                    bttAgregar.Enabled = True
                End If

                dgvAgregados.DataSource = tabladetalles

                'Hacer invisible la fila borrar
                dgvAgregados.CurrentCell = Nothing
                For Each r As DataGridViewRow In dgvAgregados.Rows

                    If Not IsDBNull(r.Cells("Accion").Value) AndAlso r.Cells("Accion").Value = "B" Then
                        r.Visible = False
                    Else
                        r.Visible = True
                    End If
                Next
                '----------------------------------------------------------------------------------------------

                AjustarTablaAgregadosMesaOcupada()

                'Calcular Total
                Dim t As Double
                For Each row As DataGridViewRow In dgvAgregados.Rows
                    t = t + row.Cells("subtotal").Value
                Next
                lblTotal.Text = "Total: $ " & t

                bttCobrar.Enabled = False
#End Region

            End If

            bttGuardar.Enabled = True
            dgvAgregados.ClearSelection()

            '#Region "Seleccionar ultimo registro del dgvAgregados"
            '            dgvAgregados.ClearSelection()
            '            Dim nRowIndex As Integer = dgvAgregados.Rows.Count - 1
            '            Dim nColumnIndex As Integer = 3
            '            dgvAgregados.Rows(nRowIndex).Selected = True
            '            dgvAgregados.Rows(nRowIndex).Cells(nColumnIndex).Selected = True

            '            'scrollea en caso de q haya scrollbar
            '            dgvAgregados.FirstDisplayedScrollingRowIndex = nRowIndex
            '#End Region

        Catch ex As Exception
            MessageBox.Show("Ocurrio un Error al intentar Agregar un Producto. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub Cafeteria_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Try


            Dim caja As New DatosCajas
            Dim dt As New DataTable
            dt = caja.BuscaCajaAbierta()

            If dt.Rows.Count = 0 Then
                'no hay caja abierta
                Dim result As DialogResult = MessageBox.Show("Antes de comenzar, debe abrir la caja, presione OK/Aceptar para continuar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                If result = DialogResult.OK Then
                    Principal.bttCaja.PerformClick()
                End If
            Else
                'hay caja abierta

                idcaja = dt.Rows(0)("id_caja")

                Dim mesa As New DatosMesas
                Dim mozos As New DatosMozos

                Llenar_CombosBox_Mozos(mozos.ObtenerTodos)

                cboxTipo.SelectedIndex = 0

                Try
                    If cboxTipo.Text = "Libres" Then
                        Llenar_DataGrid_Libres()
                    ElseIf cboxTipo.Text = "Ocupadas" Then
                        Llenar_DataGrid_Ocupadas()
                    ElseIf cboxTipo.Text = "Todas" Then
                        Llenar_DataGrid_Todos()
                    End If
                    AjustarTablaMesas()
                Catch ex As Exception
                    MessageBox.Show("Ocurrio un Error al cargar las mesas. Detalle: " & ex.Message.ToString)
                End Try
                If dgvMesas.SelectedRows.Count <> 0 Then
                    'colorear la seleccion
                    If dgvMesas.SelectedRows(0).DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                        dgvMesas.SelectedRows(0).DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
                    End If

                    If dgvMesas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Salmon Then
                        dgvMesas.SelectedRows(0).DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un Error al Abrir la ventana Cafeteria. Detalle: " & ex.Message.ToString)
        End Try

        dgvMesas.Focus()

    End Sub



    Private Sub DgvMesas_SelectionChanged(sender As Object, e As EventArgs) Handles dgvMesas.SelectionChanged

        If dgvMesas.RowCount <> 0 Then
            'colorear la seleccion
            If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
            End If

            If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.Salmon Then
                dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
            End If
        End If

    End Sub

    Private Sub BttQuitar_Click(sender As Object, e As EventArgs) Handles bttQuitar.Click
        Try
            Dim t As Double
            If idmesa <> 0 Then

                For Each row As DataRow In tabladetalles.Rows

                    If dgvAgregados.SelectedRows(0).Cells("descripcion").Value = row("descripcion") Then
                        If Not IsDBNull(row("Accion")) AndAlso row("Accion") = "A" Then
                            row.Delete()
                        Else
                            row("Accion") = "B"
                            row("cantidad") = 0
                            row("subtotal") = 0

                        End If

                        dgvAgregados.DataSource = tabladetalles
                        Exit For
                    End If

                Next
                For Each row As DataRow In tabladetalles.Rows
                    t = t + (row("cantidad") * row("precio_unitario"))
                Next

                'Hacer invisible la fila borrar
                dgvAgregados.CurrentCell = Nothing
                For Each r As DataGridViewRow In dgvAgregados.Rows

                    If Not IsDBNull(r.Cells("Accion").Value) AndAlso r.Cells("Accion").Value = "B" Then
                        r.Visible = False
                    End If
                Next
                '----------------------------------------------------------------------------------------------

            Else
                dgvAgregados.Rows.Remove(dgvAgregados.CurrentRow)

                For Each row As DataGridViewRow In dgvAgregados.Rows
                    t = t + (row.Cells("cantidad").Value * row.Cells("Precio").Value)
                Next
            End If

            dgvProductos.ClearSelection()
            bttAgregar.Enabled = False


            lblTotal.Text = "Total: $ " & t


            If dgvAgregados.Rows.GetRowCount(DataGridViewElementStates.Visible) <> 0 Then 'cuenta solamente lasa filas que son visibles
                'si el dgvagregados no esta vacio, se puede guardar
                bttGuardar.Enabled = True
            Else
                'si el dgvagregados esta vacio, no se puede guardar, xq seria ilogico guardar un pedido sin detalles
                bttGuardar.Enabled = False
            End If

            bttCobrar.Enabled = False
        Catch ex As Exception
            MessageBox.Show("Ocurrio un Error al eliminar un producto de la tabla Agregados. Detalle: " & ex.Message.ToString)
        End Try
    End Sub



    Private Sub BttQuitarUno_Click_1(sender As Object, e As EventArgs) Handles bttQuitarUno.Click
        'Si idmesa es distinto de 0 la mesa esta ocupada
        Try
            If idmesa <> 0 Then

                If dgvAgregados.CurrentRow.Cells("cantidad").Value = 1 Then
                    bttQuitarUno.Enabled = False
                Else
                    For Each row As DataRow In tabladetalles.Rows

                        If dgvAgregados.SelectedRows(0).Cells("descripcion").Value = row("descripcion") Then
                            ' Si ya está el producto en la tabla agregados
                            row("cantidad") = row("cantidad") - 1
                            row("subtotal") = row("cantidad") * row("precio_unitario")
                            If Not IsDBNull(row("Accion")) AndAlso row("Accion") = "A" Then
                                'si sumo una fila mueva sera un Alta y no una modificacion y se mantiene el valor A en el caso de que se sumen mas cantidad
                            Else
                                row("Accion") = "M"
                                dgvAgregados.DataSource = tabladetalles
                            End If
                            'If Not IsDBNull(row("Accion")) AndAlso row("Accion") = "B" Then
                            '    row("cantidad") = 0
                            'End If
                            Exit For
                        End If

                    Next
                End If

                Dim t As Double
                For Each row As DataGridViewRow In dgvAgregados.Rows
                    t = t + row.Cells("subtotal").Value
                Next

                lblTotal.Text = "Total: $ " & t

            Else
                If dgvAgregados.CurrentRow.Cells("cantidad").Value = 1 Then
                    bttQuitarUno.Enabled = False
                Else
                    dgvAgregados.SelectedRows(0).Cells("cantidad").Value = dgvAgregados.CurrentRow.Cells("cantidad").Value - 1
                End If

                'calcular total
                Dim t As Double
                For Each row As DataGridViewRow In dgvAgregados.Rows
                    t = t + (row.Cells("Cantidad").Value * row.Cells("Precio").Value)
                Next

                lblTotal.Text = "Total: $ " & t

            End If

            'dgvProductos.ClearSelection()
            'bttAgregar.Enabled = False

            bttGuardar.Enabled = True
            bttCobrar.Enabled = False
        Catch ex As Exception
            MessageBox.Show("Ocurrio un Error al Quitar un producto de la tabla Agregados. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub BttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click
        Try
            If idmesa = 0 Then

                'mesa libre

#Region "Crear nuevo pedido"

                'crear pedido

                Dim p As New DatosPedidos

                p._id_mesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value
                p._fecha = Date.Today
                p._estado = "Abierto"
                p._id_usuario = 0
                p._id_mozo = cboxMozos.SelectedValue
                p._descuento = 0
                p._total = 0
                Try


                    If cboxMozos.SelectedIndex <> -1 Then

                        Dim idpedido As Int64 = p.CrearPedidoReturnId

                        'cambiar estado de mesa
                        Dim mesa As New DatosMesas

                        mesa._id_mesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value
                        mesa._estado = "Ocupada"
                        mesa._id_mozo = cboxMozos.SelectedValue
                        mesa.ActualizarEstadoMesa()

                        'crear detalles de pedido
                        For Each row In dgvAgregados.Rows

                            Dim dp As New DatosPedidos

                            dp._descripcion = row.Cells("Descripción").value
                            dp._cantidad = row.Cells("Cantidad").value
                            dp._precio_unitario = row.Cells("Precio").value
                            dp._subtotal = row.Cells("Cantidad").value * row.Cells("Precio").value
                            dp._importe_iva = row.Cells("Importe_IVA").value
                            dp._iva = row.Cells("IVA").value
                            dp._id_pedido = idpedido
                            dp.CrearDetallePedido()

                        Next
                        MessageBox.Show("Pedido guardado correctamente.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        dgvProductos.DataSource = Nothing
                        dgvAgregados.Rows.Clear()
                        dgvAgregados.Columns.Clear()
#Region "Limpiar"
                        dgvMesas.Enabled = True
                        bttCancelarSeleccion.Enabled = True
                        'cboxMozos.SelectedIndex = -1
                        cboxTipo.Enabled = True
                        lblTotal.Text = "Total: $0,00"





#End Region
#Region "Llenar y colorear dgvMesas"
                        If cboxTipo.Text = "Libres" Then
                            Llenar_DataGrid_Libres()
                        ElseIf cboxTipo.Text = "Ocupadas" Then
                            Llenar_DataGrid_Ocupadas()
                        ElseIf cboxTipo.Text = "Todas" Then
                            Llenar_DataGrid_Todos()
                        End If
                        AjustarTablaMesas()

                        For Each row As DataGridViewRow In dgvMesas.Rows
                            If row.Cells("estado").Value = "Libre" Then
                                row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                            ElseIf row.Cells("estado").Value = "Ocupada" Then
                                row.DefaultCellStyle.BackColor = Color.Salmon
                            End If
                        Next

                        'colorear la seleccion
                        If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                            dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
                        End If

                        If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.Salmon Then
                            dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
                        End If
#End Region
                    Else
                        MessageBox.Show("Para guardar, debe seleccionar un mozo.", "Seleccione Mozo", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        cboxMozos.Focus()
                    End If
                Catch ex As Exception
                    MessageBox.Show("Ocurrio un error al crear Pedido/DetallePedido. Detalle: " & ex.Message.ToString)
                End Try
#End Region

            Else

                'mesa ocupada
                Try
#Region "Modificar pedido creado"
                    For Each row As DataRow In tabladetalles.Rows
                        Dim p As New DatosPedidos

                        If row("id_detalle") = 0 Then
                            ' id_detalle = 0, cuando se agregó un producto nuevo
                            If Not IsDBNull(row("Accion")) AndAlso row("Accion") = "A" Then
                                p._descripcion = row("descripcion")
                                p._cantidad = row("cantidad")
                                p._precio_unitario = row("precio_unitario")
                                p._subtotal = row("subtotal")
                                p._importe_iva = row("Importe_IVA")
                                p._iva = row("IVA")
                                p._id_pedido = idpedido

                                p.CrearDetallePedido()
                            End If
                        Else
                            If Not IsDBNull(row("Accion")) AndAlso row("Accion") = "B" Then
                                p._id_detalle = row("id_detalle")
                                p.EliminarDetallePedido()
                            ElseIf Not IsDBNull(row("Accion")) AndAlso row("Accion") = "M" Then
                                p._cantidad = row("cantidad")
                                p._precio_unitario = row("precio_unitario")
                                p._subtotal = row("subtotal")
                                p._importe_iva = row("Importe_IVA")
                                p._id_detalle = row("id_detalle")

                                p.ModificarDetallePedido()
                            End If
                        End If

                    Next
                    MessageBox.Show("Pedido guardado correctamente.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
#End Region
                Catch ex As Exception
                    MessageBox.Show("Ocurrio un Error al Modificar un Pedido. Detalle: " & ex.Message.ToString)
                End Try

                dgvAgregados.DataSource = Nothing
                dgvProductos.DataSource = Nothing
#Region "Limpiar"
                'Dim pedido As New DatosPedidos

                'pedido._id_mesa = idmesa

                'tabladetalles = pedido.ObtenerDetallePedidos()
                'tabladetalles.Columns.Add("Accion")
                'dgvAgregados.DataSource = tabladetalles


                dgvProductos.Enabled = False

                dgvAgregados.Enabled = False
                bttAgregar.Enabled = False
                bttQuitar.Enabled = False
                bttQuitarUno.Enabled = False
                txtBuscar.Enabled = False
                bttCancelar.Enabled = False
                bttGuardar.Enabled = False
                bttCobrar.Enabled = False

                dgvMesas.Enabled = True
                bttCancelarSeleccion.Enabled = True
                cboxMozos.Enabled = True
                'cboxMozos.SelectedIndex = -1
                cboxTipo.Enabled = True

                lblTotal.Text = "Total: $0,00"

#End Region
#Region "LLenar y colorear dgvMesas"
                If cboxTipo.Text = "Libres" Then
                    Llenar_DataGrid_Libres()
                ElseIf cboxTipo.Text = "Ocupadas" Then
                    Llenar_DataGrid_Ocupadas()
                ElseIf cboxTipo.Text = "Todas" Then
                    Llenar_DataGrid_Todos()
                End If
                AjustarTablaMesas()

                'pinta las filas de colores
                For Each row As DataGridViewRow In dgvMesas.Rows
                    If row.Cells("estado").Value = "Libre" Then
                        row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                    ElseIf row.Cells("estado").Value = "Ocupada" Then
                        row.DefaultCellStyle.BackColor = Color.Salmon
                    End If
                Next

                'colorear la seleccion
                If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                    dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
                End If

                If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.Salmon Then
                    dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
                End If


#End Region

            End If

            lblPedido.Text = "Pedido"

            dgvMesas.Focus()

        Catch ex As Exception
            MessageBox.Show("Ocurrio un Error al intentar Guardar un Pedido. Detalle: " & ex.Message.ToString)
        End Try
    End Sub



    Private Sub BttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        Dim result As DialogResult = MessageBox.Show("¿Está seguro de que desea eliminar el pedido? Se borrarán todos los productos agregados y se desocupará la mesa.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        If result = DialogResult.Yes Then
            Try
                Dim pedido As New DatosPedidos
                pedido._id_pedido = idpedido
                pedido.EliminarPedido()
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al eliminar el pedido idpedido: " & idpedido & ". Detalle: " & ex.Message.ToString)
            End Try
            Try
                For Each row As DataRow In tabladetalles.Rows
                    Dim dp As New DatosPedidos

                    dp._id_detalle = row("id_detalle")
                    dp.EliminarDetallePedido()

                Next
                MessageBox.Show("Pedido cancelado.")
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al eliminar el DetallePedido del pedido idpedido: " & idpedido & ". Detalle: " & ex.Message.ToString)
            End Try
            dgvProductos.DataSource = Nothing
            dgvProductos.Enabled = False
            dgvAgregados.DataSource = Nothing
            dgvAgregados.Enabled = False
            bttAgregar.Enabled = False
            bttQuitar.Enabled = False
            bttQuitarUno.Enabled = False
            txtBuscar.Enabled = False
            bttCancelar.Enabled = False
            bttGuardar.Enabled = False
            bttCobrar.Enabled = False
            lblPedido.Text = "Pedido"

            dgvMesas.Enabled = True
            bttCancelarSeleccion.Enabled = False
            Try
                ' actualizar estado mesa
                Dim mesa As New DatosMesas

                mesa._id_mesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value
                mesa._estado = "Libre"
                mesa._id_mozo = cboxMozos.SelectedValue
                mesa.ActualizarEstadoMesa()
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al actualizar el estado de mesa a Libre. Detalle: " & ex.Message.ToString)
            End Try

            Try
                ' actualizar mesas según el combobox
                If dgvMesas.RowCount <> 0 Then
                    If cboxTipo.Text = "Libres" Then
                        Llenar_DataGrid_Libres()
                    ElseIf cboxTipo.Text = "Ocupadas" Then
                        Llenar_DataGrid_Ocupadas()
                    ElseIf cboxTipo.Text = "Todas" Then
                        Llenar_DataGrid_Todos()
                    End If
                    AjustarTablaMesas()

                    'colorear la seleccion
                    If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
                        dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
                    End If

                    If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.Salmon Then
                        dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al obtener la tabla mesas. Detalle: " & ex.Message.ToString)
            End Try

        End If

        cboxMozos.SelectedIndex = -1

        lblTotal.Text = "Total: $0,00"
    End Sub

    Private Sub DgvMesas_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvMesas.CellMouseDoubleClick

        If dgvMesas.Rows.Count <> 0 Then

            lblPedido.Text = "Pedido - " + dgvMesas.SelectedRows(0).Cells("nro_mesa").Value

            dgvProductos.Enabled = True
            dgvAgregados.Enabled = True
            bttAgregar.Enabled = False
            bttQuitar.Enabled = False
            bttQuitarUno.Enabled = False
            txtBuscar.Enabled = True
            bttCancelar.Enabled = True


            cboxTipo.Enabled = False
            dgvMesas.Enabled = False

            For Each row As DataGridViewRow In dgvMesas.Rows
                row.DefaultCellStyle.BackColor = Color.Gray
            Next

            If dgvAgregados.Columns.Count <> 0 Then
                'Quita las columnas del datagridview 
                dgvAgregados.Columns.Clear()
            End If

            ''pregunta por el estado de la mesa
            If dgvMesas.SelectedRows(0).Cells("estado").Value = "Ocupada" Then
                ''traer todos los datos de mesa
                Dim pedido As New DatosPedidos

                idmesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value
                cboxMozos.SelectedValue = dgvMesas.SelectedRows(0).Cells("id_mozo").Value

                pedido._id_mesa = idmesa
                Try
                    tabladetalles = pedido.ObtenerDetallePedidos()
                    idpedido = tabladetalles.Rows(0).Item("id_pedido")
                    tabladetalles.Columns.Add("Accion")
                    dgvAgregados.DataSource = tabladetalles
                    AjustarTablaAgregadosMesaOcupada()

                    'Calcular Total
                    Dim total As Double
                    For Each row As DataGridViewRow In dgvAgregados.Rows
                        total = total + row.Cells("subtotal").Value
                    Next
                    lblTotal.Text = "Total: $ " & total
                    dgvAgregados.ClearSelection()
                    '-----------------------------------------------------

                    bttCancelar.Enabled = True

                    bttCobrar.Enabled = True
                Catch ex As Exception
                    MessageBox.Show("Ocurrio un Error al traer DetallePedido. Detalle: " & ex.Message.ToString)
                End Try
            Else
                ''Seleccion de mesa libre
                idmesa = 0
                bttCancelar.Enabled = False

                bttCobrar.Enabled = False
            End If

            If idmesa <> 0 Then
                cboxMozos.Enabled = False
            Else
                cboxMozos.Enabled = True
            End If

            bttCancelarSeleccion.Enabled = True

            txtBuscar.Focus()
            txtBuscar.Clear()
        End If

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles bttCancelarSeleccion.Click

        dgvMesas.Enabled = True
        If dgvMesas.Rows.Count <> 0 Then
            For Each row As DataGridViewRow In dgvMesas.Rows
                If row.Cells("estado").Value = "Libre" Then
                    row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                ElseIf row.Cells("estado").Value = "Ocupada" Then
                    row.DefaultCellStyle.BackColor = Color.Salmon
                End If
            Next
            dgvProductos.DataSource = Nothing

            'pregunto si la mesa esta ocupadaa o libre
            If dgvMesas.SelectedRows(0).Cells("estado").Value = "Ocupada" Then
                'mesa ocupada: se limpia asi porq maneja un datatable
                dgvAgregados.DataSource = Nothing

            Else
                'mesa libre: se limpia asi porq agrega filas y columnas directamente al datagridview
                If dgvAgregados.Rows.Count <> 0 Then
                    dgvAgregados.Rows.Clear()
                    dgvAgregados.Columns.Clear()
                End If

            End If


            'cboxMozos.SelectedIndex = -1

            dgvProductos.Enabled = False
            dgvAgregados.Enabled = False
            bttAgregar.Enabled = False
            bttQuitar.Enabled = False
            bttQuitarUno.Enabled = False
            txtBuscar.Enabled = False
            bttCancelar.Enabled = False
            bttGuardar.Enabled = False
            bttCobrar.Enabled = False

            lblPedido.Text = "Pedido"

            bttCancelarSeleccion.Enabled = False
            cboxMozos.Enabled = True
            cboxTipo.Enabled = True

            lblTotal.Text = "Total: $0,00"

        End If





    End Sub


    Private Sub BttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click
        If dgvAgregados.Rows.Count <> 0 Then
            Dim f As New Cobro(idpedido, tabladetalles, dgvMesas.SelectedRows(0).Cells("id_mesa").Value, cboxMozos.SelectedValue, idcaja, cboxMozos.Text)

            f.Login = Principal.l

            'pregunto si Ok(en la otra ventana declare que el boton cobrar es OK) fue presionado
            If f.ShowDialog() = DialogResult.OK Then
                CargarMesasyLimpiarTodo()
                txtBuscar.Focus()
                dgvProductos.DataSource = Nothing
                dgvAgregados.DataSource = Nothing

            Else

            End If
        Else
            MessageBox.Show("No se encontro pedido para cobrar. Por favor seleccione una mesa y vuelva a intentar")
        End If

    End Sub

    Private Sub DgvProductos_SelectionChanged_1(sender As Object, e As EventArgs) Handles dgvProductos.SelectionChanged
        If dgvProductos.SelectedRows.Count <> 0 Then

            Dim idprod, total As Integer
            total = 0

            idprod = dgvProductos.CurrentRow.Cells("id_producto").Value

            If idmesa = 0 Then

#Region "Para agregar en una mesa "
                If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                    If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                        'Comprobar que la cantidad agregada no sea mayor a la disponible
                        For Each row As DataGridViewRow In dgvAgregados.Rows

                            If idprod = row.Cells("Id").Value Then

                                total = total + row.Cells("cantidad").Value

                            End If

                        Next

                        If total < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                            bttAgregar.Enabled = True
                        Else
                            bttAgregar.Enabled = False
                        End If

                    Else
                        bttAgregar.Enabled = False
                    End If
                Else
                    bttAgregar.Enabled = True
                End If
#End Region

            Else

#Region "modificar una mesa"
                If Not IsDBNull(dgvProductos.CurrentRow.Cells("cantidad").Value) Then
                    If dgvProductos.CurrentRow.Cells("cantidad").Value <> 0 Then

                        'Comprobar que la cantidad agregada no sea mayor a la disponible
                        For Each row As DataGridViewRow In dgvAgregados.Rows

                            If dgvProductos.SelectedRows(0).Cells("descripcion").Value = row.Cells("descripcion").Value Then

                                total = total + row.Cells("cantidad").Value

                            End If

                        Next

                        If total < dgvProductos.CurrentRow.Cells("cantidad").Value Then
                            bttAgregar.Enabled = True
                        Else
                            bttAgregar.Enabled = False
                        End If

                    Else
                        bttAgregar.Enabled = False
                    End If
                Else
                    bttAgregar.Enabled = True
                End If
#End Region



            End If



        End If
    End Sub

    Private Sub DgvAgregados_SelectionChanged_1(sender As Object, e As EventArgs) Handles dgvAgregados.SelectionChanged
        If dgvAgregados.RowCount = 0 Then
            bttQuitar.Enabled = False
            bttQuitarUno.Enabled = False
            bttGuardar.Enabled = False
        Else
            If dgvAgregados.SelectedRows.Count <> 0 Then
                If dgvAgregados.SelectedRows(0).Cells("Cantidad").Value = 1 Then
                    'si la fila seleccionada tiene cantidad=1 entonces no se desabilita el boton quitar de a uno
                    bttQuitarUno.Enabled = False
                Else
                    bttQuitarUno.Enabled = True
                End If
                bttQuitar.Enabled = True
            Else
                bttQuitar.Enabled = False
                bttQuitarUno.Enabled = False
            End If


        End If
    End Sub

    Private Sub dgvProductos_Sorted(sender As Object, e As EventArgs) Handles dgvProductos.Sorted
        Try
            If dgvProductos.RowCount <> 0 Then
                'colorear de gris productos con stock 0
                For Each row As DataGridViewRow In dgvProductos.Rows
                    If Not IsDBNull(row.Cells("cantidad").Value) Then
                        If row.Cells("cantidad").Value = 0 Then
                            row.DefaultCellStyle.BackColor = Color.DarkGray
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema durante el ordenamiento de la tabla productos. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub dgvAgregados_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAgregados.CellValueChanged
        If dgvAgregados.RowCount = 0 Then
            bttQuitar.Enabled = False
            bttQuitarUno.Enabled = False
            bttGuardar.Enabled = False
        Else
            If dgvAgregados.SelectedRows.Count <> 0 Then
                If dgvAgregados.SelectedRows(0).Cells("Cantidad").Value = 1 Then
                    'si la fila seleccionada tiene cantidad=1 entonces no se desabilita el boton quitar de a uno
                    bttQuitarUno.Enabled = False
                Else
                    bttQuitarUno.Enabled = True
                End If
                bttQuitar.Enabled = True
            Else
                bttQuitar.Enabled = False
                bttQuitarUno.Enabled = False
            End If


        End If
    End Sub

    Private Sub prueba_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        ' deseleccionar mesa
        If bttCancelarSeleccion.Enabled = True Then
            If e.KeyCode = Keys.Escape Then bttCancelarSeleccion.PerformClick()
        End If

        ' buscar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.B AndAlso e.Modifiers = Keys.Control Then
            txtBuscar.Focus()
        End If

        ' seleccionar mozo
        If (e.KeyCode And Not Keys.Modifiers) = Keys.M AndAlso e.Modifiers = Keys.Control Then
            cboxMozos.Focus()
        End If

        ' guardar mesa
        If (e.KeyCode And Not Keys.Modifiers) = Keys.G AndAlso e.Modifiers = Keys.Control Then
            bttGuardar.PerformClick()
        End If

        ' cobrar mesa
        If (e.KeyCode And Not Keys.Modifiers) = Keys.C AndAlso e.Modifiers = Keys.Control Then
            bttCobrar.PerformClick()
        End If

        ' abrir ayuda
        If e.KeyCode = Keys.F1 Then
            Dim f As New Ayuda("Cafetería")
            f.ShowDialog()
        End If

    End Sub

    Private Sub dgvProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvProductos.KeyDown

        ' agregar producto
        If e.KeyCode = Keys.Enter Then
            If dgvProductos.SelectedRows.Count <> 0 And bttCancelarSeleccion.Enabled = True Then
                bttAgregar.PerformClick()
            End If
            e.SuppressKeyPress = True
        End If

        ' buscar
        If (e.KeyCode And Not Keys.Modifiers) = Keys.B AndAlso e.Modifiers = Keys.Control Then
            txtBuscar.Focus()
        End If

        ' ir a tabla agregados
        If e.KeyCode = Keys.Right Then
            If dgvAgregados.Rows.Count <> 0 Then
                dgvProductos.ClearSelection()
                dgvAgregados.Rows(0).Selected = True
                dgvAgregados.Focus()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvAgregados_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvAgregados.KeyDown

        ' quitar uno
        If e.KeyCode = Keys.Back Then
            If dgvAgregados.SelectedRows.Count <> 0 And bttCancelarSeleccion.Enabled = True Then
                bttQuitarUno.PerformClick()
            End If
            e.SuppressKeyPress = True
        End If

        ' eliminar todo el producto
        If e.KeyCode = Keys.Delete Then
            If dgvAgregados.SelectedRows.Count <> 0 And bttCancelarSeleccion.Enabled = True Then
                bttQuitar.PerformClick()
            End If

            e.SuppressKeyPress = True
        End If

        ' ir a tabla productos
        If e.KeyCode = Keys.Left Then
            If dgvProductos.Rows.Count <> 0 Then
                dgvAgregados.ClearSelection()
                dgvProductos.Rows(0).Selected = True
                dgvProductos.Focus()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvMesas_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvMesas.KeyDown

        If e.KeyCode = Keys.Enter Then
            If dgvMesas.Rows.Count <> 0 Then

                lblPedido.Text = "Pedido - " + dgvMesas.SelectedRows(0).Cells("nro_mesa").Value

                dgvProductos.Enabled = True
                dgvAgregados.Enabled = True
                bttAgregar.Enabled = False
                bttQuitar.Enabled = False
                bttQuitarUno.Enabled = False
                txtBuscar.Enabled = True
                bttCancelar.Enabled = True

                cboxTipo.Enabled = False
                dgvMesas.Enabled = False

                For Each row As DataGridViewRow In dgvMesas.Rows
                    row.DefaultCellStyle.BackColor = Color.Gray
                Next

                If dgvAgregados.Columns.Count <> 0 Then
                    'Quita las columnas del datagridview 
                    dgvAgregados.Columns.Clear()
                End If

                ''pregunta por el estado de la mesa
                If dgvMesas.SelectedRows(0).Cells("estado").Value = "Ocupada" Then
                    ''traer todos los datos de mesa
                    Dim pedido As New DatosPedidos

                    idmesa = dgvMesas.SelectedRows(0).Cells("id_mesa").Value
                    cboxMozos.SelectedValue = dgvMesas.SelectedRows(0).Cells("id_mozo").Value

                    pedido._id_mesa = idmesa
                    Try
                        tabladetalles = pedido.ObtenerDetallePedidos()
                        idpedido = tabladetalles.Rows(0).Item("id_pedido")
                        tabladetalles.Columns.Add("Accion")
                        dgvAgregados.DataSource = tabladetalles
                        AjustarTablaAgregadosMesaOcupada()

                        'Calcular Total
                        Dim total As Double
                        For Each row As DataGridViewRow In dgvAgregados.Rows
                            total = total + row.Cells("subtotal").Value
                        Next
                        lblTotal.Text = "Total: $ " & total
                        dgvAgregados.ClearSelection()
                        '-----------------------------------------------------

                        bttCancelar.Enabled = True

                        bttCobrar.Enabled = True
                    Catch ex As Exception
                        MessageBox.Show("Ocurrio un Error al traer DetallePedido. Detalle: " & ex.Message.ToString)
                    End Try
                Else
                    ''Seleccion de mesa libre
                    idmesa = 0
                    bttCancelar.Enabled = False

                    bttCobrar.Enabled = False
                End If

                If idmesa <> 0 Then
                    cboxMozos.Enabled = False
                Else
                    cboxMozos.Enabled = True
                End If

                bttCancelarSeleccion.Enabled = True

                txtBuscar.Focus()
                txtBuscar.Clear()
            End If
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub dgvMesas_Sorted(sender As Object, e As EventArgs) Handles dgvMesas.Sorted
        AjustarTablaMesas()
        'colorear la seleccion
        If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSpringGreen Then
            dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(51, 201, 144)
        End If

        If dgvMesas.CurrentRow.DefaultCellStyle.BackColor = Color.Salmon Then
            dgvMesas.CurrentRow.DefaultCellStyle.SelectionBackColor = Drawing.Color.FromArgb(201, 128, 120)
        End If
    End Sub
End Class