
USE [Chemez]
GO
/****** Object:  Table [dbo].[Caja]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caja](
	[id_caja] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha_inicio] [datetime] NULL,
	[fecha_fin] [date] NULL,
	[monto_inicial] [decimal](10, 2) NULL,
	[ingreso_total] [decimal](10, 2) NULL,
	[egreso_total] [decimal](10, 2) NULL,
	[total] [decimal](10, 2) NULL,
	[estado] [varchar](100) NULL,
	[id_cajero_abre] [bigint] NOT NULL,
	[id_cajero_cierra] [bigint] NOT NULL,
 CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED 
(
	[id_caja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cajeros]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cajeros](
	[id_cajeros] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[contraseña] [varchar](100) NULL,
 CONSTRAINT [PK_Cajeros] PRIMARY KEY CLUSTERED 
(
	[id_cajeros] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[id_cliente] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[CUIT] [varchar](50) NOT NULL,
	[direccion] [varchar](50) NOT NULL,
	[telefono] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[id_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallePedidos]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallePedidos](
	[id_detalle] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](10, 2) NULL,
	[subtotal] [decimal](10, 2) NULL,
	[id_pedido] [bigint] NOT NULL,
 CONSTRAINT [PK_DetallePedidos] PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleVentas]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleVentas](
	[id_detalle] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](10, 2) NOT NULL,
	[subtotal] [decimal](10, 2) NOT NULL,
	[venta_por] [varchar](50) NOT NULL,
	[id_venta] [bigint] NOT NULL,
 CONSTRAINT [PK_DetalleVentas] PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Egresos]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Egresos](
	[id_egreso] [bigint] IDENTITY(1,1) NOT NULL,
	[id_caja] [bigint] NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[monto] [decimal](18, 2) NOT NULL,
	[fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_Egresos] PRIMARY KEY CLUSTERED 
(
	[id_egreso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mesas]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mesas](
	[id_mesa] [bigint] IDENTITY(1,1) NOT NULL,
	[nro_mesa] [varchar](50) NULL,
	[estado] [varchar](100) NULL,
	[id_mozo] [bigint] NULL,
 CONSTRAINT [PK_Mesas] PRIMARY KEY CLUSTERED 
(
	[id_mesa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mozos]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mozos](
	[id_mozo] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
 CONSTRAINT [PK_Mozos] PRIMARY KEY CLUSTERED 
(
	[id_mozo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pedidos](
	[id_pedido] [bigint] IDENTITY(1,1) NOT NULL,
	[id_mesa] [bigint] NOT NULL,
	[fecha] [date] NOT NULL,
	[estado] [varchar](100) NOT NULL,
	[id_usuario] [bigint] NOT NULL,
	[id_mozo] [bigint] NOT NULL,
	[descuento] [decimal](10, 2) NULL,
	[total] [decimal](10, 2) NULL,
	[id_caja] [bigint] NULL,
	[tipo_pago] [varchar](50) NULL,
 CONSTRAINT [PK_Pedidos] PRIMARY KEY CLUSTERED 
(
	[id_pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[id_producto] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[precio] [decimal](10, 2) NULL,
	[cantidad] [int] NULL,
	[id_tipo] [bigint] NOT NULL,
	[venta_por] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[id_producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipos]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipos](
	[id_tipo] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[stock] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Tipos] PRIMARY KEY CLUSTERED 
(
	[id_tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[contraseña] [varchar](100) NULL,
	[tipo] [varchar](100) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 17/02/2020 0:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ventas](
	[id_venta] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha] [date] NOT NULL,
	[descuento] [decimal](8, 2) NOT NULL,
	[total] [decimal](8, 2) NOT NULL,
	[id_caja] [bigint] NULL,
	[id_usuario] [bigint] NOT NULL,
	[tipo_venta] [varchar](50) NOT NULL,
	[tipo_pago] [varchar](50) NOT NULL,
	[id_cliente] [bigint] NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[id_venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Caja] ON 

GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (5, CAST(0x0000AB6100733147 AS DateTime), CAST(0xBC400B00 AS Date), CAST(1150.00 AS Decimal(10, 2)), CAST(4663.80 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(4663.80 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (6, CAST(0x0000AB610133378A AS DateTime), CAST(0xBC400B00 AS Date), CAST(200.00 AS Decimal(10, 2)), CAST(5293.25 AS Decimal(10, 2)), CAST(-700.00 AS Decimal(10, 2)), CAST(4593.25 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (7, CAST(0x0000AB6200758732 AS DateTime), CAST(0xBD400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(2480.00 AS Decimal(10, 2)), CAST(-5048.00 AS Decimal(10, 2)), CAST(-2568.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (8, CAST(0x0000AB620114F2DE AS DateTime), CAST(0xBE400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(1905.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(1905.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (9, CAST(0x0000AB63008DD3E3 AS DateTime), CAST(0xBE400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(3930.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(3930.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (10, CAST(0x0000AB63011273B8 AS DateTime), CAST(0xBF400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(4215.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(4215.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
SET IDENTITY_INSERT [dbo].[Caja] OFF
GO
SET IDENTITY_INSERT [dbo].[Clientes] ON 

GO
INSERT [dbo].[Clientes] ([id_cliente], [nombre], [CUIT], [direccion], [telefono]) VALUES (1, N'CHACON', N'1111111111111', N'XXXXXX', N'03855040172')
GO
SET IDENTITY_INSERT [dbo].[Clientes] OFF
GO
SET IDENTITY_INSERT [dbo].[DetallePedidos] ON 

GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (1, N'Café mediano', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 1)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (2, N'Medialunas', 2, CAST(15.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), 1)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (3, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 1)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (4, N'Café chico', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 2)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (5, N'Desayuno Tradicional', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 2)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (6, N'Cappuccino', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 3)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (7, N'Facturas Hojaldradas', 2, CAST(25.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 3)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (8, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 3)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (10, N'Café mediano', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 4)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (11, N'Desayuno Protéico', 2, CAST(180.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 5)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (12, N'Desayuno Tradicional med', 2, CAST(70.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 6)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (13, N'Desayuno Tradicional Grand', 3, CAST(100.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 7)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (14, N'Desayuno Light', 3, CAST(160.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), 8)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (15, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 6)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (16, N'Desayuno Tradicional med', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 9)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (17, N'Desayuno Completo', 2, CAST(130.00 AS Decimal(10, 2)), CAST(260.00 AS Decimal(10, 2)), 10)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (18, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 10)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (19, N'Cafe Mediano', 3, CAST(70.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 11)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (20, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 11)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (21, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 12)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (22, N'Desayuno Tradicional chico', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 12)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (23, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 11)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (24, N'Cafe Mediano', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 13)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (25, N'Cappuccino', 3, CAST(90.00 AS Decimal(10, 2)), CAST(270.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (26, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (27, N'Palmera Bañada', 1, CAST(30.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (28, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (29, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (30, N'cafe grande', 2, CAST(80.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (31, N'Cafe Mediano', 4, CAST(70.00 AS Decimal(10, 2)), CAST(280.00 AS Decimal(10, 2)), 15)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (32, N'cafe grande', 1, CAST(80.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 15)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (33, N'Facturas', 5, CAST(20.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 15)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (34, N'cafe grande', 3, CAST(80.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 16)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (35, N'Facturas', 4, CAST(20.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 16)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (36, N'Cafe Mediano', 2, CAST(70.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 17)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (37, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 17)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (38, N'Smoothie', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 18)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (39, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 18)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (40, N'Tostado de jamón y queso', 3, CAST(120.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 18)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (41, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 19)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (42, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 19)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (43, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 20)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (44, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 20)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (45, N'Desayuno Florencia', 1, CAST(210.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 21)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (46, N'Desayuno Light', 2, CAST(160.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 22)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (48, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (49, N'Medialunas', 1, CAST(15.00 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (50, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (51, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (52, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 21)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (53, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 17)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (55, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 24)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (56, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 24)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (57, N'Facturas Dulce', 1, CAST(25.00 AS Decimal(10, 2)), CAST(25.00 AS Decimal(10, 2)), 24)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (58, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 25)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (59, N'cafe grande', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 25)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (60, N'Cappuccino', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 26)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (61, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 26)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (62, N'Porcion Tarta', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 26)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (63, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (64, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (65, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (66, N'Aquarius Pera x1.5 l', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (67, N'Cerveza Quilmes Stout x473 ml', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 28)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (68, N'Cerveza Quilmes Cristal 1Lts', 3, CAST(130.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), 28)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (69, N'Picada completa', 1, CAST(430.00 AS Decimal(10, 2)), CAST(430.00 AS Decimal(10, 2)), 28)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (70, N'Jugo de Naranja', 2, CAST(60.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 29)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (71, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 29)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (72, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 30)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (73, N'Picada completa', 1, CAST(430.00 AS Decimal(10, 2)), CAST(430.00 AS Decimal(10, 2)), 31)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (74, N'Picada simple', 1, CAST(300.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 31)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (75, N'Facturas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 31)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (76, N'Medialunas', 5, CAST(15.00 AS Decimal(10, 2)), CAST(75.00 AS Decimal(10, 2)), 32)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (77, N'Desayuno Tradicional med', 2, CAST(90.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 33)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (78, N'Cappuccino', 2, CAST(90.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 32)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (79, N'Desayuno Tradicional Grand', 8, CAST(100.00 AS Decimal(10, 2)), CAST(800.00 AS Decimal(10, 2)), 34)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (80, N'Cafe Chico', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 35)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (81, N'Medialunas', 1, CAST(15.00 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), 35)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (82, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 36)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (83, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 36)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (84, N'Porcion Tarta', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 36)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (86, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 38)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (87, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 38)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (88, N'Facturas', 5, CAST(20.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 38)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (89, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 39)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (90, N'Porcion Tarta', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 39)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (92, N'Smoothie', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 39)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (93, N'Cerveza Quilmes Cristal 1Lts', 3, CAST(130.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), 40)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (94, N'Coca-Cola x375 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 40)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (95, N'Mani porcion', 2, CAST(10.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 40)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (96, N'Coca-Cola x600 ml', 1, CAST(55.00 AS Decimal(10, 2)), CAST(55.00 AS Decimal(10, 2)), 41)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (97, N'Cerveza Quilmes Cristal 1Lts', 2, CAST(130.00 AS Decimal(10, 2)), CAST(260.00 AS Decimal(10, 2)), 41)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (98, N'Licuado', 3, CAST(110.00 AS Decimal(10, 2)), CAST(330.00 AS Decimal(10, 2)), 41)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (100, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (101, N'Submarino', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (102, N'cafe grande', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (103, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (104, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 43)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (105, N'Desayuno Light', 2, CAST(160.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 44)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (106, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (107, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (108, N'Desayuno Light', 2, CAST(160.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 46)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (109, N'Desayuno Tradicional Grand', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 47)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (110, N'Licuado', 3, CAST(110.00 AS Decimal(10, 2)), CAST(330.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (111, N'Jugo de Naranja', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (112, N'Desayuno Tradicional med', 2, CAST(90.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 47)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (113, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 48)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (114, N'Tostado de jamón crudo y queso', 1, CAST(140.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 48)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (115, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 48)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (116, N'Submarino', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (117, N'Jugo de Naranja', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (118, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (119, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (120, N'Desayuno Tradicional Grand', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 50)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (121, N'Desayuno Tradicional med', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 50)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (127, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 51)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (128, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 51)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (129, N'Coca-Cola fanta sprite x375 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (130, N'Coca-Cola fanta sprite Lata x354 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (131, N'Cafe Chico', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (132, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (133, N'Medialunas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (138, N'Jugo de Naranja', 2, CAST(60.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 54)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (139, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 55)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (140, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 55)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (141, N'Cafe Chico', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 56)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (142, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 56)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (145, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 58)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (146, N'Medialunas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 58)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (147, N'Agua mineral Eco x500 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 55)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (148, N'Licuado', 4, CAST(110.00 AS Decimal(10, 2)), CAST(440.00 AS Decimal(10, 2)), 59)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (149, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 59)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (150, N'Coca-Cola fanta sprite x600 ml', 1, CAST(55.00 AS Decimal(10, 2)), CAST(55.00 AS Decimal(10, 2)), 59)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (151, N'Licuado', 5, CAST(110.00 AS Decimal(10, 2)), CAST(550.00 AS Decimal(10, 2)), 60)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (152, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 60)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (153, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 61)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (154, N'Tostado de jamón y queso', 3, CAST(120.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (155, N'Licuado', 5, CAST(110.00 AS Decimal(10, 2)), CAST(550.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (156, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (157, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (158, N'Picada completa', 1, CAST(430.00 AS Decimal(10, 2)), CAST(430.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (159, N'Cerveza Lata Quilmes Stout x473 ml', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (160, N'Agua mineral Eco x500 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 63)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (161, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 63)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (162, N'Cerveza Lata Quilmes Stout x473 ml', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 63)
GO
SET IDENTITY_INSERT [dbo].[DetallePedidos] OFF
GO
SET IDENTITY_INSERT [dbo].[DetalleVentas] ON 

GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (1, N'Bizcocho', 0, CAST(350.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 1)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (2, N'Pan', 6, CAST(70.00 AS Decimal(10, 2)), CAST(455.00 AS Decimal(10, 2)), N'Minorista', 1)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (3, N'Pan', 0, CAST(70.00 AS Decimal(10, 2)), CAST(49.00 AS Decimal(10, 2)), N'Minorista', 2)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (4, N'Masa Seca', 0, CAST(350.00 AS Decimal(10, 2)), CAST(149.80 AS Decimal(10, 2)), N'Minorista', 2)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (5, N'Corazones', 0, CAST(350.00 AS Decimal(10, 2)), CAST(33.25 AS Decimal(10, 2)), N'Minorista', 3)
GO
SET IDENTITY_INSERT [dbo].[DetalleVentas] OFF
GO
SET IDENTITY_INSERT [dbo].[Egresos] ON 

GO
INSERT [dbo].[Egresos] ([id_egreso], [id_caja], [descripcion], [monto], [fecha]) VALUES (1, 6, N'propaganda radio', CAST(-700.00 AS Decimal(18, 2)), CAST(0x0000AB6100000000 AS DateTime))
GO
INSERT [dbo].[Egresos] ([id_egreso], [id_caja], [descripcion], [monto], [fecha]) VALUES (2, 7, N'coca', CAST(-5048.00 AS Decimal(18, 2)), CAST(0x0000AB6200000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Egresos] OFF
GO
SET IDENTITY_INSERT [dbo].[Mesas] ON 

GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (14, N'Mesa 01', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (15, N'Mesa 02', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (16, N'Mesa 03', N'Libre', 5)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (17, N'Mesa 04', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (18, N'Mesa 05', N'Libre', 6)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (19, N'Mesa 06', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (20, N'Mesa 07(Afuera)', N'Libre', 1)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (21, N'Mesa 08', N'Libre', 5)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (22, N'Mesa 09', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (23, N'Mesa 10', N'Libre', 5)
GO
SET IDENTITY_INSERT [dbo].[Mesas] OFF
GO
SET IDENTITY_INSERT [dbo].[Mozos] ON 

GO
INSERT [dbo].[Mozos] ([id_mozo], [nombre]) VALUES (5, N'Sandra')
GO
INSERT [dbo].[Mozos] ([id_mozo], [nombre]) VALUES (6, N'Adriana')
GO
INSERT [dbo].[Mozos] ([id_mozo], [nombre]) VALUES (7, N'René')
GO
SET IDENTITY_INSERT [dbo].[Mozos] OFF
GO
SET IDENTITY_INSERT [dbo].[Pedidos] ON 

GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (1, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 10, 5, CAST(0.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 1, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (2, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(-50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 2, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (3, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(270.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (4, 18, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (5, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (6, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (7, 18, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (8, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (9, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (10, 19, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (11, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(370.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (12, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(230.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (13, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (14, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(865.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (15, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(460.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (16, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (17, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (18, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(780.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (19, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (20, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(340.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (21, 18, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(310.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (22, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (23, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(305.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (24, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(185.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (25, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(170.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (26, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(230.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (27, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(660.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (28, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(890.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (29, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (30, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (31, 14, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(750.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (32, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(255.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (33, 16, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (34, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(800.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (35, 14, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(115.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (36, 18, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(380.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (38, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (39, 22, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (40, 14, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (41, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(645.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (42, 18, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (43, 16, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (44, 17, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (45, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(680.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (46, 16, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (47, 17, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(380.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (48, 19, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (49, 18, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (50, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (51, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (52, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(270.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (54, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (55, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(250.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (56, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (58, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (59, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(735.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (60, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(790.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (61, 19, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (62, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(1640.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (63, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
SET IDENTITY_INSERT [dbo].[Pedidos] OFF
GO
SET IDENTITY_INSERT [dbo].[Productos] ON 

GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (12, N'Té', CAST(40.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (13, N'Mate cocido', CAST(40.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (14, N'Desayuno Tradicional chico', CAST(70.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (15, N'Desayuno Tradicional med', CAST(90.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (17, N'Submarino', CAST(100.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (18, N'Cappuccino', CAST(90.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (19, N'Desayuno Tradicional Grand', CAST(100.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (20, N'Mate', CAST(100.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (22, N'Desayuno Completo', CAST(130.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (23, N'Desayuno Light', CAST(160.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (24, N'Desayuno Florencia', CAST(210.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (25, N'Desayuno Protéico', CAST(180.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (26, N'Desayuno Fresco', CAST(175.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (27, N'Desayuno Campestre', CAST(140.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (28, N'Licuado', CAST(110.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (29, N'Jugo de Naranja', CAST(60.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (30, N'Limonada', CAST(0.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (31, N'Tostado de jamón y queso', CAST(120.00 AS Decimal(10, 2)), NULL, 8, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (32, N'Tostado de jamón crudo y queso', CAST(140.00 AS Decimal(10, 2)), NULL, 8, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (33, N'Tostado de salame y queso', CAST(120.00 AS Decimal(10, 2)), NULL, 8, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (34, N'Picada simple', CAST(300.00 AS Decimal(10, 2)), NULL, 9, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (35, N'Picada completa', CAST(430.00 AS Decimal(10, 2)), NULL, 9, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (36, N'Pepsi x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (37, N'Mirinda nanranja x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (39, N'7up x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (40, N'Pepsi x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (41, N'Mirinda manzana x1.25 l', CAST(85.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (43, N'Paso de los toros x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (44, N'Awafrut pomelo x1.65 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (45, N'Agua mineral Nestlé x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (46, N'Agua mineral Eco x500 ml', CAST(50.00 AS Decimal(10, 2)), 39, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (47, N'Awafrut durazno x1.65 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (48, N'Cerveza Quilmes x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (49, N'Cerveza Lata Quilmes Stout x473 ml', CAST(70.00 AS Decimal(10, 2)), 9, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (50, N'Cerveza Brahma x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (51, N'Cerveza Budweiser x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (52, N'Cerveza Patagonia Amber Lager x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (53, N'Vino Michel Torino tinto x700 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (54, N'Vino Toro Viejo tinto x700 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (61, N'Vino Finca Las Moras malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (62, N'Vino Trapiche Alaris malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (63, N'Vino Dadá moras x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (64, N'Vino Aroldos State tinto x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (65, N'Vino Estancias malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (66, N'Vino Elementos malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (67, N'Vino Chacabuco malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (68, N'Vino Don David malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (69, N'Vino Canciller blend x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (70, N'Vino Phebus malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (71, N'Vino Nampe malbec tinto x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (72, N'Vino Viñas de Balbo tinto x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (73, N'Vino Toro Viejo tinto x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (74, N'Vino Michel Torino tinto x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (75, N'Vino Nampe malbec x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (76, N'Vino Chacabuco malbec x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (77, N'Monster Energy Green x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (78, N'Monster E. Mango Loco x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (79, N'Coca-Cola fanta sprite Lata x354 ml', CAST(50.00 AS Decimal(10, 2)), 40, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (80, N'Fanta Naranja Lata x354 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (81, N'Sprite Lata x354 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (82, N'Coca-Cola fanta sprite x600 ml', CAST(55.00 AS Decimal(10, 2)), 40, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (83, N'Coca-Cola sin azúcar x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (84, N'Fanta Naranja sin azúcar x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (85, N'Fanta Naranja x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (86, N'Aquarius Pomelo x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (87, N'Sprite LS x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (88, N'Sprite LS x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (89, N'Aquarius Naranja x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (90, N'Coca-Cola sin azúcar x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (91, N'Coca-Cola fanta sprite x375 ml', CAST(40.00 AS Decimal(10, 2)), 40, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (92, N'Aquarius Pomelo x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (93, N'Aquarius Pera x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (94, N'Aquarius Manzana x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (95, N'Aquarius Naranja x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (96, N'Aquarius Pomelo x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (97, N'Aquarius Pera x1.5 l', CAST(100.00 AS Decimal(10, 2)), 5, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (98, N'Aquarius Manzana x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (99, N'Aquarius Naranja x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (100, N'Aquarius Pomelo Rosado x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (101, N'Agua KIN sin gas x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (102, N'Agua KIN con gas x600 ml', CAST(50.00 AS Decimal(10, 2)), 41, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (103, N'Powerade Manzana x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (104, N'Powerade Mountain Blast x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (105, N'Agua KIN sin gas x375 ml', CAST(30.00 AS Decimal(10, 2)), 41, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (106, N'Agua KIN sin gas x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (107, N'Cepita Multifruta x1 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (108, N'Ades Manzana x200 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (109, N'Ades Manzana x1 l', CAST(0.00 AS Decimal(10, 2)), 8, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (110, N'Cepita Ananá x300 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (111, N'Cepita Durazno x300 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (112, N'Cepita Naranja x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (113, N'Cepita Durazno x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (114, N'Cepita Multifruta x200 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (115, N'Smoothie', CAST(100.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (118, N'Pan', CAST(70.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (119, N'Bizcocho de grasa', CAST(180.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (120, N'Medialunas', CAST(20.00 AS Decimal(10, 2)), NULL, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (121, N'Marineras', CAST(180.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (122, N'Budines', CAST(60.00 AS Decimal(10, 2)), 12, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (123, N'Apolo', CAST(80.00 AS Decimal(10, 2)), 12, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (124, N'Masa Seca', CAST(350.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (125, N'Torta', CAST(380.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (126, N'Torta de Naranja', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (127, N'Torta de Limon', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (128, N'Torta Europea', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (129, N'Torta de Coco B/N', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (130, N'Torta Merengada', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (131, N'Madrigueño', CAST(35.00 AS Decimal(10, 2)), 24, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (132, N'Bizcocho de Vino Blanco', CAST(180.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (133, N'Cremona', CAST(50.00 AS Decimal(10, 2)), 6, 13, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (134, N'Bizocho', CAST(140.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (135, N'Ferrocarril', CAST(220.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (136, N'Tortilla', CAST(140.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (137, N'palmerita', CAST(220.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (138, N'Facturas', CAST(20.00 AS Decimal(10, 2)), NULL, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (139, N'Facturas Hojaldradas', CAST(25.00 AS Decimal(10, 2)), NULL, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (140, N'Mini Facturas', CAST(250.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (141, N'Roscas Rellenas', CAST(250.00 AS Decimal(10, 2)), 11, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (142, N'Panettone', CAST(150.00 AS Decimal(10, 2)), 9, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (143, N'Cafe Mediano', CAST(60.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (144, N'cafe grande', CAST(70.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (145, N'Cafe Chico', CAST(50.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (146, N'Alfajor de Chocolate', CAST(35.00 AS Decimal(10, 2)), 35, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (147, N'Alfajor de Maicena Grande', CAST(35.00 AS Decimal(10, 2)), 35, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (148, N'Cañoncitos', CAST(25.00 AS Decimal(10, 2)), 23, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (149, N'Flotitas', CAST(35.00 AS Decimal(10, 2)), 8, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (150, N'Facturas Dulce', CAST(25.00 AS Decimal(10, 2)), 14, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (151, N'Palmera Bañada', CAST(30.00 AS Decimal(10, 2)), 18, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (152, N'Cañon Bañado', CAST(40.00 AS Decimal(10, 2)), 6, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (153, N'Palmera Rellena', CAST(40.00 AS Decimal(10, 2)), 20, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (154, N'Pan de Pancho', CAST(50.00 AS Decimal(10, 2)), 10, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (155, N'Pan de Hamburguesa', CAST(50.00 AS Decimal(10, 2)), 14, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (156, N'Prepizza', CAST(35.00 AS Decimal(10, 2)), 23, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (157, N'Brownie', CAST(20.00 AS Decimal(10, 2)), 20, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (158, N'Cerveza Quilmes Cristal 1Lts', CAST(130.00 AS Decimal(10, 2)), 28, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (159, N'Corazones', CAST(350.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (160, N'Porcion Tarta', CAST(40.00 AS Decimal(10, 2)), 27, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (161, N'Mani porcion', CAST(10.00 AS Decimal(10, 2)), NULL, 9, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (162, N'Coca-Cola fanta sprinte 1500 vidrio ret.', CAST(85.00 AS Decimal(10, 2)), 41, 10, N'Cantidad')
GO
SET IDENTITY_INSERT [dbo].[Productos] OFF
GO
SET IDENTITY_INSERT [dbo].[Tipos] ON 

GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (6, N'Infusiones', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (7, N'Jugos y Licuados', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (8, N'Sandwiches', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (9, N'Picadas', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (10, N'Bebidas', N'1')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (11, N'Desayunos', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (12, N'Panificados', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (13, N'Panificado x Unidad', N'1')
GO
SET IDENTITY_INSERT [dbo].[Tipos] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (10, N'echemez', N'040397', N'Administrador')
GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (11, N'mchemez', N'290794', N'Administrador')
GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (12, N'codeassets', N'080118', N'Administrador')
GO
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
GO
SET IDENTITY_INSERT [dbo].[Ventas] ON 

GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (1, CAST(0xBC400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(525.00 AS Decimal(8, 2)), 3, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (2, CAST(0xBC400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(198.80 AS Decimal(8, 2)), 5, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (3, CAST(0xBC400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(33.25 AS Decimal(8, 2)), 6, 11, N'Minorista', N'Efectivo', 0)
GO
SET IDENTITY_INSERT [dbo].[Ventas] OFF
GO
USE [master]
GO
ALTER DATABASE [Chemez] SET  READ_WRITE 
GO
