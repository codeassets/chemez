USE [Chemez]
GO
/****** Object:  Table [dbo].[Caja]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caja](
	[id_caja] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha_inicio] [datetime] NULL,
	[fecha_fin] [date] NULL,
	[monto_inicial] [decimal](10, 2) NULL,
	[ingreso_total] [decimal](10, 2) NULL,
	[egreso_total] [decimal](10, 2) NULL,
	[total] [decimal](10, 2) NULL,
	[estado] [varchar](100) NULL,
	[id_cajero_abre] [bigint] NOT NULL,
	[id_cajero_cierra] [bigint] NOT NULL,
 CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED 
(
	[id_caja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cajeros]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cajeros](
	[id_cajeros] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[contraseña] [varchar](100) NULL,
 CONSTRAINT [PK_Cajeros] PRIMARY KEY CLUSTERED 
(
	[id_cajeros] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[id_cliente] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[CUIT] [varchar](50) NULL,
	[direccion] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[id_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallePedidos]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallePedidos](
	[id_detalle] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](10, 2) NULL,
	[subtotal] [decimal](10, 2) NULL,
	[id_pedido] [bigint] NOT NULL,
 CONSTRAINT [PK_DetallePedidos] PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallesFacturas]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallesFacturas](
	[IdDetalle] [bigint] IDENTITY(1,1) NOT NULL,
	[IdProducto] [bigint] NULL,
	[Descripcion] [varchar](50) NULL,
	[Cantidad] [decimal](10, 3) NULL,
	[PrecioUnitario] [decimal](15, 2) NULL,
	[Subtotal] [decimal](15, 2) NULL,
	[IdFactura] [bigint] NULL,
 CONSTRAINT [PK_DetallesFacturas] PRIMARY KEY CLUSTERED 
(
	[IdDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleVentas]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleVentas](
	[id_detalle] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[cantidad] [decimal](10, 3) NOT NULL,
	[precio_unitario] [decimal](10, 2) NOT NULL,
	[subtotal] [decimal](10, 2) NOT NULL,
	[venta_por] [varchar](50) NOT NULL,
	[id_venta] [bigint] NOT NULL,
 CONSTRAINT [PK_DetalleVentas] PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Egresos]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Egresos](
	[id_egreso] [bigint] IDENTITY(1,1) NOT NULL,
	[id_caja] [bigint] NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[monto] [decimal](18, 2) NOT NULL,
	[fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_Egresos] PRIMARY KEY CLUSTERED 
(
	[id_egreso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Facturas]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Facturas](
	[IdFactura] [bigint] IDENTITY(1,1) NOT NULL,
	[IdVenta] [bigint] NULL,
	[TipoId] [varchar](50) NULL,
	[PtoVta] [nchar](10) NULL,
	[CbteTipo] [int] NULL,
	[CbteTipo_desc] [varchar](50) NULL,
	[Concepto] [int] NULL,
	[Concepto_desc] [varchar](50) NULL,
	[DocTipo] [int] NULL,
	[DocTipo_desc] [varchar](50) NULL,
	[DocNro] [bigint] NULL,
	[CbteDesde] [int] NULL,
	[CbteHasta] [nchar](10) NULL,
	[CbteFch] [datetime] NULL,
	[ImpTotal] [decimal](12, 2) NULL,
	[ImpOpEx] [decimal](12, 2) NULL,
	[ImpTotConc] [decimal](12, 2) NULL,
	[ImpNeto] [decimal](12, 2) NULL,
	[ImpTrib] [decimal](12, 2) NULL,
	[ImpIVA] [decimal](12, 2) NULL,
	[MonId] [varchar](50) NULL,
	[MonCotiz] [int] NULL,
	[AlicIva_Id] [int] NULL,
	[AlicIva_BaseImp] [decimal](12, 2) NULL,
	[AlicIva_Importe] [decimal](12, 2) NULL,
	[Estado] [varchar](50) NULL,
	[TipoPago] [varchar](50) NULL,
	[CAE] [varchar](20) NULL,
	[CAE_vto] [varchar](20) NULL,
	[Mensaje] [varchar](500) NULL,
 CONSTRAINT [PK_Facturas] PRIMARY KEY CLUSTERED 
(
	[IdFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mesas]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mesas](
	[id_mesa] [bigint] IDENTITY(1,1) NOT NULL,
	[nro_mesa] [varchar](50) NULL,
	[estado] [varchar](100) NULL,
	[id_mozo] [bigint] NULL,
 CONSTRAINT [PK_Mesas] PRIMARY KEY CLUSTERED 
(
	[id_mesa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mozos]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mozos](
	[id_mozo] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
 CONSTRAINT [PK_Mozos] PRIMARY KEY CLUSTERED 
(
	[id_mozo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pedidos](
	[id_pedido] [bigint] IDENTITY(1,1) NOT NULL,
	[id_mesa] [bigint] NOT NULL,
	[fecha] [date] NOT NULL,
	[estado] [varchar](100) NOT NULL,
	[id_usuario] [bigint] NOT NULL,
	[id_mozo] [bigint] NOT NULL,
	[descuento] [decimal](10, 2) NULL,
	[total] [decimal](10, 2) NULL,
	[id_caja] [bigint] NULL,
	[tipo_pago] [varchar](50) NULL,
 CONSTRAINT [PK_Pedidos] PRIMARY KEY CLUSTERED 
(
	[id_pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[id_producto] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[precio] [decimal](10, 2) NULL,
	[cantidad] [int] NULL,
	[id_tipo] [bigint] NOT NULL,
	[venta_por] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[id_producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketsAcceso]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketsAcceso](
	[IdTA] [bigint] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](100) NULL,
	[Destination] [varchar](100) NULL,
	[GenerationTime] [datetime] NULL,
	[ExpirationTime] [datetime] NULL,
	[Token] [varchar](1000) NULL,
	[Sign] [varchar](1000) NULL,
 CONSTRAINT [PK_TicketsAcceso] PRIMARY KEY CLUSTERED 
(
	[IdTA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipos]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipos](
	[id_tipo] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[stock] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Tipos] PRIMARY KEY CLUSTERED 
(
	[id_tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[contraseña] [varchar](100) NULL,
	[tipo] [varchar](100) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 17/04/2020 21:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ventas](
	[id_venta] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha] [date] NOT NULL,
	[descuento] [decimal](8, 2) NOT NULL,
	[total] [decimal](8, 2) NOT NULL,
	[id_caja] [bigint] NULL,
	[id_usuario] [bigint] NOT NULL,
	[tipo_venta] [varchar](50) NOT NULL,
	[tipo_pago] [varchar](50) NOT NULL,
	[id_cliente] [bigint] NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[id_venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [Chemez] SET  READ_WRITE 
GO
