USE [Chemez]
GO
/****** Object:  Table [dbo].[Caja]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caja](
	[id_caja] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha_inicio] [datetime] NULL,
	[fecha_fin] [date] NULL,
	[monto_inicial] [decimal](10, 2) NULL,
	[ingreso_total] [decimal](10, 2) NULL,
	[egreso_total] [decimal](10, 2) NULL,
	[total] [decimal](10, 2) NULL,
	[estado] [varchar](100) NULL,
	[id_cajero_abre] [bigint] NOT NULL,
	[id_cajero_cierra] [bigint] NOT NULL,
 CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED 
(
	[id_caja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cajeros]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cajeros](
	[id_cajeros] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[contraseña] [varchar](100) NULL,
 CONSTRAINT [PK_Cajeros] PRIMARY KEY CLUSTERED 
(
	[id_cajeros] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[id_cliente] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[CUIT] [varchar](50) NULL,
	[direccion] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[id_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallePedidos]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallePedidos](
	[id_detalle] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](10, 2) NULL,
	[subtotal] [decimal](10, 2) NULL,
	[id_pedido] [bigint] NOT NULL,
 CONSTRAINT [PK_DetallePedidos] PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallesFacturas]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallesFacturas](
	[IdDetalle] [bigint] IDENTITY(1,1) NOT NULL,
	[IdProducto] [bigint] NULL,
	[Descripcion] [varchar](50) NULL,
	[Cantidad] [int] NULL,
	[PrecioUnitario] [decimal](15, 2) NULL,
	[Subtotal] [decimal](15, 2) NULL,
	[IdFactura] [bigint] NULL,
 CONSTRAINT [PK_DetallesFacturas] PRIMARY KEY CLUSTERED 
(
	[IdDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleVentas]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleVentas](
	[id_detalle] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](10, 2) NOT NULL,
	[subtotal] [decimal](10, 2) NOT NULL,
	[venta_por] [varchar](50) NOT NULL,
	[id_venta] [bigint] NOT NULL,
 CONSTRAINT [PK_DetalleVentas] PRIMARY KEY CLUSTERED 
(
	[id_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Egresos]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Egresos](
	[id_egreso] [bigint] IDENTITY(1,1) NOT NULL,
	[id_caja] [bigint] NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[monto] [decimal](18, 2) NOT NULL,
	[fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_Egresos] PRIMARY KEY CLUSTERED 
(
	[id_egreso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Facturas]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Facturas](
	[IdFactura] [bigint] IDENTITY(1,1) NOT NULL,
	[IdVenta] [bigint] NULL,
	[TipoId] [varchar](50) NULL,
	[PtoVta] [nchar](10) NULL,
	[CbteTipo] [int] NULL,
	[CbteTipo_desc] [varchar](50) NULL,
	[Concepto] [int] NULL,
	[Concepto_desc] [varchar](50) NULL,
	[DocTipo] [int] NULL,
	[DocTipo_desc] [varchar](50) NULL,
	[DocNro] [bigint] NULL,
	[CbteDesde] [int] NULL,
	[CbteHasta] [nchar](10) NULL,
	[CbteFch] [datetime] NULL,
	[ImpTotal] [decimal](12, 2) NULL,
	[ImpOpEx] [decimal](12, 2) NULL,
	[ImpTotConc] [decimal](12, 2) NULL,
	[ImpNeto] [decimal](12, 2) NULL,
	[ImpTrib] [decimal](12, 2) NULL,
	[ImpIVA] [decimal](12, 2) NULL,
	[MonId] [varchar](50) NULL,
	[MonCotiz] [int] NULL,
	[AlicIva_Id] [int] NULL,
	[AlicIva_BaseImp] [decimal](12, 2) NULL,
	[AlicIva_Importe] [decimal](12, 2) NULL,
	[Estado] [varchar](50) NULL,
	[TipoPago] [varchar](50) NULL,
	[CAE] [varchar](20) NULL,
	[CAE_vto] [varchar](20) NULL,
	[Mensaje] [varchar](500) NULL,
 CONSTRAINT [PK_Facturas] PRIMARY KEY CLUSTERED 
(
	[IdFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mesas]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mesas](
	[id_mesa] [bigint] IDENTITY(1,1) NOT NULL,
	[nro_mesa] [varchar](50) NULL,
	[estado] [varchar](100) NULL,
	[id_mozo] [bigint] NULL,
 CONSTRAINT [PK_Mesas] PRIMARY KEY CLUSTERED 
(
	[id_mesa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mozos]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mozos](
	[id_mozo] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
 CONSTRAINT [PK_Mozos] PRIMARY KEY CLUSTERED 
(
	[id_mozo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pedidos](
	[id_pedido] [bigint] IDENTITY(1,1) NOT NULL,
	[id_mesa] [bigint] NOT NULL,
	[fecha] [date] NOT NULL,
	[estado] [varchar](100) NOT NULL,
	[id_usuario] [bigint] NOT NULL,
	[id_mozo] [bigint] NOT NULL,
	[descuento] [decimal](10, 2) NULL,
	[total] [decimal](10, 2) NULL,
	[id_caja] [bigint] NULL,
	[tipo_pago] [varchar](50) NULL,
 CONSTRAINT [PK_Pedidos] PRIMARY KEY CLUSTERED 
(
	[id_pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[id_producto] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[precio] [decimal](10, 2) NULL,
	[cantidad] [int] NULL,
	[id_tipo] [bigint] NOT NULL,
	[venta_por] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[id_producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketsAcceso]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketsAcceso](
	[IdTA] [bigint] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](100) NULL,
	[Destination] [varchar](100) NULL,
	[GenerationTime] [datetime] NULL,
	[ExpirationTime] [datetime] NULL,
	[Token] [varchar](1000) NULL,
	[Sign] [varchar](1000) NULL,
 CONSTRAINT [PK_TicketsAcceso] PRIMARY KEY CLUSTERED 
(
	[IdTA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipos]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipos](
	[id_tipo] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NULL,
	[stock] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Tipos] PRIMARY KEY CLUSTERED 
(
	[id_tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[contraseña] [varchar](100) NULL,
	[tipo] [varchar](100) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 07/04/2020 20:38:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ventas](
	[id_venta] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha] [date] NOT NULL,
	[descuento] [decimal](8, 2) NOT NULL,
	[total] [decimal](8, 2) NOT NULL,
	[id_caja] [bigint] NULL,
	[id_usuario] [bigint] NOT NULL,
	[tipo_venta] [varchar](50) NOT NULL,
	[tipo_pago] [varchar](50) NOT NULL,
	[id_cliente] [bigint] NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[id_venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Caja] ON 

GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (5, CAST(0x0000AB6100733147 AS DateTime), CAST(0xBC400B00 AS Date), CAST(1150.00 AS Decimal(10, 2)), CAST(4663.80 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(4663.80 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (6, CAST(0x0000AB610133378A AS DateTime), CAST(0xBC400B00 AS Date), CAST(200.00 AS Decimal(10, 2)), CAST(5293.25 AS Decimal(10, 2)), CAST(-700.00 AS Decimal(10, 2)), CAST(4593.25 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (7, CAST(0x0000AB6200758732 AS DateTime), CAST(0xBD400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(2480.00 AS Decimal(10, 2)), CAST(-5048.00 AS Decimal(10, 2)), CAST(-2568.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (8, CAST(0x0000AB620114F2DE AS DateTime), CAST(0xBE400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(1905.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(1905.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (9, CAST(0x0000AB63008DD3E3 AS DateTime), CAST(0xBE400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(3930.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(3930.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (10, CAST(0x0000AB63011273B8 AS DateTime), CAST(0xBF400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(4215.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(4215.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (11, CAST(0x0000AB64000FAA27 AS DateTime), CAST(0xC0400B00 AS Date), CAST(0.00 AS Decimal(10, 2)), CAST(3050.00 AS Decimal(10, 2)), CAST(-775.00 AS Decimal(10, 2)), CAST(2275.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (12, CAST(0x0000AB6500865E63 AS DateTime), CAST(0xC0400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(780.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(780.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (13, CAST(0x0000AB650114537A AS DateTime), CAST(0xC0400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(1500.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(1500.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (14, CAST(0x0000AB660074CBF6 AS DateTime), CAST(0xC1400B00 AS Date), CAST(2000.00 AS Decimal(10, 2)), CAST(1210.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(1210.00 AS Decimal(10, 2)), N'Cerrada', 11, 11)
GO
INSERT [dbo].[Caja] ([id_caja], [fecha_inicio], [fecha_fin], [monto_inicial], [ingreso_total], [egreso_total], [total], [estado], [id_cajero_abre], [id_cajero_cierra]) VALUES (15, CAST(0x0000AB66010FC06F AS DateTime), NULL, CAST(2000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Abierta', 11, 0)
GO
SET IDENTITY_INSERT [dbo].[Caja] OFF
GO
SET IDENTITY_INSERT [dbo].[Clientes] ON 

GO
INSERT [dbo].[Clientes] ([id_cliente], [nombre], [CUIT], [direccion], [telefono]) VALUES (1, N'CHACON', N'1111111111111', N'XXXXXX', N'03855040172')
GO
INSERT [dbo].[Clientes] ([id_cliente], [nombre], [CUIT], [direccion], [telefono]) VALUES (2, N'Leparc', N'20374110528', N'Catamarca 421', N'654564')
GO
INSERT [dbo].[Clientes] ([id_cliente], [nombre], [CUIT], [direccion], [telefono]) VALUES (3, N'asdfas', N'', N'', N'')
GO
SET IDENTITY_INSERT [dbo].[Clientes] OFF
GO
SET IDENTITY_INSERT [dbo].[DetallePedidos] ON 

GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (1, N'Café mediano', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 1)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (2, N'Medialunas', 2, CAST(15.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), 1)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (3, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 1)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (4, N'Café chico', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 2)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (5, N'Desayuno Tradicional', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 2)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (6, N'Cappuccino', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 3)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (7, N'Facturas Hojaldradas', 2, CAST(25.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 3)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (8, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 3)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (10, N'Café mediano', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 4)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (11, N'Desayuno Protéico', 2, CAST(180.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 5)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (12, N'Desayuno Tradicional med', 2, CAST(70.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 6)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (13, N'Desayuno Tradicional Grand', 3, CAST(100.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 7)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (14, N'Desayuno Light', 3, CAST(160.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), 8)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (15, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 6)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (16, N'Desayuno Tradicional med', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 9)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (17, N'Desayuno Completo', 2, CAST(130.00 AS Decimal(10, 2)), CAST(260.00 AS Decimal(10, 2)), 10)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (18, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 10)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (19, N'Cafe Mediano', 3, CAST(70.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 11)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (20, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 11)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (21, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 12)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (22, N'Desayuno Tradicional chico', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 12)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (23, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 11)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (24, N'Cafe Mediano', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 13)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (25, N'Cappuccino', 3, CAST(90.00 AS Decimal(10, 2)), CAST(270.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (26, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (27, N'Palmera Bañada', 1, CAST(30.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (28, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (29, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (30, N'cafe grande', 2, CAST(80.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 14)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (31, N'Cafe Mediano', 4, CAST(70.00 AS Decimal(10, 2)), CAST(280.00 AS Decimal(10, 2)), 15)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (32, N'cafe grande', 1, CAST(80.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 15)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (33, N'Facturas', 5, CAST(20.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 15)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (34, N'cafe grande', 3, CAST(80.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 16)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (35, N'Facturas', 4, CAST(20.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 16)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (36, N'Cafe Mediano', 2, CAST(70.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 17)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (37, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 17)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (38, N'Smoothie', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 18)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (39, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 18)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (40, N'Tostado de jamón y queso', 3, CAST(120.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 18)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (41, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 19)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (42, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 19)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (43, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 20)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (44, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 20)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (45, N'Desayuno Florencia', 1, CAST(210.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 21)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (46, N'Desayuno Light', 2, CAST(160.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 22)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (48, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (49, N'Medialunas', 1, CAST(15.00 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (50, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (51, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 23)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (52, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 21)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (53, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 17)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (55, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 24)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (56, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 24)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (57, N'Facturas Dulce', 1, CAST(25.00 AS Decimal(10, 2)), CAST(25.00 AS Decimal(10, 2)), 24)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (58, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 25)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (59, N'cafe grande', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 25)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (60, N'Cappuccino', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 26)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (61, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 26)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (62, N'Porcion Tarta', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 26)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (63, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (64, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (65, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (66, N'Aquarius Pera x1.5 l', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 27)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (67, N'Cerveza Quilmes Stout x473 ml', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 28)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (68, N'Cerveza Quilmes Cristal 1Lts', 3, CAST(130.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), 28)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (69, N'Picada completa', 1, CAST(430.00 AS Decimal(10, 2)), CAST(430.00 AS Decimal(10, 2)), 28)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (70, N'Jugo de Naranja', 2, CAST(60.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 29)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (71, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 29)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (72, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 30)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (73, N'Picada completa', 1, CAST(430.00 AS Decimal(10, 2)), CAST(430.00 AS Decimal(10, 2)), 31)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (74, N'Picada simple', 1, CAST(300.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 31)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (75, N'Facturas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 31)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (76, N'Medialunas', 5, CAST(15.00 AS Decimal(10, 2)), CAST(75.00 AS Decimal(10, 2)), 32)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (77, N'Desayuno Tradicional med', 2, CAST(90.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 33)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (78, N'Cappuccino', 2, CAST(90.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 32)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (79, N'Desayuno Tradicional Grand', 8, CAST(100.00 AS Decimal(10, 2)), CAST(800.00 AS Decimal(10, 2)), 34)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (80, N'Cafe Chico', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 35)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (81, N'Medialunas', 1, CAST(15.00 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), 35)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (82, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 36)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (83, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 36)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (84, N'Porcion Tarta', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 36)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (86, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 38)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (87, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 38)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (88, N'Facturas', 5, CAST(20.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 38)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (89, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 39)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (90, N'Porcion Tarta', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 39)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (92, N'Smoothie', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 39)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (93, N'Cerveza Quilmes Cristal 1Lts', 3, CAST(130.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), 40)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (94, N'Coca-Cola x375 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 40)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (95, N'Mani porcion', 2, CAST(10.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 40)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (96, N'Coca-Cola x600 ml', 1, CAST(55.00 AS Decimal(10, 2)), CAST(55.00 AS Decimal(10, 2)), 41)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (97, N'Cerveza Quilmes Cristal 1Lts', 2, CAST(130.00 AS Decimal(10, 2)), CAST(260.00 AS Decimal(10, 2)), 41)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (98, N'Licuado', 3, CAST(110.00 AS Decimal(10, 2)), CAST(330.00 AS Decimal(10, 2)), 41)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (100, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (101, N'Submarino', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (102, N'cafe grande', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (103, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 42)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (104, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 43)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (105, N'Desayuno Light', 2, CAST(160.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 44)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (106, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (107, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (108, N'Desayuno Light', 2, CAST(160.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 46)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (109, N'Desayuno Tradicional Grand', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 47)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (110, N'Licuado', 3, CAST(110.00 AS Decimal(10, 2)), CAST(330.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (111, N'Jugo de Naranja', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 45)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (112, N'Desayuno Tradicional med', 2, CAST(90.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 47)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (113, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 48)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (114, N'Tostado de jamón crudo y queso', 1, CAST(140.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 48)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (115, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 48)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (116, N'Submarino', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (117, N'Jugo de Naranja', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (118, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (119, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 49)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (120, N'Desayuno Tradicional Grand', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 50)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (121, N'Desayuno Tradicional med', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 50)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (127, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 51)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (128, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 51)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (129, N'Coca-Cola fanta sprite x375 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (130, N'Coca-Cola fanta sprite Lata x354 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (131, N'Cafe Chico', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (132, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (133, N'Medialunas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 52)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (138, N'Jugo de Naranja', 2, CAST(60.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 54)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (139, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 55)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (140, N'Facturas', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 55)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (141, N'Cafe Chico', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 56)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (142, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 56)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (145, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 58)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (146, N'Medialunas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 58)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (147, N'Agua mineral Eco x500 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 55)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (148, N'Licuado', 4, CAST(110.00 AS Decimal(10, 2)), CAST(440.00 AS Decimal(10, 2)), 59)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (149, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 59)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (150, N'Coca-Cola fanta sprite x600 ml', 1, CAST(55.00 AS Decimal(10, 2)), CAST(55.00 AS Decimal(10, 2)), 59)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (151, N'Licuado', 5, CAST(110.00 AS Decimal(10, 2)), CAST(550.00 AS Decimal(10, 2)), 60)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (152, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 60)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (153, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 61)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (154, N'Tostado de jamón y queso', 3, CAST(120.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (155, N'Licuado', 5, CAST(110.00 AS Decimal(10, 2)), CAST(550.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (156, N'Smoothie', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (157, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (158, N'Picada completa', 1, CAST(430.00 AS Decimal(10, 2)), CAST(430.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (159, N'Cerveza Lata Quilmes Stout x473 ml', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 62)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (160, N'Agua mineral Eco x500 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 63)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (161, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 63)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (162, N'Cerveza Lata Quilmes Stout x473 ml', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 63)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (178, N'Desayuno Completo', 2, CAST(130.00 AS Decimal(10, 2)), CAST(260.00 AS Decimal(10, 2)), 68)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (179, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 69)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (180, N'Desayuno Tradicional med', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 70)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (181, N'Desayuno Tradicional med', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 71)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (182, N'cafe grande', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 71)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (183, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 72)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (184, N'Medialunas', 1, CAST(20.00 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), 72)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (185, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 73)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (186, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 73)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (191, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 77)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (192, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 77)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (193, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 77)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (194, N'Cerveza Lata Quilmes Stout x473 ml', 3, CAST(70.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 78)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (195, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 79)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (196, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 79)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (198, N'Coca-Cola sin azúcar x600 ml', 2, CAST(55.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 78)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (199, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 80)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (200, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 80)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (201, N'Cerveza Quilmes Cristal 1Lts', 4, CAST(130.00 AS Decimal(10, 2)), CAST(520.00 AS Decimal(10, 2)), 78)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (202, N'Mani porcion', 1, CAST(10.00 AS Decimal(10, 2)), CAST(10.00 AS Decimal(10, 2)), 78)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (203, N'Picada simple', 1, CAST(300.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 78)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (204, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 81)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (205, N'Desayuno Tradicional med', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 81)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (206, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 82)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (207, N'Cappuccino', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 83)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (208, N'Submarino', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 83)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (209, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 83)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (210, N'Cafe Chico', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 84)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (211, N'Desayuno Completo', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 85)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (212, N'Desayuno Tradicional med', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 86)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (213, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 86)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (214, N'Coca-Cola fanta sprite Lata x354 ml', 3, CAST(50.00 AS Decimal(10, 2)), CAST(150.00 AS Decimal(10, 2)), 87)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (215, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 88)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (216, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 88)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (217, N'Coca-Cola fanta sprite Lata x354 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 88)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (218, N'Jugo de Naranja', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 88)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (219, N'Cerveza Lata Quilmes Stout x473 ml', 3, CAST(70.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 89)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (220, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 87)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (223, N'Cerveza Quilmes Cristal 1Lts', 1, CAST(130.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 89)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (224, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 91)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (225, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 92)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (226, N'Desayuno Tradicional Grand', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 93)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (227, N'Submarino', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 93)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (228, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 94)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (229, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 94)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (230, N'Desayuno Light', 1, CAST(160.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 94)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (231, N'cafe grande', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 94)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (232, N'Cappuccino', 2, CAST(100.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 95)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (233, N'Ades Manzana x200 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 96)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (234, N'Aquarius  x1.5 cc', 1, CAST(100.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 97)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (236, N'Licuado', 2, CAST(110.00 AS Decimal(10, 2)), CAST(220.00 AS Decimal(10, 2)), 99)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (237, N'Licuado chico', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 99)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (238, N'Tostado de jamón y queso', 2, CAST(120.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 99)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (240, N'Cañon Bañado', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 99)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (242, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 101)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (243, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 101)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (244, N'Cafe Mediano', 1, CAST(60.00 AS Decimal(10, 2)), CAST(60.00 AS Decimal(10, 2)), 101)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (245, N'Tostado de jamón y queso', 1, CAST(120.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 102)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (247, N'Licuado', 1, CAST(110.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 102)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (248, N'Ades Manzana x200 ml', 2, CAST(40.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 103)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (249, N'Agua KIN sin gas x375 ml', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 103)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (250, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 103)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (251, N'Agua mineral Nestlé x1.5 l', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 103)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (252, N'Budines', 4, CAST(60.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 103)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (253, N'Cañoncitos', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 103)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (254, N'Ades Manzana x1 l', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 104)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (255, N'Ades Manzana x200 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 104)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (256, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 104)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (257, N'Ades Manzana x200 ml', 5, CAST(40.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 105)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (258, N'Agua KIN sin gas x375 ml', 4, CAST(35.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 106)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (259, N'Agua KIN sin gas x375 ml', 3, CAST(35.00 AS Decimal(10, 2)), CAST(105.00 AS Decimal(10, 2)), 107)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (260, N'Alfajor de Chocolate', 2, CAST(35.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 107)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (261, N'Agua KIN sin gas x1.5 l', 3, CAST(70.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 108)
GO
INSERT [dbo].[DetallePedidos] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [id_pedido]) VALUES (262, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), 108)
GO
SET IDENTITY_INSERT [dbo].[DetallePedidos] OFF
GO
SET IDENTITY_INSERT [dbo].[DetallesFacturas] ON 

GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (1, 105, N'Agua KIN sin gas x375 ml', 3, CAST(35.00 AS Decimal(15, 2)), CAST(105.00 AS Decimal(15, 2)), 4)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (2, 146, N'Alfajor de Chocolate', 2, CAST(35.00 AS Decimal(15, 2)), CAST(70.00 AS Decimal(15, 2)), 4)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (3, 106, N'Agua KIN sin gas x1.5 l', 3, CAST(70.00 AS Decimal(15, 2)), CAST(210.00 AS Decimal(15, 2)), 5)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (4, 146, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(15, 2)), CAST(35.00 AS Decimal(15, 2)), 5)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (5, NULL, N'Ades Manzana x1 l', 1, CAST(90.00 AS Decimal(15, 2)), CAST(90.00 AS Decimal(15, 2)), 6)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (6, NULL, N'Agua KIN con gas x600 ml', 2, CAST(50.00 AS Decimal(15, 2)), CAST(100.00 AS Decimal(15, 2)), 6)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (7, NULL, N'Agua KIN sin gas x1.5 l', 1, CAST(70.00 AS Decimal(15, 2)), CAST(70.00 AS Decimal(15, 2)), 6)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (8, NULL, N'Agua KIN con gas x600 ml', 1, CAST(50.00 AS Decimal(15, 2)), CAST(50.00 AS Decimal(15, 2)), 7)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (9, NULL, N'Alfajor de Chocolate', 3, CAST(35.00 AS Decimal(15, 2)), CAST(105.00 AS Decimal(15, 2)), 7)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (10, NULL, N'Alfajor de Maicena Mediano', 3, CAST(22.00 AS Decimal(15, 2)), CAST(66.00 AS Decimal(15, 2)), 7)
GO
INSERT [dbo].[DetallesFacturas] ([IdDetalle], [IdProducto], [Descripcion], [Cantidad], [PrecioUnitario], [Subtotal], [IdFactura]) VALUES (11, NULL, N'Ades Manzana x200 ml', 4, CAST(40.00 AS Decimal(15, 2)), CAST(160.00 AS Decimal(15, 2)), 8)
GO
SET IDENTITY_INSERT [dbo].[DetallesFacturas] OFF
GO
SET IDENTITY_INSERT [dbo].[DetalleVentas] ON 

GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (1, N'Bizcocho', 0, CAST(350.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 1)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (2, N'Pan', 6, CAST(70.00 AS Decimal(10, 2)), CAST(455.00 AS Decimal(10, 2)), N'Minorista', 1)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (3, N'Pan', 0, CAST(70.00 AS Decimal(10, 2)), CAST(49.00 AS Decimal(10, 2)), N'Minorista', 2)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (4, N'Masa Seca', 0, CAST(350.00 AS Decimal(10, 2)), CAST(149.80 AS Decimal(10, 2)), N'Minorista', 2)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (5, N'Corazones', 0, CAST(350.00 AS Decimal(10, 2)), CAST(33.25 AS Decimal(10, 2)), N'Minorista', 3)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (6, N'Marineras', 0, CAST(180.00 AS Decimal(10, 2)), CAST(32.40 AS Decimal(10, 2)), N'Minorista', 4)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (7, N'Bizcocho de grasa', 0, CAST(180.00 AS Decimal(10, 2)), CAST(27.00 AS Decimal(10, 2)), N'Minorista', 5)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (8, N'Muffin', 0, CAST(350.00 AS Decimal(10, 2)), CAST(67.90 AS Decimal(10, 2)), N'Minorista', 5)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (9, N'Torta de Limon', 0, CAST(400.00 AS Decimal(10, 2)), CAST(106.00 AS Decimal(10, 2)), N'Minorista', 5)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (10, N'Facturas', 10, CAST(15.00 AS Decimal(10, 2)), CAST(150.00 AS Decimal(10, 2)), N'Minorista', 6)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (11, N'Awafrut durazno x1.65 l', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 7)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (12, N'Alfajor de Maicena Grande', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), N'Minorista', 7)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (13, N'Facturas', 1, CAST(15.00 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), N'Minorista', 7)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (14, N'Bizocho', 0, CAST(140.00 AS Decimal(10, 2)), CAST(29.96 AS Decimal(10, 2)), N'Minorista', 8)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (15, N'Roscas Rellenas', 1, CAST(200.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), N'Minorista', 8)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (16, N'Alfajor de Maicena Chico', 2, CAST(20.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), N'Minorista', 9)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (17, N'Coca-Cola fanta sprite Lata x354 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), N'Minorista', 9)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (18, N'Alfajor de Chocolate', 2, CAST(35.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 10)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (19, N'Masa Seca', 0, CAST(350.00 AS Decimal(10, 2)), CAST(50.05 AS Decimal(10, 2)), N'Minorista', 10)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (20, N'Panificados Varios', 115, CAST(1.00 AS Decimal(10, 2)), CAST(115.00 AS Decimal(10, 2)), N'Minorista', 11)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (21, N'Alfajor de Maicena Mediano', 6, CAST(22.00 AS Decimal(10, 2)), CAST(132.00 AS Decimal(10, 2)), N'Minorista', 12)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (22, N'Alfajor de Hojaldre Chico', 3, CAST(35.00 AS Decimal(10, 2)), CAST(105.00 AS Decimal(10, 2)), N'Minorista', 12)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (23, N'Budín', 0, CAST(350.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 13)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (24, N'Pionono bañado', 0, CAST(400.00 AS Decimal(10, 2)), CAST(52.00 AS Decimal(10, 2)), N'Minorista', 14)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (25, N'Alfajor de Maicena Chico', 4, CAST(20.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), N'Minorista', 14)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (26, N'Panificados Varios', 32, CAST(1.00 AS Decimal(10, 2)), CAST(32.00 AS Decimal(10, 2)), N'Minorista', 15)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (27, N'Medialunas', 2, CAST(15.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), N'Minorista', 15)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (28, N'Panificados Varios', 210, CAST(1.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), N'Minorista', 16)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (29, N'Mini Tarta de pasas', 2, CAST(35.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 17)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (30, N'Cañoncitos', 2, CAST(35.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 18)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (31, N'Medialunas', 3, CAST(15.00 AS Decimal(10, 2)), CAST(45.00 AS Decimal(10, 2)), N'Minorista', 18)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (32, N'Palmera Bañada', 1, CAST(30.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), N'Minorista', 18)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (33, N'Facturas', 3, CAST(15.00 AS Decimal(10, 2)), CAST(45.00 AS Decimal(10, 2)), N'Minorista', 19)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (34, N'Panificados Varios', 37, CAST(1.00 AS Decimal(10, 2)), CAST(37.00 AS Decimal(10, 2)), N'Minorista', 20)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (35, N'Bizcocho de grasa', 0, CAST(180.00 AS Decimal(10, 2)), CAST(19.98 AS Decimal(10, 2)), N'Minorista', 20)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (36, N'Masa Seca', 0, CAST(350.00 AS Decimal(10, 2)), CAST(52.15 AS Decimal(10, 2)), N'Minorista', 21)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (37, N'Alfajor de Maicena Mediano', 3, CAST(22.00 AS Decimal(10, 2)), CAST(66.00 AS Decimal(10, 2)), N'Minorista', 22)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (38, N'Masas finas', 0, CAST(400.00 AS Decimal(10, 2)), CAST(72.00 AS Decimal(10, 2)), N'Minorista', 22)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (39, N'Panificados Varios', 38, CAST(1.00 AS Decimal(10, 2)), CAST(38.00 AS Decimal(10, 2)), N'Minorista', 22)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (40, N'Pan', 0, CAST(70.00 AS Decimal(10, 2)), CAST(10.99 AS Decimal(10, 2)), N'Minorista', 23)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (41, N'Agua KIN sin gas x1.5 l', 2, CAST(70.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), N'Minorista', 24)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (42, N'Agua KIN con gas x600 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), N'Minorista', 24)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (43, N'Ades Manzana x1 l', 1, CAST(90.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), N'Minorista', 25)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (44, N'Agua KIN con gas x600 ml', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), N'Minorista', 25)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (45, N'Agua KIN sin gas x1.5 l', 1, CAST(70.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'Minorista', 25)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (46, N'Agua KIN con gas x600 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), N'Minorista', 26)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (47, N'Alfajor de Chocolate', 3, CAST(35.00 AS Decimal(10, 2)), CAST(105.00 AS Decimal(10, 2)), N'Minorista', 26)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (48, N'Alfajor de Maicena Mediano', 3, CAST(22.00 AS Decimal(10, 2)), CAST(66.00 AS Decimal(10, 2)), N'Minorista', 26)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (49, N'Ades Manzana x200 ml', 2, CAST(40.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), N'Minorista', 27)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (50, N'Agua KIN con gas x600 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), N'Minorista', 27)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (51, N'Alfajor de Maicena Grande', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), N'Minorista', 27)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (52, N'Apolo', 1, CAST(80.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), N'Minorista', 27)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (53, N'Agua KIN con gas x600 ml', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), N'Minorista', 28)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (54, N'Agua KIN con gas x600 ml', 2, CAST(50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), N'Minorista', 29)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (55, N'Ades Manzana x200 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), N'Minorista', 30)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (56, N'Ades Manzana x200 ml', 1, CAST(40.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), N'Minorista', 31)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (57, N'Ades Manzana x200 ml', 4, CAST(40.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), N'Mayorista', 32)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (58, N'Alfajor de Chocolate', 1, CAST(35.00 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), N'Mayorista', 33)
GO
INSERT [dbo].[DetalleVentas] ([id_detalle], [descripcion], [cantidad], [precio_unitario], [subtotal], [venta_por], [id_venta]) VALUES (59, N'Agua KIN con gas x600 ml', 1, CAST(50.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), N'Mayorista', 34)
GO
SET IDENTITY_INSERT [dbo].[DetalleVentas] OFF
GO
SET IDENTITY_INSERT [dbo].[Egresos] ON 

GO
INSERT [dbo].[Egresos] ([id_egreso], [id_caja], [descripcion], [monto], [fecha]) VALUES (1, 6, N'propaganda radio', CAST(-700.00 AS Decimal(18, 2)), CAST(0x0000AB6100000000 AS DateTime))
GO
INSERT [dbo].[Egresos] ([id_egreso], [id_caja], [descripcion], [monto], [fecha]) VALUES (2, 7, N'coca', CAST(-5048.00 AS Decimal(18, 2)), CAST(0x0000AB6200000000 AS DateTime))
GO
INSERT [dbo].[Egresos] ([id_egreso], [id_caja], [descripcion], [monto], [fecha]) VALUES (3, 11, N'huevos', CAST(-775.00 AS Decimal(18, 2)), CAST(0x0000AB6400000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Egresos] OFF
GO
SET IDENTITY_INSERT [dbo].[Facturas] ON 

GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (1, 104, N'Pedido', N'2         ', 6, N'Factura B', 1, N'Producto', 96, N'DNI', 99999999, 14, N'14        ', CAST(0x0000AB9501676626 AS DateTime), CAST(165.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(165.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'PES', 1, 3, CAST(165.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'Facturado', N'Efectivo', N'70144829581484', N'20200416', NULL)
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (2, 105, N'Pedido', N'2         ', 6, N'Factura B', 1, N'Producto', 96, N'DNI', 99999999, 15, N'15        ', CAST(0x0000AB9501683326 AS DateTime), CAST(200.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(200.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'PES', 1, 3, CAST(200.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'Facturado', N'Efectivo', N'70144829581497', N'20200416', NULL)
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (3, 106, N'Pedido', N'2         ', 6, N'Factura B', 1, N'Producto', 96, N'DNI', 99999999, 16, N'16        ', CAST(0x0000AB950168C834 AS DateTime), CAST(140.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(140.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'PES', 1, 3, CAST(140.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'Facturado', N'Efectivo', N'70144829581515', N'20200416', NULL)
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (4, 107, N'Pedido', N'2         ', 6, N'Factura B', 1, N'Producto', 96, N'DNI', 99999999, 17, N'17        ', CAST(0x0000AB9501699F1B AS DateTime), CAST(175.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(175.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'PES', 1, 3, CAST(175.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'Facturado', N'Efectivo', N'70144829581544', N'20200416', NULL)
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (5, 108, N'Pedido', N'2         ', 1, N'Factura A', 1, N'Producto', 80, N'CUIT', 99999999, NULL, NULL, CAST(0x0000AB96012C01AE AS DateTime), CAST(245.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(202.48 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(42.52 AS Decimal(12, 2)), N'PES', 1, 5, CAST(202.48 AS Decimal(12, 2)), CAST(42.52 AS Decimal(12, 2)), N'No Facturado', N'Efectivo', NULL, NULL, N'Obs: El campo  DocNro es invalido. (10015)
')
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (6, 25, N'Venta', N'2         ', 6, N'Factura B', 1, N'Producto', 96, N'DNI', 99999999, 18, N'18        ', CAST(0x0000AB96013EB83F AS DateTime), CAST(260.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(260.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'PES', 1, 3, CAST(260.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), N'Facturado', N'Efectivo', N'70144829699783', N'20200417', NULL)
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (7, 26, N'Venta', N'2         ', 1, N'Factura A', 1, N'Producto', 80, N'CUIT', 20401715224, NULL, NULL, CAST(0x0000AB96013EDF58 AS DateTime), CAST(221.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(182.64 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(38.36 AS Decimal(12, 2)), N'PES', 1, 5, CAST(182.64 AS Decimal(12, 2)), CAST(38.36 AS Decimal(12, 2)), N'No Facturado', N'Efectivo', NULL, NULL, N'Er: 10069: Campo DocNro no puede ser igual al del emisor.
')
GO
INSERT [dbo].[Facturas] ([IdFactura], [IdVenta], [TipoId], [PtoVta], [CbteTipo], [CbteTipo_desc], [Concepto], [Concepto_desc], [DocTipo], [DocTipo_desc], [DocNro], [CbteDesde], [CbteHasta], [CbteFch], [ImpTotal], [ImpOpEx], [ImpTotConc], [ImpNeto], [ImpTrib], [ImpIVA], [MonId], [MonCotiz], [AlicIva_Id], [AlicIva_BaseImp], [AlicIva_Importe], [Estado], [TipoPago], [CAE], [CAE_vto], [Mensaje]) VALUES (8, 32, N'Venta', N'2         ', 1, N'Factura A', 1, N'Producto', 80, N'CUIT', 20374110528, 2, N'2         ', CAST(0x0000AB96014B2094 AS DateTime), CAST(160.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(132.23 AS Decimal(12, 2)), CAST(0.00 AS Decimal(12, 2)), CAST(27.77 AS Decimal(12, 2)), N'PES', 1, 5, CAST(132.23 AS Decimal(12, 2)), CAST(27.77 AS Decimal(12, 2)), N'Facturado', N'Crédito/Débito', N'70144829700381', N'20200417', NULL)
GO
SET IDENTITY_INSERT [dbo].[Facturas] OFF
GO
SET IDENTITY_INSERT [dbo].[Mesas] ON 

GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (14, N'Mesa 01', N'Libre', 5)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (15, N'Mesa 02', N'Libre', 5)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (16, N'Mesa 03', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (17, N'Mesa 04', N'Libre', 6)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (18, N'Mesa 05', N'Libre', 6)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (19, N'Mesa 06', N'Libre', 6)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (20, N'Mesa 07 (Afuera)', N'Libre', 6)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (21, N'Mesa 08', N'Libre', 5)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (22, N'Mesa 09', N'Libre', 7)
GO
INSERT [dbo].[Mesas] ([id_mesa], [nro_mesa], [estado], [id_mozo]) VALUES (23, N'Mesa 10', N'Libre', 5)
GO
SET IDENTITY_INSERT [dbo].[Mesas] OFF
GO
SET IDENTITY_INSERT [dbo].[Mozos] ON 

GO
INSERT [dbo].[Mozos] ([id_mozo], [nombre]) VALUES (5, N'Sandra')
GO
INSERT [dbo].[Mozos] ([id_mozo], [nombre]) VALUES (6, N'Adriana')
GO
INSERT [dbo].[Mozos] ([id_mozo], [nombre]) VALUES (7, N'René')
GO
SET IDENTITY_INSERT [dbo].[Mozos] OFF
GO
SET IDENTITY_INSERT [dbo].[Pedidos] ON 

GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (1, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 10, 5, CAST(0.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 1, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (2, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(-50.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 2, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (3, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(270.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (4, 18, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (5, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (6, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(240.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (7, 18, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (8, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (9, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (10, 19, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (11, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(370.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (12, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(230.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (13, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (14, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(865.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (15, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(460.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (16, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 5, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (17, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (18, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(780.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (19, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (20, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(340.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (21, 18, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(310.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (22, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (23, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(305.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (24, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(185.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (25, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(170.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (26, 16, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(230.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (27, 15, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(660.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (28, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(890.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (29, 17, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (30, 14, CAST(0xBC400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 6, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (31, 14, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(750.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (32, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(255.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (33, 16, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(180.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (34, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(800.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (35, 14, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(115.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (36, 18, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(380.00 AS Decimal(10, 2)), 7, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (38, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (39, 22, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (40, 14, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (41, 15, CAST(0xBD400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(645.00 AS Decimal(10, 2)), 8, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (42, 18, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (43, 16, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (44, 17, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (45, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(680.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (46, 16, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (47, 17, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(380.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (48, 19, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (49, 18, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (50, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (51, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(320.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (52, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(270.00 AS Decimal(10, 2)), 9, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (54, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(120.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (55, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(250.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (56, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(110.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (58, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (59, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(735.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (60, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(790.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (61, 19, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (62, 14, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(1640.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (63, 15, CAST(0xBE400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(360.00 AS Decimal(10, 2)), 10, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (68, 16, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(260.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (69, 16, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (70, 17, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (71, 17, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(160.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (72, 18, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (73, 14, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(340.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (77, 16, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (78, 20, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(1150.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (79, 14, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(250.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (80, 14, CAST(0xBF400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(230.00 AS Decimal(10, 2)), 11, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (81, 15, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(190.00 AS Decimal(10, 2)), 12, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (82, 16, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 12, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (83, 18, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(310.00 AS Decimal(10, 2)), 12, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (84, 14, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), 12, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (85, 14, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 7, CAST(0.00 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), 12, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (86, 14, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 13, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (87, 20, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), 13, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (88, 16, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(570.00 AS Decimal(10, 2)), 13, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (89, 19, CAST(0xC0400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(340.00 AS Decimal(10, 2)), 13, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (91, 15, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (92, 16, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (93, 17, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(210.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (94, 16, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(460.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (95, 15, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (96, 14, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(40.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (97, 15, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 5, CAST(0.00 AS Decimal(10, 2)), CAST(100.00 AS Decimal(10, 2)), 14, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (99, 15, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(590.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (101, 15, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 6, CAST(0.00 AS Decimal(10, 2)), CAST(290.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (102, 18, CAST(0xC1400B00 AS Date), N'Cerrado', 11, 6, CAST(-60.00 AS Decimal(10, 2)), CAST(170.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (103, 14, CAST(0xF0400B00 AS Date), N'Cerrado', 13, 5, CAST(0.00 AS Decimal(10, 2)), CAST(495.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (104, 14, CAST(0xF0400B00 AS Date), N'Cerrado', 13, 5, CAST(0.00 AS Decimal(10, 2)), CAST(165.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (105, 15, CAST(0xF0400B00 AS Date), N'Cerrado', 13, 6, CAST(0.00 AS Decimal(10, 2)), CAST(200.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (106, 16, CAST(0xF0400B00 AS Date), N'Cerrado', 13, 7, CAST(0.00 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (107, 17, CAST(0xF0400B00 AS Date), N'Cerrado', 13, 6, CAST(0.00 AS Decimal(10, 2)), CAST(175.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
INSERT [dbo].[Pedidos] ([id_pedido], [id_mesa], [fecha], [estado], [id_usuario], [id_mozo], [descuento], [total], [id_caja], [tipo_pago]) VALUES (108, 15, CAST(0xF1400B00 AS Date), N'Cerrado', 13, 5, CAST(0.00 AS Decimal(10, 2)), CAST(245.00 AS Decimal(10, 2)), 15, N'Efectivo')
GO
SET IDENTITY_INSERT [dbo].[Pedidos] OFF
GO
SET IDENTITY_INSERT [dbo].[Productos] ON 

GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (12, N'Té', CAST(40.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (13, N'Mate cocido', CAST(40.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (14, N'Desayuno Tradicional chico', CAST(70.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (15, N'Desayuno Tradicional med', CAST(90.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (17, N'Submarino', CAST(110.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (18, N'Cappuccino', CAST(100.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (19, N'Desayuno Tradicional Grand', CAST(100.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (20, N'Mate', CAST(100.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (22, N'Desayuno Completo', CAST(130.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (23, N'Desayuno Light', CAST(160.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (24, N'Desayuno Florencia', CAST(210.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (25, N'Desayuno Protéico', CAST(180.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (26, N'Desayuno Fresco', CAST(175.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (27, N'Desayuno Campestre', CAST(140.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (28, N'Licuado', CAST(110.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (29, N'Jugo de Naranja', CAST(60.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (30, N'Limonada', CAST(60.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (31, N'Tostado de jamón y queso', CAST(120.00 AS Decimal(10, 2)), NULL, 8, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (32, N'Tostado de jamón crudo y queso', CAST(140.00 AS Decimal(10, 2)), NULL, 8, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (33, N'Tostado de salame y queso', CAST(120.00 AS Decimal(10, 2)), NULL, 8, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (34, N'Picada simple', CAST(330.00 AS Decimal(10, 2)), NULL, 9, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (35, N'Picada completa', CAST(460.00 AS Decimal(10, 2)), NULL, 9, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (36, N'Pepsi x500 ml', CAST(45.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (37, N'Mirinda nanranja x500 ml', CAST(50.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (39, N'7up x500 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (40, N'Pepsi x1.25 l', CAST(85.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (41, N'Mirinda manzana x1.25 l', CAST(85.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (43, N'Paso de los toros x500 ml', CAST(50.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (44, N'Awafrut pomelo x1.65 l', CAST(70.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (45, N'Agua mineral Nestlé x1.5 l', CAST(70.00 AS Decimal(10, 2)), 10, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (46, N'Agua mineral Eco x500 ml', CAST(50.00 AS Decimal(10, 2)), 39, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (47, N'Awafrut durazno x1.65 l', CAST(70.00 AS Decimal(10, 2)), 11, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (48, N'Cerveza Quilmes x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (49, N'Cerveza Lata Quilmes Stout x473 ml', CAST(70.00 AS Decimal(10, 2)), 3, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (50, N'Cerveza Brahma x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (51, N'Cerveza Budweiser x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (52, N'Cerveza Patagonia Amber Lager x473 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (53, N'Vino Michel Torino tinto x700 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (54, N'Vino Toro Viejo tinto x700 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (61, N'Vino Finca Las Moras malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (62, N'Vino Trapiche Alaris malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (63, N'Vino Dadá moras x750 ml', CAST(185.00 AS Decimal(10, 2)), 6, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (64, N'Vino Aroldos State tinto x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (65, N'Vino Estancias malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (66, N'Vino Elementos malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (67, N'Vino Chacabuco malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (68, N'Vino Don David malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (69, N'Vino Canciller blend x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (70, N'Vino Phebus malbec x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (71, N'Vino Nampe malbec tinto x750 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (72, N'Vino Viñas de Balbo tinto x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (73, N'Vino Toro Viejo tinto x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (74, N'Vino Michel Torino tinto x1.25 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (75, N'Vino Nampe malbec x1.25 l', CAST(0.00 AS Decimal(10, 2)), 6, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (76, N'Vino Chacabuco malbec x1.5 l', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (77, N'Monster Energy Green x473 ml', CAST(90.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (78, N'Monster E. Mango Loco x473 ml', CAST(90.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (80, N'Fanta Naranja Lata x354 ml', CAST(50.00 AS Decimal(10, 2)), 3, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (81, N'Sprite Lata x354 ml', CAST(50.00 AS Decimal(10, 2)), 4, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (82, N'Coca-Cola fanta sprite x600 ml', CAST(55.00 AS Decimal(10, 2)), 40, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (83, N'Coca-Cola sin azúcar x375 ml', CAST(40.00 AS Decimal(10, 2)), 40, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (84, N'Fanta Naranja sin azúcar x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (85, N'Fanta Naranja x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (87, N'Sprite LS x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (88, N'Sprite LS x375 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (90, N'Coca-Cola sin azúcar x600 ml', CAST(55.00 AS Decimal(10, 2)), 38, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (91, N'Coca-Cola fanta sprite x375 ml', CAST(40.00 AS Decimal(10, 2)), 40, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (94, N'Aquarius  x500 cc', CAST(50.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (98, N'Aquarius  x1.5 cc', CAST(100.00 AS Decimal(10, 2)), 11, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (99, N'Aquarius x2250 cc', CAST(120.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (101, N'Agua KIN sin gas x600 ml', CAST(0.00 AS Decimal(10, 2)), 0, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (102, N'Agua KIN con gas x600 ml', CAST(50.00 AS Decimal(10, 2)), 32, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (103, N'Powerade Manzana x500 ml', CAST(60.00 AS Decimal(10, 2)), 60, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (104, N'Powerade  x995cc', CAST(90.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (105, N'Agua KIN sin gas x375 ml', CAST(35.00 AS Decimal(10, 2)), 32, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (106, N'Agua KIN sin gas x1.5 l', CAST(70.00 AS Decimal(10, 2)), 6, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (107, N'Cepita x1 litro botella', CAST(90.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (108, N'Ades Manzana x200 ml', CAST(40.00 AS Decimal(10, 2)), 25, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (109, N'Ades Manzana x1 l', CAST(90.00 AS Decimal(10, 2)), 38, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (110, N'Cepita x300 ml', CAST(40.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (112, N'Cepita  x1 litro caja', CAST(80.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (113, N'Cepita  x1.5 litro botella', CAST(110.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (114, N'Cepita x200 ml', CAST(30.00 AS Decimal(10, 2)), 24, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (115, N'Smoothie', CAST(100.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (118, N'Pan', CAST(70.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (119, N'Bizcocho de grasa', CAST(180.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (120, N'Medialunas', CAST(15.00 AS Decimal(10, 2)), NULL, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (121, N'Marineras', CAST(180.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (122, N'Budines', CAST(60.00 AS Decimal(10, 2)), 4, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (123, N'Apolo', CAST(80.00 AS Decimal(10, 2)), 5, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (124, N'Masa Seca', CAST(350.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (126, N'Torta de Naranja', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (127, N'Torta de Limon', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (128, N'Torta Europea', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (129, N'Torta de Coco B/N', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (130, N'Torta Merengada', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (131, N'Alfajor de Hojaldre Chico', CAST(35.00 AS Decimal(10, 2)), 17, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (132, N'Bizcocho de Vino Blanco', CAST(180.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (133, N'Cremona', CAST(50.00 AS Decimal(10, 2)), 6, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (134, N'Bizocho', CAST(140.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (135, N'Ferrocarril', CAST(220.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (136, N'Tortilla', CAST(140.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (137, N'palmerita', CAST(220.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (138, N'Facturas', CAST(15.00 AS Decimal(10, 2)), NULL, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (139, N'Facturas Hojaldradas', CAST(25.00 AS Decimal(10, 2)), NULL, 12, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (140, N'Mini Facturas', CAST(250.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (141, N'Roscas Rellenas', CAST(200.00 AS Decimal(10, 2)), 10, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (142, N'Panettone', CAST(150.00 AS Decimal(10, 2)), 9, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (143, N'Cafe Mediano', CAST(60.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (144, N'cafe grande', CAST(70.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (145, N'Cafe Chico', CAST(50.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (146, N'Alfajor de Chocolate', CAST(35.00 AS Decimal(10, 2)), 24, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (147, N'Alfajor de Maicena Grande', CAST(35.00 AS Decimal(10, 2)), 33, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (148, N'Cañoncitos', CAST(35.00 AS Decimal(10, 2)), 19, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (149, N'Flotitas', CAST(35.00 AS Decimal(10, 2)), 8, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (150, N'Facturas Dulce', CAST(25.00 AS Decimal(10, 2)), 14, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (151, N'Palmera Bañada', CAST(30.00 AS Decimal(10, 2)), 17, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (152, N'Cañon Bañado', CAST(40.00 AS Decimal(10, 2)), 5, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (153, N'Palmera Rellena', CAST(40.00 AS Decimal(10, 2)), 20, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (154, N'Pan de Pancho', CAST(50.00 AS Decimal(10, 2)), 10, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (155, N'Pan de Hamburguesa', CAST(50.00 AS Decimal(10, 2)), 14, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (156, N'Prepizza', CAST(35.00 AS Decimal(10, 2)), 23, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (157, N'Brownie', CAST(20.00 AS Decimal(10, 2)), 20, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (158, N'Cerveza Quilmes Cristal 1Lts', CAST(130.00 AS Decimal(10, 2)), 22, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (159, N'Corazones', CAST(350.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (160, N'Porcion Torta (limon,naranja,europea,merengue)', CAST(40.00 AS Decimal(10, 2)), 27, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (161, N'Mani porcion', CAST(10.00 AS Decimal(10, 2)), NULL, 9, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (162, N'Coca-Cola fanta sprinte 1500 vidrio ret.', CAST(85.00 AS Decimal(10, 2)), 41, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (163, N'Desayuno del dia', CAST(100.00 AS Decimal(10, 2)), NULL, 11, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (164, N'Coca-Cola fanta sprite vidrio 1000cc', CAST(70.00 AS Decimal(10, 2)), 24, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (165, N'seven-up 500cc', CAST(50.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (167, N'fernet 1 litro', CAST(530.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (168, N'don david', CAST(280.00 AS Decimal(10, 2)), 6, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (169, N'estancia mendoza 750c', CAST(110.00 AS Decimal(10, 2)), 6, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (170, N'vino viña de balvo 1 y 1/4', CAST(80.00 AS Decimal(10, 2)), 6, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (171, N'aquarius de pera 1,5cc', CAST(100.00 AS Decimal(10, 2)), 12, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (172, N'Muffin', CAST(350.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (173, N'Torta/Tarta x Kg', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (174, N'Porcion torta ricota', CAST(60.00 AS Decimal(10, 2)), 7, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (175, N'Torta de Hojaldre', CAST(600.00 AS Decimal(10, 2)), 1, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (176, N'Mini Tarta de Hojaldre', CAST(150.00 AS Decimal(10, 2)), 3, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (177, N'Mini Tarta de pasas', CAST(35.00 AS Decimal(10, 2)), 4, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (178, N'Panificados Varios', CAST(1.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (179, N'Alfajor de Maicena Chico', CAST(20.00 AS Decimal(10, 2)), 0, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (180, N'Alfajor de Maicena Mediano', CAST(22.00 AS Decimal(10, 2)), 12, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (181, N'Coca-Cola Lata x354 ml', CAST(50.00 AS Decimal(10, 2)), 3, 10, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (182, N'Budín', CAST(350.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (184, N'Licuado chico', CAST(90.00 AS Decimal(10, 2)), NULL, 7, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (187, N'Pionono bañado', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (188, N'Torta pauloba', CAST(600.00 AS Decimal(10, 2)), 2, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (189, N'xx', CAST(4.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (190, N'Te', CAST(40.00 AS Decimal(10, 2)), NULL, 6, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (191, N'Masas finas', CAST(400.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (192, N'Torta Brownie', CAST(480.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (193, N'Torta Oreo', CAST(480.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (194, N'Torta Bonobon', CAST(480.00 AS Decimal(10, 2)), NULL, 12, N'Kg')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (195, N'Porcion de torta oreo', CAST(70.00 AS Decimal(10, 2)), 14, 13, N'Cantidad')
GO
INSERT [dbo].[Productos] ([id_producto], [descripcion], [precio], [cantidad], [id_tipo], [venta_por]) VALUES (196, N'Porcion torta bonobon', CAST(70.00 AS Decimal(10, 2)), 6, 13, N'Cantidad')
GO
SET IDENTITY_INSERT [dbo].[Productos] OFF
GO
SET IDENTITY_INSERT [dbo].[TicketsAcceso] ON 

GO
INSERT [dbo].[TicketsAcceso] ([IdTA], [Source], [Destination], [GenerationTime], [ExpirationTime], [Token], [Sign]) VALUES (10003, N'CN=wsaahomo, O=AFIP, C=AR, SERIALNUMBER=CUIT 33693450239', N'SERIALNUMBER=CUIT 20401715224, CN=gestionpersonal', CAST(0x0000AB950165CF14 AS DateTime), CAST(0x0000AB9600A00E14 AS DateTime), N'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMTA0MTc2MDY4NyIgZ2VuX3RpbWU9IjE1ODYyMjAxMDQiIGV4cF90aW1lPSIxNTg2MjYzMzY0Ii8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjA0MDE3MTUyMjQsIENOPWdlc3Rpb25wZXJzb25hbCIgYXV0aG1ldGhvZD0iY21zIiByZWdtZXRob2Q9IjIyIj4KICAgICAgICAgICAgPHJlbGF0aW9ucz4KICAgICAgICAgICAgICAgIDxyZWxhdGlvbiBrZXk9IjIwNDAxNzE1MjI0IiByZWx0eXBlPSI0Ii8+CiAgICAgICAgICAgIDwvcmVsYXRpb25zPgogICAgICAgIDwvbG9naW4+CiAgICA8L29wZXJhdGlvbj4KPC9zc28+Cg==', N'W22Dkcsipo7WyhsFCTphTIhfUxd1mnAfxJMOPNDcdIKu8UYLD1jpD9T24Ltbi4wuEvNls4Stcln2h030pFP3yIKQ+XH+zpaVxKp/jxr0NA2oVQixxDvVryHaouWH1DjcFFMUzKGvRmtf7yQ8N/Qx00aECnyD1ZLOH0qEVKhhNKQ=')
GO
INSERT [dbo].[TicketsAcceso] ([IdTA], [Source], [Destination], [GenerationTime], [ExpirationTime], [Token], [Sign]) VALUES (10004, N'CN=wsaahomo, O=AFIP, C=AR, SERIALNUMBER=CUIT 33693450239', N'SERIALNUMBER=CUIT 20401715224, CN=gestionpersonal', CAST(0x0000AB96012BF5E3 AS DateTime), CAST(0x0000AB97006634E3 AS DateTime), N'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iNjUzNzQ1NDkiIGdlbl90aW1lPSIxNTg2MjkzODY3IiBleHBfdGltZT0iMTU4NjMzNzEyNyIvPgogICAgPG9wZXJhdGlvbiB0eXBlPSJsb2dpbiIgdmFsdWU9ImdyYW50ZWQiPgogICAgICAgIDxsb2dpbiBlbnRpdHk9IjMzNjkzNDUwMjM5IiBzZXJ2aWNlPSJ3c2ZlIiB1aWQ9IlNFUklBTE5VTUJFUj1DVUlUIDIwNDAxNzE1MjI0LCBDTj1nZXN0aW9ucGVyc29uYWwiIGF1dGhtZXRob2Q9ImNtcyIgcmVnbWV0aG9kPSIyMiI+CiAgICAgICAgICAgIDxyZWxhdGlvbnM+CiAgICAgICAgICAgICAgICA8cmVsYXRpb24ga2V5PSIyMDQwMTcxNTIyNCIgcmVsdHlwZT0iNCIvPgogICAgICAgICAgICA8L3JlbGF0aW9ucz4KICAgICAgICA8L2xvZ2luPgogICAgPC9vcGVyYXRpb24+Cjwvc3NvPgo=', N'kxWAotg579Hjvp0TlCPurtIvgjt1D3VDq8XpnmZ7YwaagKV3wH78u83o4ukbxQZqSTUwXEoTXH4HvOtPlQCFjT0My7rEKd7m1jD2k/N54Nf9X6Z3yzTjVJOhxD8ii/98WkLnz6C1Im/JBj3A8hPMI2wZiFHJnxHHr8v2I6FBAYU=')
GO
SET IDENTITY_INSERT [dbo].[TicketsAcceso] OFF
GO
SET IDENTITY_INSERT [dbo].[Tipos] ON 

GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (6, N'Infusiones', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (7, N'Jugos y Licuados', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (8, N'Sandwiches', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (9, N'Picadas', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (10, N'Bebidas', N'1')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (11, N'Desayunos', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (12, N'Panificados', N'0')
GO
INSERT [dbo].[Tipos] ([id_tipo], [descripcion], [stock]) VALUES (13, N'Panificado x Unidad', N'1')
GO
SET IDENTITY_INSERT [dbo].[Tipos] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (10, N'echemez', N'040397', N'Administrador')
GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (11, N'mchemez', N'290794', N'Administrador')
GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (12, N'codeassets', N'080118', N'Administrador')
GO
INSERT [dbo].[Usuarios] ([id_usuario], [nombre], [contraseña], [tipo]) VALUES (13, N'asd', N'asd', N'Administrador')
GO
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
GO
SET IDENTITY_INSERT [dbo].[Ventas] ON 

GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (1, CAST(0xBC400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(525.00 AS Decimal(8, 2)), 3, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (2, CAST(0xBC400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(198.80 AS Decimal(8, 2)), 5, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (3, CAST(0xBC400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(33.25 AS Decimal(8, 2)), 6, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (4, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(32.40 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (5, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(200.90 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (6, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(150.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (7, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(120.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (8, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(229.96 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (9, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(90.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (10, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(120.05 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (11, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(115.00 AS Decimal(8, 2)), 15, 10, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (12, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(237.00 AS Decimal(8, 2)), 15, 10, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (13, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(70.00 AS Decimal(8, 2)), 15, 10, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (14, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(132.00 AS Decimal(8, 2)), 15, 10, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (15, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(62.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (16, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(210.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (17, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(70.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (18, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(145.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (19, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(45.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (20, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(56.98 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (21, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(52.15 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (22, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(176.00 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (23, CAST(0xC1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(10.99 AS Decimal(8, 2)), 15, 11, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (24, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(190.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Crédito/Débito', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (25, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(260.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (26, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(221.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (27, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(245.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (28, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(100.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (29, CAST(0xF1400B00 AS Date), CAST(-10.00 AS Decimal(8, 2)), CAST(90.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Crédito/Débito', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (30, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(40.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (31, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(40.00 AS Decimal(8, 2)), 15, 13, N'Minorista', N'Efectivo', 0)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (32, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(160.00 AS Decimal(8, 2)), 15, 13, N'Mayorista', N'Crédito/Débito', 1)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (33, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(35.00 AS Decimal(8, 2)), 15, 13, N'Mayorista', N'Efectivo', 3)
GO
INSERT [dbo].[Ventas] ([id_venta], [fecha], [descuento], [total], [id_caja], [id_usuario], [tipo_venta], [tipo_pago], [id_cliente]) VALUES (34, CAST(0xF1400B00 AS Date), CAST(0.00 AS Decimal(8, 2)), CAST(50.00 AS Decimal(8, 2)), 15, 13, N'Mayorista', N'Efectivo', 3)
GO
SET IDENTITY_INSERT [dbo].[Ventas] OFF
GO
USE [master]
GO
ALTER DATABASE [Chemez] SET  READ_WRITE 
GO
